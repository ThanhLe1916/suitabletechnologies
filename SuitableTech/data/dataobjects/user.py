
from common.constant import Constant
from data.dataobjects.invitation_settings import Invitation_Settings

class User(object):
    
    def __init__(self, ):
        self.email_address = None
        self.first_name = None
        self.last_name = None
        self.device_group = None
        self.device_groups = []
        self.organization = None
        self.organizations = []
        self.user_group = None
        self.user_groups = []
        self.password = None
        self.confirm_password = None
        self.invitation_settings = Invitation_Settings()
        self.activated = None
        
        
    def generate_data(self):
        from common.helper import Helper
        self.email_address = Helper.generate_random_email()
        self.first_name = "FN " + Helper.generate_random_string()
        self.last_name = "LN " + Helper.generate_random_string()
        self.organization = Constant.AdvancedOrgName
        self.password = Helper.generate_random_password()
        self.confirm_password = self.password
        self.activated = False
    
    
    def generate_advanced_normal_user_data(self):
        self.generate_data()
        self.first_name = "Advanced Normal User"
    
    
    def generate_advanced_org_admin_data(self):
        self.generate_data()
        self.first_name = "Advanced Org Admin"
        
    
    def generate_advanced_device_group_admin_data(self):
        self.generate_data()
        self.first_name = "Device Group Admin"
    
    
    def generate_simplified_normal_user_data(self):
        self.generate_data()
        self.first_name = "Simplified Normal User"
        self.organization = Constant.SimplifiedOrgName
        self.device_group = Constant.BeamPlusMock3Name
        
        
    def generate_simplified_device_admin_data(self):
        self.generate_simplified_normal_user_data()
        self.first_name = "Simplified Device Admin"
        
    
    def generate_simplified_org_admin_data(self):
        self.email_address = Constant.SimplifiedAdminEmail
        self.first_name = "Simplified Org Admin"
        self.organization = Constant.SimplifiedOrgName
        self.organizations = [Constant.SimplifiedOrgName]
        self.device_groups = [Constant.BeamPlusMock2Name, Constant.BeamPlusMock3Name]
        self.password = Constant.DefaultPassword
        self.confirm_password = Constant.DefaultPassword
        self.activated = True # set True because we use account already activated
    
    
    def generate_mixed_org_admin_data(self):
        self.email_address = Constant.MixedMultiOrgAdminEmail
        self.first_name = "Mixed Org Admin"
        self.organization = Constant.AdvancedOrgName
        self.organizations = [Constant.AdvancedOrgName, Constant.AdvancedOrgName_2, Constant.SimplifiedOrgName]
        self.device_groups = [Constant.DeviceGroup, Constant.BeamPlusMock2Name, Constant.BeamPlusMock3Name]
        self.password = Constant.DefaultPassword
        self.confirm_password = Constant.DefaultPassword
        self.activated = True # set True because we use account already activated
    
    
    def generate_non_gsso_user_data(self):
        self.email_address = Constant.NonGSSOUserEmail
        self.password = Constant.DefaultPassword
    
    
    def generate_un_allowed_gsso_user_data(self):
        self.email_address = Constant.UnAllowGSSOUserEmail
        self.password = Constant.DefaultPassword
    
    
    def get_displayed_name(self):        
        displayed_name = self.email_address     
        if(self.first_name!=None and self.last_name!=None):
            displayed_name = self.first_name + " " + self.last_name
        elif(self.first_name!=None and self.last_name==None):
            displayed_name = self.first_name
        elif(self.first_name==None and self.last_name!=None):
            displayed_name = self.last_name
            
        return displayed_name
    
    
    def tostring(self):
        return "email_address: {}\nfirst_name: {}\nlast_name: {}\ndevice_group: {}\nuser_group: {}\norganization: {}".format(self.email_address,
                                    self.first_name, self.last_name, self.device_group, self.user_group, self.organization)

