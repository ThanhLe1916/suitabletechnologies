from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from pages.suitable_tech.admin.advanced.admin_template_page import AdminTemplatePage
from common.application_constants import ApplicationConst

class _OrganizatioSettingsPageLocator(object):
    _txtAPIKeyName = (By.XPATH, "//input[@ng-model='newApiKey.name']")
    _btnCreateAPIKey = (By.XPATH, "//form[@name='addApiKeyForm']//button[@type='submit']")
    _lnkAPIDocumentation = (By.XPATH, "//a[@href='/documentation/site-admin/web-api/']")
    _txtOrganizationName = (By.XPATH, "//input[@ng-model='organizationSettings.name']")
    _txtEmailNotifications = (By.XPATH, "//input[@ng-model='organizationSettings.notification_from_name']")
    _txtfInviteMessage = (By.XPATH, "//textarea[@ng-model='organizationSettings.default_invite_message']")
    _btnSaveSettings = (By.XPATH, "//form[@name='organizationSettingsForm']//input[@type='submit']")
    _msgAlert = (By.XPATH, "//div[@class='alert alert-success']//span[@ng-bind-html='message.content']")
    _lblHeader = (By.XPATH, "//div[@class='row secondary-nav']//h3")
    @staticmethod
    def _lnkAPIKeyName(value):
        return (By.XPATH, u"//tr[@ng-repeat='apiKey in apiKeys']//td[.=\"{}\"]".format(value))
    @staticmethod
    def _lnkDelteAPIKey(value):
        return (By.XPATH, u"//tr[@ng-repeat='apiKey in apiKeys']//td[.=\"{}\"]//following-sibling::td//a//span[.=\"{}\"]".format(value, ApplicationConst.LBL_DELETE))    
    
    
class OrganizatioSettingsPage(AdminTemplatePage):
    """
    @description: This is page object class for Admin Organization Settings page.
        This page will be opened after clicking Setting(Gear icon) on Dashboard page.
        Please visit https://staging.suitabletech.com/manage/#/settings/ for more details.
    @page: Admin Organization Settings page
    @author: thanh.viet.le
    """

    """    Properties    """
    @property
    def _lblHeader(self):
        return Element(self._driver, *_OrganizatioSettingsPageLocator._lblHeader)
    @property
    def _txtAPIKeyName(self):
        return Element(self._driver, *_OrganizatioSettingsPageLocator._txtAPIKeyName)
    @property
    def _lnkAPIDocumentation(self):
        return Element(self._driver, *_OrganizatioSettingsPageLocator._lnkAPIDocumentation)
    @property
    def _txtOrganizationName(self):
        return Element(self._driver, *_OrganizatioSettingsPageLocator._txtOrganizationName)
    @property
    def _txtEmailNotifications(self):
        return Element(self._driver, *_OrganizatioSettingsPageLocator._txtEmailNotifications)
    @property
    def _txtfInviteMessage(self):
        return Element(self._driver, *_OrganizatioSettingsPageLocator._txtfInviteMessage)
    @property
    def _btnCreateAPIKey(self):
        return Element(self._driver, *_OrganizatioSettingsPageLocator._btnCreateAPIKey)
    @property
    def _btnSaveSettings(self):
        return Element(self._driver, *_OrganizatioSettingsPageLocator._btnSaveSettings)
    @property
    def _msgAlert(self):
        return Element(self._driver, *_OrganizatioSettingsPageLocator._msgAlert)

    def _lnkAPIKeyName(self, value):
        return Element(self._driver, *_OrganizatioSettingsPageLocator._lnkAPIKeyName(value))
    def _lnkDelteAPIKey(self, value):
        return Element(self._driver, *_OrganizatioSettingsPageLocator._lnkDelteAPIKey(value))
    
    """    Methods    """
    def __init__(self, driver):  
        """      
        @summary: Constructor method    
        @param driver: Web driver 
        @author: Khoi Ngo       
        """      
        AdminTemplatePage.__init__(self, driver)
        self._lblHeader.wait_until_displayed()
        

    def create_api_key(self, key_name):
        """      
        @summary:  Create an APT key       
        @param key_name: name of API key would like to create
        @return: OrganizatioSettingsPage
        @author: Khoi Ngo
        """
        self._txtAPIKeyName.type(key_name)
        self._btnCreateAPIKey.click()
        return self
    
    
    def is_api_key_existed(self, key_name, wait_time_out=None):
        """      
        @summary: Check if an API key is existed or not        
        @param
            - key_name: name of API key would like to check
            - wait_time_out: time to wait for API key displays
        @return: True: the API key is existed, False: the API key is not existed
        @author: Khoi Ngo
        """
        return self._lnkAPIKeyName(key_name).is_displayed(wait_time_out)
    
    
    def is_api_key_not_existed(self, key_name, wait_time_out=None):
        """      
        @summary: Check if an API key is not existed     
        @param 
            - key_name: name of API key would like to check
            - wait_time_out: time to wait for API key display
        @return: True: the API key is not existed, False: the API key is existed
        @author: Khoi Ngo
        """
        return self._lnkAPIKeyName(key_name).is_disappeared(wait_time_out)
    
    
    def delete_api_key(self, key_name):
        """      
        @summary: Delete an API key
        @param key_name: the API key name would like to delete 
        @return: OrganizatioSettingsPage
        @author: Khoi Ngo   
        """
        self._lnkDelteAPIKey(key_name).click()
        self._driver.handle_dialog(True)
        return self


    def get_text_msg(self):
        """      
        @summary: Get alert message    
        @return: Message of alert
        @author: Khoi Ngo
        """
        return self._msgAlert.text
    
    
    def submit_email_notifications_name(self, name):
        """
        @summary: This action use to input value to Email Notifications text-box
        @param: name: title of notification email
        @return: OrganizatioSettingsPage
        @author: Duy Nguyen
        """
        self._txtEmailNotifications.type(name)
        self._btnSaveSettings.click()
        self.wait_untill_success_msg_disappeared()
        return self
    
    
    def get_email_notifications_name(self):
        """
        @summary: This action use to get value of Email Notifications text-box
        @return: OrganizatioSettingsPage
        @author: Duy Nguyen
        """
        return self._txtEmailNotifications.text








  
    