from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from pages.suitable_tech.admin.advanced.admin_template_page import AdminTemplatePage
from pages.suitable_tech.user.welcome_to_beam_page import WelcomeToBeamPage
from common.constant import Language
from core.webdriver.element_list import ElementList
from core.webdriver.notification_records import NotificationRecords

class _AdminDashboardPageLocator(object):
    _btnInviteANewUser = (By.XPATH, "//button[@ng-click='inviteContact()']")
    _btnInviteATempUser = (By.XPATH, "//button[@ng-click='createTemporaryAccess()']")
    _btnImportUsers = (By.XPATH, "//button[@ng-click='importContacts()']")
    _btnAddABeam = (By.XPATH, "//button[@ng-click='addDevice()']")    
    _lnkYourAccount = (By.XPATH, "//ul[@class='dropdown-menu']/li/a[@href='#/account/settings/']/span[@class='ng-scope']")
    _lblPageTitle = (By.XPATH, "//h3//span[@class='ng-binding ng-scope']")
    _btnsReject = (By.XPATH, "//button[@ng-click='rejectAccessRequest(request.secret)']")
    _lblHeader = (By.XPATH, "//div[@class='row secondary-nav']//h3")
    _lblAccessRequestRecord = (By.XPATH, "//div[@ng-repeat='request in accessRequests']")
    

    
class AdminDashboardPage(AdminTemplatePage):
    """
    @description: This is page object class for Admin Dashboard page.
        This page will be opened after clicking signning in as Administrator.
        Please visit https://staging.suitabletech.com/manage/#/dashboard/ for more details.
    @page: Admin Dashboard page
    @author: Thanh Le
    """


    """    Properties    """
    @property
    def _lblHeader(self):
        return Element(self._driver, *_AdminDashboardPageLocator._lblHeader)
    @property
    def _btnInviteANewUser(self):
        return Element(self._driver, *_AdminDashboardPageLocator._btnInviteANewUser)
    @property
    def _btnInviteATempUser(self):
        return Element(self._driver, *_AdminDashboardPageLocator._btnInviteATempUser)
    @property
    def _btnImportUsers(self):
        return Element(self._driver, *_AdminDashboardPageLocator._btnImportUsers)
    @property
    def _lnkYourAccount(self):
        return Element(self._driver, *_AdminDashboardPageLocator._lnkYourAccount)
    @property
    def _btnAddABeam(self):
        return Element(self._driver, *_AdminDashboardPageLocator._btnAddABeam)
    @property
    def _lblPageTitle(self):
        return Element(self._driver, *_AdminDashboardPageLocator._lblPageTitle)
    @property
    def _btnsReject(self):
        return ElementList(self._driver, *_AdminDashboardPageLocator._btnsReject)
    @property
    def _lblAccessRequestRecord(self):
        return NotificationRecords( self._driver, *_AdminDashboardPageLocator._lblAccessRequestRecord)
        
    
    """    Methods    """
    def __init__(self, driver):        
        """      
        @summary: Constructor method      
        @param driver: Web driver
        @author: Thanh Le 
        """
        AdminTemplatePage.__init__(self, driver)      
        self._lblHeader.wait_until_displayed()
    
    
    def open_invite_a_new_user_dialog(self):
        """      
        @summary: Open the Invite a New User form  
        @return: AdminDashboardPage
        @author: Thanh Le
        """
        self._btnInviteANewUser.wait_until_clickable().click()
        return self
        
    
    def enter_invite_user_info(self, user):
        """      
        @summary:  Fill out user info on Invite New User form 
        @param user: user would like to invite
        @return: AdminDashboardPage
        @author: Thanh Le
        """
        from pages.suitable_tech.admin.dialogs.invite_new_user_dialog import InviteNewUserDialog
        invite_user_dialog = InviteNewUserDialog(self._driver)        
        invite_user_dialog.enter_invite_information(user)
        return self
    
    
    def is_invite_user_button_disabled(self):
        """      
        @summary: Check if the Invite User button is disabled or not
        @return: True: the Invite User button is disabled, False: the Invite User button is enabled
        @author: Thanh Le
        """
        from pages.suitable_tech.admin.dialogs.invite_new_user_dialog import InviteNewUserDialog
        return InviteNewUserDialog(self._driver).is_invite_user_button_disabled()      
    
    
    def cancel_invite_user_dialog(self):
        """      
        @summary: Cancel on the Invite a New User form
        @return: AdminDashboardPage
        @author: Thanh Le
        """
        from pages.suitable_tech.admin.dialogs.invite_new_user_dialog import InviteNewUserDialog
        InviteNewUserDialog(self._driver).cancel()
    
    
    def invite_new_user(self, user, wait_for_completed=True):
        """      
        @summary:  Invite a new user       
        @param 
            - user: user would like to invite
            - wait_for_completed: time to wait for complete inviting
        @return: AdminDashboardPage
        @author: Thanh Le
        """
        self.open_invite_a_new_user_dialog()
        from pages.suitable_tech.admin.dialogs.invite_new_user_dialog import InviteNewUserDialog
        invite_user_dialog = InviteNewUserDialog(self._driver)        
        invite_user_dialog.submit_invite_information(user)
        if(wait_for_completed):
            self.wait_untill_success_msg_disappeared()
        
        return self


    def invite_and_active_new_user(self, user):
        """      
        @summary:  Invite then activate a new user       
        @param user: user would like to invite and activate
        @return: AdminDashboardPage
        @author: Thanh Le
        """
        self.invite_new_user(user)  
        return self.logout_and_activate_new_user(user.email_address)
    
    
    def invite_and_active_new_admin_user(self, user):
        """      
        @summary:  Invite and activate a new admin user       
        @param user: user would like to invite and activate
        @return: AdminDashboardPage
        @author: Thanh Le
        """
        self.invite_new_device_group_admin_user(user)
        return self.logout_and_activate_new_user(user.email_address)
    
        
    def invite_new_device_group_admin_user(self, user, wait_for_completed=True):
        """      
        @summary: Invite a new device group admin user  
        @param 
            - user: user who would like to invite
            - wait_untill_success_msg_disappeared: dismiss the successful message or not
        @return: AdminDashboardPage
        @author: Thanh Le
        """
        return self.invite_new_user(user)\
            .goto_settings_tab_of_a_device_group(user.device_group)\
            .add_administrator(user, wait_for_completed)
    

    def create_complete_admin_user(self, user):
        """      
        @summary: Create and active a new admin user
        @param user: user would like to create and activate
        @return: AdminDashboardPage
        @author: Thanh Le
        """
        self.invite_new_device_group_admin_user(user)
        self.logout_and_activate_new_user(user.email_address).set_password(user.password)
        return self._set_user_language()
        
        
    def create_complete_normal_user(self, user):
        """      
        @summary: Create and activate a normal user
        @param user: user would like to be created
        @return: Welcome to Beams page
        @author: Thanh Le
        """
        self.invite_new_user(user)
        self.logout_and_activate_new_user(user.email_address).set_password(user.password)
        return self._set_user_language()
    
    
    def create_complete_normal_user_and_watch_video(self, user):
        """      
        @summary: Create a normal user and complete watching video      
        @param user: user would like to be created
        @return: AdminDashboardPage
        @author: Thanh Le
        """
        return self.create_complete_normal_user(user).watch_video()
        
            
    def create_complete_admin_user_and_watch_video(self, user):
        """      
        @summary: Create an admin and complete watching video       
        @param user: admin user would like to be created
        @return: AdminDashboardPage
        @author: Thanh Le
        """
        return self.create_complete_admin_user(user).watch_video()
    
    
    def get_page_title(self):
        """      
        @summary: Get title of a page     
        @return: Title of page
        @author: Thanh Le
        """
        return self._lblPageTitle.text
    
    
    def import_users(self, data_file_path, new_device_group, new_user_group):
        """      
        @summary: Import users    
        @param 
            - data_file_path: file path of imported file
            - new_device_group: device group would like to add imported user to
            - new_user_group: user group would like to add imported user to
        @return: AdminDashboardPage
        @author: Thanh Le
        """
        self._btnImportUsers.wait_until_clickable().click()
        from pages.suitable_tech.admin.dialogs.import_users_dialog import ImportUsersDialog
        ImportUsersDialog(self._driver).submit_users_from_file(data_file_path, new_device_group, new_user_group)
        
        self.wait_untill_success_msg_disappeared()
        return self
    
    
    def import_users_expecting_error(self, data_file_path):
        """      
        @summary: Import an invalid user file   
        @param data_file_path: path to imported file
        @return: AdminDashboardPage
        """
        self._btnImportUsers.wait_until_clickable().click()
        from pages.suitable_tech.admin.dialogs.import_users_dialog import ImportUsersDialog
        ImportUsersDialog(self._driver).submit_users_from_invalid_file(data_file_path)
        return self
    
    
    def invite_temporary_user(self, user, start_date=None, end_date=None, link_to_beam_sofware=None, default_invitation=None, require_session_answer=None, device_group=None):
        """
        @summary: Invite a temporary user        
        @param user: The temporary user
        @param start_date: Staring date
        @param end_date: Ending date
        @param link_to_beam_sofware: Include a link to the Beam software checkbox [True/False]
        @param default_invitation: Include the default invitation message checkbox [True/False]
        @param require_session_answer: Require session answer checkbox [True/False]        
        @return: AdminBeamAccessTimesPage
        @author: Thanh Le
        @created_date: August 08, 2016
        """
        self._btnInviteATempUser.wait_until_clickable().click()
        from pages.suitable_tech.admin.dialogs.invite_a_temporary_user import InviteTempUserDialog
        InviteTempUserDialog(self._driver).submit_invite_information(user, start_date, end_date, link_to_beam_sofware, default_invitation, require_session_answer, device_group)
        return self
        

    def add_a_beam_device(self, beam_code):
        """
        @summary: Used to add a beam device        
        @author: Duy Nguyen
        @created_date: August 17, 2016
        """
        self._btnAddABeam.wait_until_clickable().click()
        from pages.suitable_tech.admin.dialogs.link_a_beam_with_your_account_dialog import LinkABeamWithYourAccountDialog
        BeamLinkAccountDialog = LinkABeamWithYourAccountDialog(self._driver)
        BeamLinkAccountDialog.submit_beam_code(beam_code)
        return self
    
    def open_beam_linking_dialog(self):
        self._btnAddABeam.wait_until_clickable().click()
        from pages.suitable_tech.admin.dialogs.link_a_beam_with_your_account_dialog import LinkABeamWithYourAccountDialog
        return LinkABeamWithYourAccountDialog(self._driver)
        
    def input_beam_code_expecting_error(self, beam_code, submit=True):
        """
        @summary: Used to input an error beam code.       
        @author: Duy Nguyen
        @created_date: August 17, 2016
        """        
        self._btnAddABeam.wait_until_clickable().click()
        from pages.suitable_tech.admin.dialogs.link_a_beam_with_your_account_dialog import LinkABeamWithYourAccountDialog
        BeamLinkAccountDialog = LinkABeamWithYourAccountDialog(self._driver)
        if (submit):
            return BeamLinkAccountDialog.submit_beam_code(beam_code)
        else:
            return BeamLinkAccountDialog.input_beam_code(beam_code)


    def is_page_displayed(self, time_out=None):
        """
        @summary: Check if a page is displayed or not
        @return: True: the page is displayed, False: the page is not displayed    
        @author: Duy Nguyen
        @created_date: August 17, 2016
        """
        return self._lblHeader.is_displayed(time_out)


    def _set_user_language(self):
        """
        @summary: Set language for new User. This is a workarround for defect BUG_35
        @return: WelcomeToBeamPage    
        @author: Thanh Le
        @created_date: Sep 21, 2016
        """
        if(self._driver._driverSetting.language == Language.ENGLISH):
            return WelcomeToBeamPage(self._driver)
        else:
            welcome_to_beam_url = self._driver.current_url
            WelcomeToBeamPage(self._driver).goto_account_settings_page_by_menu_item()\
                .set_language(self._driver._driverSetting.language).save_change()
            
            self._driver.get(welcome_to_beam_url)
            return WelcomeToBeamPage(self._driver)
    
    
    def is_access_request_displayed(self, user):
        """
        @summary: Check whether the access request recoded is displayed or not
        @param user: User object
        @return: True if found. Otherwise, return False    
        @author: Thanh Le
        @created_date: Sep 21, 2016
        """
        return self._lblAccessRequestRecord.does_record_exist(user)
        
        
    def reject_all_access_requests(self):
        """
        @summary: Reject all existing access requests
        @return: AdminDashboardPage    
        @author: Thanh Le
        @created_date: Otc 05, 2016
        """
        all_reject_buttons = self._btnsReject.get_all_elements()
        if(len(all_reject_buttons) > 0):
            for index in range(len(all_reject_buttons)):
                reject_button = self._btnsReject.get_element_at(0)
                reject_button.click()
                self.wait_untill_success_msg_disappeared()


