from pages.suitable_tech.admin.advanced.admin_template_page import AdminTemplatePage
from core.webdriver.element import Element
from selenium.webdriver.common.by import By

class _AdminActivityPageLocator(object):
    _lblHeader = (By.XPATH, "//div[@class='row secondary-nav']//h3")
    
    
class AdminActivityPage(AdminTemplatePage):
    """
    @description: This is page object class for Admin Activity page.
        This page will be opened after clicking Activity tab on Dashboard page.
        Please visit https://staging.suitabletech.com/manage/#/activity/ for more details.
    @page: Admin Activity page
    @author: thanh.viet.le
    """
    
    """    Properties """
    @property
    def _lblHeader(self):
        return Element(self._driver, *_AdminActivityPageLocator._lblHeader)


    """    Methods    """
    def __init__(self, driver):
        """      
        @summary: Constructor method
        @param <param_name>: 
        @author: 
        @created_date: 
        """     
        AdminTemplatePage.__init__(self, driver)
        self._lblHeader.wait_until_displayed()