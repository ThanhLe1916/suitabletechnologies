from pages.suitable_tech.user.user_template_page import UserTemplatePage
from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from pages.suitable_tech.admin.advanced.dashboard.admin_dashboard_page import AdminDashboardPage
from common.application_constants import ApplicationConst


class _AdminHomePageLocator(object):
    _lnkManageYourBeams = (By.XPATH, "//a[@class='button large expand' and @href='/manage/']")
    _lnkDownloadBeamSoftware = (By.XPATH, "//a[contains(@href, '/api/1/get_installer')]")
    _lnkSetUpNewBeam = (By.XPATH, "//a[@class='button large expand' and @href='/setup/']")
    _lnkViewDocumentation = (By.XPATH, "//a[@class='button large expand' and @href='/documentation/']")
    _lnkGetHelp = (By.XPATH, "//a[@class='button large expand' and @href='/support/']")
    _lblHeader = (By.XPATH, "//section[@class='masthead info']//h2")
    
    
class AdminHomePage(UserTemplatePage):
    """
    @description: This is page object class for Admin Home page.
        This page will be opened after signing in as administrator.
        Please visit https://staging.suitabletech.com/accounts/home/ for more details.
    @page: Admin Home page
    @author: Thanh Le
    """


    """    Properties    """
    @property
    def _lblHeader(self):
        return Element(self._driver, *_AdminHomePageLocator._lblHeader)
    @property
    def _lnkManageYourBeams(self):
        return Element(self._driver, *_AdminHomePageLocator._lnkManageYourBeams)
    @property
    def _lnkDownloadBeamSoftware(self):
        return Element(self._driver, *_AdminHomePageLocator._lnkDownloadBeamSoftware)
    @property
    def _lnkSetUpNewBeam(self):
        return Element(self._driver, *_AdminHomePageLocator._lnkSetUpNewBeam)
    @property
    def _lnkViewDocumentation(self):
        return Element(self._driver, *_AdminHomePageLocator._lnkViewDocumentation)
    @property
    def _lnkGetHelp(self):
        return Element(self._driver, *_AdminHomePageLocator._lnkGetHelp)
    
    """    Methods    """
    def __init__(self, driver):
        """      
        @summary: Constructor method   
        @param driver: web driver
        @author: Thanh Le         
        """
        UserTemplatePage.__init__(self, driver)
        self._lblHeader.wait_until_displayed()
        self._lnkGetHelp.wait_until_displayed()
        
    
    def goto_admin_dashboard_page(self):
        """      
        @summary: Go to admin dashboard page  
        @return: AdminDashboardPage
        @author: Thanh Le  
        """
        self._lnkManageYourBeams.click()
        return AdminDashboardPage(self._driver)
    
    
    def goto_simplified_dashboard_page(self):
        """      
        @summary: Go to simplified admin dashboard page
        @return: SimplifiedDashboardPage
        @author: Thanh le   
        """
        self._lnkManageYourBeams.click()
        from pages.suitable_tech.admin.simplified.dashboard.simplified_dashboard_page import SimplifiedDashboardPage
        return SimplifiedDashboardPage(self._driver)
        
    
    def is_download_beam_software_displayed(self):
        """      
        @summary: Check if the 'Download the Beam Desktop Software' link is displayed or not  
        @return: True: the 'Download the Beam Desktop Software' link is displayed, False: this link is not displayed
        @author: Thanh Le
        """
        return (self._lnkDownloadBeamSoftware.is_enabled() and self.is_dropdownlist_item_existed(ApplicationConst.LBL_DOWNLOAD_INSTALLER))
    
    
    def is_set_up_new_beam_displayed(self):
        """      
        @summary: Check if the 'Add a Beam' link is displayed  
        @return: True: the 'Add a Beam' link is displayed, False: the'Add a Beam' link is not displayed  
        @author: Thanh Le
        """
        return (self._lnkSetUpNewBeam.is_enabled())
    
    
    def is_view_document_displayed(self):
        """      
        @summary: Check if 'View Document' link is displayed or not         
        @return: True: the 'View Document' link is displayed, False: the 'View Document' link is not displayed
        @author: Thanh Le
        """
        return (self._lnkViewDocumentation.is_enabled() and self.is_dropdownlist_item_existed(ApplicationConst.LBL_DOCUMENTATION))
    
    
    def is_get_help_displayed(self):
        """      
        @summary: Check if the 'Get Help' link is displayed or not         
        @return: True: the 'Get Help' link is displayed, False: the 'Get Help' link is not displayed
        @author: Thanh Le
        """
        return (self._lnkGetHelp.is_enabled() and self.is_dropdownlist_item_existed(ApplicationConst.LBL_BEAM_HELP))
    
    
    def is_manage_your_beams_function_enabled(self):
        """      
        @summary: Check if the 'Manage Your Beams' is enabled or not      
        @return: True: the 'Manage Your Beams' is enabled , False: the 'Manage Your Beams' is disabled
        @author: Thanh Le   
        """
        return (self._lnkManageYourBeams.is_enabled() and self.is_dropdownlist_item_existed(ApplicationConst.LBL_MANAGE_YOUR_BEAMS)) 
    
    
    def is_manage_your_beams_function_disabled(self):
        """      
        @summary: Check if the 'Manage Your Beams' is disabled or not   
        @return: True: the 'Manage Your Beams' is disabled , False: the 'Manage Your Beams' is enabled
        @author: Thanh Le     
        """
        return (self._lnkManageYourBeams.is_disappeared() and self.is_dropdownlist_item_unexisted(ApplicationConst.LBL_MANAGE_YOUR_BEAMS))
    
        
        
        
        
        
        
        
        
        
        
        
        