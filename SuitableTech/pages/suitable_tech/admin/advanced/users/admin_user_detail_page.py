from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from pages.suitable_tech.admin.advanced.users.admin_users_page import AdminUsersPage
from pages.suitable_tech.admin.advanced.users.admin_users_common_page import AdminUsersCommonPage
from pages.suitable_tech.admin.dialogs.edit_user_dialog import EditUserDialog
from core.webdriver.element_list import ElementList
from common.application_constants import ApplicationConst

class _AdminUserDetailPageLocator(object):
    _lblPageHeader = (By.XPATH, "//h3[@class='detail-heading ng-binding']")
    _btnRemoveFromOrg = (By.XPATH, "//button[@ng-click='delete()']")
    _btnEditUser = (By.XPATH, "//button[@ng-click='edit()']")
    
    @staticmethod
    def _lblAdministratesGroup():
        return (By.XPATH, u"//span[.=\"{}\"]/../following-sibling::dd[1]//span[@class='ng-scope']/a".format(ApplicationConst.LBL_ADMINISTERS_GROUPS))
    @staticmethod
    def _lblUserGroups():
        return (By.XPATH, u"//span[.=\"{}\"]/../following-sibling::dd[1]//span[@class='ng-scope']/a".format(ApplicationConst.LBL_USER_GROUPS))
    @staticmethod
    def _lblDeviceGroups():
        return (By.XPATH, u"//span[.=\"{}\"]/../following-sibling::dd[1]//span[@class='ng-scope']/a".format(ApplicationConst.LBL_DEVICE_GROUPS_PROPERTY))
    @staticmethod
    def _lblUserTitle(value):
        return (By.XPATH, u"//h3[contains(., \"{}\")]".format(value))
    @staticmethod
    def _lblUserInformation(property_name):
        return (By.XPATH, u"//span[.=\"{}\"]/../following-sibling::dd[1]".format(property_name))
    @staticmethod
    def _lblAdministratorNotice(value):
        return (By.XPATH, u"//dd[contains(.,\"{}\") and contains(@ng-if,'user.is_admin')]".format(value))
 
    
class AdminUserDetailPage(AdminUsersCommonPage):
    """
    @description: This is page object class for Admin Users Details page.
        This page will be opened after clicking Users link.
        Please visit https://staging.suitabletech.com/manage/#/contacts/c10930@mailinator.com/ for more details.
    @page: Admin Users Details page
    @author: Thanh Le
    """


    """    Properties    """
    @property
    def _lblPageHeader(self):
        return Element(self._driver, *_AdminUserDetailPageLocator._lblPageHeader)
    @property
    def _btnRemoveFromOrg(self):
        return Element(self._driver, *_AdminUserDetailPageLocator._btnRemoveFromOrg)
    @property
    def _lblAdministratesGroup(self):
        return ElementList(self._driver, *_AdminUserDetailPageLocator._lblAdministratesGroup())   
    @property
    def _btnEditUser(self):
        return Element(self._driver, *_AdminUserDetailPageLocator._btnEditUser)
    @property
    def _lblUserGroups(self):
        return ElementList(self._driver, *_AdminUserDetailPageLocator._lblUserGroups())
    @property
    def _lblDeviceGroups(self):
        return ElementList(self._driver, *_AdminUserDetailPageLocator._lblDeviceGroups())
    
    def _lblAdministratorNotice(self, value):
        return Element(self._driver, *_AdminUserDetailPageLocator._lblAdministratorNotice(value))
    def _lblUserTitle(self, value):
        return Element(self._driver, *_AdminUserDetailPageLocator._lblUserTitle(value))
    def _lblUserInformation(self, property_name):
        return Element(self._driver, *_AdminUserDetailPageLocator._lblUserInformation(property_name))
    
    """    Methods    """
    def __init__(self, driver):      
        """      
        @summary: Constructor method
        @param driver: Web driver 
        @author: Thanh Le         
        """  
        AdminUsersCommonPage.__init__(self, driver)
        self._wait_for_loading()


    def get_user_info(self, property_name):
        """      
        @summary: Get user information  
        @param property_name: name of user would like to get property
        @return: information of user
        @author: Thanh Le
        """
        self._lblUserInformation(property_name).wait_until_displayed(2)
        return self._lblUserInformation(property_name).text
    
    
    def get_user_administers_groups(self):
        """      
        @summary: Get admin user group      
        @return: list of user group of admin
        @author: Thanh Le
        """
        #lst_elements = []
        lst_groups = []
        lst_elements = self._lblAdministratesGroup.get_all_elements()
        for e in lst_elements:
            lst_groups.append(e.text)
        return lst_groups
    
    
    def is_user_removable(self):
        """      
        @summary: Check if the Remove button is enable for a user or not         
        @return: True: the Remove button is displayed, False: the Remove button is not displayed
        @author: Thanh Le
        """
        return self._btnRemoveFromOrg.is_displayed(5)
    

    def remove_user_from_organization(self, wait_for_completed=True):
        """      
        @summary: Remove a user form an organization 
        @param wait_for_completed: time to wait for complete removing
        @return: AdminUserDetailPage
        @author: Thanh Le     
        """
        self._btnRemoveFromOrg.click()
        self._driver.handle_dialog(True)
        if(wait_for_completed):
            self.wait_untill_success_msg_disappeared()
        return AdminUsersPage(self._driver)
    
    
    def is_user_page_displayed(self, user_displayed_name):
        """      
        @summary: Check if user detail page is displayed or not       
        @param user_displayed_name: user would like to be checked
        @return: True: user page is displayed, False: user page is not displayed
        @author: Thanh Le
        """
        return self._lblUserTitle(user_displayed_name).is_displayed()
    
    
    def is_user_page_disappeared(self, user):
        """      
        @summary: Check if user page disappears or not     
        @param user: user would like to check
        @return: True: the user is disappeared, False: user appears
        @author: True: the user disappears, False: the user appears
        """
        return self._lblUserTitle(user).is_disappeared()
    
    
    def allow_user_to_administer_org(self, wait_for_completed=True):
        """      
        @summary: Set user to be an organization administrator 
        @param wait_for_completed: time wait for completing setting
        @return: AdminUserDetailPage
        @author: Thanh Le 
        """
        self._btnEditUser.click()
        EditUserDialog(self._driver).allow_user_to_administer_org()
        if wait_for_completed:
            self.wait_untill_success_msg_disappeared()
        return self
    
    
    def disallow_user_to_administer_org(self, wait_for_completed=True):
        """      
        @summary: Disallow an org admin privilege
        @param wait_for_completed: time wait for completing settin
        @return: AdminUserDetailPage
        @author: Thanh Le   
        """
        self._btnEditUser.click()
        EditUserDialog(self._driver).disallow_user_to_administer_org()
        if wait_for_completed:
            self.wait_untill_success_msg_disappeared(2)
        return self
    
    
    def is_allow_user_to_administrater_org_disabled(self):
        """      
        @summary: Check if the 'Allow this user to administer this organization' checkbox is enabled or  not
        @return: True: the checkbox is enable, False: the checkbox is disabled
        @author: Thanh Le     
        """
        self._btnEditUser.click()
        edit_user_dialog = EditUserDialog(self._driver)
        result = edit_user_dialog.is_allow_user_to_administrater_org_disabled()
        edit_user_dialog.cancel()
        return result
    
    
    def is_save_message_successfully_display(self, wait_time=5):
        """      
        @summary: Check if the successful message when editing user is displayed or not     
        @param wait_time: time to wait for message appears
        @return: True: the successful message is displayed, False: the successful message is not displayed
        @author: Thanh Le
        """
        return self.is_success_msg_displayed(wait_time)
    
    
    def is_administator_label_notice(self, option, wait_time=2):
        """      
        @summary: Check if a user is administrator or not
        @param 
            - option: yes or no (the user is admin or not)
            - wait_time: time to wait for notice
        @return: True: the Administrator field is Yes, False: the Administrator field is No
        @author: Thanh Le
        """
        return self._lblAdministratorNotice(option).is_displayed(wait_time)
    
    
    def get_user_groups(self):
        """      
        @summary: Get list of user group of a user     
        @return: list of user groups
        @author: Thanh Le
        """
        #lst_elements = []
        lst_user_groups = []
        lst_elements = self._lblUserGroups.get_all_elements()
        for e in lst_elements:
            lst_user_groups.append(e.text)
        return lst_user_groups
    
    
    def get_device_groups(self):
        """      
        @summary: Get list of device group of a user         
        @return: list of device groups
        @author: Thanh Le
        """
        #lst_elements = []
        lst_devices = []
        lst_elements = self._lblDeviceGroups.get_all_elements()
        for e in lst_elements:
            lst_devices.append(e.text)
        return lst_devices
   
    
    def wait_for_page_displayed(self, user_displayed_name):
        """      
        @summary: Wait for user detail page displays         
        @param user_displayed_name: name of user would like to check
        @return: AdminUserDetailPage
        @author: Thanh Le  
        """
        self._lblUserTitle(user_displayed_name).wait_until_displayed()
        return self   

        
        
