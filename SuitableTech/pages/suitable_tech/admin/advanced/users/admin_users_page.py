from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from pages.suitable_tech.admin.advanced.users.admin_users_common_page import AdminUsersCommonPage
from core.webdriver.dropdownlist import DropdownList
from core.webdriver.element_list import ElementList
from common.application_constants import ApplicationConst
from pages.basepage import BasePage

class _AdminUsersPageLocator(object):
    _txtSearch = (By.XPATH, "//input[@type='search']")
    _btnClearSearch = (By.XPATH, "//span[contains(@class, 'clear')]")
    _lblProfileCard = (By.XPATH, "//div[@for='user']//div[@class='profile-title ng-binding']")
    _iconDelete = (By.XPATH, "//span[@class='clear-input glyphicon glyphicon-remove form-control-feedback text-muted']")
    _lblNoUsersFoundMessage = (By.XPATH, "//div[@ng-hide='users.length || usersContentLoading']//span[@class='ng-scope']//span")
    _btnIconViewMode = (By.XPATH, "//span[@class='glyphicon glyphicon-th-large']/..")
    _btnListViewMode = (By.XPATH, "//span[@class='glyphicon glyphicon-th-list']/..")
    _lstUserProfileCards = (By.XPATH, "//div[@for='user']//a")
    _lstUserGroupProfileCards = (By.XPATH, "//div[@for='usergroup']//a")
    _ddlShow = (By.XPATH, "//div[contains(@ng-if, 'showGroups')]")
    _iconIconView = (By.XPATH, "//span[@class='glyphicon glyphicon-th-large']/..")
    _iconListView = (By.XPATH, "//span[@class='glyphicon glyphicon-th-list']/..")
    
    @staticmethod
    def _lblUserProfileCard(value):
        return (By.XPATH, u"//h4[.=\"{}\"]/following::div[@ng-repeat='user in users']//div[@class='profile-title ng-binding' and normalize-space(.)=\"{}\"]".format(ApplicationConst.LBL_ADMIN_USERS_SECTION_USERS, value))
    @staticmethod
    def _lblUserTitle(value):
        return (By.XPATH, u"//h3[.=\"{}\"]".format(value))
    @staticmethod
    def _pnlProfileCard(profile_title):
        return (By.XPATH, u"//div[@class='profile-title ng-binding' and normalize-space(.)=\"{}\"]/../..".format(profile_title))    
    @staticmethod
    def _pnlProfileCardIcon(profile_title):
        return (By.XPATH, u"//div[@class='profile-title ng-binding' and normalize-space(.)=\"{}\"]/..//div[@class='profile-image-container ng-isolate-scope']".format(profile_title))
    @staticmethod
    def _pnlProfileRowIcon(profile_title):
        return (By.XPATH, u"//td[@class='ng-binding' and normalize-space(.)=\"{}\"]/parent::tr//div[@class='img-responsive small profile-image-container ng-isolate-scope']".format(profile_title))
    @staticmethod
    def _imgUserGroupIcon(user_group_name):
        return (By.XPATH,u"//div[@for='usergroup']//div[.=\"{}\"]/preceding-sibling::div//img".format(user_group_name))
    
class AdminUsersPage(AdminUsersCommonPage):
    """
    @description: This is page object class for Admin Users page.
        This page will be opened after clicking Users on Dashboard page.
        Please visit https://staging.suitabletech.com/manage/#/contacts/ for more details.
    @page: Admin Users page
    @author: Thanh Le
    """


    """    Properties    """
    @property
    def _iconIconView(self):
        return Element(self._driver, *_AdminUsersPageLocator._iconIconView)
    @property
    def _iconListView(self):
        return Element(self._driver, *_AdminUsersPageLocator._iconListView)    
    @property
    def _btnListViewMode(self):
        return Element(self._driver, *_AdminUsersPageLocator._btnListViewMode)
    @property
    def _btnIconViewMode(self):
        return Element(self._driver, *_AdminUsersPageLocator._btnIconViewMode)    
    @property
    def _txtSearch(self):
        return Element(self._driver, *_AdminUsersPageLocator._txtSearch)
    @property
    def _btnClearSearch(self):
        return Element(self._driver, *_AdminUsersPageLocator._btnClearSearch)
    @property
    def _ddlShow(self):
        return DropdownList(self._driver, *_AdminUsersPageLocator._ddlShow)
    @property
    def _btnCreateDeviceGroup(self):
        return Element(self._driver, *_AdminUsersPageLocator._btnCreateDeviceGroup)
    @property
    def _lblProfileCard(self):
        return Element(self._driver, *_AdminUsersPageLocator._lblProfileCard)
    @property
    def _lstUserProfileCards(self):
        return ElementList(self._driver, *_AdminUsersPageLocator._lstUserProfileCards)
    @property
    def _lstUserGroupProfileCards(self):
        return ElementList(self._driver, *_AdminUsersPageLocator._lstUserGroupProfileCards)
    @property
    def _iconDelete(self):
        return Element(self._driver, *_AdminUsersPageLocator._iconDelete)
    @property
    def _lblNoUsersFoundMessage(self):
        return Element(self._driver, *_AdminUsersPageLocator._lblNoUsersFoundMessage)
    
    def _lblUserTitle(self, value):
        return Element(self._driver, *_AdminUsersPageLocator._lblUserTitle(value))
    def _pnlProfileCard(self, value):
        return Element(self._driver, *_AdminUsersPageLocator._pnlProfileCard(value))
    def _lblUserProfileCard(self, value):
        return Element(self._driver, *_AdminUsersPageLocator._lblUserProfileCard(value))
    def _pnlProfileCardIcon(self, value):
        return Element(self._driver, *_AdminUsersPageLocator._pnlProfileCardIcon(value))
    def _pnlProfileRowIcon(self, value):
        return Element(self._driver, *_AdminUsersPageLocator._pnlProfileRowIcon(value))
    def _imgUserGroupIcon(self, group_name):
        return Element(self._driver, *_AdminUsersPageLocator._imgUserGroupIcon(group_name))
    def _pnlListViewDeviceItem(self, panel_value):
        return Element(self._driver, *_AdminUsersPageLocator._pnlListViewDeviceItem(panel_value))

    
    """    Methods    """
    def __init__(self, driver, wait_for_loading=True):       
        """      
        @summary: Constructor method    
        @param driver: web driver
        @author: Thanh Le         
        """
        if(wait_for_loading):
            AdminUsersCommonPage.__init__(self, driver)
            self.wait_for_loading()
        else:
            BasePage.__init__(self, driver)
            
    
    def is_user_existed(self, search_value, check_value=None, wait_time_out=None):
        """      
        @summary: Check if a user is existed or not         
        @param 
            - search_value: user would like to check
            - check_value: value used to check
            - wait_time_out: time wait for user exists  
        @return: True: user is existed, False: user is not existed
        @author: Thanh Le
        """
        self.search_for_user(search_value)
        if(check_value != None):
            return self._lblUserProfileCard(check_value).is_displayed(wait_time_out)
        else:
            return self._lblUserProfileCard(search_value).is_displayed(wait_time_out)
    
    
    def is_user_not_existed(self, search_value, check_value=None, wait_time_out=None):
        """      
        @summary: Check if user is not existed
        @param
            - search_value: user would like to check
            - check_value: value used to check
            - wait_time_out: time wait for user is not existed  
        @return: True: user is not existed, False: user is existed
        @author: Thanh Le
        """
        self.search_for_user(search_value)
        if(check_value != None):
            return self._lblUserProfileCard(check_value).is_disappeared(wait_time_out)
        else:
            return self._lblUserProfileCard(search_value).is_disappeared(wait_time_out)
    
    
    def click_show_button_and_select(self, value):
        """      
        @summary: Click Show button and select item in show dropdown list   
        @param value: item would like to select
        @return: AdminUsersPage
        @author: Thanh Le     
        """
        self._ddlShow.select_by_partial_text(value)
        #close popup
        self._ddlShow.click()
        return self
    
    
    def is_user_group_existed(self, search_value, check_value = None, wait_time_out=None):
        """      
        @summary: Check if a user group is existed or not     
        @param 
            - search_value: user group would like to check
            - check_value: criteria for searching
            - wait_time_out: time out to wait for completing searching
        @return: True: the user group is existed, False: the user group is not existed
        @author: Thanh Le
        """
        self._txtSearch.wait_until_displayed().type(search_value)
        self._wait_for_loading(3)
        if(check_value != None):
            return self._pnlProfileCard(check_value).is_displayed(wait_time_out)
        else:
            return self._pnlProfileCard(search_value).is_displayed(wait_time_out)
    
    
    def is_user_group_not_existed(self, search_value, check_value = None, wait_time_out=None):
        """      
        @summary: Check if a user group is not exist  
        @param 
            - search_value: user group would like to check
            - check_value: criteria for searching
            - wait_time_out: time out to wait for completing searching
        @return: True: the user group is not existed, False: the user group is existed
        @author: Thanh Le
        """
        self._txtSearch.wait_until_displayed().type(search_value)
        self._wait_for_loading(3)
        if(check_value != None):
            return self._pnlProfileCard(check_value).is_disappeared(wait_time_out)
        else:
            return self._pnlProfileCard(search_value).is_disappeared(wait_time_out)
        
    
    def goto_user_detail_page(self, user):
        """      
        @summary: Go to detail page of a user 
        @param user: user would like to go to detail page
        @return: detail page of user
        @author: Thanh Le
        """
        self.search_for_user(user.email_address)
        return self.select_user(user.get_displayed_name())
    
    
    def goto_user_detail_page_by_search_values(self, search_value, select_value):
        """      
        @summary: Go to detail page of user by search value   
        @param 
            - search_value: user would like to search
            - select_value: value to select
        @return: detail page of user
        @author: Thanh Le
        """
        self._wait_for_loading(5)
        self.search_for_user(search_value)
        self._lblUserProfileCard(select_value).click() 
        from pages.suitable_tech.admin.advanced.users.admin_user_detail_page import AdminUserDetailPage   
        return AdminUserDetailPage(self._driver)
    
    
    def goto_user_group_detail_page(self, group_name):
        """      
        @summary: Go to detail page of a user group         
        @param group_name: name of user group would like to go detail page
        @return: detail page of user group
        @author: Thanh Le
        """
        self._txtSearch.wait_until_displayed().type(group_name)
        self._wait_for_loading(3)
        return self.select_user_group(group_name)
            

    def search_for_user(self, search_value):
        """      
        @summary: Search for a user       
        @param search_value: criteria for searching a user
        @return: AdminUsersPage
        @author: Thanh Le      
        """
        self._txtSearch.wait_until_displayed().type(search_value)
        self._wait_for_loading()
        return self
     
    
    def remove_user_group_from_organization(self, group_name):
        """      
        @summary: Remove a user group form organization
        @param group_name: name of user group
        @return: AdminUsersPage
        @author: Thanh Le
        """
        self.goto_user_group_detail_page(group_name).delete_user_group()
        self._pnlProfileCard(group_name).wait_until_disappeared()
        return self
    
    
    def remove_user_from_organization(self, user):
        """      
        @summary: Remove a user from an organization         
        @param user: user would like to remove
        @return: AdminUsersPage
        @author: Thanh Le
        """
        self.goto_user_detail_page(user).remove_user_from_organization()
        return self
    
        
    def remove_all_test_users(self, keyword = "logigear1+user"):
        """      
        @summary: Remove all existing user matched searched keyword       
        @param keyword: criteria to remove user
        @return: AdminUsersPage
        @author: Thanh Le
        """
        from pages.suitable_tech.admin.advanced.users.admin_user_detail_page import AdminUserDetailPage   
        self.search_for_user(keyword)
        lst_user_profile_cards = self._lstUserProfileCards
        count = lst_user_profile_cards.count()
        while(count > 0):
            for i in range(0, count):
                web_elem = lst_user_profile_cards.get_element_at(i)
                if web_elem:
                    self._driver.execute_script("arguments[0].click();", web_elem)  
                    dialog = AdminUserDetailPage(self._driver)
                    dialog.remove_user_from_organization()
            
            self.search_for_user(keyword)
            # update runtime object
            lst_user_profile_cards = self._lstUserProfileCards
            count = lst_user_profile_cards.count(10)
                    
        return self
    
    
    def remove_all_test_user_groups(self, keyword = "LGVN"):
        """      
        @summary: Remove all existing users matched with searched criteria
        @param keyword: criteria for searching
        @return: AdminUsersPage
        @author: Thanh Le
        """
        from pages.suitable_tech.admin.advanced.users.admin_user_group_detail_page import AdminUserGroupDetailPage
        self.search_for_user(keyword)
        
        lst_user_group_profile_cards = self._lstUserGroupProfileCards   
        count = lst_user_group_profile_cards.count()
        while(count > 0):
            for i in range(0, count):
                web_elem = lst_user_group_profile_cards.get_element_at(i)
                if web_elem and web_elem.text:
                    self._driver.execute_script("arguments[0].click();", web_elem)   
                        
                    dialog = AdminUserGroupDetailPage(self._driver)
                    dialog.delete_user_group()
            
            self.search_for_user(keyword)        
            count = lst_user_group_profile_cards.count()
        
        return self
       
       
    def is_icon_delete_in_search_textbox_existed(self):
        """      
        @summary:  Check if the delete 'x' button is displayed or not       
        @return: True: 'x' button is display in search textbox, False: 'x' button is not displayed in search textbox
        @author: Thanh Le
        """
        return self._iconDelete.is_displayed(2)  
    
    
    def get_no_users_found_message(self):
        """      
        @summary: Get 'No users match your search.' message
        @return: The message 'No users match your search.'
        @author: Thanh Le
        """
        self._lblNoUsersFoundMessage.wait_until_displayed(5)
        return self._lblNoUsersFoundMessage.text
    

    def get_item_size_in_icon_view(self, value):
        """      
        @summary: Get the "imgsize" of the profile icon          
        @param value is the Name of User Group or User        
        @return: number type interger
        @author: Thanh Le
        @created_date: August 05, 2016
        """
        try:
            return int(self._pnlProfileCardIcon(value).get_attribute("imgsize"))
        except Exception:
            return 0
    
    
    def get_item_size_in_list_view(self, value):
        """      
        @summary: Get the "imgsize" of the profile icon          
        @param value is the Name of User Group or User        
        @return: number type interger
        @author: Thanh Le
        @created_date: August 05, 2016
        """
        try:
            return int(self._pnlProfileRowIcon(value).get_attribute("imgsize"))
        except Exception:
            return 0
        
        
    def switch_to_icon_view(self):
        """      
        @summary: Switch to icon view mode         
        @param: None 
        @return: None 
        @author: Thanh Le
        @created_date: 8/16/2016
        """
        self._iconIconView.click()
        return self
    
    
    def get_user_group_icon_url(self, user_group_name):
        """      
        @summary: Get url of a user group       
        @param: user_group_name: name of user group would like to get url
        @return: user group's icon url
        @author: Thanh Le
        """
        self._txtSearch.type(user_group_name)
        image_element = self._imgUserGroupIcon(user_group_name)
        image_element.wait_until_displayed()
        
        return image_element.get_attribute("src")
     
     
    def switch_to_list_view(self):
        """      
        @summary: Switch to list view mode         
        @param: None 
        @return: None 
        @author: Thanh Le
        @created_date: 8/16/2016 
        """
        self._iconListView.click()
        return self
    
    
    def is_item_displayed_in_list_mode(self, value):
        """      
        @summary: Get the "imgsize" of the profile icon          
        @param value is the Name of User Group or User or the email of the user        
        @return: True if item is displayed, and False if not
        @author: Thanh Le
        @created_date: August 16, 2016
        """
        return self._pnlProfileRowIcon(value).is_displayed(5)
    
    
    def select_user(self, user_displayed_name):
        """      
        @summary: Select a user by user's full name
        @param user_displayed_name: name of user would like to select
        @return: TAdminUserDetailPage
        @author: Thanh Le
        """
        profile =  self._pnlProfileCard(user_displayed_name)
        profile.wait_until_displayed(10)
        profile.click()
        #self._pnlProfileCard(user_displayed_name).click() 
        from pages.suitable_tech.admin.advanced.users.admin_user_detail_page import AdminUserDetailPage   
        return AdminUserDetailPage(self._driver)
    
    
    def select_user_group(self, group_name):
        """      
        @summary: Select a user by user's group name
        @param group_name: name of user group would like to select
        @return: AdminUserGroupDetailPage
        @author: Thanh Le
        """ 
        self._pnlProfileCard(group_name).wait_until_clickable().click()
        from pages.suitable_tech.admin.advanced.users.admin_user_group_detail_page import AdminUserGroupDetailPage
        return AdminUserGroupDetailPage(self._driver)
    
    
    def are_users_existed(self, all_emails):
        """      
        @summary: Check if users are existed or not     
        @param all_emails: emails of users would like to check
        @return: True: the users are existed, False: the users are not existed
        @author: Thanh Le
        """
        for email in all_emails:
            
            self.search_for_user(email)
            count = self._lstUserProfileCards.count(10)
            if count < 1:
                return False
            
        return True
        
    
    def remove_all_users_in_list(self, all_emails):
        """      
        @summary: Remove all existing users in list   
        @param all_emails: emails of users would like to remove
        @return: AdminUsersPage
        @author: Thanh Le
        """
        from pages.suitable_tech.admin.advanced.users.admin_user_detail_page import AdminUserDetailPage
        
        for email in all_emails:
            
            self.search_for_user(email)
            lst_user_profile_cards = self._lstUserProfileCards
            count = lst_user_profile_cards.count()
            while(count > 0):
                for i in range(0, count):
                    web_elem = lst_user_profile_cards.get_element_at(i)
                    if web_elem:
                        self._driver.execute_script("arguments[0].click();", web_elem)  
                        dialog = AdminUserDetailPage(self._driver)
                        dialog.remove_user_from_organization()
                count = lst_user_profile_cards.count(3)
                
        return self
    
