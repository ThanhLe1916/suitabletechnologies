from time import sleep

from selenium.webdriver.common.by import By
from common.application_constants import ApplicationConst
from core.utilities.utilities import Utilities
from core.webdriver.element import Element
from pages.suitable_tech.admin.advanced.users.admin_users_common_page import AdminUsersCommonPage
from pages.suitable_tech.admin.dialogs.choose_users_dialog import ChooseUsersDialog


class _AdminUserGroupDetailPageLocator(object):
    _lblPageHeader = (By.XPATH, "//h3[@class='detail-heading ng-binding']")
    _btnRemoveGroupIcon = (By.XPATH, "//button[@ng-click='removeImage()']")
    _btnDeleteGroup = (By.XPATH, "//button[@ng-click='delete()']")
    _btnEditGroup = (By.XPATH, "//button[@ng-click='edit()']")
    _btnAddUsers = (By.XPATH, "//button[@ng-click='addUsers()']")
    _lnkChangeIcon = (By.XPATH, "//span[@class='change-picture-icon fa fa-pencil']")
    _imgGroupIcon = (By.XPATH, "//div[@class='profile-image-editable']//img")
    
    @staticmethod
    def _lblDeviceGroups():
        return (By.XPATH, u"//span[.=\"{}\"]/../following-sibling::dd//span[@class='ng-scope']".format(ApplicationConst.LBL_DEVICE_GROUPS_PROPERTY))
    
    @staticmethod
    def _btnRemoveUser(value):
        return (By.XPATH, u"//div[@class='profile-title ng-binding' and normalize-space(.)=\"{}\"]/../..//a//div[@ng-if='canRemove']".format(value))
    @staticmethod
    def _lblUserGroupName(value):
        return (By.XPATH, u"//section[@class='ng-scope']//h3[.=\"{}\"]".format(value))
    @staticmethod
    def _pnlUser(displayed_name):
        return (By.XPATH, u"//div[@for='user']//div[contains(@class, 'profile-title') and .=\"{}\"]/..//button[contains(@ng-click, 'removeFn')]".format(displayed_name))
    @staticmethod
    def _btnRemove(value):
        return (By.XPATH, u"//h4[.=\"{}\"]/following::div[@ng-repeat='user in users']//div[@class='profile-title ng-binding' and normalize-space(.)=\"{}\"]/..//button[@class='btn btn-sm btn-danger']".format(ApplicationConst.LBL_ADMIN_USERS_SECTION_USERS, value))
   
    
class AdminUserGroupDetailPage(AdminUsersCommonPage):
    """
    @description: This is page object class for Admin Users Group Details page.
        This page will be opened after clicking Users link.
        Please visit https://staging.suitabletech.com/manage/#/contacts/groups/325/ for more details.
    @page: Admin Users Group Details page
    @author: Thanh Le
    """


    """    Properties    """ 
    @property
    def _lblPageHeader(self):
        return Element(self._driver, *_AdminUserGroupDetailPageLocator._lblPageHeader)
    @property
    def _btnRemoveGroupIcon(self):
        return Element(self._driver, *_AdminUserGroupDetailPageLocator._btnRemoveGroupIcon)
    @property
    def _imgGroupIcon(self):
        return Element(self._driver, *_AdminUserGroupDetailPageLocator._imgGroupIcon)
    @property
    def _btnDeleteGroup(self):
        return Element(self._driver, *_AdminUserGroupDetailPageLocator._btnDeleteGroup)
    @property
    def _btnEditGroup(self):
        return Element(self._driver, *_AdminUserGroupDetailPageLocator._btnEditGroup)
    @property
    def _btnAddUsers(self):
        return Element(self._driver, *_AdminUserGroupDetailPageLocator._btnAddUsers)
    @property
    def _lnkChangeIcon(self):
        return Element(self._driver, *_AdminUserGroupDetailPageLocator._lnkChangeIcon)
    @property
    def _lblDeviceGroups(self):
        return Element(self._driver, *_AdminUserGroupDetailPageLocator._lblDeviceGroups())
    
    def _lblUserGroupName(self, group_name):
        return Element(self._driver, *_AdminUserGroupDetailPageLocator._lblUserGroupName(group_name))
    def _pnlUser(self, displayed_name):
        return Element(self._driver, *_AdminUserGroupDetailPageLocator._pnlUser(displayed_name))
    def _btnRemoveUser(self, value):
        return Element(self._driver, *_AdminUserGroupDetailPageLocator._btnRemoveUser(value))
    def _btnRemove(self, value):
        return Element(self._driver, *_AdminUserGroupDetailPageLocator._btnRemove(value))
    
    """    Methods    """
    def __init__(self, driver):   
        """      
        @summary: Constructor method    
        @param driver: web driver
        @author: Thanh Le         
        """     
        AdminUsersCommonPage.__init__(self, driver)
        
        
    def is_user_group_name_displayed(self, group_name, wait_time_out=None):
        """      
        @summary: Check if a user group is displayed or not by name      
        @param
            - group_name: name of user group would like to check
            - wait_time_out: time to wait for user group displays
        @return: True: user group is displayed, False: user group is not displayed
        @author: Thanh Le
        """        
        return self._lblUserGroupName(group_name).is_displayed(wait_time_out)
    
    
    def is_change_icon_link_displayed(self):
        """      
        @summary: Check if icon link of a user group is displayed or not      
        @return: True: the icon link is displayed, False; the icon link is not displayed
        @author: Thanh Le  
        """
        return self._lnkChangeIcon.is_displayed(10)
    
        
    def edit_user_group(self, new_user_group_name):
        """      
        @summary: Edit a user group name         
        @param new_user_group_name: new user group name would like to set for editing
        @return: AdminUserGroupDetailPage
        @author: Thanh Le      
        """
        self._btnEditGroup.click()
        from pages.suitable_tech.admin.dialogs.edit_user_group_dialog import EditUserGroupDialog
        return EditUserGroupDialog(self._driver)
    
    
    def add_user_to_group(self, email_address):
        """      
        @summary: Add a user to user group by user's email         
        @param email_address: email of user would like to add to user group
        @return: AdminUserGroupDetailPage
        @author: Thanh Le     
        """
        self._btnAddUsers.click()
        ChooseUsersDialog(self._driver).choose_user(email_address)
        return self
    
    
    def get_property(self, value):
        """      
        @summary: Get property of a device group         
        @param value: property of a device group
        @return: property of a device group
        @author: Thanh Le    
        """
        if value == ApplicationConst.LBL_DEVICE_GROUPS_PROPERTY:
            return self._lblDeviceGroups.text


    def delete_user_group(self, wait_for_completed=True):
        """      
        @summary: Delete a user group      
        @param wait_for_completed: time wait for completing delete
        @return: AdminUserGroupDetailPage
        @author: Thanh Le   
        """
        self._btnDeleteGroup.click()
        self._driver.handle_dialog(True)
        
        # check succeed message
        if wait_for_completed:
            self.wait_untill_success_msg_disappeared()
        
        from pages.suitable_tech.admin.advanced.users.admin_users_page import AdminUsersPage
        return AdminUsersPage(self._driver, wait_for_completed)    
    
    
    def is_user_existed(self, displayed_name, wait_time_out=None):
        """      
        @summary: Check if a user is existed or not by full name
        @param displayed_name: full name of user would like to check
        @return: True: the user is existed, False: the user is not existed
        @author: Thanh Le
        """
        return self._pnlUser(displayed_name).is_displayed(wait_time_out)
    
    
    def is_user_not_existed(self, displayed_name, wait_time_out=None):
        """      
        @summary: Check if a user is existed or not by full name
        @param displayed_name: full name of user would like to check
        @return: True: the user is existed, False: the user is not existed
        @author: Thanh Le
        """
        return self._pnlUser(displayed_name).is_disappeared(wait_time_out)
    
    
    def remove_user(self, user_name):
        """      
        @summary: Remove a user by user name    
        @param user_name: the user would like to remove
        @return: AdminUserGroupDetailPage
        @author: Thanh Le  
        """
        self._btnRemoveUser(user_name).click()
        self.wait_for_loading(5)
        self._pnlUser(user_name).wait_until_disappeared(5)
        return self
    
    
    def change_group_name(self, new_group_name):
        """      
        @summary: Change name of user group  
        @param new_group_name: new name would like to set for user group
        @return: AdminUserGroupDetailPage
        @author:  Thanh le       
        """
        self._btnEditGroup.click()
        from pages.suitable_tech.admin.dialogs.edit_user_group_dialog import EditUserGroupDialog
        dialog = EditUserGroupDialog(self._driver)
        dialog.change_user_group_name(new_group_name)
        return self 
    
    
    def get_group_icon_link(self):
        """      
        @summary: Get icon link of a user group
        @param new_group_name: new name would like to set for user group
        @return: AdminUserGroupDetailPage
        @author:  Quang Tran     
        """
        file_url = self._imgGroupIcon.get_attribute("src")
        return Utilities.correct_link(file_url)
    
    
    def open_upload_image_dialog(self):
        """      
        @summary: Open the upload image form
        @return:  AdminUserGroupDetailPage
        @author: Quang Tran    
        """
        
        self._lnkChangeIcon.mouse_to()
        self._lnkChangeIcon.click()
        from pages.suitable_tech.admin.dialogs.upload_image_dialog import UploadImageDialog
        return UploadImageDialog(self._driver)
        
            
    def change_group_icon(self, image_path, left, top, width, height, wait_for_completed = True):
        """      
        @summary: Change a user group icon   
        @param 
            - image_path: path to  new image file
            - left, top, width, height: parameter for cropping image
            - wait_for_completed: time wait for complete changing
        @return: AdminUserGroupDetailPage
        @author: Quang Tran 
        """
        dialog = self.open_upload_image_dialog()
        
        dialog.choose_file(image_path)
        dialog.set_crop_tracker_dimension(left, top, width, height)
        dialog.submit()
            
        #wait for updating new icon
        if wait_for_completed:
            self.wait_for_icon_updated()
        
        return self
    
    
    def remove_group_icon(self):
        """      
        @summary: remove the icon of this group
        @return: AdminUserGroupDetailPage
        @author: Quang Tran
        """
        btn_remove = self._btnRemoveGroupIcon
        if btn_remove.is_displayed(5):
            btn_remove.click()
            self._driver.handle_dialog(True)
            
            self.wait_for_icon_removed()
                
        return self
    
    
    def wait_for_icon_updated(self):
        """      
        @summary: Wait for icon mage is completely updated
        @return: AdminUserGroupDetailPage
        @author: Quang Tran
        """  
        sleep(3)
        ico_url = self.get_group_icon_link()
        ext = ico_url.rsplit('.', 1)
        tried = 0
        while tried < 10:
            if len(ext) > 1 and ext[1] != 'svg':
                sleep(2)
                return self
            tried += 1
            sleep(2)
            ico_url = self.get_group_icon_link()
            ext = ico_url.rsplit('.', 1)
            
        return self
    
    
    def wait_for_icon_removed(self):
        """      
        @summary: Wait for icon mage is completely removed
        @return: AdminUserGroupDetailPage
        @author: Quang Tran
        """  
        sleep(3)
        ico_url = self.get_group_icon_link()
        ext = ico_url.rsplit('.', 1)
        tried = 0
        while tried < 10:
            if len(ext) > 1 and ext[1] == 'svg':
                sleep(2)
                return self
            tried += 1
            sleep(2)
            ico_url = self.get_group_icon_link()
            ext = ico_url.rsplit('.', 1)
        
        return self
    
    
    def goto_user_detail_page(self, user):
        """
        @summary: Go to user detail page
        @param user: user would like to view detail
        @return: User detail page
        @author: Quang Tran
        """
        self._lblUserProfileCard(user.get_displayed_name()).wait_until_clickable().click() 
        from pages.suitable_tech.admin.advanced.users.admin_user_detail_page import AdminUserDetailPage   
        return AdminUserDetailPage(self._driver)
      
      
    def remove_user_from_organization(self, user):
        """      
        @summary: Remove a user from org
        @param user: user who would like to remove
        @return: AdminUserGroupDetailPage
        @author: Quang Tran  
        """
        self.goto_user_detail_page(user).remove_user_from_organization()
        self._lblUserProfileCard(user.get_displayed_name()).wait_until_disappeared()
        from pages.suitable_tech.admin.advanced.users.admin_users_page import AdminUsersPage
        return AdminUsersPage(self._driver)
    
    def wait_for_page_displayed(self, user_group_name):
        """      
        @summary: wait for a page displays
        @param user_group_name: user group would like to wait for display
        @return: AdminUserGroupDetailPage
        @author: Thanh Le         
        """
        self._lblUserGroupName(user_group_name).wait_until_displayed()
        return self
        
    
    def remove_user_from_this_group(self, user):
        """      
        @summary: Remove a user in this group          
        @param Value is the user        
        @return: AdminUserGroupDetailPage
        @author: Thanh Le
        @created_date: August 05, 2016
        """
        self._btnRemove(user.get_displayed_name()).click()
        self._btnRemove(user.get_displayed_name()).wait_until_disappeared()        
        return self
        
