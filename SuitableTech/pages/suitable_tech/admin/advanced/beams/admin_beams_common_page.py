from pages.suitable_tech.admin.advanced.admin_template_page import AdminTemplatePage
from selenium.webdriver.common.by import By
from core.webdriver.element import Element

class _AdminBeamsPageLocator(object):
    _lnkDevices = (By.XPATH, "//li[contains(@ng-class, 'beams.group.devices')]//a[@ui-sref='beams.group.devices({groupid: activeGroupId()})']")
    _lnkMembers = (By.XPATH, "//li[@ui-sref-active='active']//a[contains(@href, '/members/')]")
    _lnkAccessTimes = (By.XPATH, "//li[@ui-sref-active='active']//a[contains(@href, '/access/')]")
    _lnkReservations = (By.XPATH, "//li[@ui-sref-active='active']//a[contains(@href, '/reservations/')]")
    _lnkSettings = (By.XPATH, "//li[@ui-sref-active='active']//a[contains(@href, '/settings/')]")
    _btnAllBeams = (By.XPATH,"//div[contains(@class, 'device-group-dropdown')]")
    _lnkCreateDeviceGroup = (By.XPATH, "//div[@class='dropdown header-buttons device-group-dropdown open']/descendant::button[@ng-click='createDeviceGroup()']")
    _lblHeader = (By.XPATH, "//div[@class='row device-group-header ng-scope']//h3")
    
    @staticmethod
    def _ddlDeviceInAllBeams(device_group_name):
        return (By.XPATH, u"//ul[@class='dropdown-menu' and @role='menu']/li/a[contains(., \"{}\")]".format(device_group_name))


class AdminBeamsCommonPage(AdminTemplatePage):
    """
    @description: This is page object class that contains all common controls/methods used across all Beams pages.
        This class is ONLY for inheriting.
    @page: Beam All Devices page
    @author: thanh.viet.le
    """


    """    Properties    """
    @property
    def _lblHeader(self):
        return Element(self._driver, *_AdminBeamsPageLocator._lblHeader)
    @property
    def _lnkDevices(self):
        return Element(self._driver, *_AdminBeamsPageLocator._lnkDevices)
    @property
    def _lnkMembers(self):
        return Element(self._driver, *_AdminBeamsPageLocator._lnkMembers)
    @property
    def _lnkAccessTimes(self):
        return Element(self._driver, *_AdminBeamsPageLocator._lnkAccessTimes)
    @property
    def _lnkReservations(self):
        return Element(self._driver, *_AdminBeamsPageLocator._lnkReservations)
    @property
    def _lnkSettings(self):
        return Element(self._driver, *_AdminBeamsPageLocator._lnkSettings)
    @property
    def _btnAllBeams(self):
        return Element(self._driver, *_AdminBeamsPageLocator._btnAllBeams)
    @property
    def _lnkCreateDeviceGroup(self):
        return Element(self._driver, *_AdminBeamsPageLocator._lnkCreateDeviceGroup)
    
    def _ddlDeviceInAllBeams(self, device_group_name):
        return Element(self._driver, *_AdminBeamsPageLocator._ddlDeviceInAllBeams(device_group_name))
    
    """    Methods      """    
    def __init__(self, driver):        
        """      
        @summary: Constructor method   
        @param driver: Web driver 
        @author: Thanh Le         
        """
        AdminTemplatePage.__init__(self, driver)

        
    def goto_devices_tab(self):
        """      
        @summary:  Go to 'Devices' sub tab of a device group    
        @return: AdminBeamsDevicesPage: Devices tab
        @author: Thanh Le
        """
        self._lnkDevices.wait_until_clickable().click()
        from pages.suitable_tech.admin.advanced.beams.admin_beams_devices_page import AdminBeamsDevicesPage
        return AdminBeamsDevicesPage(self._driver)
    
    
    def goto_setting_tab(self):
        """      
        @summary: Go to Settings tab of a device group   
        @return: AdminBeamsDevicesPage: Settings tab
        @author: Thanh Le  
        """
        self._lnkSettings.wait_until_clickable().click()
        from pages.suitable_tech.admin.advanced.beams.admin_beams_settings_page import AdminBeamsSettingsPage
        return AdminBeamsSettingsPage(self._driver)
    
    
    def goto_accesstimes_tab(self):
        """      
        @summary: Go to Access Times tab of a device group  
        @return: AdminBeamsDevicesPage: Access Times tab
        @author: Thanh Le
        """
        self._lnkAccessTimes.wait_until_clickable().click()
        from pages.suitable_tech.admin.advanced.beams.admin_beams_access_times_page import AdminBeamAccessTimesPage
        return AdminBeamAccessTimesPage(self._driver)
    
        
    def goto_members_tab(self):
        """      
        @summary: Go to Members tab of a device group 
        @return: AdminBeamsDevicesPage: Members tab
        @author: Thanh Le
        """
        self._lnkMembers.wait_until_clickable().click()
        from pages.suitable_tech.admin.advanced.beams.admin_beams_members_page import AdminBeamsMembersPage
        return AdminBeamsMembersPage(self._driver)
    
    
    def goto_reservations_tab(self):
        """      
        @summary: Go to Reservations tab of a device group   
        @return: AdminBeamsDevicesPage: Reservations tab
        @author: Thanh Le         
        """
        self._lnkReservations.wait_until_clickable().click()
        from pages.suitable_tech.admin.advanced.beams.admin_beams_reservations_page import AdminReservationsPage
        return AdminReservationsPage(self._driver)
    
    
    def select_device_group_in_dropdown_list(self, group_name):
        """      
        @summary: Select a device group in 'All Beams' drop-down list
        @param group_name: The name of device group would like to select
        @return: AdminBeamsDevicesPage
        @author: Thanh Le     
        """
        self._btnAllBeams.wait_until_clickable().click()
        self._ddlDeviceInAllBeams(group_name).wait_until_clickable().click()
        from pages.suitable_tech.admin.advanced.beams.admin_beams_devices_page import AdminBeamsDevicesPage
        return AdminBeamsDevicesPage(self._driver)
    
    
    def is_device_group_displayed_in_dropdown_list(self, group_name):
        """      
        @summary: Check if a device group is displayed in 'All Beams' drop-down list or not 
        @param group_name: name of divice group would like to check
        @return: True: The device group is displayed, False: The device group is not displayed
        @author: Thanh Le
        """
        self._btnAllBeams.wait_until_clickable().click()
        return self._ddlDeviceInAllBeams(group_name).is_displayed(3)     
    
    
    def click_create_device_group_in_dropdown_list(self):
        """      
        @summary: Click the existing device group in the 'All Beams' drop-down list
        @return: AdminBeamsDevicesPage
        @author: Thanh Le 
        """
        self._btnAllBeams.wait_until_clickable().click()
        self._lnkCreateDeviceGroup.wait_until_clickable().click()
        return self

