from selenium.webdriver.common.by import By

from common.application_constants import ApplicationConst
from core.webdriver.dropdownlist import DropdownList
from core.webdriver.element import Element
from core.webdriver.element_list import ElementList
from pages.suitable_tech.admin.advanced.beams.admin_beams_common_page import AdminBeamsCommonPage


class _AdminBeamsAllDevicePageLocator(object):
    _btnCreateDeviceGroup = (By.XPATH, "//div[@class='row secondary-nav']//button[@ng-click='createDeviceGroup()' and @type='button']")
    _iconIconView = (By.XPATH, "//span[@class='glyphicon glyphicon-th-large']/..")
    _iconListView = (By.XPATH, "//span[@class='glyphicon glyphicon-th-list']/..")
    _txtSearchBeams = (By.XPATH, "//input[@type='search']")
    
    @staticmethod
    def _lstDeviceGroup():
        return (By.XPATH, u"//h4[.=\"{}\"]/../..//div[@class='gallery-item ng-scope']".format(ApplicationConst.LBL_DEVICE_GROUPS))
    @staticmethod
    def _imgDeviceGroupIcon(device_group_name):
        return (By.XPATH,u"//div[@for='deviceGroup']//div[.=\"{}\"]/preceding-sibling::div//img".format(device_group_name))
    @staticmethod
    def _lnkDeviceGroup(device_group_name):
        return (By.XPATH, u"//div[@class='profile-title ng-binding' and .=\"{}\"]/..".format(device_group_name))
    @staticmethod
    def _lnkFirstDeviceGroup(device_group_name):
        return (By.XPATH, u"(//div[@for='deviceGroup'])[1]//div[contains(., \"{}\")]/..".format(device_group_name))
    @staticmethod
    def _lnkDevice(device_name):
        return (By.XPATH, u"//a[.=\"{}\" and @class='ng-binding ng-scope']".format(device_name))
    @staticmethod
    def _lnkFirstDevice(device_name):
        return (By.XPATH, u"(//div[@for='device'])[1]//a[.=\"{}\"]".format(device_name))
    
    
class AdminBeamsAllDevicesPage(AdminBeamsCommonPage):
    """
    @description: This is page object class for Beam All Devices page.
        This page will be opened after clicking Beams tab on Dashboard page.
        From this page, user can view all device or device group. 
        Please visit https://staging.suitabletech.com/manage/#/beams/all/ for more details.
    @page: Beam All Devices page
    @author: thanh.viet.le
    """

    """    Properties    """
    @property
    def _btnCreateDeviceGroup(self):
        return Element(self._driver, *_AdminBeamsAllDevicePageLocator._btnCreateDeviceGroup)
    @property
    def _cbxDeviceGroup(self):
        return Element(self._driver, *_AdminBeamsAllDevicePageLocator._cbxDeviceGroup)
    @property
    def _txtSearchBeams(self):
        return Element(self._driver, *_AdminBeamsAllDevicePageLocator._txtSearchBeams)
    @property
    def _ddlDeviceGroup(self):
        return DropdownList(self._driver, *_AdminBeamsAllDevicePageLocator._ddlDeviceGroup)
    @property
    def _lstDeviceGroup(self):
        return ElementList(self._driver, *_AdminBeamsAllDevicePageLocator._lstDeviceGroup())
    @property
    def _iconIconView(self):
        return Element(self._driver, *_AdminBeamsAllDevicePageLocator._iconIconView)
    @property
    def _iconListView(self):
        return Element(self._driver, *_AdminBeamsAllDevicePageLocator._iconListView)
    
    def _lnkDeviceGroup(self, device_group_name):
        return Element(self._driver, *_AdminBeamsAllDevicePageLocator._lnkDeviceGroup(device_group_name))
    def _lnkFirstDeviceGroup(self, device_group_name):
        return Element(self._driver, *_AdminBeamsAllDevicePageLocator._lnkFirstDeviceGroup(device_group_name))
    def _lnkDevice(self, device_name):
        return Element(self._driver, *_AdminBeamsAllDevicePageLocator._lnkDevice(device_name))
    def _lnkFirstDevice(self, device_name):
        return Element(self._driver, *_AdminBeamsAllDevicePageLocator._lnkFirstDevice(device_name))
    def _imgDeviceGroupIcon(self, device_group_name):
        return Element(self._driver, *_AdminBeamsAllDevicePageLocator._imgDeviceGroupIcon(device_group_name))
    
    """    Methods    """
    def __init__(self, driver):
        """      
        @summary: Constructor method    
        @param driver: Web driver
        @author: Thanh le      
        """
        AdminBeamsCommonPage.__init__(self, driver)
        self._lblHeader.wait_until_displayed()
        self.wait_for_loading()
        

    def create_new_device_group(self, device_group_name, devices=None, wait_for_completed=True):
        """      
        @summary: Create new device group         
        @param 
            - device_group_name: name of device group would like to create
            - devices: Beam device would like to add to device group
            - wait_for_completed: wait for the 'Create Device Group' form completely submitted
        @return: AdminBeamsDevicesPage
        @author: Thanh Le
        """
        self._btnCreateDeviceGroup.wait_until_clickable().click()
        from pages.suitable_tech.admin.dialogs.create_device_group import CreateDeviceGroupDialog
        CreateDeviceGroupDialog(self._driver).submit_device_group_info(device_group_name, devices, wait_for_completed)
        if(wait_for_completed):
            self.wait_untill_success_msg_disappeared()
        
        from pages.suitable_tech.admin.advanced.beams.admin_beams_devices_page import AdminBeamsDevicesPage
        return AdminBeamsDevicesPage(self._driver, wait_for_completed)
    
    
    def click_create_device_group_button(self):
        self._btnCreateDeviceGroup.wait_until_clickable().click()
        return self
    
    
    def is_device_group_existed(self, device_group_name, wait_time_out=None):
        """      
        @summary: Check if a device group is existed or not   
        @param 
            - device_group_name: name of device group would like to check
            - wait_time_out: time out to wait for device group is displayed
        @return: True: The device group is existed, False: The device group is not existed
        @author: Thanh Le
        """
        self.search(device_group_name)
        return self._lnkDeviceGroup(device_group_name).is_displayed(wait_time_out)
    
    
    def is_device_group_not_existed(self, device_group_name, wait_time_out=None):
        """      
        @summary: Check if a device group is not existed
        @param 
            - device_group_name: name of device group would like to check
            - wait_time_out: time out to wait for a device group is disappeared
        @return: True: The device group is not existed, False: The device group is existed
        @author: Thanh Le
        """
        self.search(device_group_name)
        return self._lnkDeviceGroup(device_group_name).is_disappeared(wait_time_out)
    
    
    def select_a_device(self, device_name):
        """      
        @summary: Select a device
        @param device_name: name of device would like to select
        @return: AdminBeamDetailPage
        @author: Thanh Le
        """
        self.search(device_name)
        self._lnkDevice(device_name).wait_until_clickable().click()
        from pages.suitable_tech.admin.advanced.beams.admin_beam_detail_page import AdminBeamDetailPage
        return AdminBeamDetailPage(self._driver, device_name)


    def select_device_group(self, device_group_name):
        """      
        @summary: Select a device group   
        @param device_group_name: name of device group would like to select 
        @return: AdminBeamsDevicesPage
        @author: Thanh Le
        """
        self.search(device_group_name)
        self._lnkFirstDeviceGroup(device_group_name).wait_until_clickable().jsclick()         
        from pages.suitable_tech.admin.advanced.beams.admin_beams_devices_page import AdminBeamsDevicesPage
        return AdminBeamsDevicesPage(self._driver)
    
    
    def get_device_groups_count(self):
        """      
        @summary: Count number of device groups     
        @return: Numbers of device groups
        @author: Thanh Le
        """
        return self._lstDeviceGroup.count()
    
    
    def get_device_group_icon_url(self, device_group_name):
        """      
        @summary: Get url of device group icon        
        @param device_group_name: name of device would like to get url of icon
        @return: url of device group icon
        @author: Thanh Le
        """
        self.search(device_group_name)
        image_element = self._imgDeviceGroupIcon(device_group_name)
        image_element.wait_until_displayed()
        
        return image_element.get_attribute("src")
    
    
    def remove_all_device_groups(self, keyword = "LGVN Group"):
        """      
        @summary: Remove all created device groups 
        @param keyword: criteria for removing device group
        @return: AdminBeamsDevicesPage
        @author: Thanh Le
        """
        self.search(keyword)
        self.wait_for_loading()
        lst_device_group_cards = self._lstDeviceGroup   
        count = lst_device_group_cards.count()
        while(count > 0):
            for i in range(0, count):
                web_elem = lst_device_group_cards.get_element_at(i)
                if web_elem and web_elem.text:
                    web_elem.click()
                    from pages.suitable_tech.admin.advanced.beams.admin_beams_devices_page import AdminBeamsDevicesPage
                    AdminBeamsDevicesPage(self._driver).goto_setting_tab().delete_device_group()
            
            self.search(keyword)
            self.wait_for_loading()
            lst_device_group_cards = self._lstDeviceGroup 
            count = lst_device_group_cards.count()
        
        return self
    
    
    def switch_to_icon_view(self):
        """      
        @summary: Switch to icon view mode
        @return: AdminBeamsDevicesPage
        @author: Thanh Le
        """
        self._iconIconView.wait_until_clickable().click()
        return self
    
    
    def switch_to_list_view(self):
        """      
        @summary: Switch to list view mode
        @return: AdminBeamsDevicesPage
        @author: Thanh Le
        """
        self._iconListView.wait_until_clickable().click()
        return self
    
    
    def search(self, search_item):
        """      
        @summary: Search for a Beam 
        @param search_item: keyword used for search
        @return: Beam device matched with searched keyword
        @author: Thanh Le
        """
        self._txtSearchBeams.slow_type(search_item)
        self.wait_for_loading()
        return self
    
    
    