from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from pages.suitable_tech.admin.advanced.beams.admin_beams_common_page import AdminBeamsCommonPage
from pages.suitable_tech.admin.dialogs.choose_users_dialog import ChooseUsersDialog
from pages.suitable_tech.admin.advanced.beams.admin_beams_all_devices_page import AdminBeamsAllDevicesPage
from common.application_constants import ApplicationConst
from time import sleep

class _AdminBeamsSettingsLocator(object):
    _btnSaveChanges = (By.XPATH, "//button[@ng-click='save()']")
    _btnAddAdministrators = (By.XPATH, "//button[@ng-click='addAdminUser()']")
    _btnDeleteGroup = (By.XPATH, "//button[@ng-click='delete()']")
    _chkBattery = (By.ID, "notify_admins_battery")
    _chkLastPilot = (By.ID, "notify_last_pilot")
    _chkAccessRequest = (By.ID, "notify_access_requests")
    _txtAccessRequest = (By.XPATH, "//input[@ng-model='group.access_request_link']")
    _txtDeviceGroupName = (By.XPATH, "//input[@ng-model='group.name']")
    _iconChangeImage = (By.XPATH, "//span[contains(@class, 'change-picture-icon')]")
    _lnkChangeImage = (By.XPATH, "//div[@class='change-picture-txt']/span")
    _icoProfileImage = (By.XPATH, "//div[@remove='removeProfileImage']//img")
    _btnAddSessionAnswerUser = (By.XPATH, "//button[@ng-click='addAnswerUser()']")
    _chkSuitableTechnologies = (By.XPATH, "//input[@ng-model='authMethods.s']")
    _chkGoogle = (By.XPATH, "//input[@ng-model='authMethods.g']")    

    @staticmethod
    def _lnkAdminName(value):
        return (By.XPATH, u"//span[@for='user']//a[.=\"{}\"]".format(value))
    @staticmethod
    def _lnkRemoveAdmin(displayed_name):
        return (By.XPATH, u"//a[.=\"{}\"]/../../../../../../div[@class='pull-right']//span[contains(@class, 'icon-remove')]".format(displayed_name))
    @staticmethod
    def _chkHasLabel(check_box_label):
        return (By.XPATH, u"//span[.=\"{}\"]/../../input[@type='checkbox']".format(check_box_label))
    @staticmethod
    def _lblSuccessMSGWithExpectedContent(message_content):
        return (By.XPATH, u"//div[@class='alert alert-success']/descendant::*[contains(text(),\"{}\")]".format(message_content))
    @staticmethod
    def _btnRemoveSessionAnswerUser(user_displayed_name):
        return (By.XPATH, u"//a[.=\"{}\"]/../../../../../..//a[@ng-click='removeAnswerUser($event, user)']".format(user_displayed_name))
    
    
class AdminBeamsSettingsPage(AdminBeamsCommonPage):
    """
    @description: This is page object class for Beam Settings page.
        This page will be opened after clicking Settings tab on Beams page.
        Please visit https://staging.suitabletech.com/manage/#/beams/787/settings/ for more details.
    @page: Beam Settings page
    @author: Thanh Le
    """


    """    Properties    """
    @property
    def _chkSuitableTechnologies(self):
        return Element(self._driver, *_AdminBeamsSettingsLocator._chkSuitableTechnologies)
    @property
    def _chkGoogle(self):
        return Element(self._driver, *_AdminBeamsSettingsLocator._chkGoogle)
    @property
    def _txtDeviceGroupName(self):
        return Element(self._driver, *_AdminBeamsSettingsLocator._txtDeviceGroupName)
    @property
    def _btnSaveChanges(self):
        return Element(self._driver, *_AdminBeamsSettingsLocator._btnSaveChanges)
    @property
    def _btnAddAdministrators(self):
        return Element(self._driver, *_AdminBeamsSettingsLocator._btnAddAdministrators)
    @property
    def _btnDeleteGroup(self):
        return Element(self._driver, *_AdminBeamsSettingsLocator._btnDeleteGroup)
    @property
    def _chkBattery(self):
        return Element(self._driver, *_AdminBeamsSettingsLocator._chkBattery)
    @property
    def _chkLastPilot(self):
        return Element(self._driver, *_AdminBeamsSettingsLocator._chkLastPilot)
    @property
    def _chkAccessRequest(self):
        return Element(self._driver, *_AdminBeamsSettingsLocator._chkAccessRequest)
    @property
    def _txtAccessRequest(self):
        return Element(self._driver, *_AdminBeamsSettingsLocator._txtAccessRequest)
    @property
    def _iconChangeImage(self):
        return Element(self._driver, *_AdminBeamsSettingsLocator._iconChangeImage)
    @property
    def _lnkChangeImage(self):
        return Element(self._driver, *_AdminBeamsSettingsLocator._lnkChangeImage)
    @property
    def _icoProfileImage(self):
        return Element(self._driver, *_AdminBeamsSettingsLocator._icoProfileImage)
    @property
    def _btnAddSessionAnswerUser(self):
        return Element(self._driver, *_AdminBeamsSettingsLocator._btnAddSessionAnswerUser)
    
    def _lnkAdminName(self, value):
        return Element(self._driver, *_AdminBeamsSettingsLocator._lnkAdminName(value))
    def _lnkRemoveAdmin(self, displayed_name):
        return Element(self._driver, *_AdminBeamsSettingsLocator._lnkRemoveAdmin(displayed_name))
    def _chkHasLabel(self, check_box_label):
        """
        we use this method to find and interact with all checkboxes in this page instead of capture all checkboxes Xpath
        """
        return Element(self._driver, *_AdminBeamsSettingsLocator._chkHasLabel(check_box_label))
    def _lblSuccessMSGWithExpectedContent(self, message_content):
        return Element(self._driver, *_AdminBeamsSettingsLocator._lblSuccessMSGWithExpectedContent(message_content))
    def _btnRemoveSessionAnswerUser(self, user_displayed_name):
        return Element(self._driver, *_AdminBeamsSettingsLocator._btnRemoveSessionAnswerUser(user_displayed_name))
    
    
    """    Methods    """
    def __init__(self, driver):      
        """      
        @summary: Constructor method
        @param driver: Web driver 
        @author: Thanh Le     
        """  
        AdminBeamsCommonPage.__init__(self, driver)
        self._lblHeader.wait_until_displayed()
        self.wait_for_loading(2)
        
    
    def is_administrator_existed(self, admin_name):
        """      
        @summary: Check if an administrator is existed or not 
        @param admin_name: name of administrator would like to check
        @return: True: the admin is existed, False: the admin is not existed
        @author: Thanh Le   
        """
        return self._lnkAdminName(admin_name).is_displayed()

    
    def save_changes(self, wait_for_completed=True):
        """      
        @summary: Saving change on a device group settings   
        @param wait_for_completed: time to wait for saving completely
        @return: AdminBeamsSettingsPage
        @author: Thanh Le
        """
        self._btnSaveChanges.click()
        if (wait_for_completed):
            self.wait_untill_success_msg_disappeared()
        return self
    
    
    def change_device_group_name(self, value):
        """      
        @summary: Change name of a device group     
        @param value: new name would like to change for device group
        @return: AdminBeamsSettingsPage
        @author: Thanh Le
        """
        self._txtDeviceGroupName.type(value)
        return self
    
    
    def add_administrator(self, user, wait_for_completed = True):
        """      
        @summary: Add an administrator for a device group      
        @param wait_untill_success_msg_disappeared: dismiss the successful message
        @return: AdminBeamsSettingsPage
        @author: Thanh Le  
        """
        self.wait_for_loading(2)
        self._btnAddAdministrators.wait_until_clickable().click()
        ChooseUsersDialog(self._driver).choose_user(user.email_address)
        self.wait_for_loading(2)
        self.save_changes(wait_for_completed)
        return self
    
    
    def get_link_access_request(self):
        """      
        @summary: Get the access request link
        @return: AdminBeamsSettingsPage
        @author: Khoi Ngo    
        """
        return self._txtAccessRequest.get_attribute("value")
    
    
    def is_admin_removable(self, displayed_name):
        """      
        @summary: Check if an admin can be removed or not     
        @param displayed_name: name of admin would like to check 
        @return: True: The Remove button displayed, False: The Remove button is not displayed
        @author: Thanh Le      
        """
        return self._lnkRemoveAdmin(displayed_name).is_displayed(5)
    
    
    def is_device_group_removable(self):
        """      
        @summary: Check if the Remove button in device group is enable or not   
        @return: True: the Remove button is enable, False: the Remove button is disable
        @author: Thanh Le   
        """
        return self._btnDeleteGroup.is_enabled()
    
    
    def delete_device_group(self, wait_for_completed=True):
        """      
        @summary: Delete a device group
        @param wait_for_completed: time wait for delete completely 
        @return: AdminBeamsSettingsPage
        @author: Thanh Le    
        """
        self._btnDeleteGroup.wait_until_clickable().click()
        self._driver.handle_dialog(True)
        
        if(wait_for_completed):
            self.wait_untill_success_msg_disappeared()
            
        return AdminBeamsAllDevicesPage(self._driver)
    
    
    def remove_admin_user(self, diplayed_name, wait_for_completed=True):
        """      
        @summary: Remove admin user by full name
        @param diplayed_name: full name of an admin would like to remove 
        @return: AdminBeamsSettingsPage
        @author: Thanh Le    
        """
        self._driver.scroll_down_to_bottom()
        self._lnkRemoveAdmin(diplayed_name).wait_until_clickable().click()
        self.save_changes(wait_for_completed)
        return self
    
    
    def set_device_group_icon(self, image_path, left, top, width, height):
        """      
        @summary: Set icon image for a device group      
        @param 
            - image_path: path to new image 
            - left, top, width, height: parameter to crop an image
        @return: AdminBeamsSettingsPage
        @author: Thanh Le   
        """
        self._iconChangeImage.wait_until_clickable().click()
        
        from pages.suitable_tech.admin.dialogs.upload_image_dialog import UploadImageDialog
        dialog = UploadImageDialog(self._driver).choose_file(image_path)
        dialog.set_crop_tracker_dimension(left, top, width, height)
        dialog.submit()
        
        sleep(20)
        
        return self
       
    
    def _toogle_check_box(self, check_box_label, select=True):
        """      
        @summary: Toggle a check box in Settings page by label
        @param 
            - check_box_label: label of which check box would like to toogle
            - select: check or un-check the checkbox
        @return: AdminBeamsSettingsPage
        @author: Thanh Le
        """
        if(select):
            self._chkHasLabel(check_box_label).check()
        else:
            self._chkHasLabel(check_box_label).uncheck()

    
    def toggle_request_access_notification(self, select=True):
        """      
        @summary: Toggle the 'Notify administrators when someone requests access to this group' checkbox
        @param select: check or uncheck
        @return: AdminBeamsSettingsPage
        @author: Thanh Le   
        """
        self._toogle_check_box(ApplicationConst.CHK_NOTIFY_REQUEST_ACCESS, select)
        return self       
    
    
    def toggle_left_off_charger_notification_for_admin(self, select=True):
        """      
        @summary: Toggle 'Notify administrators if a Beam is left off the charger' checkbox
        @param select: check or uncheck the checkbox
        @return: AdminBeamsSettingsPage
        @author: Thanh Le    
        """
        self._toogle_check_box(ApplicationConst.CHK_NOTIFY_ADMIN_IF_BEAM_LEFT_OFF_CHARGER, select)
        return self
    
    
    def toggle_left_off_charger_notification_for_last_pilot(self, select=True):
        """      
        @summary: Toggle 'Notify the last pilot if a Beam is left off the charger' checkbox     
        @param select: check or uncheck the checkbox
        @return: AdminBeamsSettingsPage
        @author: Thanh Le 
        """
        self._toogle_check_box(ApplicationConst.CHK_NOTIFY_PILOT_IF_BEAM_LEFT_OFF_CHARGER, select)
        return self  
    
    
    def toggle_left_off_all_authentication_methods(self, select=True):
        """      
        @summary: This action is used to toggle left off all authentication methods 
        @param <select>: boolean value to decide toogle or not
        @author: Thanh Le    
        """
        self._toogle_check_box(ApplicationConst.LBL_BEAM_ALL_AUTHENTICATION_METHODS, select)
        return self
    
    
    def toggle_left_off_suitable_technologies_methods(self, select=True):
        """      
        @summary: This action is used to toggle left of suitable technology method
        @param <select>: boolean value to decide toggle or not
        @author: Thanh Le     
        """
        if(select):
            self._chkSuitableTechnologies.check()
        else:
            self._chkSuitableTechnologies.uncheck()
        return self
    
    
    def toggle_left_off_google_methods(self, select=True):
        """      
        @summary: This action is used to toggle left off google methods
        @param <select>: boolean value to decide toggle or not
        @author: Thanh Le    
        """
        if(select):
            self._chkGoogle.check()
        else:
            self._chkGoogle.uncheck()            
        return self 
    

    def add_user_to_session_answer(self, user, wait_for_completed=True):
        """      
        @summary: Add a user to session answer
        @param 
            - user: user would like to add to session answer
            - wait_for_completed: timeout to wait for complete adding
        @return: AdminBeamsSettingsPage
        @author: Thanh Le
        """
        self._btnAddSessionAnswerUser.click()
        choose_users_dialog = ChooseUsersDialog(self._driver)
        choose_users_dialog.choose_user(user.email_address)
        self._btnSaveChanges.click()
        
        if(wait_for_completed):
            self.wait_untill_success_msg_disappeared()
            
        return self
        
    
    def remove_user_from_session_answer(self, user, wait_for_completed=True):
        """      
        @summary: Remove a user from session answer
        @param 
            - user: user would like to remove from session answer
            - wait_for_completed: timeout to wait for complete adding
        @return: AdminBeamsSettingsPage
        @author: Thanh Le
        """
        self._driver.scroll_down_to_bottom()
        self._btnRemoveSessionAnswerUser(user.get_displayed_name()).click()
        self.save_changes(wait_for_completed)
        return self


    def is_page_displayed(self, time_out=None):
        """      
        @summary: Check if a page is displayed or not
        @param time_out: time to wait for display
        @return: AdminBeamsSettingsPage
        @author: Thanh Le
        """
        return self._lblHeader.is_displayed(time_out)
        
