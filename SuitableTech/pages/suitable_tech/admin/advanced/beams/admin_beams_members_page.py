from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from pages.suitable_tech.admin.advanced.beams.admin_beams_common_page import AdminBeamsCommonPage
from pages.suitable_tech.admin.dialogs.choose_users_dialog import ChooseUsersDialog
from common.application_constants import ApplicationConst

class _AdminBeamsMembersPageLocator(object):
    _btnAddUsers = (By.XPATH, "//button[@ng-click='addMember()']")
    _btnAddUsersFn = (By.XPATH, "//button[@ng-click='addContactsFn()']")
    _txtSearch = (By.XPATH, "//input[@type='search']")
    
    @staticmethod
    def _iconUsersLoading():
        return (By.XPATH, u"//h4/span[.=\"{}\"]/../../..//div[@class='loading-indicator']/span".format(ApplicationConst.LBL_BEAMS_MEMBERS_USERS))
    @staticmethod
    def _btnRemoveGroup(group_name):
        return (By.XPATH, u"//div[@class='profile-title ng-binding' and normalize-space(.)=\"{}\"]/preceding-sibling::div//button".format(group_name)) 
    @staticmethod
    def _btnRemoveUser(displayed_name):
        return (By.XPATH, u"//div[normalize-space(.)=\"{}\"]/preceding-sibling::div[@class='profile-image-container']//button//span[.=\"{}\"]".format(displayed_name, ApplicationConst.LBL_BEAMS_MEMBERS_REMOVE_USER))
    @staticmethod
    def _pnlUser(email_address):
        return (By.XPATH, u"//h4[.=\"{}\"]/../following-sibling::div//div[normalize-space(.)=\"{}\"]".format(ApplicationConst.LBL_BEAMS_MEMBERS_USERS, email_address))
    @staticmethod
    def _pnlUserGroup(group_name):
        return (By.XPATH, u"//h4[.=\"{}\"]/../following-sibling::div//div[normalize-space(.)=\"{}\"]".format(ApplicationConst.LBL_BEAMS_MEMBERS_USER_GROUPS, group_name))
    
    
class AdminBeamsMembersPage(AdminBeamsCommonPage):
    """
    @description: This is page object class for Beam Members page.
        This page will be opened after clicking Members tab on Beams page.
        Please visit https://staging.suitabletech.com/manage/#/beams/787/members/ for more details.
    @page: Beam Members page
    @author: Thanh Le
    """


    """    Properties    """
    @property
    def _btnAddUsers(self):
        return Element(self._driver, *_AdminBeamsMembersPageLocator._btnAddUsers)
    @property
    def _btnAddUsersFn(self):
        return Element(self._driver, *_AdminBeamsMembersPageLocator._btnAddUsersFn)
    @property
    def _btnCreateDeviceGroup(self):
        return Element(self._driver, *_AdminBeamsMembersPageLocator._btnCreateDeviceGroup)
    @property
    def _btnCreateUserGroup(self):
        return Element(self._driver, *_AdminBeamsMembersPageLocator._btnCreateUserGroup)
    @property
    def _txtSearch(self):
        return Element(self._driver, *_AdminBeamsMembersPageLocator._txtSearch)
    @property
    def _iconUsersLoading(self):
        return Element(self._driver, *_AdminBeamsMembersPageLocator._iconUsersLoading())
    
    def _pnlUser(self, email_address):
        return Element(self._driver, *_AdminBeamsMembersPageLocator._pnlUser(email_address))
    def _pnlUserGroup(self, group_name):
        return Element(self._driver, *_AdminBeamsMembersPageLocator._pnlUserGroup(group_name))
    def _btnRemoveGroup(self, group_name):
        return Element(self._driver, *_AdminBeamsMembersPageLocator._btnRemoveGroup(group_name))
    def _btnRemoveUser(self, user):
        return Element(self._driver, *_AdminBeamsMembersPageLocator._btnRemoveUser(user))
    
    
    """    Methods      """    
    def __init__(self, driver):        
        """      
        @summary: Constructor method        
        @param driver: Web driver
        @author: Thanh Le  
        """
        AdminBeamsCommonPage.__init__(self, driver)
        self._lblHeader.wait_until_displayed()
        self._wait_for_loading(10)
        
    
    def add_user_to_device_group(self, user):
        """      
        @summary: Add a user to a device group  
        @param user: user would like to add to device group
        @return: AdminBeamsMembersPage
        @author: Thanh Le       
        """
        self._btnAddUsers.wait_until_clickable().click()
        ChooseUsersDialog(self._driver).choose_user(user.email_address)
        self._wait_for_loading(5)
        return self    
    
    
    def invite_new_user_and_add_to_device_group(self, user):
        """      
        @summary: Invite a new user and add this user to a device group 
        @param user: user would like invite and add to device group
        @return: AdminBeamsMembersPage
        @author: Thanh Le    
        """
        self._btnAddUsers.wait_until_clickable().click()
        choose_user_dialog = ChooseUsersDialog(self._driver)
        choose_user_dialog.invite_new_user_and_choose(user)
        return self
    
    
    def add_user_group_to_device_group(self, group_name):
        """      
        @summary: Add a user group to a device group
        @param group_name: user group would like to add to device group
        @return: AdminBeamsMembersPage
        @author: Thanh Le     
        """
        self._btnAddUsers.wait_until_clickable().click()
        choose_user_dialog = ChooseUsersDialog(self._driver)
        choose_user_dialog.choose_user(group_name)
        return self
    

    def remove_user(self, displayed_name):
        """      
        @summary: Remove a user by displayed name
        @param displayed_name: full name of user would like to be removed 
        @return: AdminBeamsMembersPage
        @author: Thanh Le
        """
        self.click_remove_user(displayed_name)
        self.accept_remove_user_dialog()
        self._wait_for_user_disappeared(displayed_name)
        return self    
    
    
    def click_remove_user(self, displayed_name):      
        """      
        @summary: Click Remove button on user icon by displayed name
        @param displayed_name: full name of user would like to be removed 
        @return: AdminBeamsMembersPage
        @author: Thanh Le   
        """  
        self.search_user(displayed_name)
        self._btnRemoveUser(displayed_name).wait_until_clickable().click()
        return self
    
    
    def accept_remove_user_dialog(self):
        """      
        @summary: Click Ok button on remove user confirmation pop-up
        @return: AdminBeamsMembersPage
        @author: Thanh Le      
        """
        self._driver.handle_dialog(True)
        return self
    
    
    def reject_remove_user_dialog(self):
        """      
        @summary: Click Ok button on remove user confirmation pop-up       
        @return: AdminBeamsMembersPage
        @author: Thanh Le      
        """
        self._driver.handle_dialog(False)
        return self
    
    
    def get_toast_msg(self, close_dialog = True):
        """      
        @summary: Get message 'Are you sure you want to remove this member
                 from the '<device group's name>' device group?' 
        @param close_dialog: close the confirm message or not 
        @return: AdminBeamsMembersPage
        @author: Thanh Le   
        """
        return self._driver.get_dialog_message(close_dialog)
        
    
    def is_user_disappeared(self, search_value, check_value=None, wait_time_out=None):
        """      
        @summary: Check if a user disappears or not    
        @param 
            - search_value: user would like to be checked
            - check_value: value used to compare
            - wait_time_out: time to wait for user disappears
        @return: True: the user disappears, False: the user still appears
        @author: Thanh Le
        """
        self.search_user(search_value)
        if(check_value==None):
            return self._pnlUser(search_value).is_disappeared(wait_time_out)
        else:
            return self._pnlUser(check_value).is_disappeared(wait_time_out)
        
    
    def is_user_existed(self, search_value, check_value=None, wait_time_out=None):
        """      
        @summary: Check if a user is existed or not  
        @param 
            - search_value: user would like to be checked
            - check_value: value used to compare
            - wait_time_out: time to wait for user displays
        @return: True: the user is existed, False: the user is not existed
        @author: Thanh Le 
        """
        self.search_user(search_value)
        if(check_value==None):
            return self._pnlUser(search_value).is_displayed(wait_time_out)
        else:
            return self._pnlUser(check_value).is_displayed(wait_time_out)
    
    
    def is_user_group_removable(self, group_name, wait_time_out=None):
        """      
        @summary: Check if a user can be removed or not (The red Remove button displays on user group)   
        @param 
            - group_name: the name of user group would like to check
            - wait_time_out: time to wait for the Remove button displays
        @return: True: The Remove button displays, False: The Remove button is not displayed
        @author: Thanh Le 
        """
        self.search_user(group_name)
        return self._btnRemoveGroup(group_name).is_displayed(wait_time_out)
    
    
    def is_user_group_disappeared(self, group_name, wait_time_out=None):
        """      
        @summary: Check if a user group disappears or not  
        @param
            - group_name: name of user group would like to check
            - wait_time_out: time to wait for a user group disappears
        @return: True: the user group disappears, False: the user group appears
        @author: Thanh Le     
        """
        self.search_user(group_name)
        return self._pnlUserGroup(group_name).is_disappeared(wait_time_out)
    
    
    def is_user_group_existed(self, group_name, wait_time_out=None):
        """      
        @summary: Check if a user group is existed or not    
        @param 
            - group_name: name of user group would like to check
            - wait_time_out: time out to wait for a user group is displayed
        @return: True: the user group is existed, False: the user group is not existed
        @author: Thanh Le 
        """
        self.search_user(group_name)
        return self._pnlUserGroup(group_name).is_displayed(wait_time_out)
    

    def remove_user_group(self, group_name):
        """      
        @summary: Remove a user group by group name 
        @param group_name: name of user group would like to remove
        @return: AdminBeamsMembersPage
        @author: Thanh Le
        """
        self.search_user(group_name)
        self._btnRemoveGroup(group_name).click()
        self._driver.handle_dialog(True)
        return self
    
    
    def search_user(self, search_value):
        """      
        @summary: Search for a user based on a search criteria    
        @param search_value: keyword used for searching
        @return: AdminBeamsMembersPage: user matched with searched value displays
        @author: Thanh Le
        """
        self._txtSearch.type(search_value)
        self._wait_for_loading(5)
        return self
    
    
    def _wait_for_loading(self, timeout=None):
        """      
        @summary: Wait for user icon loading completely
        @param timeout: time to wait for user icon loads completely
        @return: AdminBeamsMembersPage
        @author: Thanh Le  
        """
        self._iconUsersLoading.wait_until_displayed(timeout)
        self._iconUsersLoading.wait_until_disappeared(timeout)
     
        
    def _wait_for_user_disappeared(self, displayed_name, timeout=10):  
        """      
        @summary: Wait for a user disappears
        @param user_value: user would like to wait for disappear
        @return: True: the user disappears, False: the user still appears
        @author: Thanh Le     
        """      
        self._pnlUser(displayed_name).is_disappeared(timeout)

