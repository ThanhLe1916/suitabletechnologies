from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from pages.suitable_tech.admin.advanced.beams.admin_beams_common_page import AdminBeamsCommonPage
from pages.suitable_tech.admin.dialogs.edit_device_dialog import EditDeviceDialog
from core.utilities.utilities import Utilities
from time import sleep
from common.application_constants import ApplicationConst
from core.webdriver.element_list import ElementList


class _AdminBeamDetailPageLocator(object):
    _btnEdit = (By.XPATH, "//button[@type='button' and @ng-click='edit()']")
    _btnBeamAdvance = (By.XPATH, "//button[@class='btn btn-default' and @ng-click='showAdvanced()']")
    _lblAlertMessage = (By.XPATH, "//div[@class='alert alert-success']//span[@ng-bind-html='message.content']")
    _btnRemoveDeviceIcon = (By.XPATH, "//button[@ng-click='removeImage()']")
    _lnkChangeDeviceIcon = (By.XPATH, "//span[@class='change-picture-icon fa fa-pencil']/..")
    _imgDeviceIcon = (By.XPATH, "//div[@class='profile-image-editable']//img")
    
    
    @staticmethod
    def _lblBeamLabel():
        return (By.XPATH, u"//span[.=\"{}\"]/../following-sibling::dd[1]//span[@ng-show='device.tagString()']".format(ApplicationConst.LBL_LABEL_PROPERTY))
    @staticmethod
    def _lblBeamLocation():
        return (By.XPATH, u"//span[.=\"{}\"]/../following-sibling::dd[1]".format(ApplicationConst.LBL_LOCATION_PROPERTY))
    @staticmethod
    def _lblBeamGroup():
        return (By.XPATH, u"//span[.=\"{}\"]/../following-sibling::dd[1]//span".format(ApplicationConst.LBL_GROUP_PROPERTY))
    @staticmethod
    def _lblBeamName(device_name):
        return (By.XPATH, u"//h3[.=\"{}\"]".format(device_name))


class AdminBeamDetailPage(AdminBeamsCommonPage):
    """
    @description: This is page object class for Beam Details page.
        This page will be opened after clicking Beam panel on Beams page.
        Please visit https://staging.suitabletech.com/manage/#/beams/787/beam/beam135909030/ for more details.
    @page: Beam Detail page
    @author: Thanh Le
    """


    """    Properties    """
    @property
    def _btnEdit(self):
        return Element(self._driver, *_AdminBeamDetailPageLocator._btnEdit)
    @property
    def _lblBeamLabel(self):
        return ElementList(self._driver, *_AdminBeamDetailPageLocator._lblBeamLabel())
    @property
    def _lblBeamLocation(self):
        return Element(self._driver, *_AdminBeamDetailPageLocator._lblBeamLocation())
    @property
    def _btnBeamAdvance(self):
        return Element(self._driver, *_AdminBeamDetailPageLocator._btnBeamAdvance)
    def _lblBeamName(self, beam_name):
        return Element(self._driver, *_AdminBeamDetailPageLocator._lblBeamName(beam_name))
    @property
    def _lblAlertMessage(self):
        return Element(self._driver, *_AdminBeamDetailPageLocator._lblAlertMessage)
    @property
    def _lblBeamGroup(self):
        return Element(self._driver, *_AdminBeamDetailPageLocator._lblBeamGroup())
    @property
    def _btnRemoveDeviceIcon(self):
        return Element(self._driver, *_AdminBeamDetailPageLocator._btnRemoveDeviceIcon)
    @property
    def _lnkChangeDeviceIcon(self):
        return Element(self._driver, *_AdminBeamDetailPageLocator._lnkChangeDeviceIcon)
    @property
    def _imgDeviceIcon(self):
        return Element(self._driver, *_AdminBeamDetailPageLocator._imgDeviceIcon)
    
    
    """    Methods    """
    def __init__(self, driver, beam_name_to_wait_for=None):
        """      
        @summary: Constructor method    
        @param driver: Web Driver 
        @author: Thanh Le
        """     
        AdminBeamsCommonPage.__init__(self, driver)
        if(beam_name_to_wait_for!=None):
            self._lblBeamName(beam_name_to_wait_for).wait_until_displayed()
        
        
    def is_beam_name_displayed(self, beam_name):
        """      
        @summary: Check if a Beam displays or not     
        @param beam_name: name of beam device
        @return: True: The Beam displays, False: The Beam does not display
        @author: Thanh Le
        """
        return self._lblBeamName(beam_name).is_displayed() 
    
    
    def get_beam_labels(self):
        """      
        @summary: Get all labels of a Beam device from Beam detail page
        @return: String of labels
        @author: Duy Nguyen
        """
        returnList = []
        label_tag_list = self._lblBeamLabel.get_all_elements()
        for tag in label_tag_list:
            returnList.append(tag.text)
        return returnList       
    
    
    def is_any_label_existed(self):
        lst_beam_label = self.get_beam_labels()
        is_not_existed = True
        for e in lst_beam_label:
            if e != 'None':
                if len(e) >= 1:
                    is_not_existed = False
                    break
        return is_not_existed
    
    
    def is_beam_label_existed(self, label):
        """      
        @summary: Check if a label of Beam is existed or not     
        @param label: The label which would like to be checked
        @return: True: The label is existed, False: The label is not existed
        @author: Thanh Le
        """
        return (label in self.get_beam_labels())
        
    
    def is_beam_able_to_unlink(self):
        """      
        @summary: Check if the 'Unlink This Device' button displays or not       
        @return: True: The 'Unlink This Device' button displays, False: The 'Unlink This Device' button does not display
        @author: Duy Nguyen
        """
        self._btnEdit.wait_until_clickable().click()               
        dialog = EditDeviceDialog(self._driver)
        can_unlink = dialog.is_button_unlink_displayed()
        dialog.cancel()
        return can_unlink
    
    
    def get_beam_label_tag_list(self):
        """      
        @summary: Get all Beam labels from the Edit Device form 
        @return: label_tag_list: list of all Beam labels
        @author: Duy Nguyen
        """
        self._btnEdit.wait_until_clickable().click()            
        dialog = EditDeviceDialog(self._driver)
        label_tag_list = dialog.get_all_beam_labels()
        dialog.cancel()
        return label_tag_list
    
    
    def get_beam_location(self):
        """      
        @summary: Get location of Beam device  
        @return: String of Beam location
        @author: Duy Nguyen
        """
        return self._lblBeamLocation.text
    

    def set_beam_name(self, device_name):
        """      
        @summary: Set new name for a Beam  
        @param device_name: The new name would like to reset 
        @return: AdminBeamDetailPage: The Beam detail page displays after doing save from Edit Device form
        @author: Duy Nguyen
        """
        self._btnEdit.click()
        dlg = EditDeviceDialog(self._driver)
        dlg.set_beam_name(device_name)
        dlg.submit()
        
        self.wait_untill_success_msg_disappeared()
        return self

    
    def set_beam_label(self, beam_label,wait_for_completed=True):
        """      
        @summary: Set new label for a Beam
        @param beam_label: The new label would like to set for a Beam 
        @return: AdminBeamDetailPage: The Beam detail page displays after doing save from Edit Device form
        @author: Duy Nguyen
        """
        self._btnEdit.wait_until_clickable().click()
        dlg = EditDeviceDialog(self._driver)
        dlg.set_beam_label(beam_label)
        dlg.submit()
        if (wait_for_completed == True):
            self.wait_untill_success_msg_disappeared()
        return self
    
    
    def remove_all_beam_labels(self):
        """      
        @summary: Remove all beam labels of a Beam         
        @return: AdminBeamDetailPage: The Beam detail page displays after doing save from Edit Device form
        @author: Duy Nguyen
        """
        self._btnEdit.click()
        dlg = EditDeviceDialog(self._driver)
        dlg.remove_all_beam_labels()
        dlg.submit()
        return self
    
    
    def set_beam_label_tag_list(self, beam_label_tag_list):
        """      
        @summary: Set new labels for a Beam after removing all existing ones      
        @param beam_label_tag_list: The new label would like to set for Beam
        @return: AdminBeamDetailPage: The Beam detail page displays after doing save from Edit Device form
        @author: Duy Nguyen
        """
        self._btnEdit.wait_until_clickable().click()
        dlg = EditDeviceDialog(self._driver)
        dlg.remove_all_beam_labels()
        for label in beam_label_tag_list:
            dlg.add_beam_label(label)
        
        dlg.submit()
        self.wait_untill_success_msg_disappeared()
        return self
    
    
    def set_beam_location(self, beam_location):
        """      
        @summary: Set new location for a Beam 
        @param beam_location: The new location would like to set for a Beam 
        @return: AdminBeamDetailPage: The Beam detail page displays after doing save from Edit Device form
        @author: Duy Nguyen
        """
        self._btnEdit.click()
        dlg = EditDeviceDialog(self._driver)
        dlg.set_beam_location(beam_location)
        dlg.submit()
        self.wait_untill_success_msg_disappeared()
        return self 
    
    
    def set_beam_group(self, device_group, wait_for_completed=True):
        """      
        @summary: Select new device group for a Beam     
        @param device_group: The new device group would like to set for a Beam 
        @return: AdminBeamDetailPage: The Beam detail page displays after doing save from Edit Device form
        @author: Duy Nguyen
        """
        self._btnEdit.wait_until_clickable().click()
        dlg = EditDeviceDialog(self._driver)
        dlg._wait_for_dialog_appeared()
        dlg.set_beam_group(device_group)
        dlg.submit(wait_for_completed)
        if(wait_for_completed):
            self.wait_untill_success_msg_disappeared()
        return self 
    
    
    def open_edit_dialog(self):
        """      
        @summary: Method to open 'Edit Device' form
        @return: EditDeviceDialog: This is 'Edit Device' form
        @author: Duy Nguyen
        """
        self._btnEdit.wait_until_clickable().click()
        return EditDeviceDialog(self._driver)
    
    
    def goto_beam_advance_setting(self):
        """      
        @summary: Open 'Advanced Settings' of a Beam        
        @return: DeviceAdvanceSettingsDialog: This is 'Advance Settings' form
        @author: Duy Nguyen
        """
        self._btnBeamAdvance.wait_until_clickable().click()
        from pages.suitable_tech.admin.dialogs.device_advance_settings_dialog import DeviceAdvanceSettingsDialog
        return DeviceAdvanceSettingsDialog(self._driver)
  
  
    def get_alert_message(self):
        """      
        @summary: Get text contain of the message 'The device was saved successfully'         
        @return: The text 'The device was saved successfully'
        @author: Tham Nguyen
        """    
        return self._lblAlertMessage.wait_until_displayed(10).text
    
    
    def is_alert_message_displayed(self, wait_time_out=2):
        """      
        @summary: Check if the 'The device was saved successfully' is displayed or not
        @param wait_time_out: time out to wait for the message
        @return: True: The message is displayed, False: The message is not displayed
        @author: Tham Nguyen
        """
        return self._lblAlertMessage.is_displayed(wait_time_out)
    
    
    def get_beam_group(self):
        """      
        @summary: Get group name of a Beam
        @return: String of group name of Beam
        @author: Tham Nguyen
        """
        return self._lblBeamGroup.text
    
    
    def get_device_icon_link(self):
        """      
        @summary: Get the icon link of a Beam device    
        @return: String of file url
        @author: Quang Tran
        """        
        file_url = self._imgDeviceIcon.get_attribute("src")
        return Utilities.correct_link(file_url)
    
        
    def open_upload_image_dialog(self): 
        """      
        @summary: Open 'Upload New Profile Image" form     
        @return: UploadImageDialog: This is 'Upload New Profile Image" form.
        @author: Quang Tran 
        """        
        self._lnkChangeDeviceIcon.wait_until_clickable().click()
        from pages.suitable_tech.admin.dialogs.upload_image_dialog import UploadImageDialog
        return UploadImageDialog(self._driver)
    
            
    def change_device_icon(self, image_path, left, top, width, height, wait_for_completed = True):
        """      
        @summary: Change an icon of a Beam device
        @param  
            - image_path: local image file path
            - left | top | width | height : the boundary of crop-tracker element  
        @return: AdminBeamDetailPage: This page displays after changing a Beam device icon.
        @author: Quang Tran
        """
        dialog = self.open_upload_image_dialog()
        dialog.choose_file(image_path)
        dialog.set_crop_tracker_dimension(left, top, width, height)
        dialog.submit()
        
        sleep(3)
             
        #wait for updating new icon
        if wait_for_completed:
            ico_url = self.get_device_icon_link()
            ext = ico_url.rsplit('.', 1)
            tried = 0
            while tried < 10:
                if len(ext) > 1 and ext[1] != 'svg':
                    sleep(2)
                    return self
                tried += 1
                sleep(2)
                ico_url = self.get_device_icon_link()
                ext = ico_url.rsplit('.', 1)
        
        return self
    
    
    def remove_device_icon(self):
        """      
        @summary: Remove an icon of a Beam device
        @return: AdminBeamDetailPage: This page displays after removing a Beam device icon.
        @author: Quang Tran   
        """
        btn_remove = self._btnRemoveDeviceIcon
        if btn_remove.is_displayed(5):
            btn_remove.click()
            sleep(2)
            self._driver.handle_dialog(True)
            
            #wait for removing
            sleep(3)
            ico_url = self.get_device_icon_link()
            ext = ico_url.rsplit('.', 1)
            tried = 0
            while tried < 10:
                if len(ext) > 1 and ext[1] == 'svg':
                    sleep(2)
                    return self
                tried += 1
                sleep(2)
                ico_url = self.get_device_icon_link()
                ext = ico_url.rsplit('.', 1)
                
        return self

