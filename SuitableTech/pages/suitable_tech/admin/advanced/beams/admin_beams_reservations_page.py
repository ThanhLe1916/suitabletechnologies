from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from pages.suitable_tech.admin.advanced.beams.admin_beams_common_page import AdminBeamsCommonPage
from core.webdriver.element_list import ElementList
from datetime import datetime, timedelta
from common.constant import Constant


class _AdminReservationsPageLocator(object):
    _btnReserveABeam = (By.XPATH, "//div[contains(@class, 'device-group-header')]//button[@type='button' and @ng-click='addReservation()']")
    _lblSuccessMessage = (By.XPATH, "//div[@class='alert alert-success']//span[@ng-bind-html='message.content']")
    _lblErrorMessage = (By.XPATH, "//div[contains(@class, 'alert-dismissable')]//span[@class='ng-binding ng-scope']")
    _txtSearchReservation = (By.XPATH, "//input[@type='search']")
    _btnClearSearchReservation = (By.XPATH, "//span[contains(@class, 'clear-input')]")
    _lblNoReservations = (By.XPATH, "//h5//span")
    _iconLoading = (By.XPATH, "//div[@class='loading-indicator']")
    
    @staticmethod
    def _lnkReservationRecord(value):
        return (By.XPATH, u"//a[.=\"{}\"]/ancestor::tr[contains(@class, 'reservation')]".format(value))


class AdminReservationsPage(AdminBeamsCommonPage):
    """
    @description: This is page object class for Beam Reservations page.
        This page will be opened after clicking Reservations tab on Beams page.
        Please visit https://staging.suitabletech.com/manage/#/beams/787/reservations/ for more details.
    @page: Beam Reservations page
    @author: thanh.viet.le
    """


    """    Properties    """    
    @property
    def _btnReserveABeam(self):
        return Element(self._driver, *_AdminReservationsPageLocator._btnReserveABeam)
    @property
    def _lblErrorMessage(self):
        return Element(self._driver, *_AdminReservationsPageLocator._lblErrorMessage)
    @property
    def _txtSearchReservation(self):
        return Element(self._driver, *_AdminReservationsPageLocator._txtSearchReservation)
    @property
    def _btnClearSearchReservation(self):
        return Element(self._driver, *_AdminReservationsPageLocator._btnClearSearchReservation)
    @property
    def _lblNoReservations(self):
        return Element(self._driver, *_AdminReservationsPageLocator._lblNoReservations)
    @property
    def _lblSuccessMessage(self):
        return Element(self._driver, *_AdminReservationsPageLocator._lblSuccessMessage)
    @property
    def _iconLoading(self):
        return Element(self._driver, *_AdminReservationsPageLocator._iconLoading)
    
    def _lnkReservationRecord(self, value):
        return Element(self._driver, *_AdminReservationsPageLocator._lnkReservationRecord(value))
    
    
    """    Methods    """
    def __init__(self, driver):     
        """      
        @summary: Constructor method        
        @param driver: Web driver
        @author: Thanh Le  
        """
        AdminBeamsCommonPage.__init__(self, driver)
        self._lblHeader.wait_until_displayed()
        
    
    def create_reservation(self, device_name=None, user=None, start_time=None, end_time=None):
        """      
        @summary: Create a reservation 
        @parameter: <device_name>: device name
                    user: user name
                    start_time: starting time
                    end_time: ending time
        @author: Thanh le     
        """
        record_starting_date = start_time
        record_period_in_minutes = 10 # default value is 10 minutes
        if(start_time != None and end_time != None):
            record_period_in_minutes = (end_time - start_time).minutes
            
        if(start_time==None and end_time==None):
            available_record = self.find_available_device(device_name, record_starting_date, record_period_in_minutes)
            if(available_record):
                start_time = available_record.get_starting_date_time()
                end_time = available_record.get_ending_date_time()
        
        self._btnReserveABeam.click()
        from pages.suitable_tech.admin.dialogs.reserve_beam_dialog import ReserveABeamDialog        
        reserveBeam = ReserveABeamDialog(self._driver)
        reserveBeam.reserve_a_beam(device_name, user, start_time, end_time)
            
        return self
    
    
    def get_reserve_success_message(self):
        """      
        @summary: This action is used to get reserve success message
        @return: text of message
        @author: Thanh le
        """
        return self._lblSuccessMessage.text
    
    
    def is_reservation_displayed(self, value):
        """      
        @summary: Check if reservation is displayed
        @param True: the reservation is displayed
                False: the reservation is not displayed
        @author: Thanh le     
        """
        return self._lnkReservationRecord(value).is_displayed()
    
    
    def get_no_reservations_message(self):
        """      
        @summary: This action is used to get no reservation detail text
        @return: the message says no reservation text content
        @author:  Thanh Le       
        """
        return self._lblNoReservations.text
    
    
    def delete_reservation(self, value):
        """      
        @summary: This action is used to delete reservation value 
        @param <value>: reservation value
        @return: AdminReservationsPage
        @author: Thanh Le
        """
        self.search_for_reservation(value)
        self._lnkReservationRecord(value).click()
        self._btnDeleteReservation.click()
        self._driver.handle_dialog(True)
        self._btnDeleteReservation.wait_until_disappeared()
        self._btnClearSearchReservation.click()
        self._lnkReservationRecord(value).wait_until_disappeared()
        return self
    
    
    def get_reserve_error_message(self):
        """      
        @summary: This action is used to get reserve error message  
        @return: the reserve error message text content
        @author: Thanh le   
        """
        return self._lblErrorMessage.text
    
    
    def click_reservation_device_link(self, device_name):
        """      
        @summary: This action is used to click reservation device link  
        @param <device name>: device name 
        @return: AdminBeamDetailPage
        @author: Thanh le
        """
        self._get_reservation_record(device_name).click()
        from pages.suitable_tech.admin.advanced.beams.admin_beam_detail_page import AdminBeamDetailPage
        return AdminBeamDetailPage(self._driver, device_name)
    
        
    def click_reservation_user_link(self, user):
        """      
        @summary: this action is used to click reservation user link
        @param <user>: user name 
        @author: Thanh le   
        """
        self._get_reservation_record(user).click()
        from pages.suitable_tech.admin.advanced.users.admin_user_detail_page import AdminUserDetailPage
        return AdminUserDetailPage(self._driver)
    
        
    def search_for_reservation(self, value):      
        """      
        @summary: This action is used for search reservation
        @param <value>: reservation value 
        @author: Thanh Le
        """
        self._txtSearchReservation.type(value)
        return self


    def find_available_device(self, device_name=None, reserved_day = None, reserved_time_in_minutes=60):
        """      
        @summary: This action is used to find available device   
        @parameter: <device_name)>: device name 
                    reserved_day: reservation date
                    reserved_time_in_minutes: reservation time
        @author:    Thanh le 
        """
        if device_name == None:
            device_name=Constant.BeamPlusMock4Name
            
        reservation_list = self._get_all_reservation_records(device_name, reserved_day)
        available_reservation = None
        if(reservation_list):
            
            number_of_reserv = len(reservation_list)
            for i in range(0, number_of_reserv):
                last_ending_time = None
                next_starting_time = None
                
                record = reservation_list[i]
                last_ending_time = record.get_ending_date_time()
                if( (i + 1) < number_of_reserv) :
                    next_starting_time = reservation_list[i+1].get_starting_date_time()
                
                if( not next_starting_time):
                    available_reservation = _Reservation(\
                            device_name, \
                            last_ending_time, \
                            last_ending_time + timedelta(minutes = reserved_time_in_minutes))
                    return available_reservation
                else:
                    expected_ending_time = last_ending_time  + timedelta(minutes = reserved_time_in_minutes)
                    if(expected_ending_time < next_starting_time):
                        available_reservation = _Reservation(\
                                device_name, \
                                last_ending_time, \
                                expected_ending_time)
                        return available_reservation
                    else:
                        continue
        else:
            selected_datetime = datetime.now().replace(minute=0, second=0, microsecond=0 )
            selected_datetime = selected_datetime + timedelta(hours = 1)
            available_reservation = _Reservation(\
                    device_name, \
                    selected_datetime, \
                    selected_datetime + timedelta(minutes = reserved_time_in_minutes))
            return available_reservation    
    
    
    def _get_all_reservation_records(self, device_name=None, reserved_day = None):
        """      
        @summary: This action is used to get all reservation record    
        @parameter: device_name: device name
                    reserved_day: reservation date
        @author: Thanh le
        """
        reservations = []
        
        if(device_name):
            self._txtSearchReservation.type(device_name)
            self._iconLoading.wait_until_disappeared()
            
        day_locator = "//div[@ng-repeat='dateGroup in dateGroups']//h4"
        if(reserved_day):
            day_label = reserved_day.strftime("%A %b %dth")
            day_locator = u"//div[@ng-repeat='dateGroup in dateGroups']//h4[.=\"{}\"]".format(day_label)
            
        day_list = ElementList(self._driver, By.XPATH, day_locator)
        day_count = day_list.count(15)
        
        day_format = "%A %b %dth %Y" # Friday Jul 15th 2016
        current_year = datetime.now().year
        if(day_count > 0):
            all_day_label = day_list.get_all_elements()
            for day_label in all_day_label:
                try:
                    day = datetime.strptime(\
                            ("{} {}".format(day_label.text, current_year)),\
                            day_format)
                    records = self._get_all_reservation_records_on_day(day)
                    if(records):
                        reservations.extend(records)
                except ValueError:
                    pass # ignore
                
        return reservations
    
    
    def _get_all_reservation_records_on_day(self, day):
        """      
        @summary: This action is used to get all reservation record on day 
        @parameter: day: The specific date
        @author: Thanh le
        """
        reservations = []
        day_label = day.strftime("%A %b %d") + "th"
        time_locator = u"//h4[.=\"{}\"]/following-sibling::table[1]//td[@class='reservation-time ng-binding']".format(day_label)
        time_list = ElementList(self._driver, By.XPATH, time_locator)
        time_count = time_list.count()
        all_time_items = time_list.get_all_elements()
        
        device_locator = u"//h4[.=\"{}\"]/following-sibling::table[1]//td[@class='reservation-details']/div[1]/div[@class='media-body media-middle']//a".format(day_label)
        device_list = ElementList(self._driver, By.XPATH, device_locator)
        device_count = device_list.count()
        all_device_items = device_list.get_all_elements()
        
        if(time_count == device_count):
            for i in range(0, time_count):
                device_name = all_device_items[i].text
                r_time = all_time_items[i].text.replace("WIB","").strip()
                starting_time = r_time.split("\n")[0] # 11:00 AM
                ending_time = r_time.split("\n")[1]
                
                starting_date_time = self._convert_reserved_time_value_to_date_time(day, starting_time)
                ending_date_time = self._convert_reserved_time_value_to_date_time(day, ending_time)
                if(starting_date_time and ending_date_time):
                    reservations.append(_Reservation(device_name, starting_date_time, ending_date_time))
                    
        return reservations
    
    
    def _convert_reserved_time_value_to_date_time(self, current_day, time_value_to_be_converted):
        """      
        @summary: This action is used to convert reserved time value to date time 
        @parameter: current_day: current date
                    time_value_to_be_converted: time to convert
        @author: Thanh Le
        """
        result_day = None    
        full_day_format = "%b %d %I:%M %p" # Jul 18th 11:00 AM
        time_value_to_be_converted = time_value_to_be_converted.replace("th","").strip()
        try:
            result_day = datetime.strptime(time_value_to_be_converted.replace("th",""), full_day_format)
        except ValueError:
            # time_value_to_be_converted is in format '%I:%M %p'  (11:00 AM)
            day_label = datetime.strftime(current_day, "%b %d ") + time_value_to_be_converted
            try:
                result_day = datetime.strptime(day_label,full_day_format)
            except ValueError:
                pass #ignore
        
        if(result_day):
            result_day = result_day.replace(year = datetime.now().year)
        return result_day
    
      
class _Reservation(object):
    def __init__(self, device_name, starting_date_time, ending_date_time):
        """      
        @summary: Constructor method        
        @parameter: device_name: device name
                    starting_date_time: starting date
                    ending_date_time: ending date
        @author: Thanh Le  
        """
        self._device_name = device_name
        self._starting_date_time = starting_date_time
        self._ending_date_time = ending_date_time
    
        
    def get_device_name(self): 
        """      
        @summary: This action is used to get device name     
        @return: string of device name 
        @author: Thanh Le  
        """
        return self._device_name
    
    
    def get_starting_date_time(self):
        """      
        @summary: This action is used to get starting date time  
        @return: date time   
        @author: Thanh Le  
        """
        return self._starting_date_time
    
    
    def get_ending_date_time(self):   
        """      
        @summary: This action is used to get ending date time
        @return: date time        
        @author: Thanh Le  
        """
        return self._ending_date_time
    
        
