from pages.basepage import BasePage
from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from core.webdriver.dropdownlist import DropdownList
from core.utilities.gmail_utility import GmailUtility
from datetime import datetime
from pages.suitable_tech.user.password_setup_page import PasswordSetupPage
# from common.application_constants import ApplicationConst

        
class _AdminTemplateLocator(object):
    """    Top bar left section    """
    _lnkDashboard = (By.XPATH, "//a[@href='#/dashboard/']")
    _lnkBeams = (By.XPATH, "//a[@href='#/beams/all/']")
    _lnkUsers = (By.XPATH, "//a[@href='#/contacts/']")
    _lnkActivity = (By.XPATH, "//a[@href='#/activity/']")
    _lblSuccessMessage = (By.XPATH, "//div[@class='alert alert-success']//span[@ng-bind-html='message.content']")
    _lblDismissMessage = (By.XPATH, "//div[@class='alert alert-danger alert-dismissible']")
    _btnSetting = (By.XPATH, "//ul[@class='nav navbar-nav navbar-right']//a[@href='#/settings/']")
    _iconLoading = (By.XPATH, "//div[@class='loading-indicator']/span")
    """    Top bar right section    """
    _ddlAdminMenu = (By.XPATH, "//span[@class='glyphicon glyphicon-user']/../..")
    _lnkOrganizationSettings = (By.XPATH, "//a[@ui-sref='organizationSettings']")
    _ddlOrganization = (By.XPATH, "//ul[@class='nav navbar-nav navbar-right']/li[1]")
    """    Chat box    """
    #//div[@id='habla_topbar_div']
    #//button[@class='olark-launch-button']
    _lblChatboxHeader = (By.XPATH, "//div[@id='habla_topbar_div']")   
    
    
class AdminTemplatePage(BasePage):    
    """
    @description: This is page object class that contains all controls and methods shared across all Admin pages.
        This class is ONLY for inheriting.
    @page: Admin Template Page
    @author: Thanh Le
    """
    
    
    """    Properties    """    
    @property
    def _iconLoading(self):
        return Element(self._driver, *_AdminTemplateLocator._iconLoading)
    @property
    def _btnSetting(self):
        return Element(self._driver, *_AdminTemplateLocator._btnSetting)
    @property
    def _lblSuccessMessage(self):
        return Element(self._driver, *_AdminTemplateLocator._lblSuccessMessage)
    @property
    def _lblDismissMessage(self):
        return Element(self._driver, *_AdminTemplateLocator._lblDismissMessage)
    @property
    def _lnkBeams(self):
        return Element(self._driver, *_AdminTemplateLocator._lnkBeams)
    @property
    def _lnkDashboard(self):
        return Element(self._driver, *_AdminTemplateLocator._lnkDashboard)
    @property
    def _lnkUsers(self):
        return Element(self._driver, *_AdminTemplateLocator._lnkUsers)
    @property
    def _lnkOrganizationSettings(self):
        return Element(self._driver, *_AdminTemplateLocator._lnkOrganizationSettings)
    @property
    def _ddlOrganization(self):
        return DropdownList(self._driver, *_AdminTemplateLocator._ddlOrganization)
    @property
    def _ddlAdminMenu(self):
        return DropdownList(self._driver, *_AdminTemplateLocator._ddlAdminMenu)
    @property
    def _hrefYourAccount(self):
        return "#/account/settings/"
    @property
    def _hrefSignOut(self):
        return "/accounts/logout"
    
    
    
    """    Top bar right section    """
    @property
    def _lnkBeamMenuItem(self):
        return Element(self._driver, *_AdminTemplateLocator._lnkBeamMenuItem)
    
    """    Chat box    """
    @property
    def _lblChatboxHeader(self):
        return DropdownList(self._driver, *_AdminTemplateLocator._lblChatboxHeader)
    
    
    """    Methods      """
    def __init__(self, driver):
        """      
        @summary: Constructor method     
        @param driver: web driver
        @author: Thanh Le
        """
        BasePage.__init__(self, driver)
        self._lblChatboxHeader.wait_until_displayed()
        

    def is_success_msg_displayed(self, wait_time=None):
        """      
        @summary: Check if the successful message is displayed or not    
        @return: True: the successful message is displayed
        @author: Thanh Le
        """
        return self._lblSuccessMessage.is_displayed(wait_time)
    
    
    def get_msg_success(self):
        """      
        @summary: Get content of successful message 
        @return: message 'Device group settings were saved successfully'
        @author: Thanh Le
        """
        return self._lblSuccessMessage.text
    
    
    def is_dismiss_msg_displayed(self):
        """      
        @summary: Check if a toast message is displayed or not
        @return: True: The toast message is displayed, False: the toast message is not displayed
        @author: Thanh Le
        """
        return self._lblDismissMessage.is_displayed()
    
    
    def get_msg_dismiss(self):
        """      
        @summary: Get toast message content         
        @return: The content of toast message
        @author: Thanh Le
        """
        return self._lblDismissMessage.text
    
    
    def goto_org_setting_page(self):
        """      
        @summary: Go to setting page of an organization     
        @return: Setting page of org
        @author: Thanh Le
        """
        self._btnSetting.click()
        from pages.suitable_tech.admin.advanced.setting.admin_organization_setting_page import OrganizatioSettingsPage
        return OrganizatioSettingsPage(self._driver)
    
    
    def wait_untill_success_msg_disappeared(self, timeout=10):
        """      
        @summary: Wait untill the success message is disappeared     
        @return: AdminTemplatePage
        @author: Thanh Le
        """
        self._lblSuccessMessage.wait_until_displayed(timeout)
        self._lblSuccessMessage.wait_until_disappeared(timeout)
        return self
    
    
    def goto_beams_tab(self):
        """      
        @summary: Go to Beams tab         
        @return: All Beams device page
        @author: Thanh Le
        """
        self._lnkBeams.click()
        from pages.suitable_tech.admin.advanced.beams.admin_beams_all_devices_page import AdminBeamsAllDevicesPage
        return AdminBeamsAllDevicesPage(self._driver)
    
    
    def get_organization_name(self):
        """      
        @summary: Get name of an organization         
        @return: name of an org
        @author: Thanh Le
        """
        return self._ddlOrganization.get_text()
    
    
    def switch_to_advanced_organization(self, organization_name):      
        """      
        @summary: Switch to advance organization 
        @param organization_name: name of org would like to switch to
        @return: AdminTemplatePage
        @author: Thanh Le
        """  
        self._ddlOrganization.select_by_partial_text(organization_name)
        self._lblChatboxHeader.wait_until_disappeared()
        from pages.suitable_tech.admin.advanced.dashboard.admin_dashboard_page import AdminDashboardPage
        return AdminDashboardPage(self._driver)


    def switch_to_simplified_organization(self, organization_name):
        """      
        @summary: Switch to simplified organization     
        @param organization_name: name of org would like to switch to
        @return: AdminTemplatePage
        @author: Thanh Le
        """
        self._ddlOrganization.select_by_partial_text(organization_name)
        from pages.suitable_tech.admin.simplified.dashboard.simplified_dashboard_page import SimplifiedDashboardPage
        return SimplifiedDashboardPage(self._driver)
    
    
    def goto_dashboard_tab(self):
        """      
        @summary: Go to dashboard tab        
        @return: Dashboard page
        @author: Thanh Le
        """
        self._lnkDashboard.click()        
        from pages.suitable_tech.admin.advanced.dashboard.admin_dashboard_page import AdminDashboardPage
        return AdminDashboardPage(self._driver)
    
    
    def goto_users_tab(self):
        """      
        @summary: Go to Users tab     
        @return: Users page
        @author: Thanh Le
        """
        self._lnkUsers.click()
        from pages.suitable_tech.admin.advanced.users.admin_users_page import AdminUsersPage
        return AdminUsersPage(self._driver)
    
    
    def goto_members_tab_of_a_device_group(self, device_group_name):
        """      
        @summary: Go to Members tab of a device group     
        @param device_group_name: name of device group would like to go to Members tab
        @return: Members tab of device group page
        @author: Thanh Le 
        """
        beamsAllDevicePage = self.goto_beams_tab().select_device_group(device_group_name)
        return beamsAllDevicePage.goto_members_tab()
    
    
    def goto_reservations_tab_of_a_device_group(self, device_group_name):
        """      
        @summary: Go to Reservations tab of a device group  
        @param device_group_name: name of device group would like go to Reservations tab
        @return: Reservations tab of a device group page
        @author: Thanh Le
        """
        beamsAllDevicePage = self.goto_beams_tab().select_device_group(device_group_name)
        return beamsAllDevicePage.goto_reservations_tab()
    
    
    def goto_settings_tab_of_a_device_group(self, device_group_name):
        """      
        @summary: Go to Settings tab of a device group   
        @param device_group_name: name of device group would like to go to Settings tab
        @return: Settings tab of device group
        @author: Thanh Le
        """
        return self.goto_beams_tab().select_device_group(device_group_name).goto_setting_tab()
    
    
    def goto_access_times_tab_of_a_device_group(self, device_group_name):
        """      
        @summary: Go to Access Times tab of a device group   
        @param device_group_name: name of device group would like to go to Access Times tab
        @return: Access Times tab of device group
        @author: Thanh Le
        """
        return self.goto_beams_tab().select_device_group(device_group_name).goto_accesstimes_tab()
    
     
    def logout(self):
        """      
        @summary: Log out from Suitable Tech     
        @return: Home page object
        @author: Thanh Le
        @created_date: August 05, 2016
        """
        self._driver.scroll_up_to_top()
        self._ddlAdminMenu.select_by_href(self._hrefSignOut)
        from pages.suitable_tech.user.home_page import HomePage
        return HomePage(self._driver, True)
    
    
    def goto_your_account(self):
        """      
        @summary: Go to Your Account page  
        @return: Your account page
        @author: Thanh Le
        """
        self._ddlAdminMenu.select_by_href(self._hrefYourAccount)      
        from pages.suitable_tech.accountsettings.account_settings_page import AccountSettingsPage
        return AccountSettingsPage(self._driver)
    
    
    def logout_and_login_again(self, email_address, password, keepMeLoggedIn=False):
        """      
        @summary: Log out from Suitable Tech and then login again       
        @param email_address: Email address of admin user that used to login
        @param password: Password of admin user that used to login
        @param keepMeLoggedIn: Boolean value True/False. 
        @return: AdminHomePage page object
        @author: Thanh Le
        @created_date: August 05, 2016
        """
        return self.logout().goto_login_page().login(email_address, password, keepMeLoggedIn)
    
    
    def logout_and_login_again_as_unwatched_video_user(self, email_address, password, keepMeLoggedIn=False):
        """      
        @summary: Log out from Suitable Tech and then login again       
        @param email_address: Email address of admin user that used to login
        @param password: Password of admin user that used to login
        @param keepMeLoggedIn: Boolean value True/False. 
        @return: AdminHomePage page object
        @author: Thanh Le
        @created_date: August 05, 2016
        """
        return self.logout().goto_login_page().login_as_unwatched_video_user(email_address, password, keepMeLoggedIn)
    
    
    def logout_and_activate_new_user(self, email_address):
        """      
        @summary: Log out from Suitabletech and activate a user  
        @param email_address: email of user would like to activate
        @return: Password Setup page
        @author: Thanh Le
        """
        self.logout()
        
        email_activation_link = GmailUtility.get_email_activation_link(\
            receiver = email_address, sent_day = datetime.now())
        
        if email_activation_link==None:
            raise Exception("Cannot get activation link")
            
        return PasswordSetupPage(self._driver, email_activation_link)
      
    
    def get_selected_organization(self):
        """      
        @summary: Get the organization item in drop-down list
        @return: Selected org
        @author: Thanh Le
        """
        return self._ddlOrganization.get_selected_item()
    
    
    def is_organization_dropdown_displayed(self, timeout=None):
        """      
        @summary: Check if the org is displayed in org drop-down list or not      
        @return: True: the org is displayed
                False: the org is not displayed
        @author: Thanh le
        """
        return self._ddlOrganization.is_displayed(timeout)
    
    
    def wait_for_loading(self, timeout=5):
        """      
        @summary: wait for icon loading completely        
        @param timeout: time to wait for loading completely
        @author: Thanh Le
        """
        if(timeout==None):
            timeout = 0
        self._iconLoading.wait_until_displayed(timeout)
        self._iconLoading.wait_until_disappeared(timeout)

