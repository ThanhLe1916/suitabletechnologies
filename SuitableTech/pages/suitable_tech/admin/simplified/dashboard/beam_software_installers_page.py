from pages.suitable_tech.admin.advanced.admin_template_page import AdminTemplatePage
from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from common.constant import Browser
from common.helper import Helper
from core.utilities.utilities import Utilities
from core.utilities.os_utility import OSUtility

class _BeamSoftwareInstallersLocator(object):
    _btnDownload = (By.XPATH, "//a[@id='download-btn']")
    _lblHeader = (By.XPATH, "//section[@class='masthead info']//h2")
    
    
class BeamSoftwareInstallersPage(AdminTemplatePage):
    """
    @description: This is page object class for Beam Software Installer Page.
    @page:  Beam Software Installer Page
    @author: Thanh Le
    """
    
    
    """    Properties    """
    @property
    def _btnDownload(self):
        return Element(self._driver, *_BeamSoftwareInstallersLocator._btnDownload)
    @property
    def _lblHeader(self):
        return Element(self._driver, *_BeamSoftwareInstallersLocator._lblHeader)
    
    """    Methods    """
    def __init__(self, driver): 
        """      
        @summary: Constructor method    
        @param driver: Web Driver
        @author: Thanh Le
        """          
        AdminTemplatePage.__init__(self, driver)
        self._lblHeader.wait_until_displayed()
        
        
    def download_suggested_installer(self, download_wait_time_out=500):
        """
        @summary: This action is used to download suggested installer
        @return: BeamSoftwareInstallersPage
        @author: Thanh Le
        @return: itself
        """
            
        self._btnDownload.wait_until_clickable().jsclick()
        
        downloaded_file_path = Helper.beam_software_installer_path(self._driver.driverSetting.platform)
            
        if(self._driver.driverSetting.browser_name == Browser.IE):
            win = OSUtility()
            
            win.wait_for_notification_bar_displayed()
            win.handle_download_file_IE(downloaded_file_path)
        elif(self._driver.driverSetting.browser_name == Browser.Edge):
            win = OSUtility()
            win.wait_for_notification_bar_displayed()
            win.handle_download_file_Edge(downloaded_file_path)
        elif(self._driver.driverSetting.browser_name == Browser.Firefox):
            win = OSUtility()
            win.handle_download_file_FF(downloaded_file_path)
            
        elif(self._driver.driverSetting.browser_name == Browser.Chrome):
            Utilities.wait_for_file_is_downloaded(downloaded_file_path)
            
        elif(self._driver.driverSetting.browser_name == Browser.Safari):
            #TODO : need to implement this source code on MAC machine
            pass
        
 
        return self
      


