from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from pages.suitable_tech.admin.advanced.admin_template_page import AdminTemplatePage


class _SimplifiedNormalUserHomeLocator(object):
    _lnkDownloadBeamDesktopSoftware = (By.XPATH, "//a[@href='/installers/']/span")


class SimplifiedNormalUserHomePage(AdminTemplatePage):
    """
    @description: This is page object class for Simplified Normal User Home Page.
    This page appear when logging with Simpliied user
    @page:  Simplified Normal User Home Page
    @author: Thanh Le
    """
    
    
    """    Properties    """
    @property
    def _lnkDownloadBeamDesktopSoftware(self):
        return Element(self._driver, *_SimplifiedNormalUserHomeLocator._lnkDownloadBeamDesktopSoftware)
    
    
    """    Methods    """
    def __init__(self, driver):
        """      
        @summary: Constructor method    
        @param driver: Web Driver
        @author: Thanh Le
        """                
        AdminTemplatePage.__init__(self, driver)
        
        
    def goto_download_the_beam_desktop_software(self):
        """
        @summary: This action use to go to download beam desktop software page   
        @author: Thanh Le
        @return BeamSoftwareInstallersPage page object
        """
        self._lnkDownloadBeamDesktopSoftware.click()
        from pages.suitable_tech.admin.simplified.dashboard.beam_software_installers_page import BeamSoftwareInstallersPage
        return BeamSoftwareInstallersPage(self._driver)