from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from pages.suitable_tech.admin.advanced.admin_template_page import AdminTemplatePage
from core.webdriver.element_list import ElementList


class SimplifiedDashboardPageLocator(object):
    _lblHeader = (By.XPATH, "//h2[@class='heading ng-binding']")
    _lstDevices = (By.XPATH, "//div[@ng-repeat='device in devices']//a[@class='btn btn-block btn-primary hidden-xs ng-scope']")
     
    @staticmethod
    def _btnManage(beam_name):
        return (By.XPATH, u"//h3[.=\"{}\"]/../preceding-sibling::div//a[@class='btn btn-block btn-primary hidden-xs ng-scope']".format(beam_name))    
    @staticmethod
    def _btnManagebyId(value):
        return (By.XPATH, u"//a[@class='btn btn-block btn-primary hidden-xs ng-scope' and @href='#/devices/{}/']".format(value))
    
    

class SimplifiedDashboardPage(AdminTemplatePage):
    """
    @description: This is page object class for Simplified DashBoard Page.
    This page appear when logging by Simplified User and go to Dashboard page
    @page:  Simplified DashBoard Page
    @author: Thanh Le
    """
    
    
    """    Properties    """
    def _btnManage(self, beam_name):
        return Element(self._driver, *SimplifiedDashboardPageLocator._btnManage(beam_name))
    def _btnManagebyId(self, value):
        return Element(self._driver, *SimplifiedDashboardPageLocator._btnManagebyId(value))
    
    def _lstDevices(self):
        return ElementList(self._driver, *SimplifiedDashboardPageLocator._lstDevices)
    
    @property
    def _lblHeader(self):
        return Element(self._driver, *SimplifiedDashboardPageLocator._lblHeader)
    
    
    """    Methods    """
    def __init__(self, driver):    
        """      
        @summary: Constructor method    
        @param driver: Web Driver
        @author: Thanh Le
        """        
        AdminTemplatePage.__init__(self, driver)
        self._lblHeader.wait_until_displayed()
    
    
    def can_manage_device(self, beam_name):
        return self._btnManage(beam_name).is_displayed()
    
    
    def get_number_of_devices_displayed(self):
        return self._lstDevices().count(10)    
        
    def goto_manage_beam_page(self, beam_name):
        """
        @summary: This action is used to go to manage beam page   
        @author: Thanh Le
        @parameter: <beam_name>: beam name string
        @return SimplifiedBeamDetailPage page object
        """
        self._btnManage(beam_name).wait_until_clickable().click()
        from pages.suitable_tech.admin.simplified.beams.simplified_beam_detail_page import SimplifiedBeamDetailPage
        return SimplifiedBeamDetailPage(self._driver)
    
    
    def goto_manage_beam_page_by_id(self, beam_id):
        """      
        @summary: Goto manage beam page by Beam id         
        @param beam_id: The id of a Beam on App
        @return: SimplifiedBeamDetailPage
        @author: Thanh Le
        @created_date: 8/17/2016
        """
        self._btnManagebyId(beam_id).wait_until_clickable().click()
        from pages.suitable_tech.admin.simplified.beams.simplified_beam_detail_page import SimplifiedBeamDetailPage
        return SimplifiedBeamDetailPage(self._driver)
    