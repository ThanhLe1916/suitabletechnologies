from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from pages.suitable_tech.admin.dialogs.edit_device_dialog import EditDeviceDialog
from pages.suitable_tech.admin.advanced.beams.admin_beams_common_page import AdminBeamsCommonPage
from core.utilities.utilities import Utilities
from time import sleep
from common.application_constants import ApplicationConst
from core.utilities.gmail_utility import GmailUtility
from pages.suitable_tech.user.password_setup_page import PasswordSetupPage
from datetime import datetime


class _SimplifiedBeamDetailPageLocator(object):
    _btnEdit = (By.XPATH, "//button[@type='button' and @ng-click='edit()']")
    _btnBeamAdvanced = (By.XPATH, "//button[@class='btn btn-default' and @ng-click='showAdvanced()']")
    _lblBeamLabel = (By.XPATH, u"//span[.=\"{}\"]/../following-sibling::dd[1]".format(ApplicationConst.LBL_LABEL_PROPERTY))
    _lblBeamLocation = (By.XPATH, u"//span[.=\"{}\"]/../following-sibling::dd[1]".format(ApplicationConst.LBL_LOCATION_PROPERTY))
    _lblBeamName = (By.XPATH, u"//span[.=\"{}\"]/../following-sibling::dd[1]".format(ApplicationConst.LBL_LOCATION_NAME))
    _txtInvitedEmail = (By.XPATH, "//input[@ng-model='userToAdd']")
    _btnAddUser = (By.XPATH, "//button[@ng-click='addSelectedUser()']")
    _lblAlertMessage = (By.XPATH, "//div[@class='alert alert-success']//span[@ng-bind-html='message.content']")
    _iconLoading = (By.XPATH, "//div[@class='loading-indicator']")
    _lnkChangeBeamIcon = (By.XPATH, "//span[@class='change-picture-icon fa fa-pencil']/..")
    _imgBeamIcon = (By.XPATH, "//div[@class='profile-image-container ng-isolate-scope editable']//img")
    _btnRemoveBeamIcon = (By.XPATH, "//button[@ng-click='removeImage()']")
    
    @staticmethod
    def _btnRemoveUser(value):
        return (By.XPATH, u"//a[.=\"{}\"]/../following-sibling::td//div".format(value))
    @staticmethod
    def _chkCanManage(value):
        return (By.XPATH, u"//a[contains(.,\"{}\")]//parent::td//following-sibling::td//input[@ng-model='user._canManageGroup']".format(value))
    @staticmethod
    def _btnRemovePerson(value):
        return (By.XPATH, u"//a[contains(.,\"{}\")]//parent::td//following-sibling::td//button[@class='btn btn-danger btn-sm']".format(value))
    @staticmethod
    def _lnkAddedUser(value):
        return (By.XPATH, u"//table[@class='table table-hover']//a[contains(.,\"{}\")]".format(value))


class SimplifiedBeamDetailPage(AdminBeamsCommonPage):
    """
    @description: This is page object class for Simplified Beam Detail Page.
    This page appear when logging with Simplified user and select to manage a device
    @page:  Simplified Beam Detail Page
    @author: Thanh Le
    """
    
    
    """    Properties    """
    @property
    def _lblBeamName(self):
        return Element(self._driver, *_SimplifiedBeamDetailPageLocator._lblBeamName)
    @property
    def _lblAlertMessage(self):
        return Element(self._driver, *_SimplifiedBeamDetailPageLocator._lblAlertMessage)
    @property
    def _btnAddUser(self):
        return Element(self._driver, *_SimplifiedBeamDetailPageLocator._btnAddUser)
    @property
    def _txtInvitedEmail(self):
        return Element(self._driver, *_SimplifiedBeamDetailPageLocator._txtInvitedEmail)    
    @property
    def _btnEdit(self):
        return Element(self._driver, *_SimplifiedBeamDetailPageLocator._btnEdit)
    @property
    def _btnBeamAdvanced(self):
        return Element(self._driver, *_SimplifiedBeamDetailPageLocator._btnBeamAdvanced)
    @property
    def _lblBeamLabel(self):
        return Element(self._driver, *_SimplifiedBeamDetailPageLocator._lblBeamLabel)
    @property
    def _lblBeamLocation(self):
        return Element(self._driver, *_SimplifiedBeamDetailPageLocator._lblBeamLocation)
    @property
    def _iconLoading(self):
        return Element(self._driver, *_SimplifiedBeamDetailPageLocator._iconLoading)
    @property
    def _lnkChangeBeamIcon(self):
        return Element(self._driver, *_SimplifiedBeamDetailPageLocator._lnkChangeBeamIcon)    
    @property
    def _imgBeamIcon(self):
        return Element(self._driver, *_SimplifiedBeamDetailPageLocator._imgBeamIcon) 
    @property
    def _btnRemoveBeamIcon(self):
        return Element(self._driver, *_SimplifiedBeamDetailPageLocator._btnRemoveBeamIcon) 
    
    def _chkCanManage(self, value):
        return Element(self._driver, *_SimplifiedBeamDetailPageLocator._chkCanManage(value))
    def _lnkAddedUser(self, value):
        return Element(self._driver, *_SimplifiedBeamDetailPageLocator._lnkAddedUser(value))
    def _btnRemoveUser(self, value):
        return Element(self._driver, *_SimplifiedBeamDetailPageLocator._btnRemoveUser(value))
    def _btnRemovePerson(self, value):
        return Element(self._driver, *_SimplifiedBeamDetailPageLocator._btnRemovePerson(value))
    
    
    """    Methods    """
    def __init__(self, driver):       
        """      
        @summary: Constructor method    
        @param driver: Web Driver
        @author: Thanh Le
        """   
        AdminBeamsCommonPage.__init__(self, driver)
        self.wait_for_loading()
    
        
    def set_beam_name(self, new_beam_name):
        """      
        @summary: Used to set beam name         
        @param new_beam_name: The new name of Beam
        @return: SimplifiedBeamDetailPage
        @author: Thanh Le
        @created_date: 8/17/2016
        """
        self._btnEdit.click()
        dlg = EditDeviceDialog(self._driver)
        dlg.set_beam_name(new_beam_name)
        dlg.submit()
        self.wait_untill_success_msg_disappeared()
        return self    
    
    
    def set_beam_location(self, beam_location):
        """
        @summary: This action is used to set beam location   
        @author: Thanh Le
        @parameter: <beam_location>: Beam location string
        @return: SimplifiedBeamDetailPage
        """
        self._btnEdit.click()
        dlg = EditDeviceDialog(self._driver)
        dlg.set_beam_location(beam_location)
        dlg.submit()
        self.wait_untill_success_msg_disappeared()
        return self
    
    
    def set_beam_label(self, beam_label):
        """
        @summary: This action is used to set beam label   
        @author: Thanh Le
        @parameter: <beam_label>: Beam label string
        @return: SimplifiedBeamDetailPage
        """        
        self._btnEdit.click()
        dlg = EditDeviceDialog(self._driver)
        dlg.set_beam_label(beam_label)
        dlg.submit()
        self.wait_untill_success_msg_disappeared()
        return self
    
    
    def get_beam_label(self):
        """
        @summary: This action is used to get beam label   
        @return:  string label of Beam
        @author: Thanh Le
        """  
        return self._lblBeamLabel.text
    
    
    def get_beam_name(self):
        """
        @summary: This action is used to get beam name
        @return: string of Beam name
        @author: Thanh Le
        @return: beam name value
        """          
        return self._lblBeamName.text
    
    
    def get_beam_location(self):
        """
        @summary: This action is used to get beam location
        @return: string of Beam's location
        @author: Thanh Le
        @return: beam location value
        """  
        return self._lblBeamLocation.text
    
    
    def is_beam_label_displayed(self, label):
        """
        @summary: Check if a Beam's label is displayed or not
        @return: True: the Beam's label is displayed
                False: the Beam's label is not displayed
        @author: Thanh Le
        """
        beam_labels = self.get_beam_label().split(",")
        
        for lbl in beam_labels:
            if(lbl == label):
                return True
        return False
        

    def set_user_can_manage(self, user, canmanage = True):
        """
        @summary: This action is used to set user can manage Beam device   
        @author: Thanh Le
        @parameter: <user>: user object
                    <canmanage>: boolean value to decide user can manage or not
        @return: SimplifiedBeamDetailPage
        """
        if(canmanage):
            self._chkCanManage(user.email_address).check()
        else:
            self._chkCanManage(user.email_address).uncheck()
        
        self.wait_for_loading()
        return self
    
            
    def add_user(self, user, wait_for_completed=True):
        """
        @summary: This action is used to add user   
        @parameter: <user>: user object
                    <dismiss_alert>: boolean value to decide dismiss alert or not
        @return: SimplifiedBeamDetailPage
        @author: Thanh Le
        """
        self._txtInvitedEmail.type(user.email_address)
        self._btnAddUser.click()
        if(wait_for_completed):
            self.wait_untill_success_msg_disappeared(60)       
            
        return self
    
    
    def create_completed_simplified_normal_user(self, user):
        """      
        @summary: Create and active a new normal user
        @param user: user would like to create and activate
        @return: Home page
        @author: Thanh Le
        """
        self.add_user(user).logout()
        activation_link = GmailUtility.get_email_activation_link(receiver = user.email_address, sent_day=datetime.now())
        if(activation_link==None):
            raise Exception("Cannot get activation link")
        else:
            return PasswordSetupPage(self._driver, activation_link).set_password(user.password)\
                .goto_account_settings_page_by_menu_item()\
                .set_user_language(user)\
                .logout()
    
    
    def create_completed_simplified_admin_user(self, user):
        """      
        @summary: Create and active a new admin user
        @param user: user would like to create and activate
        @return: Home page
        @author: Thanh Le
        """
        self.add_user(user).set_user_can_manage(user).logout()
        activation_link = GmailUtility.get_email_activation_link(receiver = user.email_address, sent_day=datetime.now())
        if(activation_link==None):
            raise Exception("Cannot get activation link")
        else:
            return PasswordSetupPage(self._driver, activation_link).set_password(user.password)\
                .goto_account_settings_page_by_menu_item()\
                .set_user_language(user)\
                .logout()
    
    
    def get_alert_message(self):
        """
        @summary: This action is used to add user get alert message
        @return: message content of alert
        @author: Thanh Le
        """        
        return self._lblAlertMessage.wait_until_displayed(30).text
    
    
    def is_alert_message_displayed(self, wait_time_out=None):
        """
        @summary: Check if the alert message is displayed or not
        @return: True: the alert message is displayed
            False: the alert message is not disolayed
        @author: Thanh Le
        """
        return self._lblAlertMessage.is_displayed(wait_time_out)
    
    
    def remove_user(self, user, wait_for_completed=True):
        """
        @summary: This action is used to remove user from manage Beam device   
        @parameter: <user>: user object
                    <wait_for_completed>: boolean value to decide wait for complete or not
        @return: SimplifiedBeamDetailPage
        @author: Thanh Le
        """
        self._driver.scroll_down_to_bottom()
        self._btnRemovePerson(user.email_address).click()
        self._driver.handle_dialog(True)
        if(wait_for_completed):
            self.wait_untill_success_msg_disappeared(10)
            
        return self
     
     
    def is_user_added(self, user):     
        """
        @summary: Check if a user is added or not 
        @return: True: user is added
                False: user is not added
        @author: Thanh Le
        """
        return self._lnkAddedUser(user.email_address).is_displayed()
    
    
    def is_user_not_existed(self, user):
        """
        @summary: Check if a user is not existed
        @return: True: the user is not existed
                False: the user is existed
        @author: Thanh Le
        """
        return self._lnkAddedUser(user.email_address).is_disappeared()
    
    
    def is_user_can_manage_checkbox_disabled(self, user):
        """
        @summary: Check if the user can manage checkbox is disabled
        @return: True: the user can manage check-box is enabled
                False: the user can manage check-box is disabled
        @author: Thanh Le
        """
        if(self._chkCanManage(user.email_address).get_attribute("disabled") == "true"):
            return True
        else:
            return False
    
    
    def is_user_can_manage_checkbox_selected(self, user):
        """
        @summary: Check if the user can manage checkbox is disabled
        @return: True: the user can manage check-box is enabled
                False: the user can manage check-box is disabled
        @author: Thanh Le
        """
        return self._chkCanManage(user.email_address).is_selected()
        

    def is_unlink_device_button_displayed(self):
        """
        @summary: Check if the Unlink a Beam device button is displayed
        @param: True: the Unlink a Beam device is displayed
                False: the Unlink a Beam device is not displayed
        @author: Thanh Le
        @return: True if the Unlink a Beam device button is displayed. False for vice versa
        """
        self._btnEdit.click()
        dlg = EditDeviceDialog(self._driver)
        return dlg.is_button_unlink_displayed()
    
        
    def goto_beam_advance_setting(self):
        """
        @summary: This action is used to go to beam advance setting page
        @return: DeviceAdvanceSettingsDialog
        @author: Thanh Le
        """
        self._btnBeamAdvance.click()
        from pages.suitable_tech.admin.dialogs.device_advance_settings_dialog import DeviceAdvanceSettingsDialog
        return DeviceAdvanceSettingsDialog(self._driver)
    
    
    def get_beam_label_tag_list(self):
        """
        @summary: This action is used to get beam label tag list in Edit Device dialog
        @return: the list of Beam label
        @author: Thanh Le
        """
        self._btnEdit.click()                
        dialog = EditDeviceDialog(self._driver)
        label_tag_list = dialog.get_all_beam_labels()
        dialog.cancel()
        return label_tag_list
    
    
    def set_beam_label_tag_list(self, beam_label_tag_list):
        """
        @summary: This action is used to set beam label tag list in Edit Device dialog
        @parameter: beam_label_tag_list: list of labels
        @return: SimplifiedBeamDetailPage
        @author: Thanh Le
        """        
        self._btnEdit.click()
        dlg = EditDeviceDialog(self._driver)
        dlg.remove_all_beam_labels()
        for label in beam_label_tag_list:
            dlg.add_beam_label(label)
        
        dlg.submit()
        self.wait_untill_success_msg_disappeared()
        return self
    
    
    def clear_beam_location(self):
        """
        @summary: This action is used to clear beam location
        @return: SimplifiedBeamDetailPage
        @author: Thanh Le
        """
        self._btnEdit.click()
        dlg = EditDeviceDialog(self._driver)
        dlg.clear_beam_location()
        dlg.submit()
        dlg._wait_for_dialog_disappeared()
        return self
    
    
    def clear_all_beam_label(self):
        """
        @summary: This action is used to clear all beam label
        @return: SimplifiedBeamDetailPage
        @author: Thanh Le
        """
        self._btnEdit.click()
        EditDeviceDialog(self._driver).remove_all_beam_labels().submit()
        return self


    def get_beam_icon_link(self):
        """
        @summary: This action is used to get beam icon link
        @return: url icon link of Beam
        @Author: Duy Nguyen
        """
        file_url = self._imgBeamIcon.get_attribute("src")
        return Utilities.correct_link(file_url)
    
    
    def change_beam_icon(self, image_path, left, top, width, height):
        """  
        @summary: This action is used to change beam icon    
        @param:
            - image_path: local image file path
            - left | top | width | height : the boundary of crop-tracker element  
        @return: SimplifiedBeamDetailPage
        @author: Duy Nguyen
        """
        self._lnkChangeBeamIcon.wait_until_clickable().click()
        from pages.suitable_tech.admin.dialogs.upload_image_dialog import UploadImageDialog
        dialog = UploadImageDialog(self._driver).choose_file(image_path)
        dialog.set_crop_tracker_dimension(left, top, width, height)
        dialog.submit()
        
        #wait for updating new icon
        sleep(3)
        ico_url = self.get_beam_icon_link()
        ext = ico_url.rsplit('.', 1)
        tried = 0
        while tried < 10:
            #print(" ico url > " + ico_url)
            if len(ext) > 1 and ext[1] != 'svg':
                #print(" ext[1] > " + ext[1])
                sleep(2)
                return self
            tried += 1
            sleep(2)
            ico_url = self.get_beam_icon_link()
            ext = ico_url.rsplit('.', 1)
        return self


    def remove_beam_icon(self):
        """      
        @summary: remove the icon of this group
        @return: SimplifiedBeamDetailPage
        @author: Duy Nguyen  
        """
        btn_remove = self._btnRemoveBeamIcon
        if btn_remove.is_displayed(5):
            btn_remove.click()
            self._driver.handle_dialog(True)
            
            #wait for removing
            sleep(3)
            ico_url = self.get_beam_icon_link()
            ext = ico_url.rsplit('.', 1)
            tried = 0
            while tried < 10:
                if len(ext) > 1 and ext[1] == 'svg':
                    sleep(2)
                    return self
                tried += 1
                sleep(2)
                ico_url = self.get_beam_icon_link()
                ext = ico_url.rsplit('.', 1)  
        return self
    
    
    def wait_for_loading(self, timeout=None):
        self._iconLoading.wait_until_displayed(5)
        self._iconLoading.wait_until_disappeared()
    
