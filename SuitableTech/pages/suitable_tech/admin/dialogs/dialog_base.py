from selenium.webdriver.common.by import By
from core.webdriver.element import Element


class DialogBase(object):
    """
    @description: This is page object class of dialog base. This class is ONLY for inheriting.
    @page: Dialog Base
    @author: Thanh Le
    """


    """    Properties    """
    @property
    def _icoIconLoading(self):
        return Element(self._driver, By.XPATH, "//div[@class='modal-content']//div[@class='loading-indicator']/span")
    @property
    def _btnCancel(self):
        return Element(self._driver, By.XPATH, "//div[@class='modal-content']//button[@ng-click='cancel()']")
    @property
    def _btnSubmitChanges(self):
        return Element(self._driver, By.XPATH, "//div[@class='modal-content']//button[@type='submit']")
    
    
    """    Methods    """
    def __init__(self, driver):  
        """      
        @summary: Constructor method    
        @param driver: Web Driver
        @author: Thanh Le
        """             
        self._driver = driver
        self._wait_for_dialog_appeared()
    
    
    def submit(self, wait_for_completed=True):
        """
        @summary: This action is used to click submit button
        @author: Thanh Le
        @parameter: wait_for_completed: boolean value to decide wait or not
        """
        self._btnSubmitChanges.wait_until_clickable()
        self._btnSubmitChanges.jsclick()
        if(wait_for_completed):
            self._wait_for_dialog_disappeared()
    
    
    def cancel(self):
        """
        @summary: This action is used to click cancel button
        @author: Thanh Le
        """
        self._btnCancel.click()
        self._wait_for_dialog_disappeared()
    
    
    def _wait_for_dialog_appeared(self):
        """
        @summary: This action is used to wait for dialog appeared
        @author: Thanh Le
        """
        self._driver.wait_for_element_visible(By.XPATH, "//div[@class='modal-content']") 
    
    
    def _wait_for_dialog_disappeared(self):
        """
        @summary: This action is used to wait for dialog disappeared
        @author: Thanh Le
        """
        self._driver.wait_for_element_invisible(By.XPATH, "//div[@class='modal-content']")    
    
    
    def _wait_for_loading_completed(self):
        """
        @summary: This action is used to wait for loading completed
        @author: Thanh Le
        """
        self._icoIconLoading.wait_until_disappeared()