from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from pages.suitable_tech.admin.dialogs.dialog_base import DialogBase
from time import sleep


class _LinkABeamWithYourAccountLocator(object):
    _txtBeamCode = (By.XPATH, "//div[@class='modal-content']//input[@ng-model='claimKey.key']")
    
    
class LinkABeamWithYourAccountDialog(DialogBase):
    """
    @description: This is page object class for Link a Beam with your Account Dialog. You need to init it before using in page class.
    @page: Link a Beam with your Account Dialog
    @author: Thanh Le
    """
    
    
    """    Properties    """   
    @property
    def _txtBeamCode(self):
        return Element(self._driver, *_LinkABeamWithYourAccountLocator._txtBeamCode)      
    
    
    """    Methods    """   
    def __init__(self, driver):
        """      
        @summary: Constructor method    
        @param driver: Web Driver
        @author: Thanh Le
        """           
        DialogBase.__init__(self, driver)
        
        
    def is_link_a_beam_with_your_account_dialog_displayed(self):
        """
        @summary: Check if link beam dialog appear
        @return: True: the Link a Beam with your account dialog is displayed
                False: the Link a Beam with your account dialog is not displayed
        @author: Thanh Le
        """   
        return self._txtBeamCode.is_displayed(10)
    
    
    def submit_beam_code(self,beam_code):
        """
        @summary: This action is used to submit beam code
        @return: LinkABeamWithYourAccountDialog
        @Author: Duy Nguyen
        """
        self._txtBeamCode.slow_type(beam_code)
        self._btnSubmitChanges.click()
        return self
    
    
    def cancel_link_beam_with_account(self):
        """
        @summary: This action is used to cancel link beam with account
        @return: AdminDashboardPage
        @Author: Duy Nguyen
        """
        self._btnCancel.click()
        from pages.suitable_tech.admin.advanced.dashboard.admin_dashboard_page import AdminDashboardPage
        return AdminDashboardPage(self._driver)
    
    
    def is_submit_button_disabled(self):
        """
        @summary: check if submit button is disabled
        @return: True: the Submit button is displayed
                False: the Submit button is not displayed
        @Author: Duy Nguyen
        """
        return self._btnSubmitChanges.is_enabled() == False

        
    def input_beam_code(self, beam_code):
        """
        @sumnary: This action use when user only want to input Beam Code without clicking Submit button
        @return: AdminDashboardPage
        @parameter: beam_code: beam code string
        @Author: Duy Nguyen
        """
        self._txtBeamCode.type(beam_code)
        sleep(2)
        return self