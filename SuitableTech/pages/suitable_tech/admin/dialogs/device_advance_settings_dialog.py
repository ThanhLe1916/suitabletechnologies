from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from pages.suitable_tech.admin.dialogs.dialog_base import DialogBase
from core.webdriver.dropdownlist import DropdownList
from common.application_constants import ApplicationConst
from core.webdriver.element_list import ElementList
from data.dataobjects.device_information import DeviceInfo, CurrentNetwork,\
    NetworkInterface, RelayServer, DeviceNetworkInformation,\
    DeviceSystemInformation

    
class DeviceAdvanceSettingsDialog(DialogBase):
    """
    @description: This is page object class for Device Advance Settings Dialog. You need to init it before using in page class.
    @page: Device Advance Settings Dialog
    @author: Thanh Le
    """
    
    _information = None
    _settings = None
    
    
    """    Properties    """
    @property
    def _deviceInformation(self):
        if( not self._information):
            self._information = DeviceInformation(self._driver)
        return self._information
    @property
    def _deviceSettings(self):
        if( not self._settings):
            self._settings = DeviceSettings(self._driver )
        return self._settings  
    
    
    """    Methods    """
    def __init__(self, driver): 
        """      
        @summary: Constructor method
        @param driver: web driver
        @author: Thanh Le         
        """        
        self._driver = driver
    
    
    def print_info(self):
        """      
        @summary: Print information of a Beam device
        @return: Device's information
        @author: Duy Nguyen      
        """     
        infor = self.get_device_information()
        
        print(ApplicationConst.LBL_DEV_ATTR_SYSTEM_SERIAL_NUMBER + ":" + infor.system_info.serial_number)
        print(ApplicationConst.LBL_DEV_ATTR_SYSTEM_UID + ":" + infor.system_info.uid)
        print(ApplicationConst.LBL_DEV_ATTR_SYSTEM_SOFTWARE_VERSION + ":" + infor.system_info.sofware_version)
        
        print(ApplicationConst.LBL_DEV_ATTR_NETWORK_SSID + ":" + infor.network_info.current_network.ssid)
        print(ApplicationConst.LBL_DEV_ATTR_NETWORK_MAC + ":" + infor.network_info.current_network.mac)
        print(ApplicationConst.LBL_DEV_ATTR_NETWORK_FREQUENCY + ":" + infor.network_info.current_network.frequency)
        
        for interface in infor.network_info.network_interfaces:
            print("Interface Name:" + interface.name)
            print("Type:" + interface.type_name)
            print("MAC:" + interface.mac)
            print("IP Address:" + interface.ip_address)
            
            
    def is_network_interface_information_correct(self,interfacename,interfacetype,iterfacemac,iterfaceip):
        """      
        @summary: Check if network interface information is correct or not
        @param: 
            - interfacename, interfacetype, iterfacemac, and iterfaceip: info would like to check
        @return: True: the network interface information is correct
                False: the network interface information is incorrect
        @author: Duy Nguyen      
        """   
        infor = self.get_device_information()
        for interface in infor.network_info.network_interfaces:
            if interface.name == interfacename and interface.type_name == interfacetype and interface.mac == iterfacemac and interface.ip_address == iterfaceip:
                return True
            else:
                return False    
    
    
    def get_device_information(self):
        """      
        @summary: Get information of a device
        @return: Information of device
        @author: Duy Nguyen      
        """ 
        
        self._deviceInformation._active()
        return self._deviceInformation.get_data()
    
    
    def is_restart_dropdownlist_existed(self):
        """      
        @summary: Check if the restart drop-down list is existed or not
        @return: True: The Restart drop-down list is existed
                False: The Restart drop-down list is not existed
        @author: Duy Nguyen      
        """ 
        self._deviceSettings._active()
        return self._deviceSettings.is_restart_ddl_existed()
    
    
    def is_shutdown_dropdownlist_existed(self):
        """      
        @summary: Check if the Shutdown drop-down list is existed or not
        @return: True: The Shutdown drop-down list is existed
                False: The Shutdown drop-down list is not existed
        @author: Duy Nguyen      
        """    
        self._deviceSettings._active()
        return self._deviceSettings.is_shutdown_ddl_existed()
    
    
    def shutdown(self, option):
        """      
        @summary: Shut down a Beam device
        @param: option: select the way to shut down (Immediately/ When idle)
        @return: DeviceAdvanceSettingsDialog
        @author: Duy Nguyen      
        """ 
        self._deviceSettings._active()
        self._deviceSettings.shutdown(option)
    
    
    def restart(self, option):
        """      
        @summary: Restart a Beam device
        @param: option: the way to restart (Immediately/ When idle)
        @return: DeviceAdvanceSettingsDialog
        @author: Duy Nguyen      
        """     

        self._deviceSettings._active()
        self._deviceSettings.restart(option)
    
    
    def close(self):
        """      
        @summary: Close the dialog
        @return: DeviceAdvanceSettingsDialog
        @author: Duy Nguyen      
        """ 
        self.cancel()
    
    
class DeviceAdvanceSettingLocator(object):
    @staticmethod
    def _lblSerialNumber():
        return (By.XPATH, u"//div[@class='modal-content']//h4[.=\"{}\"]/following-sibling::dl//span[.=\"{}\"]/../following-sibling::dd[1]"\
                .format(ApplicationConst.LBL_DEV_ATTR_SECTION_SYSTEM, ApplicationConst.LBL_DEV_ATTR_SYSTEM_SERIAL_NUMBER)) 
    @staticmethod
    def _lblUID():
        return (By.XPATH, u"//div[@class='modal-content']//h4[.=\"{}\"]/following-sibling::dl//span[.=\"{}\"]/../following-sibling::dd[1]"\
                .format(ApplicationConst.LBL_DEV_ATTR_SECTION_SYSTEM, ApplicationConst.LBL_DEV_ATTR_SYSTEM_UID))
    @staticmethod
    def _lblSoftwareVersion():
        return (By.XPATH, u"//div[@class='modal-content']//h4[.=\"{}\"]/following-sibling::dl//span[.=\"{}\"]/../following-sibling::dd[1]"\
                .format(ApplicationConst.LBL_DEV_ATTR_SECTION_SYSTEM, ApplicationConst.LBL_DEV_ATTR_SYSTEM_SOFTWARE_VERSION))
    @staticmethod
    def _lblCurrentNetworkSSID():
        return (By.XPATH, u"//div[@class='modal-content']//h4[.=\"{}\"]/following-sibling::dl[@class='dl-horizontal item-details']//span[.=\"{}\"]/../following-sibling::dd[1]"\
                .format(ApplicationConst.LBL_DEV_ATTR_SECTION_NETWORK, ApplicationConst.LBL_DEV_ATTR_NETWORK_SSID))
    @staticmethod
    def _lblCurrentNetworkMAC():
        return (By.XPATH, u"//div[@class='modal-content']//h4[.=\"{}\"]/following-sibling::dl[@class='dl-horizontal item-details']//span[.=\"{}\"]/../following-sibling::dd[1]"\
                .format(ApplicationConst.LBL_DEV_ATTR_SECTION_NETWORK, ApplicationConst.LBL_DEV_ATTR_NETWORK_MAC))
    @staticmethod
    def _lblCurrentNetworkFrequency():
        return (By.XPATH, u"//div[@class='modal-content']//h4[.=\"{}\"]/following-sibling::dl[@class='dl-horizontal item-details']//span[.=\"{}\"]/../following-sibling::dd[1]"\
                .format(ApplicationConst.LBL_DEV_ATTR_SECTION_NETWORK, ApplicationConst.LBL_DEV_ATTR_NETWORK_FREQUENCY))
    
    _lstNetworkInterface = (By.XPATH, "//div[@class='modal-content']//dl[@ng-repeat='interface in device.network_info.interfaces']")
    
    @staticmethod
    def _lblRelayServer():
        return (By.XPATH, u"//div[@class='modal-content']//h5[.=\"{}\"]/following-sibling::dd[1]"\
                .format(ApplicationConst.LBL_DEV_ATTR_NETWORK_RELAY_SERVER))
    
    
class DeviceInformation(object):
    _tab_info_element = None
    
    
    """    Methods    """
    def __init__(self, driver):
        """      
        @summary: Constructor method
        @param: driver: web driver
        @author: Duy Nguyen      
        """ 
           
        DialogBase.__init__(self, driver)
        self._tab_info_element = Element(self._driver, By.XPATH, u"//div[@class='modal-content']//a[.=\"{}\"]".format(ApplicationConst.LBL_DEV_ATTR_INFORMATION))
     
        
    def _active(self):
        """      
        @summary: Click settings tab
        @return: DeviceInformation
        @author: Duy Nguyen      
        """ 
        self._tab_info_element.click()
    
    
    def get_data(self):
        """      
        @summary: Get data of a device 
        @return: Data of device 
        @author: Duy Nguyen      
        """ 
        system_info = self._get_system_info()
        network_info = self._get_network_info()
        device_information = DeviceInfo(system_info, network_info)
        return device_information
    
    
    def _get_network_info(self):
        """      
        @summary: Get net work information
        @return: Network information 
        @author: Duy Nguyen      
        """ 
        #current network
        current_ssid = Element(self._driver, *DeviceAdvanceSettingLocator._lblCurrentNetworkSSID()).text
        current_mac = Element(self._driver, *DeviceAdvanceSettingLocator._lblCurrentNetworkMAC()).text
        current_frequency = Element(self._driver, *DeviceAdvanceSettingLocator._lblCurrentNetworkFrequency()).text
        
        current_network = CurrentNetwork(current_ssid, current_mac, current_frequency)
        
        # network interface list
        interfaces = []
        interface_list = ElementList(self._driver, *DeviceAdvanceSettingLocator._lstNetworkInterface).get_all_elements()
        for interface in interface_list:
            interface_name = interface.find_element(By.XPATH, "./h5").text
            interface_type = interface.find_element(By.XPATH, u".//span[.=\"{}\"]/../following-sibling::dd[1]".format(ApplicationConst.LBL_DEV_ATTR_NETWORK_INTERFACE_TYPE)).text
            interface_mac = interface.find_element(By.XPATH, u".//span[.=\"{}\"]/../following-sibling::dd[1]".format(ApplicationConst.LBL_DEV_ATTR_NETWORK_MAC)).text
            interface_ip = interface.find_element(By.XPATH, u".//span[.=\"{}\"]/../following-sibling::dd[1]".format(ApplicationConst.LBL_DEV_ATTR_NETWORK_IP_ADDR)).text
            
            interface = NetworkInterface(interface_name, interface_type, interface_mac, interface_ip)
            interfaces.append(interface)
        
        relay_server_ip = Element(self._driver, *DeviceAdvanceSettingLocator._lblRelayServer()).text
        relay_server = RelayServer(relay_server_ip)
        
        network_infor = DeviceNetworkInformation(current_network, interfaces, relay_server)
        
        return network_infor
    
    
    def _get_system_info(self):
        """      
        @summary: Get system information of a device
        @return: System information of a device 
        @author: Duy Nguyen      
        """ 
        #system infor
        serial_number = Element(self._driver, *DeviceAdvanceSettingLocator._lblSerialNumber()).text
        uid = Element(self._driver, *DeviceAdvanceSettingLocator._lblUID()).text
        software_version = Element(self._driver, *DeviceAdvanceSettingLocator._lblSoftwareVersion()).text
        
        system_infor = DeviceSystemInformation(serial_number, uid, software_version)
        return system_infor
     
            
class DeviceSettings(object):
    _tab_settings_element = None
    
    
    """    Properties    """    
    @property
    def _ddlRestart(self):
        return DropdownList(self._driver, By.XPATH, "//div[@class='modal-content']//div[@is-open='restartOpen']")
    @property
    def _ddlShutdown(self):
        return DropdownList(self._driver, By.XPATH, "//div[@class='modal-content']//div[@is-open='shutdownOpen']")
    
    
    """    Methods    """
    def __init__(self, driver):
        """      
        @summary: Constructor method
        @param: driver: web driver
        @author: Duy Nguyen      
        """       
        self._driver = driver
        self._tab_settings_element = Element(self._driver, By.XPATH, u"//div[@class='modal-content']//a[.=\"{}\"]".format(ApplicationConst.LBL_DEV_ATTR_SETTINGS))
    
        
    def _active(self):
        """      
        @summary: Click Settings tab
        @return: DeviceSettings 
        @author: Duy Nguyen      
        """ 
        self._tab_settings_element.click()       
    
    
    def is_restart_ddl_existed(self):
        """      
        @summary: Check if the restart dll is existed or not
        @return: True: The restart dll is existed, False: The restart dll is not existed
        @author: Duy Nguyen      
        """ 
        return self._ddlRestart.is_displayed(10)
    
    
    def is_shutdown_ddl_existed(self):
        """      
        @summary: Check if the shut-down dll is existed or not
        @return: True: the shut-down dall is existed, False: the shut-down dll is not existed
        @author: Duy Nguyen      
        """ 
        return self._ddlShutdown.is_displayed(10)
    
    
    def restart(self, option):
        """      
        @summary: Restart a Beam device
        @param: option: the way device restarts
        @return: DeviceSettings
        @author: Duy Nguyen      
        """ 
        self._active()
        self._ddlRestart.select_by_text(option)
    
        
    def shutdown(self, option):
        """      
        @summary: Shutdown a Beam device
        @param: option: the way the Beam shutdowns
        @return: DeviceSettings
        @author: Duy Nguyen      
        """ 
        self._active()
        self._ddlShutdown.select_by_text(option)