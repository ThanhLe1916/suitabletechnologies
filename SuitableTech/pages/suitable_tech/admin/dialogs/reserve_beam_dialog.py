from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from core.webdriver.editable_combobox import EditableCombobox
from pages.suitable_tech.admin.dialogs.dialog_base import DialogBase
from datetime import datetime
from core.webdriver.datepicker import DatePicker

class _ReserveABeamDialogLocator(object):
    _ecbxDevice = (By.XPATH, "//div[@class='modal-content']//div[@ng-model='reservation.device']//div[@placeholder='Choose a device...']/span")
    _ecbxUser = (By.XPATH, "//div[@class='modal-content']//div[@ng-model='reservation.user']//div[@placeholder='Search for a user...']/span")
    _txtStartingDate = (By.XPATH, "//div[@class='modal-content']//label[@for='start_date']/following-sibling::div//input")
    _txtStartingHour = (By.XPATH, "//div[@class='modal-content']//label[@for='start_time']/following-sibling::table//input[@ng-model='hours']")
    _txtStartingMinute = (By.XPATH, "//div[@class='modal-content']//label[@for='start_time']/following-sibling::table//input[@ng-model='minutes']")
    _btnStartingMeridian = (By.XPATH, "//div[@class='modal-content']//label[@for='start_time']/following-sibling::table//button")
    _btnStartingDatePicker = (By.XPATH, "//div[@class='modal-content']//label[@for='start_date']/following-sibling::div/span[@class='input-group-btn']//button")
    _txtEndingDate = (By.XPATH, "//div[@class='modal-content']//label[@for='end_date']/following-sibling::div//input")
    _txtEndingHour = (By.XPATH, "//div[@class='modal-content']//label[@for='end_time']/following-sibling::table//input[@ng-model='hours']")
    _txtEndingMinute = (By.XPATH, "//div[@class='modal-content']//label[@for='end_time']/following-sibling::table//input[@ng-model='minutes']")
    _btnEndingMeridian = (By.XPATH, "//div[@class='modal-content']//label[@for='end_time']/following-sibling::table//button")
    _btnEndingDatePicker = (By.XPATH, "//div[@class='modal-content']//label[@for='end_date']/following-sibling::div/span[@class='input-group-btn']//button")
    
    
class ReserveABeamDialog(DialogBase):
    """
    @description: This is page object class for Reserve A Beam Dialog. You need to init it before using in page class.
    @page: Reserve A Beam Dialog
    @author: Thanh Le
    """


    """    Properties    """
    @property
    def _ecbxDevice(self):
        return EditableCombobox(self._driver, *_ReserveABeamDialogLocator._ecbxDevice)
    @property
    def _ecbxUser(self):
        return EditableCombobox(self._driver, *_ReserveABeamDialogLocator._ecbxUser)
    @property
    def _txtStartingDate(self):
        return Element(self._driver, *_ReserveABeamDialogLocator._txtStartingDate)    
    @property
    def _txtStartingHour(self):
        return Element(self._driver, *_ReserveABeamDialogLocator._txtStartingHour)        
    @property
    def _txtStartingMinute(self):
        return Element(self._driver, *_ReserveABeamDialogLocator._txtStartingMinute)        
    @property
    def _btnStartingMeridian(self):
        return Element(self._driver, *_ReserveABeamDialogLocator._btnStartingMeridian)
    @property
    def _btnStartingDatePicker(self):
        return Element(self._driver, *_ReserveABeamDialogLocator._btnStartingDatePicker)
    @property
    def _txtEndingDate(self):
        return Element(self._driver, *_ReserveABeamDialogLocator._txtEndingDate)
    @property
    def _txtEndingHour(self):
        return Element(self._driver, *_ReserveABeamDialogLocator._txtEndingHour)        
    @property
    def _txtEndingMinute(self):
        return Element(self._driver, *_ReserveABeamDialogLocator._txtEndingMinute)        
    @property
    def _btnEndingMeridian(self):
        return Element(self._driver, *_ReserveABeamDialogLocator._btnEndingMeridian)
    @property
    def _btnEndingDatePicker(self):
        return Element(self._driver, *_ReserveABeamDialogLocator._btnEndingDatePicker)
    @property
    def _lblSuccessMessage(self):
        return Element(self._driver, *_ReserveABeamDialogLocator._lblSuccessMessage)
    
    
    """    Methods    """
    def __init__(self, driver):  
        """      
        @summary: Constructor method    
        @param driver: Web Driver
        @author: Thanh Le
        """          
        DialogBase.__init__(self, driver)
    
        
    def reserve_a_beam(self, device_name, user_name, start_date = None, end_time = None):
        """
        @summary: This action use to reserve a beam
        @author: Thanh Le
        @parameter: device_name: beam device
                    user_name: user name string
                    start_date: starting date
                    end_date: ending date
        """
        from core.i18n.i18n_support import I18NSupport
        if(device_name != None):
            self._ecbxDevice.select(device_name)
        if(user_name != None):
            self._ecbxUser.select(user_name)

        if(start_date):
            self.select_starting_date(start_date)
            self._txtStartingHour.type(datetime.strftime(start_date, "%I"))
            self._txtStartingMinute.type(datetime.strftime(start_date, "%M"))
            meridian = I18NSupport.localize_date_time_string(datetime.strftime(start_date, "%p"))
            
            if(self._btnStartingMeridian.text != meridian):
                self._btnStartingMeridian.click()
                
        if(end_time):
            self.select_ending_date(end_time)
            
            self._txtEndingHour.type(datetime.strftime(end_time, "%I"))
            self._txtEndingMinute.type(datetime.strftime(end_time, "%M"))
            meridian = I18NSupport.localize_date_time_string(datetime.strftime(end_time, "%p"))
            if(self._btnEndingMeridian.text != meridian):
                self._btnEndingMeridian.click()
                
        self.submit()
    
        
    def select_starting_date(self, start_date):
        """
        @summary: This action is used to select starting date
        @author: Thanh Le
        @parameter: start_date:: starting date
        """
        self._btnStartingDatePicker.click()
        date_picker = DatePicker(self._driver, By.XPATH, "//label[@for='start_date']/following-sibling::div//ul")
        date_picker.select_day(start_date.day, start_date.month, start_date.year)
    
    
    def select_ending_date(self, end_date):
        """
        @summary: This action is used to select ending date
        @author: Thanh Le
        @parameter: end_date: ending date
        """
        self._btnEndingDatePicker.click()
        date_picker = DatePicker(self._driver, By.XPATH, "//label[@for='end_date']/following-sibling::div//ul")
        date_picker.select_day(end_date.day, end_date.month, end_date.year)
    
