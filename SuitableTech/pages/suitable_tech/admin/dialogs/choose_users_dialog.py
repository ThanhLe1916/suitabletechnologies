from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from pages.suitable_tech.admin.dialogs.dialog_base import DialogBase


class _ChooseUsersDialogLocator(object):
    _txbSearchUser = (By.XPATH, "//div[@class='modal-content']//input[@type='search']")
    _btnAddSelectedUser = (By.XPATH, "//div[@class='modal-content']//button[@ng-click='ok()']")
    _btnInviteUser = (By.XPATH, "//div[@class='modal-content']//button[@ng-click='createContact()']")
    
    @staticmethod
    def _lblAvailableUser(search_value):
        return (By.XPATH, u"//div[@ng-repeat='contact in availableContacts']//*[.=\"{}\"]".format(search_value))
    @staticmethod
    def _lblSelectedUser(select_value):
        return (By.XPATH, u"//div[@class='selected-contacts contacts-list']//*[.=\"{}\"]".format(select_value))
    
class ChooseUsersDialog(DialogBase):
    """
    @description: This is page object class for Choose Users Dialog. You need to init it before using in page class.
    @page: Choose Users Dialog
    @author: Thanh Le
    """
    

    """    Properties    """
    @property
    def _txbSearchUser(self):
        return Element(self._driver, *_ChooseUsersDialogLocator._txbSearchUser)
    @property
    def _btnAddSelectedUser(self):
        return Element(self._driver, *_ChooseUsersDialogLocator._btnAddSelectedUser)    
    @property
    def _btnInviteUser(self):
        return Element(self._driver, *_ChooseUsersDialogLocator._btnInviteUser)
    
    def _lblAvailableUser(self, search_value):
        return Element(self._driver, *_ChooseUsersDialogLocator._lblAvailableUser(search_value))
    def _lblSelectedUser(self,select_value):
        return Element(self._driver, *_ChooseUsersDialogLocator._lblSelectedUser(select_value))
    
    
    """    Methods    """
    def __init__(self, driver):    
        """      
        @summary: Constructor method
        @param driver: web driver
        @author: Thanh Le         
        """    
        DialogBase.__init__(self, driver)
    
    
    def select_user(self, search_value):
        """      
        @summary: Select a user by searched value   
        @param search_value: value use to search for selecting user
        @return: User detail page
        @author: Thanh Le
        """
        self._txbSearchUser.type(search_value)
        self._lblAvailableUser(search_value).wait_until_displayed()
        self._lblAvailableUser(search_value).click()
        self._lblSelectedUser(search_value).wait_until_displayed()
        return self
    
        
    def invite_new_user(self, user):
        """      
        @summary: Invite a new user
        @param user: user would like to invite
        @return: ChooseUsersDialog
        @author: Thanh Le
        """
        self._btnInviteUser.click()
        from pages.suitable_tech.admin.dialogs.invite_new_user_dialog import InviteNewUserDialog
        simple_invite_new_user_dialog = InviteNewUserDialog(self._driver)
        simple_invite_new_user_dialog.submit_invite_information_for_simple_form(user)
        return self
    
    
    def choose_user(self, search_value):
        """      
        @summary: Choose user by search value
        @param search_value: value to search for choosing user
        @return: ChooseUsersDialog
        @author: Thanh Le     
        """
        self._wait_for_dialog_appeared()
        self.select_user(search_value)
        self._btnAddSelectedUser.click()
        self._wait_for_dialog_disappeared()
        return None
    
    
    def invite_new_user_and_choose(self, user):
        """      
        @summary: Invite and select the new invited user    
        @param user: user would like to invite
        @return: ChooseUsersDialog
        @author: Thanh Le       
        """
        return self.invite_new_user(user).choose_user(user.email_address)