from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from pages.suitable_tech.admin.dialogs.dialog_base import DialogBase


class _EditUserDlgLocator(object):
    _chkAllowUserAsAdminOrg = (By.XPATH,"//input[@type='checkbox' and @ng-model='user.is_admin']")


class EditUserDialog(DialogBase):
    """
    @description: This page object is used for Edit User Dialog.
    This dialog appear when user want to edit user
    @page: Edit User Dialog.
    
    """
    
    """    Properties    """
    @property
    def _chkAllowUserAsAdminOrg(self):
        return Element(self._driver, *_EditUserDlgLocator._chkAllowUserAsAdminOrg)
    
    
    """    Methods    """
    def __init__(self, driver):      
        """      
        @summary: Constructor method    
        @param driver: Web Driver
        @author: Thanh Le
        """   
        DialogBase.__init__(self, driver)
    
    
    def allow_user_to_administer_org(self):
        """
        @summary: This action is used to set user to administer organization
        @author: Thanh Le
        """
        self._chkAllowUserAsAdminOrg.check()
        self.submit()

    
    def disallow_user_to_administer_org(self):
        """
        @summary: This action is used to disallow user to administer organization
        @author: Thanh Le
        """
        self._chkAllowUserAsAdminOrg.uncheck()
        self.submit()
                        
        
    def is_allow_user_to_administrater_org_disabled(self):
        """
        @summary: Check if allow user administer checkbox is disabled
        @return: True: the Allow user as Administrator of org is enabled
                False: the Allow user as  Administrator of org is disabled
        @author: Thanh Le
        """
        return self._chkAllowUserAsAdminOrg.is_enabled()