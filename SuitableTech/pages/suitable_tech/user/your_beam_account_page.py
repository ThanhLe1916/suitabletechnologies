from pages.suitable_tech.user.user_template_page import UserTemplatePage
from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from common.application_constants import ApplicationConst
from builtins import staticmethod
from core.webdriver.dropdownlist import DropdownList


class _YourBeamAccountPageLocator(object):
    _lblHeader = (By.XPATH, "//h2[@class='heading ng-binding']")
    _lnkDownloadtheBeamDesktopSoftware = (By.XPATH, "//div[@class='icon-link-icon']/a[@href='/installers/']")
    _lnkGetHelp = (By.XPATH, "//div[@class='icon-link-icon']/a[@href='/support/']")
    _lnkAddABeam = (By.XPATH, "//div[@class='icon-link-icon']/a[@ng-click='addDevice($event)']")
    _lnkViewDocumentation = (By.XPATH, "//div[@class='icon-link-icon']/a[@href='/documentation/']")
    _ddlUserHomeMenu = (By.XPATH, "//span[@class='glyphicon glyphicon-user']/../..")
    
    @staticmethod
    def _lblDeviceTitle(value):
        return (By.XPATH, u"//h3[@class='device-title ng-binding'][.=\"{}\"]".format(value))
    @staticmethod
    def _lblDeviceLocation(beam_name):
        return (By.XPATH, u"//h3[contains(text(),\"{}\")]/ancestor::div[@class='device-section ng-scope']/descendant::dt[contains(.,\"{}\")]/following-sibling::dd[1]".format(beam_name, ApplicationConst.LBL_LOCATION_PROPERTY))
    @staticmethod
    def _lblDeviceLabel(beam_name):
        return (By.XPATH, u"//h3[contains(text(),\"{}\")]/ancestor::div[@class='device-section ng-scope']/descendant::dt[contains(.,\"{}\")]/following-sibling::dd[1]".format(beam_name, ApplicationConst.LBL_LABEL_PROPERTY))
    @staticmethod
    def _lblDeviceTimeZone(beam_name):
        return (By.XPATH, u"//h3[contains(text(),\"{}\")]/ancestor::div[@class='device-section ng-scope']/descendant::dt[contains(.,\"{}\")]/following-sibling::dd[1]".format(beam_name, ApplicationConst.LBL_TIMEZONE_PROPERTY))
    @staticmethod
    def _lblDeviceBattery(beam_name):
        return (By.XPATH, u"//h3[contains(text(),\"{}\")]/ancestor::div[@class='device-section ng-scope']/descendant::span[@ng-show='device.state.batt_percent']".format(beam_name))
    @staticmethod
    def _lblDeviceBatteryNoInfo(beam_name):
        return (By.XPATH, u"//h3[contains(text(),\"{}\")]/ancestor::div[@class='device-section ng-scope']/descendant::span[@ng-hide='device.state.batt_percent']".format(beam_name))
    @staticmethod
    def _lblDeviceStatus(beam_name):
        return (By.XPATH, u"//h3[contains(text(),\"{}\")]/ancestor::div[@class='device-section ng-scope']/descendant::span[@ng-class='device.stateIcon()']/following-sibling::span[1]".format(beam_name))
    
        
class YourBeamAccountPage(UserTemplatePage):
    """
    @description: This is page object class for Your Beam Account Page.
        Please visit https://staging.suitabletech.com/manage/#/home/ for more details
    @page: Your Beam Account Page
    @author: Thanh Le
    """    
        
        
    """    Properties    """
    @property
    def _lblHeader(self):
        return Element(self._driver, *_YourBeamAccountPageLocator._lblHeader)    
    @property
    def _lnkDownloadtheBeamDesktopSoftware(self):
        return Element(self._driver, *_YourBeamAccountPageLocator._lnkDownloadtheBeamDesktopSoftware) 
    @property
    def _lnkGetHelp(self):
        return Element(self._driver, *_YourBeamAccountPageLocator._lnkGetHelp)        
    @property
    def _lnkAddABeam(self):
        return Element(self._driver, *_YourBeamAccountPageLocator._lnkAddABeam)
    @property
    def _lnkViewDocumentation(self):
        return Element(self._driver, *_YourBeamAccountPageLocator._lnkViewDocumentation)    
    
    def _lblDeviceTitle(self, value):
        return Element(self._driver, *_YourBeamAccountPageLocator._lblDeviceTitle(value))
    def _lblDeviceLocation(self, value):
        return Element(self._driver, *_YourBeamAccountPageLocator._lblDeviceLocation(value))
    def _lblDeviceLabel(self, value):
        return Element(self._driver, *_YourBeamAccountPageLocator._lblDeviceLabel(value))
    def _lblDeviceTimeZone(self, value):
        return Element(self._driver, *_YourBeamAccountPageLocator._lblDeviceTimeZone(value))
    def _lblDeviceBattery(self, value):
        return Element(self._driver, *_YourBeamAccountPageLocator._lblDeviceBattery(value))    
    def _lblDeviceStatus(self, value):
        return Element(self._driver, *_YourBeamAccountPageLocator._lblDeviceStatus(value))     
    def _lblDeviceBatteryNoInfo(self, value):
        return Element(self._driver, *_YourBeamAccountPageLocator._lblDeviceBatteryNoInfo(value))  
    
        
    @property
    def _ddlUserMenu(self):
        return DropdownList(self._driver, *_YourBeamAccountPageLocator._ddlUserHomeMenu)
    @property
    def _hrefSignOut(self):
        return "/accounts/logout"
    
    
    """    Methods    """
    def __init__(self, driver):
        """      
        @summary: Constructor method    
        @param driver: Web Driver 
        @author: Thanh Le
        """             
        UserTemplatePage.__init__(self, driver)
        self._lblHeader.wait_until_displayed()
        
    
    def logout(self):
        """      
        @summary: Log out from Suitable Tech     
        @return: Home page object
        @author: Thanh Le
        @created_date: August 05, 2016
        """
        self._driver.scroll_up_to_top()
        self._ddlUserMenu.select_by_href(self._hrefSignOut)
        from pages.suitable_tech.user.home_page import HomePage
        return HomePage(self._driver, True)
    
    
    def is_device_title_displayed(self, devicetitle, wait_time_out=5):
        """
        @summary: Check if device title is displayed
        @author: Thanh Le
        @parameter: <devicetitle>: string device title
                    <wait_time_out>: waiting time
        @return: True if device title displayed, False for vice versa
        """
        return self._lblDeviceTitle(devicetitle).is_displayed(wait_time_out)
    
    
    def is_device_title_disappeared(self, devicetitle, wait_time_out=None):
        """
        @summary: Check if device title is disappeared
        @author: Thanh Le
        @parameter: <devicetitle>: string device title
                    <wait_time_out>: waiting time
        @return: Return True if device title is disappeared, False for vice versa
        """
        return self._lblDeviceTitle(devicetitle).is_disappeared(wait_time_out)
    
    
    def open_beam_help_center_page(self):
        """
        @summary: This action is used to go Beam Help Center page
        @author: Thanh Le
        @return: BeamHelpCenterPage page object
        """
        self._lnkGetHelp.click()
        self._driver.switch_to_window(1)
        self._driver.maximize_window()
        from pages.suitable_tech.user.beam_help_center_page import BeamHelpCenterPage
        return BeamHelpCenterPage(self._driver)
    
    
    def open_suitable_technology_documentation_page(self):
        """
        @summary: This action is used to go Suitable Technology Document page
        @author: Thanh Le
        @return: BeamDocumentationPage page object
        """
        self._lnkViewDocumentation.click()
        self._driver.switch_to_window(1)
        self._driver.maximize_window()
        from pages.suitable_tech.user.beam_documentation_page import BeamDocumentationPage
        return BeamDocumentationPage(self._driver)
    
    
    def open_add_a_beam_dialog(self):
        """
        @summary: This action is used to open "add a beam" dialog
        @author: Thanh Le
        @return: LinkABeamWithYourAccountDialog page object
        """
        self._lnkAddABeam.click()
        from pages.suitable_tech.admin.dialogs.link_a_beam_with_your_account_dialog import LinkABeamWithYourAccountDialog
        return LinkABeamWithYourAccountDialog(self._driver)
    
    
    def get_beam_property_value(self, beam_name, property_name):
        """
        @Author: Duy Nguyen
        @Description: use to get property value of Beam device such as "Location", "Labels", "Time Zone". Variable is contains in ApplicationConst class
        @return: property value
        """
        lst_property = [ApplicationConst.LBL_LOCATION_PROPERTY, ApplicationConst.LBL_LABEL_PROPERTY, ApplicationConst.LBL_TIMEZONE_PROPERTY]
        lst_control = [self._lblDeviceLocation(beam_name), self._lblDeviceLabel(beam_name), self._lblDeviceTimeZone(beam_name)]
        index = lst_property.index(property_name)
        return lst_control[index].text
    
    
    def get_beam_connected_status(self, beam_name):
        """
        @Description: This action use to get beam connected status
        @Author: Duy Nguyen
        @return: beam connected status value
        """
        return self._lblDeviceStatus(beam_name).text
    
    
    def is_download_link_displayed(self):
        """
        @summary: Check if download link is displayed
        @author: Thanh Le
        @return: True if download link is displayed, False for vice versa
        """    
        return self._lnkDownloadtheBeamDesktopSoftware.is_displayed()
        
        
    def is_get_help_link_displayed(self):
        """
        @summary: Check if get help link is displayed
        @author: Thanh Le
        @return: True if get help link is displayed, False for vice versa
        """       
        return self._lnkGetHelp.is_displayed()
    
    
    def is_add_beam_link_displayed(self):
        """
        @summary: Check if add beam link is displayed
        @author: Thanh Le
        @return: True if add beam link is displayed, False for vice versa
        """         
        return self._lnkAddABeam.is_displayed()
    
    
    def is_view_documentation_link_displayed(self):
        """
        @summary: Check if view documentation link is displayed
        @author: Thanh Le
        @return: True if view documentation link is displayed, False for vice versa
        """         
        return self._lnkViewDocumentation.is_displayed()
    
    
    def is_beam_location_info_displayed(self, beam_name):
        """
        @summary: Check if beam location info is displayed
        @author: Thanh Le
        @return: True if beam location info is displayed, False for vice versa
        """         
        return self._lblDeviceLocation(beam_name).is_displayed()
    
    
    def is_beam_labels_info_displayed(self, beam_name):
        """
        @summary: Check if beam label info is displayed
        @author: Thanh Le
        @return: True if beam label info is displayed, False for vice versa
        """         
        return self._lblDeviceLabel(beam_name).is_displayed()
    
    
    def is_beam_timezone_info_displayed(self, beam_name):
        """
        @summary: Check if beam time zone info is displayed
        @author: Thanh Le
        @return: True if beam time zone info is displayed, False for vice versa
        """         
        return self._lblDeviceTimeZone(beam_name).is_displayed()
