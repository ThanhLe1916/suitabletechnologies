from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from pages.suitable_tech.user.password_reset_page import PasswordResetPage
from pages.basepage import BasePage
from common.application_constants import ApplicationConst
from pages.suitable_tech.user.user_template_page import UserTemplatePage


class _DisassociationCompletedPageLocator(object):
    _btnLoginButton = (By.XPATH,"//a[@class='large radius button' and @href='//stg1.suitabletech.com/accounts/home/']")
    _lnkResetPassword = (By.XPATH, "//a[@href='/accounts/password_reset/']")
    _lnkLogin = (By.XPATH, "//a[@href='/accounts/login/']")
    
    @staticmethod
    def _lblHeader():
        return (By.XPATH,u"//h2[contains(text(),\"{}\")]".format(ApplicationConst.LBL_DISCONNECT_SUCCESSFUL))
        
        
class DisassociationCompletedPage(UserTemplatePage):
    """
    @description: This is page object class for Disassociate Complete page.
    This page appear after user get out of GSSO Authentication
    @page: Confirm Association Page 
    @author: Thanh Le
    """
    
    
    """    Properties    """      
    @property
    def _lblSusscessfulLogout(self):
        return Element(self._driver, *_DisassociationCompletedPageLocator._lblSusscessfulLogout)
    @property
    def _btnLoginButton(self):
        return Element(self._driver, *_DisassociationCompletedPageLocator._btnLoginButton)
    @property
    def _lnkResetPassword(self):
        return Element(self._driver, *_DisassociationCompletedPageLocator._lnkResetPassword)
    @property
    def _lnkLogin(self):
        return Element(self._driver, *_DisassociationCompletedPageLocator._lnkLogin)
    @property
    def _lblHeader(self):
        return Element(self._driver, *_DisassociationCompletedPageLocator._lblHeader())
    
    
        """    Methods    """
    def __init__(self, driver):
        """      
        @summary: Constructor method    
        @param driver: Web Driver
        @author: Thanh Le
        """   
        BasePage.__init__(self, driver)
        self._lblHeader.wait_until_displayed()
    
    
    def is_page_displayed(self):
        """
        @summary: Check if page is displayed
        @return: True if page is displayed. False for page is not displayed
        @author: Duy Nguyen
        """
        return self._lblHeader.is_displayed()
        
        
    def is_disconnect_successful_message_displayed(self):
        """
        @summary: Check if disconnect successful message is displayed
        @return: True if disconnect successful message is displayed, False if the disconnect successful message is not displayed
        @author: Duy Nguyen
        """
        return self._lblHeader.is_displayed(2)
    
    
    def reset_password(self, user):
        """
        @summary: This action is used to reset password for a user
        @parameter: <user>: user would like to reset password
        @return: PasswordResetPage
        @author: Duy Nguyen
        """
        self._lnkResetPassword.click()
        return PasswordResetPage(self._driver).reset_user_password(user.email_address, user.password)
    
    
    
    