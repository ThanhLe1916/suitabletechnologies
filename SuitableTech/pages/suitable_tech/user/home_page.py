from pages.suitable_tech.user.user_template_page import UserTemplatePage
from common.constant import Constant
from core.webdriver.element import Element
from selenium.webdriver.common.by import By

class _HomePageLocator(object):
    _pnlSlideShow = (By.XPATH, "//div[@class='slideshow-wrapper']")


class HomePage(UserTemplatePage):
    """
    @description: This is page object class for Home page.
        Please visit https://staging.suitabletech.com/ for more details
    @page: Home page
    @author: Thanh Le
    """


    """    Properties    """      
    @property
    def _pnlSlideShow(self):
        return Element(self._driver, *_HomePageLocator._pnlSlideShow)
    
    
    """    Methods    """
    def __init__(self, driver, wait_for_loading=False):
        """      
        @summary: Constructor method    
        @parameter: driver: Web Driver
                    wait_for_loading: boolean value to decide wait for loading or not
        @author: Thanh Le
        """  
        UserTemplatePage.__init__(self, driver, wait_for_loading)
        if(wait_for_loading):
            self._pnlSlideShow.wait_until_displayed()
        
        
    def open(self):
        """
        @summary: This action use to navigate ST page
        @return HomePage page object
        @author: Thanh Le
        """
        self._driver.get(Constant.SuitableTechURL)
        return HomePage(self._driver, True)
    
    
    def open_and_goto_login_page(self):
        """
        @summary: This action use to navigate ST page and go to Login page
        @return LoginPage page object
        @author: Thanh Le
        """
        return self.open().goto_login_page()


    def is_page_displayed(self, timeout=5):
        """
        @summary: return True if current page is Home page. Otherwise, return False
        @return True/False
        @author: Thanh Le
        """
        
        return (self._driver.current_url==Constant.SuitableTechURL and self.is_login_button_displayed(timeout))
        
        
        
        