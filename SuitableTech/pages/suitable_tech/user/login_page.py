from pages.suitable_tech.user.user_template_page import UserTemplatePage
from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from pages.suitable_tech.admin.advanced.admin_home_page import AdminHomePage
from pages.gmail_page.gmail_sign_in_page import GmailSignInPage
from pages.suitable_tech.user.password_reset_page import PasswordResetPage
from common.constant import Constant


class _LoginPageLocator(object):
    _txtEmail = (By.ID, "id_username")
    _txtPassword = (By.ID, "id_password")
    _chbKeepMeLoggedIn = (By.ID, "id_remember_me")
    _btnSignIn = (By.ID, "id_submit")
    _lblLoginError = (By.XPATH, "//p[@class='errornote']")
    _btnLoginGoogle = (By.XPATH, "//div[@id='googleBtn']")
    _lnkForgotPassword = (By.XPATH, "//a[@id='id_forgot']")
    

class LoginPage(UserTemplatePage):
    """
    @description: This is page object class for Login page.
        Please visit https://staging.suitabletech.com/accounts/login/ for more details
    @page: Login page
    @author: Thanh Le
    """
    
    
    """    Properties    """      
    @property
    def _txtEmail(self):
        return Element(self._driver, *_LoginPageLocator._txtEmail)
    @property
    def _txtPassword(self):
        return Element(self._driver, *_LoginPageLocator._txtPassword)
    @property
    def _chbKeepMeLoggedIn(self):
        return Element(self._driver, *_LoginPageLocator._chbKeepMeLoggedIn)
    @property
    def _btnSignIn(self):
        return Element(self._driver, *_LoginPageLocator._btnSignIn)
    @property
    def _lblLoginError(self):
        return Element(self._driver, *_LoginPageLocator._lblLoginError)
    @property
    def _btnLoginGoogle(self):
        return Element(self._driver, *_LoginPageLocator._btnLoginGoogle)
    @property
    def _lnkForgotPassword(self):
        return Element(self._driver, *_LoginPageLocator._lnkForgotPassword)
    
    
    """    Methods    """
    def __init__(self, driver):
        """      
        @summary: Constructor method    
        @param driver: Web Driver
        @author: Thanh Le
        """    
        UserTemplatePage.__init__(self, driver)
        self._btnSignIn.wait_until_displayed()
        
    
    def is_page_displayed(self):
        """
        @summary: Check if login page is displayed or not
        @return: True if login page is displayed , False if the login page is not displayed
        @author: Thanh Le
        """
        current_url = self._driver.current_url      
        return (current_url == Constant.SuitableTechLoginURL or current_url == "{}#".format(Constant.SuitableTechLoginURL))  
    

    def login(self, email, password, keepMeLoggedIn=False):
        """
        @summary: This action use to login ST page
        @parameter: <email>: email string
                    <password>: password string
                    <keepMeLoggedIn>: keep login state checkbox
        @return: AdminHomePage
        @author: Thanh Le
        """
        self._submit_login_form(email, password, keepMeLoggedIn)
        return AdminHomePage(self._driver)
    
    
    def login_as_unwatched_video_user(self, email_address, password=None, keepMeLoggedIn=False):
        """
        @summary: This action use to login ST page with account which have never watch video
        @parameter: <email_address>: email string
                    <password>: password string
                    <keepMeLoggedIn>: keep login state checkbox
        @return: WelcomeToBeamPage
        @author: Thanh Le
        """
        if password is None:
            password = Constant.DefaultPassword
        self._submit_login_form(email_address, password, keepMeLoggedIn)
        from pages.suitable_tech.user.welcome_to_beam_page import WelcomeToBeamPage
        return WelcomeToBeamPage(self._driver)
    
    
    def login_as_advanced_admin_and_invite_new_user(self, user, email_address, password, keepMeLoggedIn=False, is_admin_watched_video=True):
        """      
        @summary: Log into Suitabletech and invite a new normal user (Not including activation step)        
        @parameter: user: User object that contains user information
                    email_address: Email address of admin user that used to login
                    password: Password of admin user that used to login
                    keepMeLoggedIn: Boolean value True/False. 
        @return: AdminDashboardPage page object
        @author: Thanh Le
        @created_date: August 05, 2016
        """
        dashboard_page = None
        if(is_admin_watched_video):
            dashboard_page = self.login(email_address, password, keepMeLoggedIn)\
                .goto_admin_dashboard_page()
        else:
            dashboard_page = self.login_as_unwatched_video_user(email_address, password, keepMeLoggedIn)\
                .goto_admin_dashboard_page_by_menu_item()
                
        return dashboard_page.invite_new_user(user)
    
    
    def login_as_advanced_admin_and_create_complete_new_user(self, user, email_address, password, keepMeLoggedIn=False, is_admin_watched_video=True):
        """      
        @summary: Log into Suitabletech and create a complete normal user (including activation step)        
        @parameter: user: User object that contains user information
                    email_address: Email address of admin user that used to login
                    password: Password of admin user that used to login
                    keepMeLoggedIn: Boolean value True/False. 
        @return: WelcomeToBeamPage page object after new user set password (logged with new user)
        @author: Thanh Le
        @created_date: August 05, 2016
        """
        dashboard_page = None
        if(is_admin_watched_video):
            dashboard_page = self.login(email_address, password, keepMeLoggedIn)\
                .goto_admin_dashboard_page()
        else:
            dashboard_page = self.login_as_unwatched_video_user(email_address, password, keepMeLoggedIn)\
                .goto_admin_dashboard_page_by_menu_item()
                
        return dashboard_page.create_complete_normal_user(user)
    
    
    def login_expecting_error(self, email, password, keepMeLoggedIn=False):
        """
        @summary: This action use to work with expected failure login case
        @parameter: <email_address>: email string
                    <password>: password string
                    <keepMeLoggedIn>: keep login state checkbox
        @return: Login page itself
        @author: Thanh Le
        """
        self._submit_login_form(email, password, keepMeLoggedIn)
        return self
    
        
    def _submit_login_form(self, email, password, keepMeLoggedIn=False):
        """
        @summary: This action use to perform submit login form
        @parameter: <email_address>: email string
                    <password>: password string
                    <keepMeLoggedIn>: keep login state checkbox
        @author: Thanh Le
        """
        self._txtEmail.type(email)
        self._txtPassword.type(password)
        
        if(keepMeLoggedIn):
            self._chbKeepMeLoggedIn.check()
        else:
            self._chbKeepMeLoggedIn.uncheck()        
        
        self._btnSignIn.click()
     
        
    def get_login_error_message(self): 
        """
        @summary: This action use to get login error message
        @return: login error message
        @author: Thanh Le
        """
        return self._lblLoginError.text
    
    
    def log_in_with_exist_google_account(self, email):
        """
        @summary: This action use to login ST page by GSSO authentication with remember google account
        @parameter: <email>: google email string
        @return: WelcomeToBeamPage page object
        @author: Thanh Le
        """
        self._btnLoginGoogle.click()
        gmail = GmailSignInPage(self._driver)
        gmail.sign_in_exist_account(email)
        from pages.suitable_tech.user.welcome_to_beam_page import WelcomeToBeamPage
        return WelcomeToBeamPage(self._driver)
    
    
    def goto_google_signin_page(self):
        """
        @summary: This action use to click on "sign in with google button"
        @return: GmailSignInPage
        @author: Thanh Le
        """
        self._btnLoginGoogle.click()
        return GmailSignInPage(self._driver)
    

    def login_with_google(self, email, password=Constant.DefaultPassword, keepMeLoggedIn=False):
        """
        @summary: This action use to login ST page by GSSO authentication with account have already watch video
        @parameter: <email>: google email string
                    <password>: password string
                    <keepMeLoggedIn>: keep login state checkbox
        @return: AdminHomePage page object
        @author: Thanh Le
        """
        self._btnLoginGoogle.click()
        gmail = GmailSignInPage(self._driver)
        gmail.sign_in_exist_account(email, password, keepMeLoggedIn)
        return AdminHomePage(self._driver)
    
    
    def login_as_unwatched_google_user(self, email=None, password=Constant.DefaultPassword, keepMeLoggedIn=False):
        """
        @summary: This action use to login ST page by GSSO authentication with account has't watched video
        @parameter: <email>: google email string
                    <password>: password string
                    <keepMeLoggedIn>: keep login state checkbox
        @return: WelcomeToBeamPage page object
        @author: Thanh Le
        """
        self._btnLoginGoogle.click()
        gmail = GmailSignInPage(self._driver)
        gmail.sign_in_exist_account(email, password, keepMeLoggedIn)
        from pages.suitable_tech.user.welcome_to_beam_page import WelcomeToBeamPage
        return WelcomeToBeamPage(self._driver)
        
        
    def goto_forgot_password_page(self):
        """
        @summary: This action use to go to forgot password page
        @return: PasswordResetPage page object
        @author: Thanh Le
        """
        self._lnkForgotPassword.click()
        return PasswordResetPage(self._driver)
    
    
    def login_with_google_as_new_auth(self, email, password=None, keepMeLoggedIn=False, logged_in_before=False):
        """
        @summary: This action use to work with google account which haven't got GSSO authentication
        @parameter: <email>: google email string
                    <password>: password string
                    <keepMeLoggedIn>: keep login state checkbox
                    <logged_in_before>: boolean value to know if this account used to have GSSO authentication
        @return: ConfirmAssociationPage page object
        @author: Thanh Le
        """
        if password is None:
            password = Constant.DefaultPassword
        self._btnLoginGoogle.click()
        gmail = GmailSignInPage(self._driver)
        if logged_in_before:
            gmail.sign_in_exist_account(email, password, keepMeLoggedIn)
        else:
            gmail.sign_in_gmail(email, password, keepMeLoggedIn)
            from pages.gmail_page.allow_association_page import AllowAssociationPage
            allow_access_page = AllowAssociationPage(self._driver)
            if(allow_access_page.is_policy_message_displayed()):
                allow_access_page.approve_access_account_info()
        from pages.suitable_tech.user.confirm_association_page import ConfirmAssociationPage
        return ConfirmAssociationPage(self._driver)
    
    
    def login_with_google_account_expecting_error(self, email):
        """
        @summary: This action use to work with expected failure google login
        @return: GmailSignInPage
        @author: Duy Nguyen
        """
        self._btnLoginGoogle.click()
        gmail = GmailSignInPage(self._driver)
        return gmail.sign_in_with_expecting_error_google_account(email)
    
    
    def goto_google_allow_access_account_information_page(self, email, password=None, keepMeLoggedIn=False):
        
        """
        @summary: This action use to go to allow access page of Google when a brand new google account login with GSSO authentication
        @parameter: <email>: google email string
                    <password>: password string
                    <keepMeLoggedIn>: keep login state checkbox
        @return: AllowAssociationPage
        @author: Duy Nguyen
        """
        if password is None:
            password = Constant.DefaultPassword        
        self._btnLoginGoogle.click()
        gmail = GmailSignInPage(self._driver)
        gmail.sign_in_gmail(email, password, keepMeLoggedIn)
        from pages.gmail_page.allow_association_page import AllowAssociationPage
        return AllowAssociationPage(self._driver)
    
    
    def login_to_force_restore_gsso_authentication_account(self, email, password=None, keepMeLoggedIn=False):
        """
        @summary: This action use to login by GSSO authentication to restore gsso authentication
        @parameter: <email>: google email string
                    <password>: password string
                    <keepMeLoggedIn>: keep login state checkbox
        @return: Welcome Page
        @author: Duy Nguyen
        """
        if password is None:
            password = Constant.DefaultPassword        
        self._btnLoginGoogle.click()
        gmail = GmailSignInPage(self._driver)
        gmail.sign_in_exist_account(email, password, keepMeLoggedIn)
        try:
            from pages.suitable_tech.user.confirm_association_page import ConfirmAssociationPage
            ConfirmAssociationPage(self._driver).is_confirm_new_auth_message_displayed()
        finally:
            from pages.suitable_tech.user.welcome_to_beam_page import WelcomeToBeamPage
            return WelcomeToBeamPage(self._driver)
        
