from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from common.constant import Constant
from core.utilities.gmail_utility import GmailUtility
from core.webdriver.dropdownlist import DropdownList
from pages.basepage import BasePage


class _UserTemplatePageLocator(object):
    _lnkHome = (By.XPATH, "//a[@href='/']")
    _lnkBeamPro = (By.XPATH, "//section[@class='top-bar-section']//a[@href='/beampro/']")
    _lnkBeamPlus = (By.XPATH, "//section[@class='top-bar-section']//a[@href='/beam-plus/']")
    _lnkBlog = (By.XPATH, "//section[@class='top-bar-section']//a[@href='http://blog.suitabletech.com/']")
    _lnkTestDrive = (By.XPATH, "//section[@class='top-bar-section']//a[@href='/testdrive/']")
    _lnkGetBeam = (By.XPATH, "//section[@class='top-bar-section']//a[@href='/getbeam/']")
    _lnkLogin = (By.XPATH, "//a[@href='//stg1.suitabletech.com/accounts/home/']")
    _ddlUserMenu = (By.XPATH, "//nav[@class='top-bar']//li[@class='has-dropdown login']")
    _lnkLogout = (By.XPATH, "//a[@href='/accounts/logout/']")
    _lblNotificationMessage = (By.XPATH, "//div[@class='large-12 columns text-center']")        
    
    """    Chat box    """
    _lblChatboxHeader = (By.XPATH, "//div[@id='habla_topbar_div']")
    
class UserTemplatePage(BasePage):
    """
    @description: This is page object class that contains all controls and methods shared across all User pages. 
        This class is ONLY for inheriting.
    @page: User Template Page
    @author: Thanh Le
    """
    
    
    """    Properties    """    
    @property
    def _lnkHome(self):
        return Element(self._driver, *_UserTemplatePageLocator._lnkHome)
    @property
    def _lblNotificationMessage(self):
        return Element(self._driver, *_UserTemplatePageLocator._lblNotificationMessage)
    @property
    def _lnkBeamPro(self):
        return Element(self._driver, *_UserTemplatePageLocator._lnkBeamPro)
    @property
    def _lnkBeamPlus(self):
        return Element(self._driver, *_UserTemplatePageLocator._lnkBeamPlus)
    @property
    def _lnkBlog(self):
        return Element(self._driver, *_UserTemplatePageLocator._lnkBlog)
    @property
    def _lnkTestDrive(self):
        return Element(self._driver, *_UserTemplatePageLocator._lnkTestDrive)
    @property
    def _lnkGetBeam(self):
        return Element(self._driver, *_UserTemplatePageLocator._lnkGetBeam)
    @property
    def _lnkLogin(self):
        return Element(self._driver, *_UserTemplatePageLocator._lnkLogin)
    @property
    def _ddlUserMenu(self):
        return DropdownList(self._driver, *_UserTemplatePageLocator._ddlUserMenu)
    @property
    def _lnkLogout(self):
        return Element(self._driver, *_UserTemplatePageLocator._lnkLogout)
    @property
    def _hrefLogOut(self):
        return "/accounts/logout/"
    @property
    def _hrefManageYourBeams(self):
        return "/manage/"
    @property
    def _hrefAccountSettings(self):
        return "/accounts/settings/"
    @property
    def _hrefDownloadInstaller(self):
        return "/installers"
    @property
    def _hrefDocumentation(self):
        return "/documentation/"
    @property
    def _hrefBeamHelp(self):
        return "/support/"
    
    """    Chat box    """
    @property
    def _lblChatboxHeader(self):
        return Element(self._driver, *_UserTemplatePageLocator._lblChatboxHeader)
    
    
    """    Methods      """
    def __init__(self, driver, wait_for_loading=True):
        """      
        @summary: Constructor method    
        @param driver: Web Driver
                wait_for_loading: Boolean value to decide wait for loading or not
        @author: Thanh Le
        """ 
        BasePage.__init__(self, driver)
        if(wait_for_loading):
            self._lblChatboxHeader.wait_until_displayed()      
    
    
    def goto_home_page(self):
        """
        @summary: This action use to go to home page   
        @author: Thanh Le
        @return HomePage page object
        """
        self._lnkHome.wait_until_clickable().click()
        from pages.suitable_tech.user.home_page import HomePage
        return HomePage(self._driver)        
    
    
    def is_logged_in(self, time_out=5): 
        """
        @summary: Check if page is in login state
        @author: Thanh Le
        @parameter:<time_out>: waiting time
        @return: True if page is in login state, False for vice versa
        """
        if(self.is_login_button_displayed(time_out) and (Constant.SuitableTechURL in self._driver.current_url)):
            return False
        
        return True
    
    
    def get_current_user_displayed_name(self):
        if self._ddlUserMenu.is_displayed():
            return self._ddlUserMenu.get_selected_item()
        return None    
        
        
    def goto_login_page(self):
        """
        @summary: This action use to go to login page   
        @author: Thanh Le
        @return LoginPage page object
        """ 
        self._lnkLogin.wait_until_clickable().click()
        from pages.suitable_tech.user.login_page import LoginPage
        return LoginPage(self._driver)
    
        
    def is_login_button_displayed(self, timeout=None):
        """
        @summary: Check if login button is displayed
        @author: Thanh Le
        @return True if login button is displayed. False for vice versa
        """
        return self._lnkLogin.is_displayed(timeout)
     
         
    def logout(self):
        """
        @summary: This action use to logout Suitable Tech page   
        @author: Thanh Le
        @return: HomePage
        """ 
        self._driver.scroll_up_to_top()
        self._ddlUserMenu.select_by_href(self._hrefLogOut)
        
        from pages.suitable_tech.user.home_page import HomePage
        return HomePage(self._driver, True)
    
    
    def logout_and_login_again(self, email, password, keepMeLoggedIn=False):
        """
        @summary: This action use to logout Suitable Tech page then login again with default admin user   
        @author: Thanh Le
        @parameter: email: email address
                    password: password
                    keepMeLoggedIn: keep login state checkbox
        @return: AdminHomePage
        """ 
        return self.logout().goto_login_page().login(email, password, keepMeLoggedIn)
        
    
    def logout_and_login_again_as_unwatched_video_user(self, email, password, keepMeLoggedIn=False):
        """
        @summary: This action use to logout and login again with default admin user which has never watch fully introduction video  
        @author: Thanh Le
        @parameter: email: email address
                    password: password
                    keepMeLoggedIn: keep login state checkbox
        @return: WelcomeToBeamPage
        """ 
        return self.logout().goto_login_page().login_as_unwatched_video_user(email, password, keepMeLoggedIn)
    
    
    def logout_and_login_again_as_new_user(self, email, password, keepMeLoggedIn=False):
        """
        @summary: This action use to logout and login again with default user which has never watch fully introduction video  
        @author: Thanh Le
        @parameter: email: email address
                    password: password
                    keepMeLoggedIn: keep login state checkbox
        @return: WelcomeToBeamPage
        """ 
        return self.logout().goto_login_page().login_as_unwatched_video_user(email, password, keepMeLoggedIn)
    
    
    def is_dropdownlist_item_existed(self, text):
        """
        @summary: Check if dropdownlist item is existed
        @author: Thanh Le
        @parameter: text: item string
        @return: True if dropdownlist item is existed. False for vice versa
        """
        return self._ddlUserMenu.is_item_existed(text)
    
    
    def is_dropdownlist_item_unexisted(self, text):
        """
        @summary: Check if dropdownlist item is not existed
        @author: Thanh Le
        @parameter: text: item string
        @return: True if dropdownlist item is unexisted. False for vice versa
        """
        return self._ddlUserMenu.is_item_not_existed(text)
    
    
    def goto_admin_dashboard_page_by_menu_item(self):
        """
        @summary: This action use to go to admin dashboard page by menu item
        @author: Thanh Le
        @return: AdminDashboardPage page object
        """
        self._ddlUserMenu.select_by_href(self._hrefManageYourBeams)
        from pages.suitable_tech.admin.advanced.dashboard.admin_dashboard_page import AdminDashboardPage
        return AdminDashboardPage(self._driver)
    
    
    def goto_simplified_dashboard_page_by_menu_item(self):        
        """
        @summary: This action use to go to simplified dashboard page by menu item
        @author: Thanh Le
        @return: SimplifiedDashboardPage page object
        """
        self._ddlUserMenu.select_by_href(self._hrefManageYourBeams)
        from pages.suitable_tech.admin.simplified.dashboard.simplified_dashboard_page import SimplifiedDashboardPage 
        return SimplifiedDashboardPage(self._driver)
    
    
    def goto_account_settings_page_by_menu_item(self):
        """
        @summary: This action use to go to personal account setting page by menu item
        @author: Thanh Le
        @return: SimplifiedDashboardPage page object
        """
        self._ddlUserMenu.select_by_href(self._hrefAccountSettings)
        from pages.suitable_tech.accountsettings.account_settings_page import AccountSettingsPage
        return AccountSettingsPage(self._driver)
    
    
    def approve_request_beam_access(self, user_email_address, admin_email_address):
        """      
        @summary: This action use to go to approve request beam access        
        @param: <user_email_address>: email address of user
                <admin_email_address>: ermail address of admin
        @return: User Template page
        @author: Thanh Le
        """
        approve_request_link = GmailUtility.get_approve_request_link(reply_to=user_email_address, receiver=admin_email_address)
        if approve_request_link:
            self._driver.get(approve_request_link)  
        else:
            raise Exception("Cannot get approve request link")          
        return self
    
    
    def reject_request_beam_access(self, user_email_address, admin_email_address):
        """      
        @summary: This action use to go to reject request beam access  
        @param: <user_email_address>: email address of user
                <admin_email_address>: ermail address of admin
        @return: User Template page
        @author: Thanh Le
        """
        reject_request_link = GmailUtility.get_reject_request_link(reply_to=user_email_address, receiver=admin_email_address)
        if reject_request_link:
            self._driver.get(reject_request_link)
        else:
            raise Exception("Cannot get reject request link")          
        return self
    
    
    def get_notification_message(self):
        """      
        @summary: This action use to get notification message    
        @author: Thanh Le
        @return: notification message
        """
        return self._lblNotificationMessage.text

