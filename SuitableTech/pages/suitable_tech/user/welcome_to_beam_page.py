from pages.suitable_tech.user.user_template_page import UserTemplatePage
from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from common.application_constants import ApplicationConst


class _WelcomeToBeamPageLocator(object):
    _lblWelcomeMessage = (By.XPATH, "//div[@class='alert-box success']")  
    _chkAcceptRisks = (By.ID, "id_accept_risks")
    _btnPlayVideo = (By.XPATH, "//div[@data-testid='big-play-button__button']")
    _lblHeader = (By.XPATH, "//section[@class='masthead info']//h2") 
    _btnSetting = (By.XPATH, "//button[@aria-label='Settings']")
    _btnSpeed = (By.XPATH, "//button[@aria-label='Speed']")
    _btnSpeedx2 = (By.XPATH, "//button[@aria-label='2x']")
    
    @staticmethod
    def _btnContinue():
        return (By.XPATH, u"//button[.=\"{}\"]".format(ApplicationConst.LBL_PLAY_VIDEO_CONTINUE))
    
class WelcomeToBeamPage(UserTemplatePage):
    """
    @description: This is page object class for Welcome To Beam page. 
        This page will be opened after setting up password for new User.
        Please visit https://staging.suitabletech.com/welcome/ for more details.
    @page: Welcome To Beam page
    @author: Thanh Le
    """
    
    
    """    Properties    """    
    @property
    def _lblHeader(self):
        return Element(self._driver, *_WelcomeToBeamPageLocator._lblHeader)    
    @property
    def _lblWelcomeMessage(self):
        return Element(self._driver, *_WelcomeToBeamPageLocator._lblWelcomeMessage)  
    @property
    def _chkAcceptRisks(self):
        return Element(self._driver, *_WelcomeToBeamPageLocator._chkAcceptRisks)  
    @property
    def _btnContinue(self):
        return Element(self._driver, *_WelcomeToBeamPageLocator._btnContinue())  
    @property
    def _btnPlayVideo(self):
        return Element(self._driver, *_WelcomeToBeamPageLocator._btnPlayVideo)
    @property
    def _btnSetting(self):
        return Element(self._driver, *_WelcomeToBeamPageLocator._btnSetting)
    @property
    def _btnSpeed(self):
        return Element(self._driver, *_WelcomeToBeamPageLocator._btnSpeed)
    @property
    def _btnSpeedx2(self):
        return Element(self._driver, *_WelcomeToBeamPageLocator._btnSpeedx2)
    
    
    """    Methods    """
    def __init__(self, driver):
        """      
        @summary: Constructor method    
        @param driver: Web Driver 
        @author: Thanh Le
        """         
        UserTemplatePage.__init__(self, driver)
        self._lblHeader.wait_until_displayed()
        #This is to make sure the page loads complete. Please consider when removing.
        self._btnPlayVideo.wait_until_displayed()
        
                        
    def is_welcome_user_page_displayed(self, wait_time = None):
        """
        @summary: Check if welcome user page is displayed
        @author: Thanh Le
        @parameter:<wait_time>: waiting time
        @return: True if welcome user page is displayed, False for vice versa
        """
        return (self._lblHeader.is_displayed(wait_time) and self._btnPlayVideo.is_displayed(wait_time))
    
    
    def get_welcome_message(self):
        """
        @summary: This action use to get welcome message text
        @author: Thanh Le
        @return: welcome message
        """
        return self._lblWelcomeMessage.text.split('\n')[1]
    
    
    def watch_video(self):
        """
        @summary: This action use to run introduction video
        @author: Thanh Le
        @return: AdminHomePage page object
        """
        self._chkAcceptRisks.click()
        self._btnPlayVideo.click()
        self.set_double_speed()
        self._btnContinue.wait_until_displayed(600)
        self._btnContinue.click()
        from pages.suitable_tech.admin.advanced.admin_home_page import AdminHomePage
        return AdminHomePage(self._driver)
    
    
    def set_double_speed(self):
        self._btnSetting.click()
        self._btnSpeed.click()
        self._btnSpeedx2.click()
        return self
        
        
