from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from pages.basepage import BasePage
from common.application_constants import ApplicationConst
from pages.suitable_tech.user.user_template_page import UserTemplatePage


class _ConfirmAssociationPageLocator(object):
    _txtfFormContent = (By.XPATH, "//section[@class='content']//form[@method='post']")  
    _lblChatboxHeader = (By.XPATH, "//div[@id='habla_topbar_div']")
    
    @staticmethod
    def _lblheader():
        return (By.XPATH, u"//h2[.=\"{}\"]".format(ApplicationConst.LBL_CONFIRM_NEW_AUTH_METHOD))
    @staticmethod
    def _btnYes():
        return (By.XPATH, u"//button[.=\"{}\"]".format(ApplicationConst.LBL_YES))
    @staticmethod
    def _btnNo():
        return (By.XPATH, "//a[@href='/accounts/login/']")

    
class ConfirmAssociationPage(UserTemplatePage):
    """
    @description: This is page object class for Confirm Associate page.
    @page: Confirm Association Page 
    @author: Thanh Le
    """
    
    
    """    Properties    """  
    @property
    def _lblheader(self):
        return Element(self._driver, *_ConfirmAssociationPageLocator._lblheader())
    @property
    def _btnYes(self):
        return Element(self._driver, *_ConfirmAssociationPageLocator._btnYes())
    @property
    def _btnNo(self):
        return Element(self._driver, *_ConfirmAssociationPageLocator._btnNo())
    @property
    def _txtfFormContent(self):
        return Element(self._driver, *_ConfirmAssociationPageLocator._txtfFormContent)
    @property
    def _lblChatboxHeader(self):
        return Element(self._driver, *_ConfirmAssociationPageLocator._lblChatboxHeader)
    
    
        """    Methods    """
    def __init__(self, driver):
        """      
        @summary: Constructor method    
        @param driver: Web Driver
        @author: Duy Nguyen
        """     
        BasePage.__init__(self, driver)
        self._lblChatboxHeader.wait_until_displayed()
        
        
    def is_confirm_new_auth_message_displayed(self):
        """
        @summary: Check if confirm new auth message is displayed or not
        @return: True if confirm new auth message is displayed, False: the message is not displayed
        @author: Duy Nguyen
        """
        return self._lblheader.is_displayed(10)
    
    
    def change_auth(self, value=True):
        """      
        @summary: This action is used to set an account become GSSO authentication
        @param value: boolean value to decide change GSSO authentication or not
        @return WelcomeToBeamPage if True, LoginPage if False
        @author: Duy Nguyen
        """ 
        if (value):
            self._btnYes.click()
            from pages.suitable_tech.user.welcome_to_beam_page import WelcomeToBeamPage
            return WelcomeToBeamPage(self._driver)
        else:
            self._btnNo.click()
            from pages.suitable_tech.user.login_page import LoginPage
            return LoginPage(self._driver)
            
    
    def accept_change_authentication(self):
        self._btnYes.click()
        from pages.suitable_tech.user.welcome_to_beam_page import WelcomeToBeamPage
        return WelcomeToBeamPage(self._driver)
    
    
    def decline_change_authentication(self):
        self._btnNo.click()
        from pages.suitable_tech.user.login_page import LoginPage
        return LoginPage(self._driver)
        
    
    def get_warning_message(self):
        """      
        @summary: This action is used to get warning message text
        @return: warning message text
        @author: Duy Nguyen
        """ 
        return self._txtfFormContent.text


    def is_page_displayed(self, timeout=1):
        return self._btnYes.is_displayed(timeout)
        
        
        
        
        
        