from pages.suitable_tech.user.user_template_page import UserTemplatePage
from selenium.webdriver.common.by import By
from core.webdriver.element import Element


class _PasswordChangeCompletePageLocator(object):
    _lblHeader = (By.XPATH, "//section[@class='masthead registration']//h2")
    

class PasswordChangeCompletePage(UserTemplatePage):
    """
    @description: This is page object class for Password Change Complete page. 
        This page will be opened after changing password in Change Password page.
        Please visit https://staging.suitabletech.com/accounts/password_change/done/ for more details.
    @page: Password Change Complete page
    @author: Thanh Le
    """


    """    Properties    """   
    @property
    def _lblHeader(self):
        return Element(self._driver, *_PasswordChangeCompletePageLocator._lblHeader) 
    
    
    """    Methods    """
    def __init__(self, driver):    
        """      
        @summary: Constructor method    
        @param driver: Web Driver
        @author: Thanh Le
        """        
        UserTemplatePage.__init__(self, driver)   
        self._lblHeader.wait_until_displayed()