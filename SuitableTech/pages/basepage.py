
class BasePage(object):
    """
    @description: This class is ONLY for inheriting.
    @page: Base of all pages
    @author: Thanh Le
    """


    def __init__(self, driver):
        """
        @summary: Constructor method  
        @parameter: <driver> : web driver
        @author: Thanh Le
        """
        self._driver = driver


    def get_dialog_message(self, close_dialog=True):
        """
        @summary: This action use to get web driver dialog message
        @parameter: <close_dialog>: close or keep dialogs with Boolean value
        @author: Thanh Le    
        @return: dialog message
        """
        return self._driver.get_dialog_message(close_dialog)
    
    
    def goto_request_access_page(self, request_access_link):
        """
        @summary: This action use to go to Request Beam Access page
        @parameter: <request_access_link>: url link to go Request Beam Access Page
        @author: Thanh Le
        @return: RequestBeamAccessPage     
        """
        self._driver.get(request_access_link)
        from pages.suitable_tech.user.request_beam_access import RequestBeamAccessPage
        return RequestBeamAccessPage(self._driver)