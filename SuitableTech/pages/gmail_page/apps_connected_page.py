from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from pages.basepage import BasePage
from common.constant import Constant
from selenium.webdriver.common.keys import Keys


class _AppsConnectedPageLocator(object):
    _lnkBeamApp = (By.XPATH, "//div[@role='listitem']//div[starts-with(@aria-label, 'Beam')]")
    _btnRemoveBeamApp = (By.XPATH, "//div[@role='listitem']//div[starts-with(@aria-label, 'Beam')]/..//span[.='Remove']")
    _btnOK = (By.XPATH, "//span[.='OK']/../..")
    _lblRemoveSuccess = (By.XPATH, "//div[@aria-live='assertive' and contains(., 'Beam')]")
    

class AppsConnectedPage(BasePage):
    """
    @description: This is page object class for Sign In Google page.
    @page: GmailSignInPage
    @author: Duy Nguyen
    """   
    
    
    """ Property """
    @property
    def _lnkBeamApp(self):
        return Element(self._driver, *_AppsConnectedPageLocator._lnkBeamApp)
    @property
    def _btnRemoveBeamApp(self):
        return Element(self._driver, *_AppsConnectedPageLocator._btnRemoveBeamApp)
    @property
    def _btnOK(self):
        return Element(self._driver, *_AppsConnectedPageLocator._btnOK)
    @property
    def _lblRemoveSuccess(self):
        return Element(self._driver, *_AppsConnectedPageLocator._lblRemoveSuccess)
    
    
    
        """    Methods    """
    def __init__(self, driver):
        """
        @summary: Constructor method  
        @parameter: <driver> : web driver
        @author: Duy Nguyen
        """
        BasePage.__init__(self, driver)
    
    
    
    def open(self):
        """
        @Author: Thanh Le
        @Description: use to navigate to Apps connected page of google account
        @return: GmailSignInPage page object
        """
        self._driver.get(Constant.AppsConnectedURL)
        return self
        
        
    def remove_beam_from_connected_apps(self):
        self._lnkBeamApp.wait_until_clickable().click()
        self._btnRemoveBeamApp.wait_until_clickable().click()
        self._btnOK.wait_until_displayed(10)
        self._driver.wrapped_driver.switch_to_active_element().send_keys(Keys.ENTER)
        self._driver.wrapped_driver.switch_to_active_element().send_keys(Keys.ENTER)
#         self._btnOK
#         sleep(10)
#         self._btnOK.wait_until_displayed().wait_until_clickable().click()
        self._lblRemoveSuccess.wait_until_displayed()
#         self._lblRemoveSuccess.wait_until_disappeared()
        return self
        
        
    def is_beam_connected(self):
        return self._lnkBeamApp.is_displayed(2)
        
        
        
        
        
        
        
        
        
        
        
        
        
        