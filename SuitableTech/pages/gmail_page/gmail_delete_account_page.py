from pages.basepage import BasePage
from core.webdriver.element import Element
from selenium.webdriver.common.by import By


class _GmailDeleteAccountPageLocator(object):
    _lblFirstAgreement = (By.XPATH, "//label[@for='i1']")
    _lblSecondAgreement = (By.XPATH,"//label[@for='i2']")
        
    @staticmethod
    def _btnDeleteAccount():
        return (By.XPATH,"//div[@class='O Ya rb pa']")
    
    
class GmailDeleteAccountPage(BasePage):
    """
    @description: This is page object class for Delete Account Google page.
    @page: Account Preference Page
    @author: Duy Nguyen
    """
    
    """    Properties    """  
    @property
    def _lblFirstAgreement(self):
        return Element(self._driver, *_GmailDeleteAccountPageLocator._lblFirstAgreement)
    @property
    def _lblSecondAgreement(self):
        return Element(self._driver, *_GmailDeleteAccountPageLocator._lblSecondAgreement)
    @property
    def _btnDeleteAccount(self):
        return Element(self._driver, *_GmailDeleteAccountPageLocator._btnDeleteAccount())
    
    
    """    Methods    """
    def __init__(self, driver):
        """
        @summary: Constructor method  
        @parameter: <driver> : web driver
        @author: Duy Nguyen
        """
        BasePage.__init__(self, driver)


    def complete_delete_account(self):
        """
        @summary: This action use to click on 2 confirm checkbox in order to complete deleting google account.
        @parameter: None
        @author: Duy Nguyen      
        """
        self._lblFirstAgreement.click()
        self._lblSecondAgreement.click()
        self._btnDeleteAccount.click()