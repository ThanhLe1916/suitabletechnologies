from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from pages.basepage import BasePage
from common.application_constants import ApplicationConst
from common.constant import Constant
from pages.suitable_tech.user.confirm_association_page import ConfirmAssociationPage
from pages.gmail_page.allow_association_page import AllowAssociationPage


class _GmailSignInPageLocator(object):
    _txtUserName = (By.XPATH, "//input[@name='Email']")
    _btnNext = (By.XPATH, "//input[@value='Next']")
    _txtPassword = (By.XPATH, "//input[@name='Passwd']")
    _btnSignin = (By.XPATH, "//input[@value='Sign in']")
    _chkKeepLogin = (By.XPATH, "//label[@class='remember']/descendant::input[@id='PersistentCookie']")

    @staticmethod
    def _lblErrorMessage():
        return (By.XPATH, "//div[@class='input-wrapper focused']/descendant::span[contains(text(),\"{}\")]".format(ApplicationConst.LBL_ERROR_MESSAGE))
        
    @staticmethod
    def _lnkAccount(value):
        return (By.XPATH, u"//span[.='{}']/..".format(value))


class GmailSignInPage(BasePage):
    """
    @description: This is page object class for Sign In Google page.
    @page: GmailSignInPage
    @author: Duy Nguyen
    """   
    
    
    """ Property """
    @property
    def _txtUserName(self):
        return Element(self._driver, *_GmailSignInPageLocator._txtUserName)
    @property
    def _btnNext(self):
        return Element(self._driver, *_GmailSignInPageLocator._btnNext)
    @property
    def _txtPassword(self):
        return Element(self._driver, *_GmailSignInPageLocator._txtPassword)
    @property
    def _btnSignin(self):
        return Element(self._driver, *_GmailSignInPageLocator._btnSignin)
    @property
    def _chkKeepLogin(self):
        return Element(self._driver, *_GmailSignInPageLocator._chkKeepLogin)
    @property
    def _lblErrorMessage(self):
        return Element(self._driver, *_GmailSignInPageLocator._lblErrorMessage())    
    
    def _lnkAccount(self, value):
        return Element(self._driver, *_GmailSignInPageLocator._lnkAccount(value))
    
    
        """    Methods    """
    def __init__(self, driver):
        """
        @summary: Constructor method  
        @parameter: <driver> : web driver
        @author: Duy Nguyen
        """
        BasePage.__init__(self, driver)
    
    
    def sign_in_exist_account(self, email, password=None, keepMeLoggedIn=False):
        """
        @summary: This action use to submit username, password or click on existing google account.
        @note: This action is used when tester want to handle authenticate both existing account and newly account
        @parameter: <email>: google account
                    <password>: password
                    <keepMeLoggedIn>: keep remember account checkbox.
        @author: Duy Nguyen    
        """
        if (self._lnkAccount(email).is_displayed(5)):
            self._lnkAccount(email).click()
        else:
            self.sign_in_gmail(email, password, keepMeLoggedIn)
    
    
    def is_reenter_password(self, email):
        """
        @summary: This action is used as a checkpoint for specific testcase's verify point
        @parameter: <email>: google account
        @author: Khoi Nguyen
        @return: Boolean value
        """
        self._txtUserName.type(email)
        self._btnNext.click()
        self._txtPassword.wait_until_displayed(5)
        return self._txtPassword.is_displayed()
       
        
    def sign_in_gmail(self, email, password, keepMeLoggedIn=False):
        """
        @summary: This action is used for inputting email and password textbox
        @parameter: <email>: google account
                    <password>: password
                    <keepMeLoggedIn>: keep remember account checkbox.
        @author: Duy Nguyen
        """
        
        self._txtUserName.type(email)
        self._btnNext.click()
        self._txtPassword.type(password)
        if(keepMeLoggedIn):
            self._chkKeepLogin.check()
        else:
            self._chkKeepLogin.uncheck()  
        self._btnSignin.click()
        
        
    def open(self):
        """
        @Author: Duy Nguyen
        @Description: use to navigate directly to Google SignIn Page
        @return: GmailSignInPage page object
        """
        self._driver.get(Constant.GoogleSignInURL)
        return self
    
    
    def log_in_gmail(self, email, password, keepMeLoggedIn=False):
        """
        @Author: Duy Nguyen
        @Description: Action to work only when navigate directly to Google Sign in Page. It return object as Google Introduction Page"
        @parameter: <email>: google account
                    <password>: password
                    <keepMeLoggedIn>: keep remember account checkbox.
        @return: GmailIntroPage page object
        """
        self._txtUserName.type(email)
        self._btnNext.click()
        self._txtPassword.wait_until_displayed(5)
        self._txtPassword.type(password)
        if(keepMeLoggedIn):
            self._chkKeepLogin.check()
        else:
            self._chkKeepLogin.uncheck()  
        self._btnSignin.click()
        from pages.gmail_page.gmail_intro_page import GmailIntroPage
        return GmailIntroPage(self._driver)
    
    
    def sign_in_with_expecting_error_google_account(self, email):
        """
        @Author: Duy Nguyen
        @Description: This action is used to work when we sign-in with expected error of google account.
        @parameter: <email>: google account
        @return: itself
        """
        self._txtUserName.type(email)
        self._btnNext.click()
        return self
    
    
    def is_google_error_message_displayed(self):
        """
        @Description: This action is used as a checkpoint for specific testcase's verify point.
        @return: True: the google error message is displayed
                False: the google error message is not displayed
        @Author: Duy Nguyen
        """
        return self._lblErrorMessage.is_displayed()

    
    def sign_in_with_non_gsso_account(self, email, password, keepMeLoggedIn=False):
        self.submit_sign_in_credential(email, password, keepMeLoggedIn)
        return ConfirmAssociationPage(self._driver)
        
    
    def sign_in_with_fresh_new_non_gsso_account(self, email, password, keepMeLoggedIn=False):
        self.submit_sign_in_credential(email, password, keepMeLoggedIn)
        return AllowAssociationPage(self._driver)
    
    
    def submit_sign_in_credential(self, email, password, keepMeLoggedIn=False):
        self._txtUserName.type(email)
        self._btnNext.click()
        self._txtPassword.type(password)
        if(keepMeLoggedIn):
            self._chkKeepLogin.check()
        else:
            self._chkKeepLogin.uncheck()  
        self._btnSignin.click()
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        