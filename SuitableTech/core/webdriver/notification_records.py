from selenium.webdriver.common.by import By
from core.webdriver.element_list import ElementList
from common.application_constants import ApplicationConst

class NotificationRecords(ElementList):
    
    def __init__(self, driver, by=By.XPATH, locator_value=None):
        ElementList.__init__(self, driver, by, locator_value)
        
    
    def does_record_exist(self, user):
        try:
            expected_text = ApplicationConst.LBL_ACCESS_REQUEST_RECORD_MESSAGE\
                                .replace("{fn}", user.first_name)\
                                .replace("{ln}", user.last_name)\
                                .replace("{em}", user.email_address)\
                                .format(user.device_group)\
                                .strip()
                
            elements = self._get_elements()
            if(elements == None):
                raise Exception("Element list is not found!")
            else:
                for w_elem in elements:
                    displayed_text = w_elem.find_element(By.XPATH, ".//div[@class='media-body']//translate").text
                    if displayed_text == expected_text:
                        return True
                    
        except Exception as ex:
            raise Exception("Element is not found. Error: {}".format(ex))
        
        return False
    
    
