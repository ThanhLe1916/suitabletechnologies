from datetime import datetime
import re, platform, os

from selenium import webdriver
from selenium.common.exceptions import StaleElementReferenceException, TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from common.constant import Browser, Constant, Language
from common.helper import Helper, EmailDetailHelper
from common.stopwatch import Stopwatch
from core.webdriver.driver_mac import Driver_MAC
from core.webdriver.driver_windows import Driver_Windows
POLL_FREQUENCY = 1

class Driver(object):
    
    _driver = None
    _driverSetting = None
    browser = ""
    tc_name = ""
    folder_screen_shot = ""
    sep = os.path.sep
    def __init__(self, driverSetting):
        self._driverSetting = driverSetting
        os = platform.platform()
        if(driverSetting.hub_url == None):  # Local WebDriver
            if ("Darwin" in os):
                self._driver = Driver_MAC().get_driver(driverSetting)
            if ("Windows" in os):
                self._driver = Driver_Windows().get_driver(driverSetting)
        else:  # Remote WebDriver
            if(driverSetting.browser_name == Browser.Firefox):
                capabilities = DesiredCapabilities.FIREFOX.copy()
            elif(driverSetting.browser_name == Browser.IE):
                capabilities = DesiredCapabilities.INTERNETEXPLORER.copy()
                self.browser = "Internet Explorer"
            elif(driverSetting.browser_name == Browser.Edge):
                capabilities = DesiredCapabilities.EDGE.copy()
                self.browser = "MicrosoftEdge"
            elif(driverSetting.browser_name == Browser.Safari):
                self.browser = "Safari"
                capabilities = DesiredCapabilities.SAFARI.copy()
            elif(driverSetting.browser_name == Browser.Chrome):
                self.browser = "Chrome"
                capabilities = DesiredCapabilities.CHROME.copy()
            
            capabilities['platform'] = driverSetting.platform
            '''capabilities["native_events", False]'''
            self._driver = webdriver.Remote(command_executor=driverSetting.hub_url, desired_capabilities=capabilities)
        
        self.set_page_load_timeout(driverSetting.page_wait_timeout)

        try:
            self.delete_all_cookies()
        except:
            pass
        
        from core.i18n.i18n_support import I18NSupport, I18NLanguage
        # set testing language
        if self._driverSetting.language == Language.JAPANESE:
            I18NSupport.set_language(I18NLanguage.ja_JP)
        elif self._driverSetting.language == Language.FRENCH:
            I18NSupport.set_language(I18NLanguage.fr_FR)
        else:
            I18NSupport.set_language(I18NLanguage.default)
            
        EmailDetailHelper.set_language(self._driverSetting.language)    
                    
        # Update run environment file at data\RunEnv.txt
        self._update_run_env(driverSetting)
    
    """ Properties """
    @property
    def wrapped_driver(self):
        return self._driver
    @property
    def title(self):
        return self._driver.title
    @property
    def current_url(self):
        return self._driver.current_url
    @property
    def window_handles(self):
        return self._driver.window_handles
    @property
    def driverSetting(self):
        return self._driverSetting
    
    """All Web_driver's methods"""
    def _update_run_env(self, driverSetting):
        
        # Get browser name & version
        os_name = machine_name = browser = ""
        
        if(driverSetting.hub_url == None):
            os_name = platform.platform()
            machine_name = platform.node()
            version_key = "version"
            for key in self._driver.capabilities:
                if ("version" in key.lower()):
                    version_key = key
            browser = str(self._driver.capabilities["browserName"]).capitalize() + ", version " + self._driver.capabilities[version_key]
        else:
            sys_info = self.execute_script("return window.navigator.userAgent")
            os_name = re.search("\((.+?)\;", sys_info).group(1)
            if (os_name == "Windows NT 6.1"):
                os_name = "Windows 7"
            try:
                if(self.browser == "Safari"):
                    version = re.search("Version/(.+?) ", sys_info).group(1)
                else:
                    version = re.search("rv:(.+?)\)", sys_info).group(1)
            except:
                version = "Unknown"
            
            try:
                browser = self.browser + ", version " + version
            except:
                browser = "Unknown"
        # Write information
        self.write_information(os_name, machine_name, browser)
    
    
    def write_information(self, os_name, machine_name, browser):
        delim = "="
        new_line = "\n"        
        file_env_path = Helper.base_dir() + os.path.sep + Constant.RunEnvironmentFile
        file_env = open(file_env_path, 'r+')
        if os.stat(file_env_path).st_size == 0:
            folder_name = "screen_shot_" + datetime.now().strftime('%b %d %Y %H-%M-%S')
            self.folder_screen_shot = Helper.base_dir() + "\\test\\test_run\\test_result\\".replace("\\", self.sep) + folder_name
            file_env.truncate()
            file_env.write("os" + delim + os_name + new_line)
            file_env.write("machine" + delim + machine_name + new_line)
            file_env.write("browser" + delim + browser + new_line)
            file_env.write("language" + delim + self._driverSetting.language + new_line)
            file_env.write("screen_shot" + delim + self.folder_screen_shot)
        else:
            with file_env as fp:
                for line in fp:
                    if "screen_shot=" in line:
                        self.folder_screen_shot = line.split("=")[1]
        file_env.close()
    
    
    def execute_script(self, script, *args):
        result = self._driver.execute_script(script, *args)
        return result
    
    
    def get(self, url):
        self._driver.get(url)
        
    
    def find_element(self, by=By.ID, value=None, timeout=None):    
        if(timeout == None):
            timeout = self._driverSetting.element_wait_timeout
            
        sw = Stopwatch()
        sw.start()
        
        while(timeout > 0):
            try:
                return WebDriverWait(self._driver, timeout, POLL_FREQUENCY).until(EC.visibility_of_element_located((by, value)))
            except StaleElementReferenceException:
                timeout -= sw.elapsed().total_seconds()
            except TimeoutException:
                print("Element {{By: {}, Value: {}}} is not displayed after waiting for {} seconds".format(by, value, timeout))
                break
            except Exception as ex:
                print("There was an exception when finding Element {{By: {}, Value: {}}}. \nException: {}".format(by, value, str(ex)))
                break
        
        return None
    
    
    def find_elements(self, by=By.ID, value=None, timeout=None):
        if(timeout == None):
            timeout = self._driverSetting.element_wait_timeout
            
        sw = Stopwatch()
        sw.start()
        
        while(timeout > 0):
            try:
                return WebDriverWait(self._driver, timeout, POLL_FREQUENCY).until(EC.presence_of_all_elements_located((by, value)))                           
            except StaleElementReferenceException:               
                timeout -= sw.elapsed().total_seconds()
            except TimeoutException:
                print("Elements {{By: {}, Value: {}}} are not displayed after waiting for {} seconds".format(by, value, timeout))
                break
            except Exception as ex:
                print("There was an exception when finding Elements {{By: {}, Value: {}}}. \nException: {}".format(by, value, str(ex)))
                break
        
        return None
    
    
    def wait_for_element_invisible(self, by=By.ID, value=None, timeout=None):
        self.is_element_invisible(by, value, timeout)
    
    
    def wait_for_element_visible(self, by=By.ID, value=None, timeout=None):
        self.is_element_visible(by, value, timeout)
    
    
    def wait_for_element_clickable(self, by=By.ID, value=None, timeout=None):
        self.is_element_clickable(by, value, timeout)

    
    def is_element_visible(self, by=By.ID, value=None, timeout=None):
        if(timeout == None):
            timeout = self._driverSetting.element_wait_timeout
        
        sw = Stopwatch()
        sw.start()
        
        result = None
        while(timeout > 0):
            try:
                result = WebDriverWait(self._driver, timeout, POLL_FREQUENCY).until(EC.visibility_of_element_located((by, value)))
                break
            except StaleElementReferenceException:
                timeout -= sw.elapsed().total_seconds()
            except Exception:
                break
        
        return (result != None)
        
    
    def is_element_invisible(self, by=By.ID, value=None, timeout=None):
        if(timeout == None):
            timeout = self._driverSetting.element_wait_timeout
        
        sw = Stopwatch()
        sw.start()
        
        result = None
        while(timeout > 0):
            try:
                result = WebDriverWait(self._driver, timeout, POLL_FREQUENCY).until(EC.invisibility_of_element_located((by, value)))
                break
            except Exception:
                break
        
        return (result == True)
        
        
    def is_element_clickable(self, by=By.ID, value=None, timeout=None):
        if(timeout == None):
            timeout = self._driverSetting.element_wait_timeout
        
        sw = Stopwatch()
        sw.start()
        
        result = None
        while(timeout > 0):
            try:
                result = WebDriverWait(self._driver, timeout, POLL_FREQUENCY).until(EC.element_to_be_clickable((by, value)))
                break
            except StaleElementReferenceException:
                timeout -= sw.elapsed().total_seconds()
            except Exception:
                break
        
        return (result != None)
    
    
    def get_dialog_message(self, close_dialog=True):
        result = self._driver.switch_to_alert().text
        
        if(close_dialog == True):
            self.handle_dialog(close_dialog)
        
        return result    
    
    
    def handle_dialog(self, accept=False):
        if(accept == True):
            self._driver.switch_to_alert().accept()
        else:
            self._driver.switch_to_alert().dismiss()
    
    
    def get_text_handle_dialog(self):
        return self._driver.switch_to_alert().text
    
    
    def maximize_window(self):
        self._driver.maximize_window()
        
        
    def close(self):
        self._driver.close()
        if(self.window_handles):
            self.switch_to_main_window()
    
    
    def refresh(self):
        self._driver.refresh()
    
    
    def quit(self):
        self._driver.quit()
        
    
    def switch_to_alert(self):
        return self._driver.switch_to.alert    
        
        
    def switch_to_main_window(self):
        self.switch_to_window(0)


    def switch_to_window(self, index=0):
        self._driver.switch_to.window(self.window_handles[index])
        self.switch_to_default_content()
    
    
    def switch_to_default_content(self):
        self._driver.switch_to.default_content()
    
    
    def switch_to_frame(self, frame_id):
        self._driver.switch_to.frame(frame_id)    
    
    
    def save_screenshot(self):
        file_name = self.folder_screen_shot + os.path.sep +self.tc_name + ".png"
        dir_name = os.path.dirname(file_name)
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)
        
        self._driver.save_screenshot(file_name)
    
    
    def set_page_load_timeout(self, time_to_wait):
        self._driver.set_page_load_timeout(time_to_wait)
    
    
    def delete_all_cookies(self):
        self._driver.delete_all_cookies()
    
    
    def open_new_tab(self):
        self.execute_script("window.open(\"https://www.google.com/\");")
        self.switch_to_window(len(self.window_handles) - 1)
    
    
    def scroll_down_to_bottom(self):
        self.execute_script("window.scrollTo(0, document.body.scrollHeight)")
        
    
    def scroll_up_to_top(self):
        self.execute_script("window.scrollTo(0, 0)")

    
    def get_cookies(self):
        return self._driver.get_cookies()
    
    
    def get_cookie(self, cookie_name):
        return self._driver.get_cookie(cookie_name)
        
        
        
        
         
