from selenium.webdriver.common.by import By
from core.webdriver.element import Element
from core.webdriver.element_base import ElementBase
from common.stopwatch import Stopwatch
from common.constant import Browser

class DropdownList(ElementBase):
    
    def __init__(self, driver, by=By.XPATH, value=None):       
        ElementBase.__init__(self, driver, by, value)
        
        
    def select_by_text(self, text, click=True):
        """      
        @summary: select an item in SuitableTech dropdown list by text        
        @param text: Text of an item which to be selected
        @author: Thanh Le
        @created_date: August 10, 2016
        """
        self._select_element(self._get_child_item(by="text", value=text), click)
    
    
    def select_by_partial_text(self, text, click=True):
        """      
        @summary: select an item in SuitableTech dropdown list by partial text        
        @param text: Partial text of an item which to be selected
        @author: Thanh Le
        @created_date: August 10, 2016
        """
        self._select_element(self._get_child_item_by_partial_text(text), click)
    
    
    def select_by_href(self, value, click=True):
        """      
        @summary: select an item in SuitableTech dropdown list by text        
        @param text: Text of an item which to be selected
        @author: Thanh Le
        @created_date: August 10, 2016
        """
        self._select_element(self._get_child_item(by="href", value=value), click)
    
              
    def is_item_existed(self, text):
        """      
        @summary: return True if item exists. Otherwise, return False.       
        @param text: Text of an item which to be checked
        @return: Boolean value True/False
        @author: Thanh Le
        @created_date: August 10, 2016
        """
        self.mouse_to()
        self.click()
        return self._get_child_item(value=text).is_displayed()
    
    
    def is_item_not_existed(self, text):
        """      
        @summary: return True if item Not exists. Otherwise, return False.       
        @param text: Text of an item which to be checked
        @return: Boolean value True/False
        @author: Thanh Le
        @created_date: August 10, 2016
        """
        self.mouse_to()
        self.click()
        return self._get_child_item(value=text).is_disappeared()
    
    
    def get_text(self):
        """      
        @summary: return Text currently displays in SuitableTech dropdown list     
        @return: string
        @author: Thanh Le
        @created_date: August 10, 2016
        """
        elem = Element(self._driver, By.XPATH, u"{}".format(self._value))
        elem.wait_until_displayed()
        return elem.text
        
    
    def get_selected_item(self):
        """      
        @summary: return Text of selected item in SuitableTech dropdown list     
        @return: string
        @author: Thanh Le
        @created_date: August 10, 2016
        """
        return Element(self._driver, By.XPATH, u"{}/a".format(self._value)).text
    
    
    def _get_child_item(self, by="text", value=""):
        """
        @param by: "text", "href"
        """
        child_xpath = u""
        if(by=="text"):
            child_xpath = u"{}//li//a[.=\"{}\"]".format(self._value, value)
        elif(by=="href"):
            child_xpath = u"{}//a[@href='{}']".format(self._value, value)
            
        return Element(self._driver, By.XPATH, child_xpath)
    
    
    def _get_child_item_by_partial_text(self, text):
        return Element(self._driver, By.XPATH, u"{}//li//*[contains(., \"{}\")]".format(self._value, text))
    
    
    def _select_element(self, element, click=True):
        is_dropdown_displayed = self.is_displayed()
        
        sw = Stopwatch()
        sw.start()
        
        if(is_dropdown_displayed):
            can_be_clicked = False
            
            while(can_be_clicked == False and sw.elapsed().total_seconds() < self._timeout): 
                if(self._driver.driverSetting.browser_name == Browser.IE 
                   or self._driver.driverSetting.browser_name == Browser.Edge
                   or self._driver.driverSetting.browser_name == Browser.Firefox):
                    self.jsmouse_to()
                else:
                    self.mouse_to()
                
                if(click):
                    self.click()
                
                can_be_clicked = element.is_clickable(2)
#                 self._timeout -= sw.elapsed().total_seconds()
                
                if(can_be_clicked):
                    if(self._driver.driverSetting.browser_name == Browser.IE 
                       or self._driver.driverSetting.browser_name == Browser.Edge
                       or self._driver.driverSetting.browser_name == Browser.Firefox):
                        element.jsmouse_to()
                    else:
                        element.mouse_to()
                    
                    can_be_clicked = element.is_clickable(2)
#                     self._timeout -= sw.elapsed().total_seconds()
                    
                    if(can_be_clicked):                        
                        if(self._driver.driverSetting.browser_name == Browser.IE 
                           or self._driver.driverSetting.browser_name == Browser.Edge
                           or self._driver.driverSetting.browser_name == Browser.Firefox):
                            element.jsclick()
                        else:
                            element.click()
                        return
            
            self._driver.save_screenshot()
            raise Exception(self._error_msg.format("select item in dropdown list", "dropdown list item cannot be selected", element._locator))
        
        else:
            self._driver.save_screenshot()
            raise Exception(self._error_msg.format("select item in dropdown list", "dropdown list not found", self._locator))            
                        
