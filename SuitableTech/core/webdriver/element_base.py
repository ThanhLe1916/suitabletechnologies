from selenium.webdriver.common.by import By
from common.stopwatch import Stopwatch
from selenium.common.exceptions import WebDriverException, NoSuchWindowException
from selenium.webdriver.common.action_chains import ActionChains
from common.constant import Browser
import pyautogui

class ElementBase(object):
    _action_msg = "{} element {}"
    _error_msg = "{} failed because {}. Element's locator {}"
    
    """ Methods """
    def __init__(self, driver, by=By.XPATH, value=None):        
        self._by = by
        self._value = value
        self._driver = driver        
        self._timeout = self._driver.driverSetting.element_wait_timeout
        self._element = None
        self._locator = "{{By: {}, Value: {}}}".format(by, value)
    
    
    @property
    def native_element(self):
        return self._get_element(self._timeout)
    
    
    def _get_element(self, timeout=None):
        return self._driver.find_element(self._by, self._value, timeout)
    
    
    def click(self):
        print(self._action_msg.format("Click", self._locator))
        sw = Stopwatch()
        sw.start()
        while(self._timeout > 0):
            try:
                elem = self._get_element(self._timeout)
                if(elem==None):
                    self._driver.save_screenshot()
                    raise Exception(self._error_msg.format("click", "the Element is None", self._locator))
                else:
                    elem.click() 
                    break
            except WebDriverException as wdex:
                print("********** Error while clicking Element: " + str(wdex))
                self.wait_until_clickable(2)
                self._timeout -= sw.elapsed().total_seconds()
            except NoSuchWindowException:
                raise Exception("********** Cannot interact with with Element {} because the window is closed.".format(self._locator))
            except Exception as ex:
                self._driver.save_screenshot()
                raise Exception("********** There was an exception when clicking Element {}. \nException: {}".format(self._locator, str(ex)))
    
    
    def jsclick(self):
        print(self._action_msg.format("Java-script click", self._locator))
        sw = Stopwatch()
        sw.start()
        while(self._timeout > 0):
            try:
                elem = self._get_element(self._timeout)
                if(elem==None):
                    self._driver.save_screenshot()
                    raise Exception(self._error_msg.format("Java-script click", "the Element is None", self._locator))
                else:
                    self._driver.execute_script("arguments[0].click();", elem)
                    break
            except WebDriverException as wdex:
                print("********** Error while Java-script click: " + str(wdex))
                self._timeout -= sw.elapsed().total_seconds()
            except NoSuchWindowException:
                raise Exception("********** Cannot interact with with Element {} because the window is closed.".format(self._locator))
            except Exception as ex:
                self._driver.save_screenshot()
                raise Exception("********** There was an exception when Java-script clicking Element {}. \nException: {}".format(self._locator, str(ex)))
    
  
    def mouse_to(self):
        print(self._action_msg.format("Mouse-to ", self._locator))
        sw = Stopwatch()
        sw.start()
        while(self._timeout > 0):
            try:
                elem = self._get_element(self._timeout)
                if(elem==None):
                    self._driver.save_screenshot()
                    raise Exception(self._error_msg.format("Mouse-to", "the Element is None", self._locator))
                else:
                    
                    if(self._driver.driverSetting.browser_name == Browser.Safari):
                        x = elem.location["x"]
                        y = elem.location["y"]
                        width = elem.size["width"]
                        height = elem.size["height"]                        
                        x_click_point = (x + width/2)
                        y_click_point = (y + height*2 + 10)
                        pyautogui.click(x_click_point, y_click_point)
                        
                    else:
                        ActionChains(self._driver.wrapped_driver).move_to_element(elem).perform()
                    break
            except WebDriverException as wdex:
                print("********** Error while Mouse-to: " + str(wdex))
                self._timeout -= sw.elapsed().total_seconds()
            except NoSuchWindowException:
                raise Exception("********** Cannot interact with with Element {} because the window is closed.".format(self._locator))
            except Exception as ex:
                self._driver.save_screenshot()
                raise Exception("********** There was an exception when moving mouse to Element {}. \nException: {}".format(self._locator, str(ex)))
    
    
    def jsmouse_to(self):
        print(self._action_msg.format("Java-script mouse_to", self._locator))
        sw = Stopwatch()
        sw.start()
        while(self._timeout > 0):
            try:
                elem = self._get_element(self._timeout)
                if(elem==None):
                    self._driver.save_screenshot()
                    raise Exception(self._error_msg.format("Java-script mouse_to", "the Element is None", self._locator))
                else:
                    mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}"
                    self._driver.execute_script(mouseOverScript, elem)
                    break
            except WebDriverException as wdex:
                print("********** Error while Java-script mouse_to: " + str(wdex))
                self._timeout -= sw.elapsed().total_seconds()
            except NoSuchWindowException:
                raise Exception("********** Cannot interact with with Element {} because the window is closed.".format(self._locator))
            except Exception as ex:
                self._driver.save_screenshot()
                raise Exception("********** There was an exception when moving mouse using Java-script to Element {}. \nException: {}".format(self._locator, str(ex)))

    
    def is_enabled(self, wait_time_out=None):
        print(self._action_msg.format("Get is_enabled attribute", self._locator))
        try:
            if(wait_time_out==None):
                wait_time_out = self._timeout
            
            elem = self._get_element(wait_time_out)
            if(elem==None):
                return False
            else:
                return elem.is_enabled()
        except Exception:
            return False
    
    
    def is_clickable(self, wait_time_out=None):
        print(self._action_msg.format("Get is_clickable attribute", self._locator))
        try:
            if(wait_time_out==None):
                wait_time_out = self._timeout
            return self._driver.is_element_clickable(self._by, self._value, wait_time_out)
        except Exception:
            return False
        
    
    def is_displayed(self, wait_time_out=None):
        print(self._action_msg.format("Get is_displayed attribute", self._locator))
        try:
            if(wait_time_out==None):
                wait_time_out = self._timeout
            return self._driver.is_element_visible(self._by, self._value, wait_time_out)
        except Exception:
            return False  
    
    
    def is_disappeared(self, wait_time_out=None):
        print(self._action_msg.format("Get is_disappeared attribute", self._locator))
        try:
            if(wait_time_out==None):
                wait_time_out = self._timeout
                
            return self._driver.is_element_invisible(self._by, self._value, wait_time_out)
        except Exception:
            return False
    
     
    def find_element(self, by=By.XPATH, value=None):
        print(self._action_msg.format("Find child element", self._locator))
        
        if(self._by==By.XPATH):
            return self._driver.find_element(self._by, self._value + value, self._timeout)
        else:
            try:
                elem = self._get_element(self._timeout)
                return elem.find_element(by, value)
            except:
                return None
        
    
    def wait_until_displayed(self, wait_time_out=None):
        print(self._action_msg.format("Wait until displayed", self._locator))
        if(wait_time_out==None):
            wait_time_out = self._timeout
        self._driver.wait_for_element_visible(self._by, self._value, wait_time_out)
        return self
    
      
    def wait_until_disappeared(self, wait_time_out=None):
        print(self._action_msg.format("Wait until disappeared", self._locator))
        if(wait_time_out==None):
            wait_time_out = self._timeout
        self._driver.wait_for_element_invisible(self._by, self._value, wait_time_out)
    
    
    def wait_until_clickable(self, wait_time_out=None):
        print(self._action_msg.format("Wait until clickable", self._locator))
        if(wait_time_out==None):
            wait_time_out = self._timeout
        self._driver.wait_for_element_clickable(self._by, self._value, wait_time_out)
        return self


    def scroll_to(self):
        print(self._action_msg.format("Java-script Scroll to", self._locator))
        sw = Stopwatch()
        sw.start()
        while(self._timeout > 0):
            try:
                elem = self._get_element(self._timeout)
                if(elem==None):
                    self._driver.save_screenshot()
                    raise Exception(self._error_msg.format("Java-script Scroll to", "the Element is None", self._locator))
                else:
                    self._driver.execute_script("arguments[0].scrollIntoView(true);", elem)
                    break
            except WebDriverException as wdex:
                print("********** Error while scrolling to Element using Java-script: " + str(wdex))
                self._timeout -= sw.elapsed().total_seconds()
            except NoSuchWindowException:
                raise Exception("********** Cannot interact with with Element {} because the window is closed.".format(self._locator))
            except Exception as ex:
                self._driver.save_screenshot()
                raise Exception("********** There was an exception when scrolling to Element {}. \nException: {}".format(self._locator, str(ex)))


    
    
    