from selenium.webdriver.common.by import By
# from core.webdriver.element import Element

class ElementList(object):
    
    def __init__(self, driver, by=By.XPATH, locator_value=None):
        self._by = by
        self._locator_value = locator_value
        self._driver = driver
        self._time_left = self._driver.driverSetting.element_wait_timeout
        self._elements = []
        
        
    def _get_elements(self, wait_time_out=None):
        elements = self._driver.find_elements(self._by, self._locator_value, wait_time_out)
        
        self._elements.clear()
        
        if elements == None:
            return []

        for element in elements:
            self._elements.append(element)
             
        return self._elements
    
    
    def count(self, wait_time_out=None):
        try:
            elements = self._get_elements(wait_time_out)
            if(elements == None):
                return 0
            else:
                return len(elements) 
        except Exception as ex:
            raise Exception("Cannot count ElementList. Error: {}".format(ex))
    
    
    def get_all_elements(self, wait_time_out=None):
        try:
            elements = self._get_elements()
            if(elements == None):
                raise Exception("Element list is not found!")
            
            return elements
        except Exception as ex:
            raise Exception("Cannot count ElementList. Error: {}".format(ex))
    
    
    def get_element_at(self, index, wait_time_out=None):
        try:
            elements = self._get_elements()
            if(elements == None):
                raise Exception("Element list is not found!")
            else:
                if index < len(elements):
                    return elements[index]
                else:
                    raise Exception("Index is out of range")
        except Exception as ex:
            raise Exception("Element is not found. Error: {}".format(ex))

