import os

from selenium import webdriver

from common.constant import Browser
from common.helper import Helper


class Driver_MAC():
    _driver = None      
    sep = os.path.sep
    def get_driver(self, driverSetting):
        if(driverSetting.hub_url == None):  # Local WebDriver
            if(driverSetting.browser_name == Browser.Firefox):
                self._driver = webdriver.Firefox()
            elif(driverSetting.browser_name == Browser.Safari):
                file_path = Helper.base_dir() + "\\libs\\remote_execution\\selenium-server-standalone-2.53.0.jar".replace("\\", self.sep)
                self._driver = webdriver.Safari(executable_path=file_path, quiet=False)
            elif(driverSetting.browser_name == Browser.Chrome):
                file_path = Helper.base_dir() + "\\libs\\remote_execution\\macchromedriver".replace("\\", self.sep)
                options = webdriver.ChromeOptions()
                options.add_argument("--kiosk")
                options.add_argument("--safebrowsing-disable-download-protection")
                dir_name = Helper.base_dir() + "/data/download"
                if not os.path.exists(dir_name):
                    os.makedirs(dir_name)
                prefs = {"download.default_directory": dir_name, "download.prompt_for_download": False, "safebrowsing.enabled": "true"}
                options.add_experimental_option("prefs", prefs)
                self._driver = webdriver.Chrome(executable_path=file_path, chrome_options=options)
        
        return self._driver 