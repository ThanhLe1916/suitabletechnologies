'''
Created on Jun 15, 2016

@description: This is a wrapper class for the DIV element that represents an editable combobox. 
    When using this class, make sure that you select the correct DIV element, where it contains the 'input' 
    and 'listbox' element.
@author: thanh.viet.le
'''
from selenium.webdriver.common.by import By
from core.webdriver.element import Element

class EditableCombobox(Element):
    
    def __init__(self, driver, by=By.XPATH, value=None):       
        Element.__init__(self, driver, by, value)
        
        
    def select(self, value):
        element = Element(self._driver, self._by, self._value)
        element.click()
        input_elem = Element(self._driver, By.XPATH, "{}//input[@type='search']".format(self._value))
        input_elem.slow_type(value)
        
        item_to_be_selected = Element(self._driver, By.XPATH, u"(//div[@role='option'])[1]//span[.='{}']".format(value))
        item_to_be_selected.wait_until_displayed()
        item_to_be_selected.click()
    
    
        
        
        