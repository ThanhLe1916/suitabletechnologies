from common.constant import Browser, Language
from common.constant import Platform

class DriverSetting(object):

    def __init__(self, browser_name = Browser.Chrome, platform = Platform.WINDOWS, element_wait_timeout = 30, page_wait_timeout = 30, hub_url = None, language = Language.ENGLISH):
        self.browser_name = browser_name
        self.platform = platform
        self.element_wait_timeout = element_wait_timeout
        self.page_wait_timeout = page_wait_timeout
        self.hub_url = hub_url
        self.language = language
        