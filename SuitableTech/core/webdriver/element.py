from selenium.webdriver.common.by import By
from core.webdriver.element_base import ElementBase
from selenium.common.exceptions import WebDriverException, NoSuchWindowException
from common.stopwatch import Stopwatch

class Element(ElementBase):
    
    """ Methods """
    def __init__(self, driver, by=By.XPATH, value=None):       
        ElementBase.__init__(self, driver, by, value) 
        
    
    def submit(self):
        print(self._action_msg.format("Submit", self._locator))
        sw = Stopwatch()
        sw.start()
        while(self._timeout > 0):
            try:
                elem = self._get_element(self._timeout)
                if(elem==None):
                    self._driver.save_screenshot()
                    raise Exception(self._error_msg.format("Submit", "the Element is None", self._locator))
                else:
                    elem.submit()
                    break
            except WebDriverException as wdex:
                print("********** Error while submitting Element: " + str(wdex))
                self._timeout -= sw.elapsed().total_seconds()
            except Exception as ex:
                self._driver.save_screenshot()
                raise Exception("********** There was an exception when submitting Element {}. \nException: {}".format(self._locator, str(ex)))
    
    
    def clear(self):
        print(self._action_msg.format("Clear", self._locator))
        sw = Stopwatch()
        sw.start()
        while(self._timeout > 0):
            try:
                elem = self._get_element(self._timeout)
                if(elem==None):
                    self._driver.save_screenshot()
                    raise Exception(self._error_msg.format("Clear", "the Element is None", self._locator))
                else:
                    elem.clear()
                    break
            except WebDriverException as wdex:
                print("********** Error while clearing Element: " + str(wdex))
                self._timeout -= sw.elapsed().total_seconds()
            except Exception as ex:
                self._driver.save_screenshot()
                raise Exception("********** There was an exception when clearing Element {}. \nException: {}".format(self._locator, str(ex)))

    
    def send_keys(self, *value):
        print(self._action_msg.format("Send keys", self._locator))
        sw = Stopwatch()
        sw.start()
        while(self._timeout > 0):
            try:
                elem = self._get_element(self._timeout)
                if(elem==None):
                    self._driver.save_screenshot()
                    raise Exception(self._error_msg.format("Send keys", "the Element is None", self._locator))
                else:
                    elem.send_keys(*value)
                    break
            except NoSuchWindowException:
                raise Exception("********** Cannot interact with with Element {} because the window is closed.".format(self._locator))
            except WebDriverException as wdex:
                print("********** Error while sending keys to Element: " + str(wdex))
                self._timeout -= sw.elapsed().total_seconds()
            except Exception as ex:
                self._driver.save_screenshot()
                raise Exception("********** There was an exception when sending keys to Element {}. \nException: {}".format(self._locator, str(ex)))

    
    def check(self):
        print(self._action_msg.format("Check", self._locator))
        sw = Stopwatch()
        sw.start()
        while(self._timeout > 0):
            try:
                elem = self._get_element(self._timeout)
                if(elem==None):
                    self._driver.save_screenshot()
                    raise Exception(self._error_msg.format("Check", "the Element is None", self._locator))
                else:
                    if(elem.is_selected() == False):
                        elem.click()
                    break
            except NoSuchWindowException:
                raise Exception("********** Cannot interact with with Element {} because the window is closed.".format(self._locator))
            except WebDriverException as wdex:
                print("********** Error while checking Element: " + str(wdex))
                self._timeout -= sw.elapsed().total_seconds()
            except Exception as ex:
                self._driver.save_screenshot()
                raise Exception("********** There was an exception when checking Element {}. \nException: {}".format(self._locator, str(ex)))


    def uncheck(self):
        print(self._action_msg.format("Uncheck", self._locator))
        sw = Stopwatch()
        sw.start()
        while(self._timeout > 0):
            try:
                elem = self._get_element(self._timeout)
                if(elem==None):
                    self._driver.save_screenshot()
                    raise Exception(self._error_msg.format("Uncheck", "the Element is None", self._locator))
                else:
                    if(elem.is_selected() == True):
                        elem.click()
                    break
            except NoSuchWindowException:
                raise Exception("********** Cannot interact with with Element {} because the window is closed.".format(self._locator))
            except WebDriverException as wdex:
                print("********** Error while unchecking Element: " + str(wdex))
                self._timeout -= sw.elapsed().total_seconds()
            except Exception as ex:
                self._driver.save_screenshot()
                raise Exception("********** There was an exception when unchecking Element {}. \nException: {}".format(self._locator, str(ex)))

    
    def type(self, *value):
        self.clear()
        self.send_keys(*value)
    
    
    def slow_type(self, value):
        self.clear()
        length = len(value)
        number_of_fast_chars = round(length * 0.7) 
        fast_chars = value[:number_of_fast_chars]
        slow_chars = value[number_of_fast_chars:]
        self.send_keys(fast_chars)
        for char in slow_chars:
            self.send_keys(char)
    
    
    def get_attribute(self, name):
        print(self._action_msg.format("Get attribute", self._locator))
        sw = Stopwatch()
        sw.start()
        while(self._timeout > 0):
            try:
                elem = self._get_element(self._timeout)
                if(elem==None):
                    return None
                else:
                    return elem.get_attribute(name)
            except NoSuchWindowException:
                raise Exception("********** Cannot interact with with Element {} because the window is closed.".format(self._locator))
            except WebDriverException as wdex:
                print("********** Error while getting attribute of Element: " + str(wdex))
                self._timeout -= sw.elapsed().total_seconds()
            except Exception as ex:
                self._driver.save_screenshot()
                raise Exception("********** There was an exception when getting attribute of Element {}. \nException: {}".format(self._locator, str(ex)))
        
    
    def is_selected(self):
        print(self._action_msg.format("Get is_selected attribute", self._locator))
        sw = Stopwatch()
        sw.start()
        while(self._timeout > 0):
            try:
                elem = self._get_element(self._timeout)
                if(elem==None):
                    return None
                else:
                    return elem.is_selected()
            except NoSuchWindowException:
                raise Exception("********** Cannot interact with with Element {} because the window is closed.".format(self._locator))
            except WebDriverException as wdex:
                print("********** Error while getting is_selected attribute of Element: " + str(wdex))
                self._timeout -= sw.elapsed().total_seconds()
            except Exception as ex:
                self._driver.save_screenshot()
                raise Exception("********** There was an exception when getting is_selected attribute of Element {}. \nException: {}".format(self._locator, str(ex)))
    
            
    """ Properties """
    @property
    def text(self):
        print(self._action_msg.format("Get text", self._locator))
        sw = Stopwatch()
        sw.start()
        while(self._timeout > 0):
            try:
                elem = self._get_element(self._timeout)
                if(elem==None):
                    return None
                else:
                    return elem.text
            except WebDriverException as wdex:
                print("********** Error while getting text of Element: " + str(wdex))
                self._timeout -= sw.elapsed().total_seconds()
            except Exception as ex:
                self._driver.save_screenshot()
                raise Exception("********** There was an exception when getting text of Element {}. \nException: {}".format(self._locator, str(ex)))


    
