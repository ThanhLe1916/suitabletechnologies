import os
from selenium import webdriver
from common.constant import Browser
from common.helper import Helper
from selenium.webdriver.chrome.options import Options


class Driver_Windows(object):
    _driver = None
    sep = os.path.sep
    def get_driver(self, driverSetting):
        dir_name = Helper.base_dir() + "\\data\\download"
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)
        
        if(driverSetting.browser_name == Browser.Firefox):
            from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
            fp = FirefoxProfile()
            if fp:
                fp.set_preference('browser.download.folderList', 2) # custom location
                fp.set_preference('browser.download.manager.showWhenStarting', False)
                fp.set_preference("browser.download.manager.focusWhenStarting", False)
                fp.set_preference("browser.download.useDownloadDir", True)
                fp.set_preference("browser.helperApps.alwaysAsk.force", False)
                fp.set_preference("browser.download.manager.alertOnEXEOpen", False)
                fp.set_preference("browser.download.manager.closeWhenDone", True)
                fp.set_preference("browser.download.manager.showAlertOnComplete", False)
                fp.set_preference("browser.download.manager.useWindow", False)
                fp.set_preference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", False)
                fp.set_preference("pdfjs.disabled", True)
                fp.set_preference('browser.download.dir', Helper.download_dir())
                fp.set_preference('browser.helperApps.neverAsk.saveToDisk', "application/octet-stream,application/vnd.ms-excel,text/csv,application/zip,application/exe")
            file_path = Helper.base_dir() + "\\libs\\remote_execution\\geckodriver-v0.11.1.exe".replace("\\", self.sep)
            self._driver = webdriver.Firefox(firefox_profile=fp, executable_path=file_path)
            
        elif(driverSetting.browser_name == Browser.IE):
            file_path = Helper.base_dir() + "\\libs\\remote_execution\\IEDriverServer.exe".replace("\\", self.sep)
            self._driver = webdriver.Ie(file_path)
        elif(driverSetting.browser_name == Browser.Safari):
            self._driver = webdriver.Safari()
        elif(driverSetting.browser_name == Browser.Chrome):
            file_path = Helper.base_dir() + "\\libs\\remote_execution\\chromedriver.exe".replace("\\", self.sep)
            chrome_options = Options()
            chrome_options.add_argument("--disable-extensions")
            chrome_options.add_argument("--disable-logging")
            prefs = {"download.default_directory": dir_name, "download.prompt_for_download": False, "safebrowsing.enabled": "false"}
            chrome_options.add_experimental_option("prefs", prefs)
            self._driver = webdriver.Chrome(file_path, chrome_options=chrome_options)
        elif (driverSetting.browser_name == Browser.Edge):
            file_path = Helper.base_dir() + "\\libs\\remote_execution\\MicrosoftWebDriver.exe".replace("\\", self.sep)
            self._driver = webdriver.Edge(file_path)
            self._driver.desired_capabilities['elementScrollBehavior'] = '1'
        return self._driver
