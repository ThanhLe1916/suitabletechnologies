��    ,      |  ;   �      �     �     �     �          3     R     o     �  !   �     �     �       %   !  #   G      k     �     �     �  #   �     	     &  "   A      d     �     �  *   �  (   �          /     I     c  "   {      �     �     �     �          .     I     ]     o      �     �    �  �  �	  *   r  �  �  *   +  {  V      �  �  �  )   �    �  !   4  7  V  1   �  �  �  /   �  �  �  )   p  }  �  )     �   B  A  @  +   �  *   �  &   �  �        �  d  �        �  9      !"  �  ;"     �#  �  �#  $   \%  �  �%  0   .'     _'  �  h'     )  l  3)     �*  S  �*  �  
,  #   �-     #   &                     +      $          (      "   '                       %                 !      *              	                                    
      )                       ,                 AccessTimeHasChangedContent AccessTimeHasChangedTitle AddToUserGroupEmailContent AddToUserGroupEmailTitle AddedToDeviceGroupEmailContent AddedToDeviceGroupEmailTitle AddedToOrgAdminEmailContent AddedToOrgAdminEmailTitle BeamRemovedFromDeviceGroupContent BeamRemovedFromDeviceGroupTitle BeamRequestAccessEmailContent BeamRequestAccessEmailTitle CanNoLongerAcceptSessionsEmailContent CanNoLongerAcceptSessionsEmailTitle CanNowAcceptSessionsEmailContent CanNowAcceptSessionsEmailTitle DeleteDeviceGroupEmailContent DeleteDeviceGroupEmailTitle GoogleChangeAuthConnectEmailContent GoogleChangeAuthEmailContent GoogleChangeAuthEmailTitle InvitationEmailDeviceList_Multiple InvitationEmailDeviceList_Single ManageDeviceGroupEmailContent ManageDeviceGroupEmailTitle NotificationAddedToDeviceGroupEmailContent NotificationAddedToDeviceGroupEmailTitle PasswordChangedEmailContent PasswordChangedEmailTitle PasswordResetEmailContent PasswordResetEmailTitle RemovedFromDeviceGroupEmailContent RemovedFromDeviceGroupEmailTitle RemovedFromOrgAdminEmailContent RemovedFromOrgAdminEmailTitle RequestMessageLabel WelcomeAdminCopyEmailContent WelcomeAdminCopyEmailTitle WelcomeEmailContent WelcomeEmailTitle WelcomeExistingEmailContent WelcomeTemporaryUserEmailContent WelcomeTemporaryUserEmailTitle Project-Id-Version: 
POT-Creation-Date: 
PO-Revision-Date: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.8
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: en_US
 Your access time for {} has changed


{} changed your access time(s) for {}.



Have questions? Simply reply to this email or visit our support site: http://support.suitabletech.com/

You can change your email notification settings here: {}/manage/#/account/notifications/



Thanks,
Suitable Technologies

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 [Beam] Your access time for {} has changed You have been added to the group {}


{} added you to the user group {}.



Have questions? Simply reply to this email or visit our support site: http://support.suitabletech.com/

You can change your email notification settings here: {}/manage/#/account/notifications/




Suitable Technologies

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 [Beam] You have been added to the group {} You have been added to {}


{} added you to {}.



Have questions? Simply reply to this email or visit our support site: http://support.suitabletech.com/

You can change your email notification settings here: {}/manage/#/account/notifications/



Thanks,
Suitable Technologies

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 [Beam] You have been added to {} You are now an administrator of {}


{} made you an administrator of {}.




Have questions? Simply reply to this email or visit our support site: http://support.suitabletech.com/

You can change your email notification settings here: {}/manage/#/account/notifications/



Thanks,
Suitable Technologies

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 [Beam] You are now an administrator of {} A Beam was removed from {}


{} removed {} from {}.



Have questions? Simply reply to this email or visit our support site: http://support.suitabletech.com/

You can change your email notification settings here: {}/manage/#/account/notifications/



Thanks,
Suitable Technologies

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 [Beam] A Beam was removed from {} ^
Someone is requesting access to your Beams



{} has requested access to {}.





{}
To approve this request, click on the link below:
{}/r/(.*)/a/

Otherwise, reject this request by clicking the link below:
{}/r/(.*)/r/


Have questions\? Simply reply to this email or visit our support site: http://support.suitabletech.com/

You can change your email notification settings here: {}/manage/#/account/notifications/



Thanks,
Suitable Technologies

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326$ [Beam] Someone is requesting access to your Beams You can no longer accept sessions for {}


{} removed you from the list of users who can answer session requests for {}.



Have questions? Simply reply to this email or visit our support site: http://support.suitabletech.com/

You can change your email notification settings here: {}/manage/#/account/notifications/



Thanks,
Suitable Technologies

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 [Beam] You can no longer accept sessions for {} You can now accept sessions for {}


{} added you to the list of users who can answer session requests for {}.



Have questions? Simply reply to this email or visit our support site: http://support.suitabletech.com/

You can change your email notification settings here: {}/manage/#/account/notifications/



Thanks,
Suitable Technologies

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 [Beam] You can now accept sessions for {} A device group was removed from {}


{} removed the device group {}.



Have questions? Simply reply to this email or visit our support site: http://support.suitabletech.com/

You can change your email notification settings here: {}/manage/#/account/notifications/



Thanks,
Suitable Technologies

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 [Beam] A device group was removed from {} User account: {}

This email acknowledges that your authentication method has been changed to Google. You must use Google to login to the website and Beam client from now on.

If you believe this is in error, please contact support@suitabletech.com. User account: {}

This email acknowledges that you have disconnected your Beam account from Google.

If you wish to sign in with a Suitable Technologies username and password, you must first reset your password: {}/accounts/password_reset/.

If you believe this is in error, please contact support@suitabletech.com. Your authentication method has been changed You have access to the following {} Beams: You have access to the following Beam: You can now manage {}


{} added you to the list of users who can manage {}.



Have questions? Simply reply to this email or visit our support site: http://support.suitabletech.com/

You can change your email notification settings here: {}/manage/#/account/notifications/



Thanks,
Suitable Technologies

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 [Beam] You can now manage {} A user was added to {}


{} added {} to {}.



Have questions? Simply reply to this email or visit our support site: http://support.suitabletech.com/

You can change your email notification settings here: {}/manage/#/account/notifications/



Thanks,
Suitable Technologies

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 [Beam] A user was added to {} Your password for Beam and suitabletech.com has been changed.
Your username:
{}

If you believe this is in error, please contact support@suitabletech.com.

Have questions? Simply reply to this email or visit our support site: http://support.suitabletech.com/
You can change your email notification settings here: {}/manage/#/account/notifications/


Thanks,
Suitable Technologies Support

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 Your password has changed ^
You're receiving this e-mail because you requested a password reset for your user account at Suitable Technologies.

Please go to the following page and choose a new password:

{}/accounts/reset/(.*)/

Your username, in case you've forgotten: {}


Thanks,
Suitable Technologies Support

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326

$ Beam Password Reset You have been removed from {}


{} removed you from {}.



Have questions? Simply reply to this email or visit our support site: http://support.suitabletech.com/

You can change your email notification settings here: {}/manage/#/account/notifications/



Thanks,
Suitable Technologies

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 [Beam] You have been removed from {} You are no longer an administrator for {}


{} removed you from the list of {} administrators.




Have questions? Simply reply to this email or visit our support site: http://support.suitabletech.com/

You can change your email notification settings here: {}/manage/#/account/notifications/



Thanks,
Suitable Technologies

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 [Beam] You are no longer an administrator for {} Message: ----------
The following is your requested copy of the email sent to {}.
----------

{},

{} invited you to Beam into {}.

Hello


To get started, visit this link to activate your account and set a password.

[Link Removed]

This link expires in 7 days.



Your username is your email address: {}






Thanks,
{}

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 Welcome to Beam at {} (copy) ^

{},

{} invited you to Beam into {}.

Hello


To get started, visit this link to activate your account and set a password.

{}/accounts/reset/(.*)/\?new=1

This link expires in 7 days.



Your username is your email address: {}






Thanks,
{}

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326
$ Welcome to Beam at {} {},

{} invited you to Beam into {}.




You can use your existing account with the username: {}




You can see which devices are available to use by signing in to the Beam software on your computer or mobile device.




Thanks,
{}

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 ^

{},

{} invited you to Beam into {}.

Hello


To get started, visit this link to activate your account and set a password.

{}/accounts/reset/(.*)/\?new=1

This link expires in 7 days.



Your username is your email address: {}



{}

{}

You can Beam in during the following time period:
Beginning: {}
Ending: {}
These times are in the time zone of the device to which you're connecting.



Thanks,
{}

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326
$ You've been invited to Beam into {} 