��    ,      |  ;   �      �     �     �     �          3     R     o     �  !   �     �     �       %   !  #   G      k     �     �     �  #   �     	     &  "   A      d     �     �  *   �  (   �          /     I     c  "   {      �     �     �     �          .     I     ]     o      �     �    �  �  �	  *   {     �     �  �  �  '   L  �  t  4   $  �  Y  %   �  c    .   y  �  �  7   �  �  �  ;   �  �    4   �  �   �  �  �  3   {  0   �  %   �  �    '   �  �  �  +   �     �      �"  �  �"  &   �$  �  �$  )   k&  �  �&  0   ^(  	   �(  �  �(  !   B*  j  d*     �+  o  �+  2  Z-  3   �/     #   &                     +      $          (      "   '                       %                 !      *              	                                    
      )                       ,                 AccessTimeHasChangedContent AccessTimeHasChangedTitle AddToUserGroupEmailContent AddToUserGroupEmailTitle AddedToDeviceGroupEmailContent AddedToDeviceGroupEmailTitle AddedToOrgAdminEmailContent AddedToOrgAdminEmailTitle BeamRemovedFromDeviceGroupContent BeamRemovedFromDeviceGroupTitle BeamRequestAccessEmailContent BeamRequestAccessEmailTitle CanNoLongerAcceptSessionsEmailContent CanNoLongerAcceptSessionsEmailTitle CanNowAcceptSessionsEmailContent CanNowAcceptSessionsEmailTitle DeleteDeviceGroupEmailContent DeleteDeviceGroupEmailTitle GoogleChangeAuthConnectEmailContent GoogleChangeAuthEmailContent GoogleChangeAuthEmailTitle InvitationEmailDeviceList_Multiple InvitationEmailDeviceList_Single ManageDeviceGroupEmailContent ManageDeviceGroupEmailTitle NotificationAddedToDeviceGroupEmailContent NotificationAddedToDeviceGroupEmailTitle PasswordChangedEmailContent PasswordChangedEmailTitle PasswordResetEmailContent PasswordResetEmailTitle RemovedFromDeviceGroupEmailContent RemovedFromDeviceGroupEmailTitle RemovedFromOrgAdminEmailContent RemovedFromOrgAdminEmailTitle RequestMessageLabel WelcomeAdminCopyEmailContent WelcomeAdminCopyEmailTitle WelcomeEmailContent WelcomeEmailTitle WelcomeExistingEmailContent WelcomeTemporaryUserEmailContent WelcomeTemporaryUserEmailTitle Project-Id-Version: 
POT-Creation-Date: 
PO-Revision-Date: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.8
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: fr_FR
 [BUG_34] Your access time for {} has changed


{} changed your access time(s) for {}.



Have questions? Simply reply to this email or visit our support site: http://support.suitabletech.com/

You can change your email notification settings here: {}/manage/#/account/notifications/



Thanks,
Suitable Technologies

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 [Beam] Your access time for {} has changed [TBD] [TBD] Vous avez été ajouté(e) à {}.


{} vous a ajouté(e) à {}.



Des questions ? Répondez simplement à cet e-mail ou visitez notre site d'assistance : http://support.suitabletech.com/

Vous pouvez modifier vos paramètres de notification par e-mail ici : {}/manage/#/account/notifications/



Merci,
Suitable Technologies

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 [Beam] Vous avez été ajouté(e) à {} Vous êtes maintenant un administrateur de {}


{} vous a fait administrateur de {}.




Des questions ? Répondez simplement à cet e-mail ou visitez notre site d'assistance : http://support.suitabletech.com/

Vous pouvez modifier vos paramètres de notification par e-mail ici : {}/manage/#/account/notifications/



Merci,
Suitable Technologies

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 [Beam] Vous êtes maintenant un administrateur de {} Un Beam a été supprimé de {}.


{} a supprimé {} de {}.



Des questions ? Répondez simplement à cet e-mail ou visitez notre site d'assistance : http://support.suitabletech.com/

Vous pouvez modifier vos paramètres de notification par e-mail ici : {}/manage/#/account/notifications/



Merci,
Suitable Technologies

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 Beam] Un Beam a été supprimé de {} ^
Quelqu&#39;un demande l&#39;accès à vos Beams.



{} a demandé l'accès à {}.





{}


Pour approuver cette demande, cliquez sur le lien ci-dessous:
{}/r/(.*)/a/

Dans le cas contraire, rejetez cette demande en cliquant sur le lien ci-dessous:
{}/r/(.*)/r/


Des questions \? Répondez simplement à cet e-mail ou visitez notre site d'assistance : http://support.suitabletech.com/

Vous pouvez modifier vos paramètres de notification par e-mail ici : {}/manage/#/account/notifications/



Merci,
Suitable Technologies

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326$ [Beam] Quelqu'un demande l'accès à vos Beams Vous ne pouvez plus accepter de sessions pour {}


{} vous a supprimé(e) de la liste des utilisateurs pouvant répondre à des demandes de session pour {}.



Des questions ? Répondez simplement à cet e-mail ou visitez notre site d'assistance : http://support.suitabletech.com/

Vous pouvez modifier vos paramètres de notification par e-mail ici : {}/manage/#/account/notifications/



Merci,
Suitable Technologies

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 [Beam] Vous ne pouvez plus accepter de sessions pour {} Vous pouvez maintenant accepter des sessions pour {}


{} vous a ajouté(e) à la liste des utilisateurs pouvant répondre à des demandes de sessions pour {}.



Des questions ? Répondez simplement à cet e-mail ou visitez notre site d'assistance : http://support.suitabletech.com/

Vous pouvez modifier vos paramètres de notification par e-mail ici : {}/manage/#/account/notifications/



Merci,
Suitable Technologies

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 [Beam] Vous pouvez maintenant accepter des sessions pour {} Un groupe d&#39;appareils a été supprimé de {}


{} a supprimé le groupe d'appareil {}.



Des questions ? Répondez simplement à cet e-mail ou visitez notre site d'assistance : http://support.suitabletech.com/

Vous pouvez modifier vos paramètres de notification par e-mail ici : {}/manage/#/account/notifications/



Merci,
Suitable Technologies

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 [Beam] Un groupe d'appareils a été supprimé de {} User account: {}

This email acknowledges that your authentication method has been changed to Google. You must use Google to login to the website and Beam client from now on.

If you believe this is in error, please contact support@suitabletech.com. Compte utilisateur : {}

Cet e-mail confirme que vous avez bien déconnecté votre compte Beam de Google.

Si vous souhaitez vous connecter à l'aide d'un nom d'utilisateur Suitable Technologies et d'un mot de passe, vous devez d'abord réinitialiser votre mot de passe : {}/accounts/password_reset/.

Si vous pensez qu'il s'agit d'une erreur, veuillez contacter support@suitabletech.com. Votre méthode d'authentification a été changée. Vous avez accès aux 2 appareils Beam suivants : Vous avez accès à la Beam suivante: Vous pouvez maintenant gérer {}


{} vous a ajouté(e) à la liste des utilisateurs pouvant gérer {}.



Des questions ? Répondez simplement à cet e-mail ou visitez notre site d'assistance : http://support.suitabletech.com/

Vous pouvez modifier vos paramètres de notification par e-mail ici : {}/manage/#/account/notifications/



Merci,
Suitable Technologies

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 [Beam] Vous pouvez maintenant gérer {} Un utilisateur a été ajouté à {}.


{} a ajouté {} à {}.



Des questions ? Répondez simplement à cet e-mail ou visitez notre site d'assistance : http://support.suitabletech.com/

Vous pouvez modifier vos paramètres de notification par e-mail ici : {}/manage/#/account/notifications/



Merci,
Suitable Technologies

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 [Beam] Un utilisateur a été ajouté à {} Votre mot de passe pour Beam et suitabletech.com a été modifié.
Votre nom d'utilisateur :
{}

Si vous pensez qu'il s'agit d'une erreur, merci de contacter support@suitabletech.com.

Des questions ? Répondez simplement à cet e-mail ou visitez notre site d'assistance : http://support.suitabletech.com/
Vous pouvez modifier vos paramètres de notification par e-mail ici : {}/manage/#/account/notifications/


Merci,
Suitable Technologies Support

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 Votre mot de passe a changé ^
Vous recevez cet e-mail parce que vous avez demandé la réinitialisation du mot de passe de votre compte utilisateur chez Suitable Technologies.

Veuillez vous rendre sur la page suivante pour choisir un nouveau mot de passe :

{}/accounts/reset/(.*)/

Votre nom d'utilisateur, au cas où vous l'avez oublié : {}


Merci,
Suitable Technologies Support

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326

$ Réinitialisation du mot de passe Beam Vous avez été supprimé(e) de {}.


{} vous a supprimé(e) de {}.



Des questions ? Répondez simplement à cet e-mail ou visitez notre site d'assistance : http://support.suitabletech.com/

Vous pouvez modifier vos paramètres de notification par e-mail ici : {}/manage/#/account/notifications/



Merci,
Suitable Technologies

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 [Beam] Vous avez été supprimé(e) de {} Vous n&#39;êtes plus un administrateur de {}


{} vous a supprimé(e) de la liste des administrateurs de {}.




Des questions ? Répondez simplement à cet e-mail ou visitez notre site d'assistance : http://support.suitabletech.com/

Vous pouvez modifier vos paramètres de notification par e-mail ici : {}/manage/#/account/notifications/



Merci,
Suitable Technologies

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 [Beam] Vous n'êtes plus un administrateur de {} Message : ----------
Voici la copie de l'e-mail envoyé à {} que vous avez demandée.
----------

{},

{} invited you to Beam into {}.

Hello


Pour commencer, visitez ce lien pour activer votre compte et définir un mot de passe.

[Link Removed]

Ce lien expire dans 7 jours.



Votre nom d'utilisateur est votre adresse e-mail : {}






Merci,
{}

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 Bienvenue sur Beam chez {} (copy) ^[BUG_34]

{},

{} invited you to Beam into {}.


Pour commencer, visitez ce lien pour activer votre compte et définir un mot de passe.

{}/accounts/reset/(.*)/\?new=1

Ce lien expire dans 7 jours.



Votre nom d'utilisateur est votre adresse e-mail : {}






Merci,
{}

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326
$ Bienvenue sur Beam chez {} [BUG_34]{},

{} invited you to Beam into {}.




Vous pouvez utiliser votre compte existant avec le nom d'utilisateur : {}




Vous pouvez voir les appareils disponibles à l'utilisation en vous connectant au logiciel Beam sur votre ordinateur ou votre appareil mobile.




Merci,
{}

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326 ^[BUG_34]

{},

{} invited you to Beam into {}.

Hello


Pour commencer, visitez ce lien pour activer votre compte et définir un mot de passe.

{}/accounts/reset/(.*)/\?new=1

Ce lien expire dans 7 jours.



Votre nom d'utilisateur est votre adresse e-mail : {}



{}

{}

Vous pouvez utiliser le Beam au cours des plages horaires suivantes :
Début: {}
Fin: {}
Ces plages horaires sont indiquées dans le fuseau horaire de l'appareil auquel vous vous connectez.



Merci,
{}

Suitable Technologies, Inc.
921 E Charleston Rd
Palo Alto, CA 94303
1-855-200-2326
$ Vous avez été invité(e) à utiliser Beam chez {} 