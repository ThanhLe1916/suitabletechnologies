from enum import Enum
from core import i18n
import os
from common.constant import Constant
from common.application_constants import ApplicationConst
from common.email_detail_constants import EmailDetailConstants
class I18NLanguage(Enum):
    default = 1
    ja_JP = 2
    fr_FR = 3
    
class I18NSupport(object):
    
    @staticmethod
    def set_language(language = I18NLanguage.default):
        lc_dir = os.path.join( os.path.dirname(i18n.__file__), "locale")
        
        import gettext
        gettext.translation('appstring', localedir=lc_dir, languages=[language.name]).install()
        Constant.initialize(language.name)
        ApplicationConst.initialize()
        
        #print("I18NSupport::set_language > "+ApplicationConst.LBL_MANAGE_YOUR_BEAMS)
        
        gettext.translation('date_time', localedir=lc_dir, languages=[language.name]).install()
        ApplicationConst.initialize_date_time_localization()
        
        gettext.translation('email_content', localedir=lc_dir, languages=[language.name]).install()
        EmailDetailConstants.initialize()
        
    
    @staticmethod
    def localize_date_time_string(english_text, has_month = True, has_weekday = True, has_time=True):
        tmp = english_text
        
        for key,value in ApplicationConst._date_time_localized_values.items():
            tmp = tmp.replace(key, value)
        
        return tmp
        
        