from datetime import datetime
from datetime import timedelta
import json
from time import sleep

from common.constant import Constant
from common.stopwatch import Stopwatch
from core.suitabletechapis.device_api import DeviceAPI
from core.suitabletechapis.device_group_api import DeviceGroupAPI
from core.suitabletechapis.organization_settings_api import OrganizationSettingAPI
from core.suitabletechapis.user_api import UserAPI
from core.suitabletechapis.user_group_api import UserGroupAPI
from core.utilities.gmail_utility import GmailUtility
from core.utilities.utilities import Utilities
from data.dataobjects.user import User
from pages.suitable_tech.user.home_page import HomePage
from pages.suitable_tech.user.password_setup_page import PasswordSetupPage
from pages.suitable_tech.user.welcome_to_beam_page import WelcomeToBeamPage
from pages.gmail_page.gmail_sign_in_page import GmailSignInPage
from pages.gmail_page.apps_connected_page import AppsConnectedPage
from core.webdriver.driver import Driver
from common.helper import Helper


class TestCondition():
    
    SIMPLIFIED_ACCESS_TOKEN = ""
    
    @staticmethod
    def create_advanced_normal_users(driver, user_array, activate_user=True):
        """      
        @summary: Create new normal users as precondition for test case   
        @param driver: WebDriver
        @param user_array: Array of users to be created
        @param activate_user: Set to True to activate all user. Set to False to only create new Users
        @author: Thanh Le         
        """
        for user in user_array:
            TestCondition._create_advanced_normal_user(driver, user)
            if(activate_user == True):
                TestCondition._activate_user(driver, user)
        
        # Back to home page after all
        TestCondition.force_log_out(driver)
    
    
    @staticmethod
    def create_advanced_multi_org_normal_users(driver, user_array, organization_array, activate_user=True):
        """
        @summary: Create new normal users as precondition for test case   
        @param driver: WebDriver
        @param user_array: Array of users to be created
        @param activate_user: Set to True to activate all user. Set to False to only create new Users
        @author: Thanh Le         
        """
        for user in user_array:
            for index in range(len(organization_array)):
                user.organization = organization_array[index]
                if(index > 0):
                    activate_user = False
                TestCondition.create_advanced_normal_users(driver=driver, user_array=[user], activate_user=activate_user)
        
        # Back to home page after all
        TestCondition.force_log_out(driver)
        
        
    @staticmethod
    def create_advanced_device_group_admin_on_multi_organization(driver, device_group_admin, device_group_name, device_list=[], organization_array=[Constant.AdvancedOrgName, Constant.AdvancedOrgName_2]):
        """      
        @summary: Create new device group admin in two different organization as precondition for test case   
        @param driver: WebDriver
        @param device_group_admin: A device group admin to be created
        @param device_group_name: Device group name
        @param device_list: Array of devices
        @param organization_array: List of organization which device group is created
        @author: Khoi Ngo
        """
        try:
            for org in organization_array:
                device_group_admin.organization = org
                device_group_admin.device_group = device_group_name
                TestCondition.create_device_group(device_group_name, device_list, org)
                TestCondition.create_advanced_device_group_admins(driver, [device_group_admin])
        except:
            raise Exception("Error while create device group admin on multi org.")


    @staticmethod
    def create_advanced_device_group_admins(driver, user_array, admin_email=None, admin_password=Constant.DefaultPassword, is_new_admin=False):
        """      
        @summary: Create new device group admins as precondition for test case   
        @param driver: WebDriver
        @param user_array: Array of device group admins to be created
        @param admin_email: Email of the admin who grants device group permission for the new users. Leave it None to automatically create new org admin
        @param admin_password: Password of the admin who grants device group permission for the new users
        @param is_new_admin: Set to True is the Admin is new and not completed the video. Set to False if the Admin has already watched the video.
        @author: Thanh Le         
        """
        for user in user_array:
            TestCondition._create_advanced_normal_user(driver, user)
            TestCondition.set_advanced_device_group_admin(user.email_address, user.device_group, user.organization)
            TestCondition._activate_user(driver, user)
        
        TestCondition.force_log_out(driver)
    
    
    @staticmethod
    def create_advanced_organization_admins(driver, user_array, activate_user=True):
        """      
        @summary: Create new organization admins as precondition for test case
        @param driver: WebDriver   
        @param user_array: Array of organization admins to be created
        @param activate_user: Set to True to activate all user. Set to False to only create new Users
        @author: Thanh Le         
        """
        
        try:
            for user in user_array:
                TestCondition._create_advanced_normal_user(driver, user)
                if (activate_user):
                    TestCondition._activate_user(driver, user)
                    
                response = UserAPI.set_org_admin(user.email_address, user.organization)
                if(response.status_code != 202):
                    raise Exception("Error while setting a user {} to be organization admin. Reason: {}".format(user.email_address, response.reason))
            # Back to home page after all
            TestCondition.force_log_out(driver)
        except:
            raise Exception("Error while granting Organization Admin permission for users")
    
    
    @staticmethod
    def create_advanced_multi_organization_admin(driver, user, organization_array=[Constant.AdvancedOrgName, Constant.AdvancedOrgName_2]):
        """      
        @summary: Create new organization admin in 2 advanced organizations
        @param driver: WebDriver   
        @param user: Organization admin to be created
        @param organization_array: Array of organizations
        @param activated: Set to True to activate all user. Set to False to only create new Users
        @author: Thanh Le         
        """
        for org in organization_array:
            user.organization = org 
            TestCondition.create_advanced_organization_admins(driver, [user])
                
    
    @staticmethod
    def create_advanced_temporary_user(driver, user, device_group, start_date=datetime.now(), end_date=(datetime.now() + timedelta(minutes=15)), answer_required=False, activate_user=False):
        """      
        @summary: Create new organization admins as precondition for test case   
        @param driver: WebDriver
        @param user: User to be created
        @param device_group: Device group to add user
        @param start_date: Start time
        @param end_date: End time
        @param answer_required: Whether or not calls by this user must be accepted on the receiving side
        @author: Thanh Le         
        """
        response = UserAPI.invite_new_temporary_user(user, device_group, start_date, end_date, answer_required)
        if(response.status_code != 201):
            raise Exception("Error while creating temporary user {}. Reason: {}".format(user.tostring(), response.reason))
        
        if(activate_user):
            TestCondition._activate_user(driver, user, True)
        
        # Back to home page after all
        TestCondition.force_log_out(driver)
    
    
    @staticmethod
    def create_advanced_non_gsso_user(driver, user):
        is_confirm_association_page_displayed = TestCondition.force_log_out(driver).goto_login_page()\
            .goto_google_signin_page()\
            .sign_in_with_non_gsso_account(user.email_address, user.password)\
            .is_page_displayed()
            
        if not is_confirm_association_page_displayed:
            welcome_to_beam_page = WelcomeToBeamPage(driver)
            if welcome_to_beam_page.is_welcome_user_page_displayed(1):
                welcome_to_beam_page.goto_account_settings_page_by_menu_item()\
                    .disconect_from_google()\
                    .reset_password(user)
                    
        TestCondition.set_language(driver, user)
        
    
    @staticmethod
    def create_advanced_gsso_user(driver, user):
        confirm_page = TestCondition.force_log_out(driver).goto_login_page()\
            .goto_google_signin_page()\
            .sign_in_with_non_gsso_account(user.email_address, user.password)
            
        if confirm_page.is_page_displayed():
            confirm_page.accept_change_authentication()
        
        welcome_page = WelcomeToBeamPage(driver)
        welcome_page.goto_account_settings_page_by_menu_item()\
            .set_language(driver.driverSetting.language)\
            .save_change()\
            .logout()          
        
    
    @staticmethod
    def create_advanced_unallowed_gsso_user(driver, user):
        driver = TestCondition._remove_beam_from_connected_app(driver, user)
        driver = TestCondition.reopen_browser(driver)
        HomePage(driver).open_and_goto_login_page()\
            .goto_google_signin_page()\
            .sign_in_with_fresh_new_non_gsso_account(user.email_address, user.password)\
            .approve_access_account_info()\
#             .accept_change_authentication()
        driver = TestCondition.reopen_browser(driver)
        TestCondition.create_advanced_non_gsso_user(driver, user)
        TestCondition._remove_beam_from_connected_app(driver, user)
    
    
    @staticmethod
    def _remove_beam_from_connected_app(driver, user):
        driver = TestCondition.reopen_browser(driver)        
        GmailSignInPage(driver).open().log_in_gmail(user.email_address, user.password)
        apps_connected_page = AppsConnectedPage(driver).open()
        if apps_connected_page.is_beam_connected():
            apps_connected_page.remove_beam_from_connected_apps()
        return driver
    
    @staticmethod
    def reopen_browser(driver):
        driver.quit()
        driver = Driver(Helper.load_execution_setting())
        driver.maximize_window()
        return driver
    
    
    @staticmethod
    def is_device_group_exist(device_group_name, organization):
        """      
        @summary: Check device group is exist.   
        @param device_group_name: Device group name
        @param organization: Organization which device group is created
        @author: Khoi Ngo  
        """
        response = DeviceGroupAPI.get_device_group_id(device_group_name, organization)
        if response is None:
            return False
        return True
    
            
    @staticmethod
    def create_device_group(device_group_name, device_array=[], organization_name=Constant.AdvancedOrgName):
        """      
        @summary: Create new device groups as precondition for test case   
        @param device_group_name: Device group name
        @param device_array: Array of devices
        @param organization_name: Organization which device group is created
        @author: Thanh Le         
        """
        try:
            response = DeviceGroupAPI.create_device_group(device_group_name, device_array, organization_name)
            if(response.status_code != 201):
                raise Exception("Error while creating Device Group {} for Organization {}. Reason: {}".format(device_group_name, organization_name, response.reason))
        except Exception as ex:
            raise Exception("Error while creating device group. Message: " + str(ex))
        
    
    @staticmethod
    def create_user_group(group_name, user_array=[], organization_name=Constant.AdvancedOrgName):
        """      
        @summary: Create new user group in expected organization as precondition for test case   
        @param group_name: group name to be created
        @param user_array: users to add to user group
        @param organization_name: organization name
        @author: Thanh Le         
        """
        try:
            
            user_email_array = []
            for user in user_array:
                user_email_array.append(user.email_address)
            
            response = UserGroupAPI.create_user_group(group_name, user_email_array, organization_name)
            if(response.status_code != 201):
                raise Exception("Error while creating User Group {} for Organization {}. Reason: {}. Content: {}".format(group_name, organization_name, response.reason, response.content))
        except Exception as ex:
            raise Exception("Error while creating user group. Message: " + str(ex))
    
    
    @staticmethod
    def create_simplified_normal_users(driver, user_array, device_name, organization, activate_user=True):
        """      
        @summary: Create new normal users as precondition for test case   
        @param driver: WebDriver
        @param user_array: Array of users to be created
        @param activated: Set to True to activate all user. Set to False to only create new Users
        @author: Thanh Le         
        """
        for user in user_array:
            TestCondition._create_simplified_normal_user(driver=driver, user=user, device_name=device_name, organization=organization)
            if activate_user:
                TestCondition._activate_user(driver, user, is_simplified=True)
        
        # Back to home page after all
        TestCondition.force_log_out(driver)

    
    @staticmethod
    def create_simplified_organization_admin(driver, user):
        """      
        @summary: Create new organization admin in 2 advanced organizations
        @param driver: WebDriver   
        @param user: Organization admin to be created
        @param organization_array: Array of organizations
        @param activated: Set to True to activate all user. Set to False to only create new Users
        @author: Thanh Le         
        """
        TestCondition.set_language(driver, user)
        TestCondition._activate_user(driver, user, is_simplified=True)
        TestCondition.force_log_out(driver)
    
    
    @staticmethod
    def create_mixed_organization_admin(driver, user):
        """      
        @summary: Create new organization admin in 2 advanced organizations
        @param driver: WebDriver   
        @param user: Organization admin to be created
        @param organization_array: Array of organizations
        @param activated: Set to True to activate all user. Set to False to only create new Users
        @author: Thanh Le         
        """
        TestCondition.set_language(driver, user)
        TestCondition._activate_user(driver, user, is_simplified=True)
        TestCondition.force_log_out(driver)
    
    
    @staticmethod
    def create_simplified_device_admin(driver, user_array, device_name, organization, activate_user=True):
        """      
        @summary: Create new normal users as precondition for test case   
        @param driver: WebDriver
        @param user_array: Array of users to be created
        @param activated: Set to True to activate all user. Set to False to only create new Users
        @author: Thanh Le         
        """
        for user in user_array:
            TestCondition._create_simplified_normal_user(driver=driver, user=user, device_name=device_name, organization=organization)
            DeviceGroupAPI.set_simplified_device_admin(user.email_address, device_name, organization, access_token=TestCondition._get_access_token(driver))
            
            if(activate_user):
                TestCondition._activate_user(driver, user, is_simplified=True)
        
        # Back to home page after all
        TestCondition.force_log_out(driver)
                                       
                                       
    @staticmethod
    def delete_device_groups(device_group_array, organization_name=Constant.AdvancedOrgName):
        """      
        @summary: Delete device groups as postcondition for test case   
        @param device_group_array: device groups to be deleted
        @param organization_name: organization name
        @author: Thanh Le         
        """
        print("\n******************************\n*** Cleaning up test Device Groups ***\n******************************")
        
        try:
            for device_group_name in device_group_array:
                response = DeviceGroupAPI.delete_device_group(device_group_name, organization_name)
                if(response.status_code != 204):
                    print("Error while deleting device group {}. Reason: {}".format(device_group_name, response.reason))
             
                print("Device group {} is deleted.".format(device_group_name))
        except Exception as ex:
            print(str(ex))
            
            
    @staticmethod
    def add_user_group_to_device_group(device_group_name, user_group_array=[], organization=Constant.AdvancedOrgName):
        """      
        @summary: Add User Groups to a Device Group   
        @param device_group_name: Device group name
        @param user_group_array: Array of user groups to be added
        @param organization: organization name
        @author: Thanh Le         
        """
        try:
            for user_group in user_group_array:
                response = DeviceGroupAPI.add_user_group(device_group_name, user_group, organization)
                if(response.status_code != 201):
                    raise Exception("Error while adding User Group {} to Device Group {} for Organization {}. Reason: {}".format(user_group, device_group_name, organization, response.reason))
        except Exception as ex:
            raise Exception("Error while updating device group. Message: " + str(ex))


    @staticmethod
    def delete_user_groups(user_group_array, organization=Constant.AdvancedOrgName):
        """      
        @summary: Delete user groups in expected organization as precondition for test case   
        @param user_group_array: group names to be delete
        @param organization_name: organization name
        @author: Thanh Le         
        """
        print("\n******************************\n*** Cleaning up test User Groups ***\n******************************")
        
        try:
            for user_group_name in user_group_array:
                response = UserGroupAPI.delete_user_group(user_group_name, organization)
                if(response.status_code != 204):
                    print("Error while delete User Group for Organization {}. Reason: {}".format(organization, response.reason))
                
                print("Device group {} is deleted.".format(user_group_name))
        except Exception as ex:
            print(str(ex))
            
    
    @staticmethod
    def delete_advanced_users(user_array, organization=Constant.AdvancedOrgName):
        """      
        @summary: Delete users as postcondition for test case   
        @param user_array: users to be deleted
        @author: Thanh Le         
        """
        print("\n******************************\n*** Cleaning up Advanced Users ***\n******************************")
        try:
            for user in user_array:
                response = UserAPI.delete_user(user.email_address, organization)
                if(response.status_code != 204):
                    print("Error while deleting user {}. Reason: {}".format(user.email_address, response.reason))
             
                print("User {} is deleted.".format(user.email_address))
                GmailUtility.delete_all_emails(user.email_address)
                print("All emails related to User {} is deleted.".format(user.email_address))
        except Exception as ex:
            print(str(ex))
        
    
    @staticmethod
    def delete_simplified_users(driver, user_array, device_name, organization=Constant.SimplifiedOrgName):
        """      
        @summary: Delete users as postcondition for test case   
        @param user_array: users to be deleted
        @param device_name: device which user is added
        @param organization: organization name
        @author: Thanh Le         
        """
        print("\n******************************\n*** Cleaning up Simplified Users ***\n******************************")
        try:
            for user in user_array:
                response = DeviceGroupAPI.delete_user(user_email=user.email_address, device_name=device_name, organization=organization, access_token=TestCondition._get_access_token(driver))
                if(response.status_code != 204):
                        print("Error while deleting simplified user {}. Reason: {}".format(user.email_address, response.reason))
                 
                print("User {} is deleted.".format(user.email_address))
                GmailUtility.delete_all_emails(user.email_address)
                print("All emails related to User {} is deleted.".format(user.email_address))
        except Exception as ex:
            print(str(ex))
        
        
    @staticmethod
    def delete_file(file_path):
        """      
        @summary: Delete file   
        @param file_path: path to the file
        @author: Thanh Le         
        """
        try:
            if file_path:
                Utilities.delete_file(file_path)
        except Exception as ex:
            raise Exception("Error while deleting file {}. Error message: {}".format(file_path, str(ex)))
    
    
    @staticmethod
    def delete_api_key(driver, key_name, organization_name=None):
        try:
            if(organization_name == None):
                organization_name = Constant.AdvancedOrgName
                
            user = User()
            user.generate_advanced_org_admin_data()
            user.organization = organization_name
            TestCondition.create_advanced_organization_admins(driver, [user])
                
            HomePage(driver).open_and_goto_login_page()\
                .login_as_unwatched_video_user(user.email_address, user.password)\
                .goto_admin_dashboard_page_by_menu_item()\
                .goto_org_setting_page().delete_api_key(key_name)
                
            TestCondition.delete_advanced_users([user], user.organization)
        except Exception:
            print("*** There is an error while deleting api key {}. Please consider to delete manually".format(key_name))
            
    
    @staticmethod
    def remove_all_user_groups(delete_user_group_template="LGVN User Group", organization=Constant.AdvancedOrgName):
        UserGroupAPI.delete_all_user_groups(delete_user_group_template, organization)
            
    
    @staticmethod
    def remove_test_advanced_users(keyword_array=["logigear1+user", "logigear2+user"], organization=Constant.AdvancedOrgName):
        for keywork in keyword_array:
            UserAPI.delete_all_users(keywork, organization)
            
    
    @staticmethod
    def remove_test_simplified_users(driver, organization, device_name, keyword_array=["logigear1+user", "logigear2+user"]):
        user_emails = DeviceGroupAPI.get_all_users_in_simplified_device_group(device_name=device_name, access_token=TestCondition._get_access_token(driver), organization=organization)
        user = User()
        for email in user_emails:
            for keywork in keyword_array:
                if(keywork in email):
                    user.email_address = email
                    TestCondition.delete_simplified_users(driver, [user], device_name, organization)
            
    
    @staticmethod
    def remove_all_device_groups(delete_device_group_template="LGVN Device Group", organization=Constant.AdvancedOrgName):
        DeviceGroupAPI.delete_all_device_groups(delete_device_group_template, organization)
        

    @staticmethod
    def reject_all_access_requests(driver, organization=Constant.AdvancedOrgName):
        try:
            user = User()
            user.generate_advanced_org_admin_data()
            user.organization = organization
            TestCondition.create_advanced_organization_admins(driver, [user])
                
            HomePage(driver).open_and_goto_login_page()\
                .login_as_unwatched_video_user(user.email_address, user.password)\
                .goto_admin_dashboard_page_by_menu_item()\
                .reject_all_access_requests()
                
            TestCondition.delete_advanced_users([user], user.organization)
        except Exception as ex:
            print(str(ex))
            
            
    @staticmethod
    def force_log_out(driver):
        try:
            home_page = HomePage(driver, False)
            if(not home_page.is_page_displayed(1)):
                home_page.open()
            if(home_page.is_logged_in(1)):
                home_page = home_page.logout()
                driver.refresh()
            
            return home_page
        except Exception:
            print("*** There is an error while logging out from Suitable Technologies")
    
    
    @staticmethod
    def set_organization_setting(setting_name, setting_value, organization=Constant.AdvancedOrgName):
        """
        @param: setting_name - input these values below:
                "organization name"
                "default invite message"
                "email notifications"
        """
        try:
            response = OrganizationSettingAPI.update_an_organization_setting(setting_name, setting_value, organization)
            if(response.status_code != 202):
                raise Exception("Error while Update Organization Setting. Reason: {}".format(response.reason))
        except Exception as ex:
            print(str(ex))

    
    @staticmethod
    def set_advanced_device_group_admin(user_email, device_group_name, organization):
        response = DeviceGroupAPI.set_advanced_device_group_admin([user_email], device_group_name, organization)
        if(response.status_code != 204):
            raise Exception("Error while granting Device Group Admin permission for user {}. Reason: {}".format(user_email, response.reason))
    
    
    @staticmethod
    def restore_advanced_beam_name(beam_name, organization):
        TestCondition._update_advanced_beam_device(beam_name, "name", beam_name, organization)
    
    
    @staticmethod
    def restore_advanced_beam_location(beam_name, location, organization):
        TestCondition._update_advanced_beam_device(beam_name, "location", location, organization)
        
            
    @staticmethod
    def restore_advanced_beam_labels(beam_name, label_array, organization):
        TestCondition._update_advanced_beam_device(beam_name, "labels", label_array, organization)
        
            
    @staticmethod
    def restore_simplified_beam_name(driver, beam_name, organization):
        TestCondition._update_simplified_beam_device(driver, beam_name, "name", beam_name, organization)
    
    
    @staticmethod
    def restore_simplified_beam_labels(driver, beam_name, label_tag_list, organization):
        TestCondition._update_simplified_beam_device(driver, beam_name, "labels", label_tag_list, organization)
        
    
    @staticmethod
    def restore_simplified_beam_location(driver, beam_name, location, organization):
        TestCondition._update_simplified_beam_device(driver, beam_name, "location", location, organization)
        
    
    @staticmethod
    def restore_gmail_non_gsso_auth_forwarded_state(driver, user_account):
        """
        @author: Duy Nguyen
        @description: This action use to handle while there is failure occur in test case cause the condition
         of GMailNonGSSOAuthenForwarded account is invalid.
        """
        try:
            welcome_page = TestCondition.force_log_out(driver=driver).goto_login_page()\
                .login_to_force_restore_gsso_authentication_account(user_account.email_address)
            
            if (welcome_page.is_welcome_user_page_displayed(3)):
                welcome_page.goto_account_settings_page_by_menu_item()\
                    .disconect_from_google()\
                    .reset_password(user_account).logout()
            
        except Exception:
            print ("*** There is failure when restoring GMailNonGSSOAuthenForwarded account")
    
    
    @staticmethod
    def _get_api_key(organization_name):
        return Constant.APIKeys[organization_name]
    
    
    @staticmethod
    def _re_get_access_token(driver):
        TestCondition.SIMPLIFIED_ACCESS_TOKEN = ""
        return TestCondition._get_access_token(driver)
        
    
    @staticmethod
    def _set_access_token(driver, admin_email, admin_password):
        TestCondition.SIMPLIFIED_ACCESS_TOKEN = ""
        TestCondition.force_log_out(driver).goto_login_page()\
            .login(admin_email, admin_password)
            
        try:
            csrftoken = driver.get_cookie("csrftoken")["value"]
            driver.execute_script("""
                return (function SendRequest(csrfToken){\
                    var url = '/accounts/token/';\
                    var params = 'grant_type=password&client_id=AaYV4palQjKafAN7BMyUQsuxuQEFGRLtGPzeL8dJ';\
                    var http = new XMLHttpRequest();\
                    http.onreadystatechange = function(){\
                        if (http.readyState==4 && http.status == 200){\
                            window._logigear_test_token = http.responseText;\
                        }\
                    };\
                    http.open('POST', url, true);\
                    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');\
                    http.setRequestHeader('Content-length', params.length);\
                    http.setRequestHeader('X-CSRFToken', csrfToken);\
                    http.send(params);\
                    return true;\
                })(arguments[0]);
                """, csrftoken)
            sleep(2)
            result = driver.execute_script("return window._logigear_test_token;")
            result_dict = json.loads(result)
            TestCondition.SIMPLIFIED_ACCESS_TOKEN = result_dict["access_token"]
        except Exception as ex:
            raise Exception(ex)
        
    
    @staticmethod
    def _get_access_token(driver, timeout=30):
        sw = Stopwatch()
        sw.start()
        
        while(timeout > 0):
            if(TestCondition.SIMPLIFIED_ACCESS_TOKEN == ""):
                TestCondition._set_access_token(driver, Constant.SimplifiedAdminEmail, Constant.DefaultPassword)

            response = UserAPI.get_info(Constant.SimplifiedOrgName, TestCondition.SIMPLIFIED_ACCESS_TOKEN)
            if(response.status_code == 200):
                return TestCondition.SIMPLIFIED_ACCESS_TOKEN
            elif(response.status_code == 401):
                TestCondition._set_access_token(driver, Constant.SimplifiedAdminEmail, Constant.DefaultPassword)
            
            timeout -= sw.elapsed().total_seconds()
        
        raise Exception("Cannot get access token with account: {}/{}".format(Constant.SimplifiedAdminEmail, Constant.DefaultPassword))
        
    
    @staticmethod        
    def _create_advanced_normal_user(driver, user):
        """      
        @summary: Create new advanced normal user   
        @param user: user to be created
        @author: Thanh Le         
        """
        response = UserAPI.invite_advanced_user(user)
        if(response.status_code != 201):
            raise Exception("Error while creating advanced user {}. Reason: {}".format(user.tostring(), response.reason))
            

    @staticmethod        
    def _create_simplified_normal_user(driver, user, device_name, organization):
        """      
        @summary: Create new simplified normal user   
        @param user: user to be created
        @author: Thanh Le         
        """
        response = UserAPI.invite_simplified_user(user, device_name, organization, TestCondition._get_access_token(driver))
        if(response.status_code != 201):
            raise Exception("Error while creating simplified user {}. Reason: {}".format(user.tostring(), response.reason))
        
        
    @staticmethod
    def set_language(driver, user):
        """      
        @summary: Set language for user 
        @param user: user to be created
        @author: Khoi Ngo        
        """
        TestCondition.force_log_out(driver).goto_login_page()\
            .login(user.email_address, user.password)\
            .goto_account_settings_page_by_menu_item()\
            .set_language(driver.driverSetting.language)\
            .save_change()\
            .logout()        
    
    
    @staticmethod
    def _activate_user(driver, user, is_temporary=False, is_simplified=False):
        """      
        @summary: Activate new user  
        @param driver: WebDriver 
        @param user: user to be activated
        @param is_temporary: Set to False if activate normal user. Set to True if activate temporary user
        @author: Thanh Le         
        """
        try:
            if not user.activated:
                TestCondition.force_log_out(driver)
                
                # NOTE: creating user using API always sent email in English. Thus hardcode the email subjects.
                if(is_temporary == False):
                    activation_link = GmailUtility.get_email_activation_link(email_subject="Welcome to Beam at", receiver=user.email_address, sent_day=datetime.now())
                else:
                    activation_link = GmailUtility.get_temporary_activation_link(email_subject="You've been invited to Beam", receiver=user.email_address, sent_day=datetime.now())
                    
                if activation_link == None:
                    raise Exception("Cannot get activation link")
                
                account_settings_page = PasswordSetupPage(driver, activation_link).set_password(user.password)\
                    .goto_account_settings_page_by_menu_item()\
                    .set_language(driver.driverSetting.language)
                # set user activated
                user.activated = True
                if(is_simplified):
                    account_settings_page.set_first_last_name(user)
                
                account_settings_page.save_change()
                print("User {} is created.".format(user.email_address))
        except:
            raise Exception("Error while activating and setting language for user {}".format(user.email_address))
    
    
    @staticmethod
    def _update_advanced_beam_device(beam_name, beam_property, property_value, organization=Constant.AdvancedOrgName):
        """
        @parameter: beam_name: Original Beam Device Name
                    beam_property: only choosing - name,location,labels,time zone,group
                    property_value: value of edit property
                    organization: The Organization which device was located.
        """
        response = DeviceAPI.edit_advanced_device(beam_name, beam_property, property_value, organization)    
        
        if(response.status_code != 202):
            raise Exception("Error while updating {} property of beam {}. Reason: {}".format(beam_property, beam_name, response.reason))

    
    @staticmethod
    def _update_simplified_beam_device(driver, beam_name, beam_property, property_value, organization):
        """
        @parameter: beam_name: Original Beam Device Name
                    beam_property: only choosing - name,location,labels
                    property_value: value of edit property
                    organization: The Organization which device was located.
        """
        response = DeviceAPI.edit_simplified_device(device_name=beam_name, device_property=beam_property, property_value=property_value, organization=organization, access_token=TestCondition._get_access_token(driver))    
        
        if(response.status_code != 204):
            raise Exception("Error while updating {} property of beam {}. Reason: {}".format(beam_property, beam_name, response.reason))
        
        
    
       
         



