import os.path
import re
from time import sleep

from PIL import Image, ImageChops

from common.helper import Helper
from data import testdata
import shutil


class Utilities(object):
    
    @staticmethod
    def are_strings_equal(string1, string2):
        import unicodedata
        unicodedata.normalize('NFKD', string1).encode('ascii', 'ignore')
        unicodedata.normalize('NFKD', string2).encode('ascii', 'ignore')
        return string1 == string2
    
    
    @staticmethod
    def does_contain_whole_word(word):
        return re.compile(r'\b({0})\b'.format(word), flags=re.IGNORECASE).search
    
    
    @staticmethod
    def is_number(txt):
        try: 
            float(txt)
            return True
        except ValueError:
            return False
        
        
    @staticmethod
    def does_contain(search_list, primary_list):
        """
        @description: check if the first list is a child collection of the second list
        """
        for item in search_list:
            if not item in primary_list:
                return False
        return True
    
    
    @staticmethod
    def generate_temporary_access_time_label(date_label, starting_datetime=None, ending_datetime=None):
        from common.application_constants import ApplicationConst
        time_label = ApplicationConst.get_date_time_label("All day")
        
        if starting_datetime and ending_datetime:
            start_hr = str(int (starting_datetime.strftime("%I")))
            start_mn = starting_datetime.strftime("%M")
            start_mr = ApplicationConst.get_date_time_label(starting_datetime.strftime("%p")).lower()  # lower() only use for temporary
            
            end_hr = str(int (ending_datetime.strftime("%I")))
            end_mn = ending_datetime.strftime("%M")
            end_mr = ApplicationConst.get_date_time_label(ending_datetime.strftime("%p")).lower()  # lower() only use for temporary
            
            time_label = "{}:{} {} - {}:{} {}".format(start_hr, start_mn, start_mr, end_hr, end_mn, end_mr)
            
        item_label = "{} {}".format(date_label, time_label)        
        
        return item_label


    @staticmethod
    def generate_access_time_label(access_days, starting_datetime=None, ending_datetime=None):
        from common.application_constants import ApplicationConst
        access_days = sorted(access_days, key=lambda c: c.value)
        
        time_label = ApplicationConst.get_date_time_label("All day")
        
        if starting_datetime and ending_datetime:
            start_hr = str(int (starting_datetime.strftime("%I")))
            start_mn = starting_datetime.strftime("%M")
            start_mr = ApplicationConst.get_date_time_label(starting_datetime.strftime("%p"))
            
            end_hr = str(int (ending_datetime.strftime("%I")))
            end_mn = ending_datetime.strftime("%M")
            end_mr = ApplicationConst.get_date_time_label(ending_datetime.strftime("%p"))
            
            time_label = "{}:{} {} - {}:{} {}".format(start_hr, start_mn, start_mr, end_hr, end_mn, end_mr)
        
        day_lbl = ""
        if(len(access_days) == 7):
            day_lbl = ApplicationConst.get_date_time_label("every day")
        else:
            day_lbl = ', '.join((ApplicationConst.get_date_time_label(d.name) for d in access_days))
            
        item_label = "{} {}".format(time_label, day_lbl)        
        
        return item_label   
    
    
    @staticmethod
    def get_test_image_file_path(file_name):
        return os.path.join(os.path.dirname(testdata.__file__), "images", file_name)
    
    
    @staticmethod
    def delete_file(file_path):
        try:
            if os.path.exists(file_path):
                os.remove(file_path)
        except:
            pass
    
    
    @staticmethod
    def does_file_existed(file_path):
        try:
            folder, file_name = os.path.split(file_path)
            for file in os.listdir(folder):
                if file == file_name:
                    return True
            
        except:
            pass
        return False
    
    
    @staticmethod
    def delete_all_files(folder_path):
        try:
            if(os.path.exists(folder_path)):
                shutil.rmtree(folder_path)
                os.makedirs(folder_path)
        except Exception as ex:
            print(str(ex))
    
    
    @staticmethod
    def correct_link(file_url):
        head, tail = os.path.split(file_url)
        file_name = tail.split('?')[0]
        return head + "/" + file_name
    
    
    @staticmethod
    def get_file_name(file_url):
        file_name = os.path.split(file_url)[1]
        file_name = file_name.split("?")[0]
        if not file_name:
            file_name = "empty.tmp"
        return file_name
    
    
    @staticmethod
    def download_file(file_url):
        file_name = Utilities.get_file_name(file_url)
        file_path = Utilities.get_test_image_file_path(file_name)
        
        import urllib.request
        f = open(file_path, 'wb')
        f.write(urllib.request.urlopen(file_url).read())
        f.close()
        
        return file_path
    
    
    @staticmethod
    def trimmed_text(text_content):
        if text_content:
            text_content = re.sub(r'\\r|\\n|\r|\n', '', text_content).strip()
            text_content = text_content.replace(u'\xa0', u' ')
            return text_content
        return None
    
    
    @staticmethod
    def normalize_text(text_content):
        text_content = text_content.replace(u'\xa0', u' ')
        return text_content
    
    
    @staticmethod
    def wait_for_file_is_downloaded(file_path, timeout=300):
        count = 0
        t_sleep = 1
        max_count = int(timeout / t_sleep)
        does_file_exist = Utilities.is_file_existed(file_path)  
        file_size = Utilities.get_file_size(file_path)
        
        while(count < max_count):
            if(does_file_exist == False or file_size <= 0):
                sleep(t_sleep)  # sleep 1 seconds to wait downloading completed
                does_file_exist = Utilities.is_file_existed(file_path)  
                file_size = Utilities.get_file_size(file_path)
                count += 1
            else:
                return
    
    
    @staticmethod
    def is_file_existed(file_path):
        return os.path.isfile(file_path.replace("\\", os.path.sep))
            
    @staticmethod
    def get_file_size(file_path):
        try:
            return os.path.getsize(file_path)
        except:
            return 0
    
    
class CSV_Utilities(object):
    
    @staticmethod
    def _generate_invalid_first_last_name_array():
        return ["'@%$#^%$&^%*&^()(*)*)&(*(^%%$$#%@$!/\")'",
                "***[(.)_(.)b]***",
                "Hello".zfill(135),
                "<script>alert('hello');</script>",
                "<jsscript>alert(\"Hello world!\");</jsscript>"]
    
        
    @staticmethod
    def _generate_valid_first_last_name_array():
        return ["'http://g.nordstromimage.com/imagegallery/store/product/Medium/3/_9488583.jpg'",
                "https://www.google.com/images/srpr/logo11w.png",
                "suitable01", "suitable02", "tester9488505",
                "<CTRL+ALT+DEL>", "^altDel", "CTRL+ALT+DEL",
                "^a", "<CTRL+a>",
                "^c", "<CTRL+c>",
                "'./ps -ef'",
                "www.cnn.com/video/live/live_asx.html",
                "'www.cnn.com/video/live/live_asx_1234.html'"]
    
    
    @staticmethod
    def generate_invalid_users(number_of_users):
        from random import randint
        valid_array = CSV_Utilities._generate_valid_first_last_name_array()
        invalid_array = CSV_Utilities._generate_invalid_first_last_name_array()
        invalid_length = len(invalid_array) 
        valid_length = len(valid_array)        
        results = []   
        
        count = 0
        while count < number_of_users:
            count += 1
            rand = randint(0, valid_length - 1)
            fn = valid_array[rand]            
            rand2 = randint(0, invalid_length - 1)
            ln = invalid_array[rand2]
            email = Helper.generate_random_email()
            results.append([email, fn, ln])
        
        return results
    
    
    @staticmethod
    def generate_valid_users(number_of_users, special_chracter=True):
        from random import randint
        test_array = CSV_Utilities._generate_valid_first_last_name_array()
        length = len(test_array)        
        results = []   
        
        count = 0
        while count < number_of_users:
            if special_chracter:
                rand = randint(0, length - 1)
                fn = test_array[rand]
                rand2 = randint(0, length - 1)
                while rand2 == rand:
                    rand2 = randint(0, length - 1)
                ln = test_array[rand2]
            else:
                fn = "FN " + Helper.generate_random_not_special_string()
                ln = "LN " + Helper.generate_random_not_special_string()
            count += 1
            email = Helper.generate_random_email()
            results.append([email, fn, ln])
        
        return results
    
    
    @staticmethod
    def generate_users_in_csv(number_of_users=5, generate_valid_data=True, special_character=True):
        import csv
        _quoting = csv.QUOTE_NONE
        
        if(generate_valid_data):
            if special_character:
                users = CSV_Utilities.generate_valid_users(number_of_users)
            else:
                users = CSV_Utilities.generate_valid_users(number_of_users, special_character)
            csv_file_path = os.path.join(os.path.dirname(testdata.__file__), "valid_users.csv")
        else:
            users = CSV_Utilities.generate_invalid_users(number_of_users)
            csv_file_path = os.path.join(os.path.dirname(testdata.__file__), "invalid_users.csv")
            _quoting = csv.QUOTE_MINIMAL
        
        # users.append(["abc", "fnfnfn", "lnlnln"])
        with open(csv_file_path, 'w') as tested_csv_file:
            csv_writer = csv.writer(tested_csv_file, lineterminator="\n", quoting=_quoting)
            for user in users:
                csv_writer.writerow(user)
            
        return csv_file_path
    
    
    @staticmethod
    def find_all_users_in_csv(csv_file_path):
        results = []
        try:
            with open(csv_file_path) as fff:
                for line in fff:
                    email = line.split(',')[0].strip()
                    results.append(email)
        except:
            print("Failed to read data from CSV file.")
        
        return results
    
    
    @staticmethod
    def find_users_info_in_csv(csv_file_path):
        from data.dataobjects.user import User
        results = []
        try:
            with open(csv_file_path) as fff:
                for line in fff:
                    user = User()
                    user.email_address = line.split(',')[0].strip()
                    user.first_name = line.split(',')[1].strip()
                    user.last_name = line.split(',')[2].strip()
                    results.append(user)
        except:
            print("Failed to read data from CSV file.")
        
        return results
    
    
class Image_Utilities(object):
    
        
    @staticmethod
    def are_images_equal(img_actual, img_expected):
        """
        @summary:  compare 2 images pixel-by-pixel       
        @param <img_actual>,<img_expected>: local path of those images that need to be compared.
        @return:  Returns true if the images are identical(all pixels in the difference image are zero)"
        """
        result_flag = False
        # Check that img_actual exists
        if not os.path.exists(img_actual):
            print('Could not locate the generated image: %s' % img_actual)
     
        # Check that img_expected exists
        if not os.path.exists(img_expected):
            print('Could not locate the baseline image: %s' % img_expected)
     
        if os.path.exists(img_actual) and os.path.exists(img_expected):
            actual = Image.open(img_actual)
            expected = Image.open(img_expected)
            result_image = ImageChops.difference(actual, expected)
     
            # Where the real magic happens
            if (result_image.getbbox() is None):
                result_flag = True
     
            # Bonus code to store the overlay
            # Result image will look black in places where the two images match
            # color_matrix = ([0] + ([255] * 255))
            # result_image = result_image.convert('L')
            # result_image = result_image.point(color_matrix)
            # result_image.save(result)#Save the result image
     
        return result_flag


    @staticmethod
    def are_images_similar(image_filepath1, image_filepath2, percentage_accuracy=95):
        """
        calculate the similarity between 2 images. 
        """
        try:
            similarity = Image_Utilities.image_similarity_vectors_via_numpy(image_filepath1, image_filepath2)
            if similarity * 100 >= percentage_accuracy:
                return True
        except:
            return False
        
        return False
    
    
    @staticmethod
    def get_thumbnail(image, size=(128, 128), stretch_to_fit=False, greyscale=False):
        " get a smaller version of the image - makes comparison much faster/easier"
        if not stretch_to_fit:
            image.thumbnail(size, Image.ANTIALIAS)
        else:
            image = image.resize(size);  # for faster computation
        if greyscale:
            image = image.convert("L")  # Convert it to grayscale.
        return image
    

    @staticmethod
    def image_similarity_bands_via_numpy(filepath1, filepath2):
        import numpy
        
        image1 = Image.open(filepath1)
        image2 = Image.open(filepath2)
     
        # create thumbnails - resize em
        image1 = Image_Utilities.get_thumbnail(image1)
        image2 = Image_Utilities.get_thumbnail(image2)
        
        # this eliminated unqual images - though not so smarts....
        if image1.size != image2.size or image1.getbands() != image2.getbands():
            return -1
        similarity = 0
        for band_index, band in enumerate(image1.getbands()):
            m1 = numpy.array([p[band_index] for p in image1.getdata()]).reshape(*image1.size)
            m2 = numpy.array([p[band_index] for p in image2.getdata()]).reshape(*image2.size)
            similarity += numpy.sum(numpy.abs(m1 - m2))
        return similarity
    
    
    @staticmethod
    def image_similarity_vectors_via_numpy(filepath1, filepath2):
        """
        One of the things is that if you wanted to compute how similar two images are, 
        you’d treat their pixels as vectors, normalize them, then take their dot product. 
        The result is a float between 0 and 1 that indicated the percent similarity of the two images. 
        This process is called the 'normalized cross correlation'. 
        After you got that number, it was a matter of setting a threshold as to what you wanted to accept as similar or not. 
        """
        # source: http://www.syntacticbayleaves.com/2008/12/03/determining-image-similarity/
        # may throw: Value Error: matrices are not aligned . 
        from numpy import average, linalg, dot
        
        image1 = Image.open(filepath1)
        image2 = Image.open(filepath2)
     
        image1 = Image_Utilities.get_thumbnail(image1, stretch_to_fit=True)
        image2 = Image_Utilities.get_thumbnail(image2, stretch_to_fit=True)
        
        images = [image1, image2]
        vectors = []
        norms = []
        for image in images:
            vector = []
            for pixel_tuple in image.getdata():
                vector.append(average(pixel_tuple))
            vectors.append(vector)
            norms.append(linalg.norm(vector, 2))
        a, b = vectors
        a_norm, b_norm = norms
        # ValueError: matrices are not aligned !
        res = dot(a / a_norm, b / b_norm)
        return res

    
