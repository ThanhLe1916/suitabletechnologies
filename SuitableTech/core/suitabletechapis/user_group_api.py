from common.constant import Constant
from core.suitabletechapis.api_base import APIBase
import json


class UserGroupAPI(APIBase):
    _baseurl = Constant.APIBaseURL + "user-groups/"
    
    
    @staticmethod
    def _get_user_group(user_group_id="", organization=Constant.AdvancedOrgName):
        return APIBase.get_method(url=UserGroupAPI._baseurl + user_group_id, headers=APIBase.generate_header(organization))
    
    
    @staticmethod
    def _delete_user_group(user_group_id, organization=Constant.AdvancedOrgName):
        return APIBase.delete_method(url=UserGroupAPI._baseurl + user_group_id, headers=APIBase.generate_header(organization))
    
    
    @staticmethod
    def create_user_group(name, user_email_array, organization=Constant.AdvancedOrgName):
        data_object = json.dumps({'name':name, 'users':user_email_array})
        return APIBase.post_method(url=UserGroupAPI._baseurl, headers=APIBase.generate_header(organization), data=data_object)
        
    
    @staticmethod
    def delete_user_group_bylistid(list_id, organization=Constant.AdvancedOrgName):
        try:
            for item in list_id:
                UserGroupAPI._delete_user_group(item, organization=organization)
        except Exception as ex:
            raise(ex)
        
        
    @staticmethod
    def delete_all_user_groups(keyword="LGVN User Group", organization=Constant.AdvancedOrgName):
        try:
            response = UserGroupAPI._get_user_group(organization=organization)
            for item in response.json()["objects"]:
                if ((keyword in item["name"])):
                    UserGroupAPI.delete_user_group(item["name"], organization)
                    print ("Deleting User Group: {}".format(item["name"]))
        except Exception as ex:
            raise(ex)
        
    
    @staticmethod
    def _get_user_groups(organization=Constant.AdvancedOrgName):
        return APIBase.get_method(url=UserGroupAPI._baseurl, headers=APIBase.generate_header(organization))
    
    
    @staticmethod
    def get_user_group_id(user_group_name, organization=Constant.AdvancedOrgName):
        try:
            get_user_groups_response = UserGroupAPI._get_user_groups(organization)
            for user_group in get_user_groups_response.json()["objects"]:
                if (user_group["name"] == user_group_name):
                    return user_group["id"]
            
        except Exception as ex:
            raise(ex)
        
        
    @staticmethod
    def delete_user_group(user_group_name, organization=Constant.AdvancedOrgName):
        try:
            user_group_id = UserGroupAPI.get_user_group_id(user_group_name, organization)
            return UserGroupAPI._delete_user_group(user_group_id, organization)
        except Exception as ex:
            raise(ex)        

