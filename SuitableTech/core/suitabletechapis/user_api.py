from common.constant import Constant
from core.suitabletechapis.api_base import APIBase
from core.suitabletechapis.device_group_api import DeviceGroupAPI
from core.suitabletechapis.user_group_api import UserGroupAPI
import json
from datetime import datetime
from core.utilities.gmail_utility import GmailUtility

class UserAPI(APIBase):
    _baseurl = Constant.APIBaseURL + "users/"
    _invite_url = Constant.APIBaseURL + "invite/"
    _temporary_access = Constant.APIBaseURL + "temporary-access/"
    _setting_url = Constant.APIBaseURL + "settings/"
    _info_url = Constant.APIBaseURL + "info/"
    
    
    @staticmethod
    def _convert_to_json(user, organization=Constant.AdvancedOrgName):
        device_groups = []
        user_groups = []
        
        if(user.device_group):
            device_groups.append(DeviceGroupAPI.get_device_group_id(user.device_group, organization))
        if(user.user_group):
            user_groups.append(UserGroupAPI.get_user_group_id(user.user_group, organization))
        
        data_object = {'email_address':user.email_address, 'first_name':user.first_name, 'last_name':user.last_name, 'from_name':user.organization, 'device_groups': device_groups, 'user_groups':user_groups}
        return json.dumps(data_object)
    

    @staticmethod
    def _get_user(user_email="", organization=Constant.AdvancedOrgName):
        return APIBase.get_method(url=UserAPI._baseurl + user_email, headers=APIBase.generate_header(organization))
    
    
    @staticmethod
    def delete_user(user_email="", organization=Constant.AdvancedOrgName):
        return APIBase.delete_method(url=UserAPI._baseurl + user_email, headers=APIBase.generate_header(organization))
    
    
    @staticmethod
    def invite_advanced_user(user):
        return APIBase.post_method(url=UserAPI._invite_url, headers=APIBase.generate_header(user.organization), data=UserAPI._convert_to_json(user, user.organization))


    @staticmethod
    def invite_simplified_user(user, device_name, organization, access_token):
        data = {'email_address':user.email_address, 'device_groups': Constant.DeviceIDs[device_name]}
        return APIBase.post_method(url=UserAPI._invite_url, headers=APIBase.generate_simplified_header(access_token=access_token, organization=organization), data=data)
    
    
    @staticmethod
    def invite_new_temporary_user(user, device_group, start_date, end_date, answer_required):
        _device_group_id = DeviceGroupAPI.get_device_group_id(device_group, user.organization)
        _start_date = datetime.strftime(start_date, "%Y-%m-%dT%H:%M:%S")
        _end_date = datetime.strftime(end_date, "%Y-%m-%dT%H:%M:%S")
        data_object = {'email_address':user.email_address, 'first_name':user.first_name, 'last_name':user.last_name, 'device_group': _device_group_id, 'start': _start_date, 'end': _end_date, 'answer_required': answer_required}
        data = json.dumps(data_object)
        return APIBase.post_method(url=UserAPI._temporary_access, headers=APIBase.generate_header(user.organization), data=data)
    
    
    @staticmethod
    def set_org_admin(user_email, organization=Constant.AdvancedOrgName):
        data = json.dumps({'is_admin': True })
        return APIBase.patch_method(UserAPI._baseurl + user_email + "/", headers=APIBase.generate_header(organization), data=data)
    
    
    @staticmethod
    def delete_all_users(keyword="logigear1+user", organization=Constant.AdvancedOrgName):
        response = UserAPI._get_user("", organization)
        for item in response.json()["objects"]:
            if (keyword in item["email_address"]):
                UserAPI.delete_user(item["email_address"], organization)
                GmailUtility.delete_all_emails(item["email_address"])
                print ("Deleting User: {}".format(item["email_address"]))
                
    
    @staticmethod
    def set_user_settings():
        pass
    
    @staticmethod
    def get_info(organization, access_token):
        return APIBase.get_method(url=UserAPI._info_url, headers=APIBase.generate_simplified_header(access_token=access_token, organization=organization))
        
        