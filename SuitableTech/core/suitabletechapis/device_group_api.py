from common.constant import Constant
import json
from core.suitabletechapis.api_base import APIBase
from core.suitabletechapis.device_api import DeviceAPI
from core.suitabletechapis.user_group_api import UserGroupAPI

class DeviceGroupAPI(APIBase):
    _baseurl = Constant.APIBaseURL + "device-groups/"
    _device_group_memberships = Constant.APIBaseURL + "device-group-memberships/"
    property_dict = {'name':'name', 'location':'location', 'labels':'tags', 'time zone':'time_zone', 'group':'device_group'}
    
    @staticmethod
    def delete_all_device_groups(keyword="LGVN Group", organization=None):    
        if(organization == None):
            organization = Constant.AdvancedOrgName
            
        response = DeviceGroupAPI._get_device_groups(organization)
        for item in response.json()["objects"]:
            if ((keyword in item["name"]) ):
                DeviceGroupAPI.delete_device_group(item["name"], organization)
                print ("Deleting Device Group: {}".format(item["name"]))
           
           
    @staticmethod
    def _get_device_groups(organization=None):
        if(organization == None):
            organization = Constant.AdvancedOrgName
            
        return APIBase.get_method(url=DeviceGroupAPI._baseurl, headers=APIBase.generate_header(organization))
    
    
    @staticmethod
    def create_device_group(name, device_array, organization=None):
        if(organization == None):
            organization = Constant.AdvancedOrgName
            
        response = DeviceAPI.get_devices(organization)
        device_id_list = []
        
        if (device_array):
            for item in response.json()["objects"]:
                if (item["name"] in device_array):
                    device_id_list.append(item["id"])
        
        if(device_array != [] and device_id_list == []):
            raise Exception("Cannot find the devices {} to add into device group".format(', '.join(device_array)))
        
        data_object = json.dumps({'name':name, 'devices':device_id_list})
        return APIBase.post_method(url=DeviceGroupAPI._baseurl, headers=APIBase.generate_header(organization), data=data_object)       
        
    
    @staticmethod
    def get_device_group_id(device_group_name, organization=None):
        if(organization == None):
            organization = Constant.AdvancedOrgName
            
        get_device_groups_response = DeviceGroupAPI._get_device_groups(organization)
        for device_group in get_device_groups_response.json()["objects"]:
            if (device_group["name"] == device_group_name):
                return device_group["id"]
        return None
            
    
    @staticmethod
    def get_advanced_device_group(device_group_name, organization):
        get_device_groups_response = DeviceGroupAPI._get_device_groups(organization)
        for device_group in get_device_groups_response.json()["objects"]:
            if (device_group["name"] == device_group_name):
                return device_group
        return None
    
    
    @staticmethod
    def delete_device_group(device_group_name, organization=None):
        if(organization == None):
            organization = Constant.AdvancedOrgName
            
        get_device_group_id = DeviceGroupAPI.get_device_group_id(device_group_name, organization)
        return APIBase.delete_method(url=DeviceGroupAPI._baseurl + get_device_group_id + "/", headers=APIBase.generate_header(organization))
                   
    
    @staticmethod
    def add_user_group(device_group_name, user_group_name, organization=None):
        device_group_id = DeviceGroupAPI.get_device_group_id(device_group_name, organization)
        user_group_id = UserGroupAPI.get_user_group_id(user_group_name, organization)
        
        data_object = json.dumps({'device_group':device_group_id, 'user_group':user_group_id})
        return APIBase.post_method(url=DeviceGroupAPI._device_group_memberships, headers=APIBase.generate_header(organization), data=data_object)
    
    
    @staticmethod
    def _get_simplified_device_group(device_group_name, access_token, organization):
        url = DeviceGroupAPI._baseurl + Constant.DeviceIDs[device_group_name] + "/"
        return APIBase.get_method(url=url, headers=APIBase.generate_simplified_header(access_token=access_token, organization=organization))
        
        
    @staticmethod
    def _get_simplified_device_group_more_detail(device_group_name, access_token, organization):
        url = DeviceGroupAPI._device_group_memberships + "?device_group=" + Constant.DeviceIDs[device_group_name]
        return APIBase.get_method(url=url, headers=APIBase.generate_simplified_header(access_token=access_token, organization=organization))
    
    
    @staticmethod
    def _get_simplified_user_id(user_email, device_name, organization, access_token):
        response = DeviceGroupAPI._get_simplified_device_group_more_detail(device_group_name=device_name, access_token=access_token, organization=organization)
        
        for user_info in response.json()["objects"]:
            if(user_info["user"]==user_email):
                return str(user_info["id"])
        
        return None
    
    
    @staticmethod
    def set_simplified_device_admin(user_email, device_name, organization, access_token):
        response = DeviceGroupAPI._get_simplified_device_group(device_group_name=device_name, access_token=access_token, organization=organization)
        origin_data = response.json()
        origin_data["admins"].append(user_email)
        data_object = json.dumps(origin_data)
        url = DeviceGroupAPI._baseurl + Constant.DeviceIDs[device_name] + "/"
        return APIBase.put_method(url=url, headers=APIBase.generate_simplified_header(access_token=access_token, organization=organization), data=data_object)
    
       
    @staticmethod
    def set_advanced_device_group_admin(user_email_array, device_group_name, organization):
        device_group_info = DeviceGroupAPI.get_advanced_device_group(device_group_name, organization)
        
        device_group_id = device_group_info["id"]
        cr_addmins = device_group_info["admins"]
        cr_devices = device_group_info["devices"]
        url = DeviceGroupAPI._baseurl + device_group_id + "/"
        data = {"name": device_group_name, "add_admins": user_email_array, "admins": cr_addmins, "devices": cr_devices}
        return APIBase.put_method(url=url, headers=APIBase.generate_header(organization), data=data)
    
    
    @staticmethod
    def delete_user(user_email, device_name, organization, access_token):
        user_id = DeviceGroupAPI._get_simplified_user_id(user_email, device_name, organization, access_token)
        return APIBase.delete_method(DeviceGroupAPI._device_group_memberships + user_id + "/", headers=APIBase.generate_simplified_header(access_token=access_token, organization=organization))

    
    @staticmethod
    def get_all_users_in_simplified_device_group(device_name, access_token, organization):
        response = DeviceGroupAPI._get_simplified_device_group_more_detail(device_group_name=device_name, access_token=access_token, organization=organization)
        users = []
        
        for user_info in response.json()["objects"]:
            users.append(user_info["user"])
            
        return users
    
    #https://stg1.suitabletech.com/admin-api/1/device-groups/1524/
    
    
    @staticmethod
    def edit_simplified_device_group(device_name, device_property, property_value, organization, access_token):
        response = DeviceGroupAPI._get_simplified_device_group(device_group_name=device_name, access_token=access_token, organization=organization)
        origin_data = response.json()
        origin_data[DeviceGroupAPI.property_dict[device_property]] = property_value
        data_object = json.dumps(origin_data)
        url = DeviceGroupAPI._baseurl + Constant.DeviceIDs[device_name] + "/"
        return APIBase.put_method(url=url, headers=APIBase.generate_simplified_header(access_token=access_token, organization=organization), data=data_object)
    