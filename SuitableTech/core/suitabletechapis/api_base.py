import requests
from common.constant import Constant

class APIBase(object):
    _timeout = 30
    
    @staticmethod
    def get_method(url="", headers=None, params=None):
        """
        @Author: Duy Nguyen
        @Parameter: url: Url REST address
                    header: header values with format dictionary
                    params: parameter values with format dictionary
                    data: data value with format json object
        """
        try:
            response = requests.get(url, headers=headers, params=params, timeout=APIBase._timeout)
            return response
        except Exception as ex:
            raise(ex)
    
    
    @staticmethod
    def post_method(url="", headers=None, data=None):
        """
        @Author: Duy Nguyen
        @Parameter: url: Url REST address
                    header: header values with format dictionary
                    params: parameter values with format dictionary
                    data: data value with format json object
        """
        try:
            response = requests.post(url, headers=headers, data=data, timeout=APIBase._timeout)
            return response
        except Exception as ex:
            raise(ex)
    
    
    @staticmethod
    def delete_method(url=None, headers=None, data=None): 
        """
        @Author: Duy Nguyen
        @Parameter: url: Url REST address
                    header: header values with format dictionary
                    params: parameter values with format dictionary
                    data: data value with format json object
        """
        try:
            response = requests.delete(url, headers=headers, data=data, timeout=APIBase._timeout)
            return response
        except Exception as ex:
            raise(ex)
        
        
    @staticmethod
    def patch_method(url=None, headers=None, data=None):
        """
        @Author: Duy Nguyen
        @Parameter: url: Url REST address
                    header: header values with format dictionary
                    params: parameter values with format dictionary
                    data: data value with format json object
        """
        try:
            response = requests.patch(url, headers=headers, data=data, timeout=APIBase._timeout)
            return response
        except Exception as ex:
            raise(ex)   
    
    
    @staticmethod
    def put_method(url=None, headers=None, data=None):
        """
        @Author: Duy Nguyen
        @Parameter: url: Url REST address
                    header: header values with format dictionary
                    params: parameter values with format dictionary
                    data: data value with format json object
        """
        try:
            response = requests.put(url, headers=headers, data=data, timeout=APIBase._timeout)
            return response
        except Exception as ex:
            raise(ex)
        
        
    @staticmethod
    def generate_header(organization=Constant.AdvancedOrgName):
        return {'Authorization':Constant.APIKeys[organization]}
    
    
    @staticmethod
    def generate_simplified_header(access_token, organization):
        return {'Authorization': "Bearer " + access_token, 'X-Organization': Constant.SimplifiedOrgIDs[organization]}
    
