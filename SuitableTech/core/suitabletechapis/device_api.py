from common.constant import Constant
from core.suitabletechapis.api_base import APIBase
import json



class DeviceAPI(APIBase):
    _baseurl = Constant.APIBaseURL + "devices/"
    property_dict = {'name':'name', 'location':'location', 'labels':'tags', 'time zone':'time_zone', 'group':'device_group'}
    
    
#     @staticmethod
#     def _get_device(device_id="", organization=Constant.AdvancedOrgName):
#         return APIBase.get_method(url=DeviceAPI._baseurl + device_id, headers=APIBase.generate_header(organization))
    
    
    @staticmethod
    def _patch_device(device_id, headers, setting_name, setting_value):
        data = json.dumps({setting_name: setting_value })
        if (device_id != ""):
            device_id = device_id + "/"
        return APIBase.patch_method(url=DeviceAPI._baseurl + device_id, headers=headers, data=data)
    
    
    @staticmethod
    def get_devices(organization=Constant.AdvancedOrgName):
        return APIBase.get_method(url=DeviceAPI._baseurl, headers=APIBase.generate_header(organization))
    
    
    @staticmethod
    def get_simplified_device_info(device_name, organization, access_token):
        device_id = DeviceAPI.get_device_id(device_name, organization, access_token)
        return APIBase.get_method(DeviceAPI._baseurl + device_id + "/", headers=APIBase.generate_simplified_header(access_token, organization))
    
    
    @staticmethod
    def get_simplified_devices(device_name, organization, access_token):
        return APIBase.get_method(DeviceAPI._baseurl + "?inline=device_group", headers=APIBase.generate_simplified_header(access_token, organization))

    
    @staticmethod
    def edit_advanced_device(device_name, device_property, property_value, organization=Constant.AdvancedOrgName):
        return DeviceAPI._patch_device(Constant.DeviceIDs[device_name], headers=APIBase.generate_header(organization), setting_name=DeviceAPI.property_dict[device_property], setting_value=property_value)
    
    
    @staticmethod
    def get_device_id(device_name, organization, access_token):
        response = DeviceAPI.get_simplified_devices(device_name, organization, access_token)
        devices = response.json()["objects"]
        for device in devices:
            if(device["name"] == device_name or device["device_group"]["id"] == Constant.DeviceIDs[device_name]):
                return device["id"]
    
        return None
    
    
    @staticmethod
    def edit_simplified_device(device_name, device_property, property_value, organization, access_token):
        device_id = DeviceAPI.get_device_id(device_name, organization, access_token)
        device_info = DeviceAPI.get_simplified_device_info(device_name, organization, access_token)
        device_info = device_info.json()
        device_info[DeviceAPI.property_dict[device_property]] = property_value
        data = json.dumps(device_info)
        return APIBase.put_method(DeviceAPI._baseurl + device_id + "/", headers=APIBase.generate_simplified_header(access_token, organization), data=data)
    
