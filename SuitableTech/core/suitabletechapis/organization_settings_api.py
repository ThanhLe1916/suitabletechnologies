from common.constant import Constant
from core.suitabletechapis.api_base import APIBase
import json


class OrganizationSettingAPI(APIBase):
    _baseurl = Constant.APIBaseURL + "organizationsettings/"
    setting_dict = {"organization name":"name", "default invite message":"default_invite_message", "email notifications":"notification_from_name"}
    
    @staticmethod
    def _patch_organization_setting(setting_name, setting_value, organization=Constant.AdvancedOrgName):
        data_object = json.dumps({setting_name:setting_value})
        return APIBase.patch_method(url=OrganizationSettingAPI._baseurl, headers=APIBase.generate_header(organization), data=data_object)
    
    
    @staticmethod
    def update_an_organization_setting(setting_name, setting_value, organization=Constant.AdvancedOrgName):
        return OrganizationSettingAPI._patch_organization_setting(OrganizationSettingAPI.setting_dict[setting_name], setting_value, organization)
