# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
import os
import random
import string
from uuid import uuid4

from common.constant import Constant, Language, Platform
from common.email_detail_constants import EmailDetailConstants
from core.webdriver.driversetting import DriverSetting
from data import testdata
from data.dataobjects.message import Message
import re
from common.application_constants import ApplicationConst
from random import randint

class Helper(object):
    
    _utf8chars = u"éêàáâơớờởùứîçアイウエオカキク ケコサシスセソ"
    
    @staticmethod
    def base_dir():
        """      
        @summary: Get base directory      
        @author: 
        """
        return os.path.dirname(os.path.dirname(__file__))
    
        
    @staticmethod
    def generate_random_string(length=10):
        randomstring = ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits + Helper._utf8chars) for _ in range(length))
        return randomstring.strip()
    
    
    @staticmethod
    def generate_random_not_special_string(length = 6):
        return ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(length))
    

    @staticmethod
    def generate_random_device_group_name(length=10):
        return "LGVN Device Group " + Helper.generate_random_string(length)
    
    
    @staticmethod
    def generate_random_device_name(length=10):
        return "LGVN Device " + Helper.generate_random_string(length)
    
    
    @staticmethod
    def generate_random_label_name(length=10):
        return "LGVN Label " + Helper.generate_random_string(length)


    @staticmethod
    def generate_random_user_group_name(length=10):
        return "LGVN User Group " + Helper.generate_random_string(length)
    
    
    @staticmethod
    def generate_random_email():
        """
        This method is used to generated a testing email with the following format:
        logigear1+userYYDDmmHHMMSSfff@suitabletech.com
        """
        base_email = Constant.BaseEmails[random.randint(1,5)]
        date_time = datetime.strftime(datetime.now(), "%b%d%Y.%H%M%S")
        random_str = str(uuid4()).replace("-", "")[:randint(5, 15)]
        
        separator = "@"
        parts = base_email.split(separator)
        return (parts[0] + "+user{}.{}".format(random_str, date_time) + separator + parts[1]).lower()
        
    
    @staticmethod
    def generate_random_password(length=25):
        return ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits + string.punctuation) for _ in range(length))
    
    
    @staticmethod
    def generate_date_time(hour_delta=1, minute_delta=0, second_delta=0):
        return datetime.now().replace(hour=0, minute=0, second=0, microsecond=0 ) + timedelta(days = 1, hours = hour_delta, minutes = minute_delta, seconds = second_delta)
    
    
    @staticmethod
    def generate_access_day():
        return datetime.today() + timedelta(days=1)
    
    
    @staticmethod
    def generate_time_range_label(start_datetime, end_datetime):
        # format: 12:30 AM - 1:45 PM 
        result = "{}:{} {} - {}:{} {}".format(
                str(int (start_datetime.strftime("%I"))),
                start_datetime.strftime("%M"),
                ApplicationConst.get_date_time_label(start_datetime.strftime("%p")),
                str(int (end_datetime.strftime("%I"))),
                end_datetime.strftime("%M"),
                ApplicationConst.get_date_time_label(end_datetime.strftime("%p")))
        return result
    
    
    @staticmethod
    def get_data_from_csv_file(data_file_name):
        # Define constant
        csv_delim = ","
        end_line = "\n"
        
        # Read file and put to array
        file_path = os.path.dirname(testdata.__file__)
        f_stream = open(file_path + "\\" + data_file_name, "r")
        lines = f_stream.readlines()
        f_stream.close()
        
        # Get items from lines
        item = []
        result = []
        for i in range (1, len(lines)):
            lines[i] = lines[i].replace(end_line, "")
            item = lines[i].split(csv_delim)
            result.append(item)

        return result
    
    
    @staticmethod
    def load_execution_setting():
        driverSetting = DriverSetting()
        
        # Define constant
        delim = "="
        end_line = "\n"
        
        # Read execution setting info from ExecutionSetting.txt
        file_path = Helper.base_dir() + "\\data\\setting".replace("\\", os.path.sep)
        f_read = open(file_path + "\\ExecutionSetting.txt".replace("\\", os.path.sep), "r")
        line = f_read.readline()
        execution_setting_file_name = line.replace(end_line, "")
        f_read.close()

        # Read file and put to array
        f_stream = open(file_path + os.path.sep + execution_setting_file_name, "r")
        lines = f_stream.readlines()
        f_stream.close()
        
        # Get values
        items = []
        for i in range (0, len(lines)):
            lines[i] = lines[i].replace(end_line, "")
            items = lines[i].split(delim)
            if (len(items) == 2):
                if (items[0] == "browser_name"):
                    driverSetting.browser_name = items[1]
                elif (items[0] == "element_wait_timeout"):
                    driverSetting.element_wait_timeout = int(items[1])
                elif (items[0] == "page_wait_timeout"):
                    driverSetting.page_wait_timeout = int(items[1])  
                elif (items[0] == "hub_url"):
                    if (items[1] == "None"):
                        driverSetting.hub_url = None
                    else:
                        driverSetting.hub_url = items[1]
                elif (items[0] == "platform"):
                    driverSetting.platform = items[1]
                elif (items[0] == "language"):
                    driverSetting.language = items[1]

        return driverSetting
        
    
    @staticmethod
    def download_dir():
        """      
        @summary: Get download directory       
        @author: 
        """
        return Helper.base_dir() + "\\data\\download".replace("\\", os.path.sep)
    
    
    @staticmethod
    def beam_software_installer_path(platform):
        """      
        @summary: Get download directory       
        @author: 
        """
        file_name = ""
        if(platform == Platform.WINDOWS):
            file_name = Constant.WindowsBeamClientName
        elif(platform == Platform.MAC):
            file_name = Constant.MacBeamClientName
        return "{}{}{}".format( Helper.download_dir(), os.path.sep, file_name)
    
    
    @staticmethod
    def get_dict_value(dictionary, key, value, search_key):
        return next(item for item in dictionary if item.get(key) == value)[search_key]
        
    
class EmailDetailHelper(object):
    _language = Language.ENGLISH
    
    @staticmethod
    def set_language(language):
        EmailDetailHelper._language = language
    
    
    @staticmethod
    def generate_google_change_auth_email(receiver_email):
        """      
        @summary: Generate an expected "Your authentication method has been changed" template for asserting with the actual email        
        @param receiver_email: User Email object that receive email        
        @return: Message object
        @note: Because this email contains a random activation code so we cannot assert the trimmed_text_content directly. Instead, we should use re.match to check the content
                Please see the below example:            
                result = re.match(tmp_mail.trimmed_text_content, actual_msg[0].trimmed_text_content, re.I|re.M)
                self.assertTrue(result, "Assertion Error: Email content does not display as expected")
        @author: Thanh Le
        @created_date: August 16, 2016
        """
        
        msg = Message()
        msg.set_subject(EmailDetailConstants.GoogleChangeAuthEmailTitle)
        content = EmailDetailConstants.GoogleChangeAuthEmailContent.format(receiver_email, Constant.SuitableTechURL)
        
        msg.set_text_content(content)
        return msg    
    
    
    @staticmethod
    def generate_connect_google_change_auth_email(receiver_email):
        """
        @summary: Generate an expected "Your authentication method has been changed" template for asserting with the actual email
        @description: This email template only use when asserting email when change authentication from ST -> GSSO
        @author: Duy Nguyen
        """
        msg = Message()
        msg.set_subject(EmailDetailConstants.GoogleChangeAuthEmailTitle)
        content = EmailDetailConstants.GoogleChangeAuthConnectEmailContent.format(receiver_email)
        
        msg.set_text_content(content)
        return msg 
        
        
    @staticmethod
    def generate_welcome_email(user, admin_full_name=None):
        """      
        @summary: Generate an expected "Welcome to Beam at <Org Name>" template for asserting with the actual email        
        @param user: User object that receive welcome email
        @param admin_full_name: Admin full name
        @return: Message object
        @note: Because this email contains a random activation code so we cannot assert the trimmed_text_content directly. Instead, we should use re.match to check the content
                Please see the below example:            
                result = re.match(tmp_mail.trimmed_text_content, actual_msg[0].trimmed_text_content, re.I|re.M)
                self.assertTrue(result, "Assertion Error: Email content does not display as expected")
        @author: Thanh Le
        @created_date: August 02, 2016
        """
        msg = Message()
        raw_title = EmailDetailConstants.WelcomeEmailTitle
        raw_content = EmailDetailConstants.WelcomeEmailContent
                
        msg.set_subject(raw_title.format(user.organization))
        content = raw_content.format(user.get_displayed_name(),
                                         admin_full_name,
                                         user.organization,
                                         Constant.SuitableTechURL,
                                         user.email_address.replace("+", "(.{1})"),
                                         admin_full_name)
        
        msg.set_text_content(content)
        return msg
    
    
    @staticmethod
    def generate_welcome_existing_email(user, admin_full_name=None):
        """      
        @summary: Generate an expected "Welcome to Beam at <Org Name>" template for asserting with the actual email. The email has existed in an org in advance and you create the same email in another org
        @param user: User object that receive welcome email
        @param admin_full_name: Admin full name
        @return: Message object
        @note: 
                Please see the below example:            
                self.assertEqual(expected_message.trimmed_text_content, lst_emails[0].trimmed_text_content, "Assertion Error: Email content does not display as expected")
        @author: Tham Nguyen
        @created_date: August 05, 2016
        """
#         if admin_full_name is None:
#             admin_full_name = Constant.AdvancedAdminFullName
        msg = Message()
        msg.set_subject(EmailDetailConstants.WelcomeEmailTitle.format(user.organization))
        content = EmailDetailConstants.WelcomeExistingEmailContent.format(user.get_displayed_name(), \
                                                                          admin_full_name, \
                                                                          user.organization, \
                                                                          user.email_address, \
                                                                          admin_full_name)
        
        msg.set_text_content(content)
        return msg
    
    
    @staticmethod
    def generate_welcome_admin_copy_email(user, admin_full_name=None):
        """      
        @summary: Generate an expected "Welcome to Beam at <Org Name> (copy)" email template for asserting with the actual email        
        @param user: User object that receive welcome email
        @param admin_full_name: Admin full name
        @return: Message object
        @author: Thanh Le
        @created_date: August 02, 2016
        """
#         if admin_full_name is None:
#             admin_full_name = Constant.AdvancedAdminFullName
        msg = Message()
        msg.set_subject(EmailDetailConstants.WelcomeAdminCopyEmailTitle.format(user.organization))
        content = EmailDetailConstants.WelcomeAdminCopyEmailContent.format(user.email_address, \
                                                                            user.get_displayed_name(), \
                                                                            admin_full_name,
                                                                            user.organization,
                                                                            user.email_address,
                                                                            admin_full_name)
        msg.set_text_content(content)
        return msg
    
    
    @staticmethod
    def generate_request_access_email(device_group_name, requester, request_message=None):
        """      
        @summary: Generate an expected "[Beam] Someone is requesting access to your Beams" email template for asserting with the actual email        
        @param device_group_name: device group name that user requests access
        @param requester: User object that requests access
        @param request_message: Request message
        @return: Message object
        @note: Because this email contains a random activation code so we cannot assert the trimmed_text_content directly. Instead, we should use re.match to check the content
                Please see the below example:            
                result = re.match(tmp_mail.trimmed_text_content, actual_msg[0].trimmed_text_content, re.I|re.M)
                self.assertTrue(result, "Assertion Error: Email content does not display as expected")
        @author: Thanh Le
        @created_date: August 02, 2016
        """
        msg = Message()
        msg.set_subject(EmailDetailConstants.BeamRequestAccessEmailTitle)
        
        user_info = requester.email_address.replace("+", "(.{1})")
        if(requester.email_address != requester.get_displayed_name()):
            if EmailDetailHelper._language == Language.JAPANESE:
                user_info = r"{}（{}）".format(requester.get_displayed_name(), user_info)
            else:
                user_info = r"{} \({}\)".format(requester.get_displayed_name(), user_info)
        
        if(request_message == None or request_message == ""):
            request_message = ""
        else:
            request_message = r"{}\r\n{}\r\n\r\n".format(EmailDetailConstants.RequestMessageLabel, request_message)
            
        content = EmailDetailConstants.BeamRequestAccessEmailContent.format(user_info, \
                                                                            device_group_name, \
                                                                            request_message, \
                                                                            Constant.SuitableTechURL, \
                                                                            Constant.SuitableTechURL, \
                                                                            Constant.SuitableTechURL)
                                                                            
        msg.set_text_content(content)
        return msg
    
    
    @staticmethod
    def generate_added_to_device_group_email(device_group_name, admin_full_name=None):
        """      
        @summary: Generate an expected "[Beam] You have been added to <device group>" email template for asserting with the actual email        
        @param device_group_name: device group name that user requests access
        @param admin_full_name: Admin full name
        @return: Message object
        @author: Thanh Le
        @created_date: August 02, 2016
        """
#         if admin_full_name is None:
#             admin_full_name = Constant.AdvancedAdminFullName
        msg = Message()
        msg.set_subject(EmailDetailConstants.AddedToDeviceGroupEmailTitle.format(device_group_name))
        
        content = EmailDetailConstants.AddedToDeviceGroupEmailContent.format(device_group_name,
                                                                            admin_full_name,
                                                                            device_group_name,
                                                                            Constant.SuitableTechURL)
        msg.set_text_content(content)
        return msg
    
    
    @staticmethod
    def generate_notification_added_to_device_group_email(device_group_name, admin_full_name=None, user_full_name=None):
        """      
        @summary: Generate an expected "[Beam] A user was added to <device group>" email template for asserting with the actual email        
        @param device_group_name: device group name that user requests access
        @param admin_full_name: Admin full name
        @return: Message object
        @author: Thanh Le
        @created_date: August 02, 2016
        """
#         if admin_full_name is None:
#             admin_full_name = Constant.AdvancedAdminFullName
        msg = Message()
        msg.set_subject(EmailDetailConstants.NotificationAddedToDeviceGroupEmailTitle.format(device_group_name))
        
        content = EmailDetailConstants.NotificationAddedToDeviceGroupEmailContent.format(device_group_name,admin_full_name,user_full_name,device_group_name,Constant.SuitableTechURL)
        msg.set_text_content(content)
        return msg
    
    
    @staticmethod
    def generate_beam_removed_from_device_group_email(device_group_name, admin_full_name = None, beam_name = None):
        """      
        @summary: Generate an expected "[Beam] A Beam was removed from <device group>" email template for asserting with the actual email        
        @param device_group_name: device group name that beam is removed from
        @param admin_full_name: Admin full name who removed beam
        @param beam_name: Beam name
        @return: Message object
        @author: Thanh Le
        @created_date: August 09, 2016
        """
#         if admin_full_name == None:
#             admin_full_name = Constant.AdvancedAdminFullName
        if beam_name == None:
            beam_name = Constant.BeamPlusMock4Name
        msg = Message()
        msg.set_subject(EmailDetailConstants.BeamRemovedFromDeviceGroupTitle.format(device_group_name))
        
        content = EmailDetailConstants.BeamRemovedFromDeviceGroupContent.format(device_group_name,
                                                                            admin_full_name,
                                                                            beam_name,
                                                                            device_group_name,
                                                                            Constant.SuitableTechURL)
        msg.set_text_content(content)
        return msg
    
    
    @staticmethod
    def generate_removed_from_device_group_email(device_group_name, admin_full_name=None):
        """      
        @summary: Generate an expected "[Beam] You have been removed from <device group>" email template for asserting with the actual email        
        @param device_group_name: device group name that user requests access
        @param admin_full_name: Admin full name
        @return: Message object
        @author: Thanh Le
        @created_date: August 02, 2016
        """
#         if admin_full_name is None:
#             admin_full_name = Constant.AdvancedAdminFullName
        msg = Message()
        msg.set_subject(EmailDetailConstants.RemovedFromDeviceGroupEmailTitle.format(device_group_name))
        content = EmailDetailConstants.RemovedFromDeviceGroupEmailContent.format(device_group_name,
                                                                            admin_full_name,
                                                                            device_group_name,
                                                                            Constant.SuitableTechURL)
        msg.set_text_content(content)
        return msg
    
    
    @staticmethod
    def generate_manage_device_group_email(device_group_name, admin_full_name=None):
        """      
        @summary: Generate an expected "[Beam] You have been removed from <device group>" email template for asserting with the actual email        
        @param device_group_name: device group name that user requests access
        @param admin_full_name: Admin full name
        @return: Message object
        @author: Thanh Le
        @created_date: August 02, 2016
        """
#         if admin_full_name is None:
#             admin_full_name = Constant.AdvancedAdminFullName
        msg = Message()
        msg.set_subject(EmailDetailConstants.ManageDeviceGroupEmailTitle.format(device_group_name))
        content = EmailDetailConstants.ManageDeviceGroupEmailContent.format(device_group_name,
                                                                            admin_full_name,
                                                                            device_group_name,
                                                                            Constant.SuitableTechURL)
        msg.set_text_content(content)
        return msg


    @staticmethod
    def generate_removed_from_org_admin_email(organization_name=None, admin_full_name=None):
        """      
        @summary: Generate an expected "[Beam] You are no longer an administrator for <Organization name>" email template for asserting with the actual email        
        @param organization_name: organization name
        @param admin_full_name: Admin full name
        @return: Message object
        @author: Thanh Le
        @created_date: August 02, 2016
        """
        if organization_name is None:
            organization_name = Constant.AdvancedOrgName
#         if admin_full_name is None:
#             admin_full_name = Constant.AdvancedAdminFullName
        msg = Message()
        msg.set_subject(EmailDetailConstants.RemovedFromOrgAdminEmailTitle.format(organization_name))
        content = EmailDetailConstants.RemovedFromOrgAdminEmailContent.format(organization_name,
                                                                            admin_full_name,
                                                                            organization_name,
                                                                            Constant.SuitableTechURL)
        msg.set_text_content(content)
        return msg
    
    
    @staticmethod
    def generate_added_to_org_admin_email(organization_name, admin_full_name):
        """      
        @summary: Generate an expected "[Beam] You are now an administrator of <Organization name>" email template for asserting with the actual email        
        @param organization_name: organization name
        @param admin_full_name: Admin full name
        @return: Message object
        @author: Thanh Le
        @created_date: August 05, 2016
        """
#         if admin_full_name is None:
#             admin_full_name = Constant.AdvancedAdminFullName
        msg = Message()
        msg.set_subject(EmailDetailConstants.AddedToOrgAdminEmailTitle.format(organization_name))
        content = EmailDetailConstants.AddedToOrgAdminEmailContent.format(organization_name,
                                                                            admin_full_name,
                                                                            organization_name,
                                                                            Constant.SuitableTechURL)
        msg.set_text_content(content)
        return msg

    
    @staticmethod
    def generate_welcome_temporary_user_email(user, start_time, end_time, device_list=None, admin_full_name=None, language=Language.ENGLISH):
        """      
        @summary: Generate an expected "You've been invited to Beam into <Org_Name>" email template for asserting with the actual email        
        @param user: User object that requests access
        @param start_time: Start time (A combination of date and time e.g. datetime.combine(date(2016, 8, 9), time(12, 00)) )
        @param end_time: End time (A combination of date and time e.g. datetime.combine(date(2016, 8, 9), time(12, 00)) )
        @param device_list: list of all devices in a the invited device group
        @param admin_full_name: Admin full name
        @return: Message object
        @note: Because this email contains a random activation code so we cannot assert the trimmed_text_content directly. Instead, we should use re.match to check the content
                Please see the below example:            
                result = re.match(tmp_mail.trimmed_text_content, actual_msg[0].trimmed_text_content, re.I|re.M)
                self.assertTrue(result, "Assertion Error: Email content does not display as expected")
        @author: Thanh Le
        @created_date: August 09, 2016
        """
#         if admin_full_name is None:
#             admin_full_name = Constant.AdvancedAdminFullName
        msg = Message()
        msg.set_subject(EmailDetailConstants.WelcomeTemporaryUserEmailTitle.format(user.organization))
        
        beam_number = ""
        beams_list = ""
        if(device_list):
            for device in device_list:
                beams_list += r"{} \(America/Los_Angeles\){}".format(device, r"\n\n")
            
            if(len(device_list) == 1):
                beam_number = EmailDetailConstants.InvitationEmailDeviceList_Single
            elif(len(device_list) > 1):
                beam_number = EmailDetailConstants.InvitationEmailDeviceList_Multiple.format(len(device_list))

        
        _start_time = EmailDetailHelper._convert_to_email_datetime(start_time)
        _end_time = EmailDetailHelper._convert_to_email_datetime(end_time)
        
        content = EmailDetailConstants.WelcomeTemporaryUserEmailContent.format(user.get_displayed_name(),
                                                                            admin_full_name,
                                                                            user.organization,
                                                                            Constant.SuitableTechURL,
                                                                            user.email_address.replace("+", "(.{1})"),
                                                                            beam_number,
                                                                            beams_list,
                                                                            _start_time,
                                                                            _end_time,
                                                                            admin_full_name)
                                                                            
        msg.set_text_content(content)
        return msg
    
    
    @staticmethod
    def generate_access_time_has_changed_email(device_group_name, admin_full_name=None):
        """      
        @summary: Generate an expected "[Beam] Your access time for <device group> has changed" email template for asserting with the actual email        
        @param device_group_name: device group name that beam is removed from
        @param admin_full_name: Admin full name who removed beam
        @return: Message object
        @author: Thanh Le
        @created_date: August 10, 2016
        """
#         if admin_full_name is None:
#             admin_full_name = Constant.AdvancedAdminFullName
        msg = Message()
        msg.set_subject(EmailDetailConstants.AccessTimeHasChangedTitle.format(device_group_name))
        
        content = EmailDetailConstants.AccessTimeHasChangedContent.format(device_group_name,
                                                                            admin_full_name,
                                                                            device_group_name,
                                                                            Constant.SuitableTechURL)
        msg.set_text_content(content)
        return msg
    
    
    @staticmethod
    def _convert_to_email_datetime(date_time):
        if EmailDetailHelper._language == Language.JAPANESE:
            return EmailDetailHelper._convert_to_email_datetime_ja_JP(date_time)
        elif EmailDetailHelper._language == Language.FRENCH:
            return EmailDetailHelper._convert_to_email_datetime_fr_FR(date_time)
        else:
            return EmailDetailHelper._convert_to_email_datetime_default(date_time)
    
    @staticmethod
    def _convert_to_email_datetime_default(date_time):
        result = re.sub(' +',' ',date_time.strftime('%b. %e, %Y, '))
        
        if(datetime.strftime(date_time, "%p") == "PM"):
            am_pm = "p.m."
        else:
            am_pm = "a.m."
        
        hh = datetime.strftime(date_time, "%I")
        mm = datetime.strftime(date_time, "%M")
        
        if(hh[0] == "0"):
            hh = hh[1]
            
        if(mm == "00"):
            mm = " "
        else:
            mm = ":" + mm + " "
        
        am_pm = hh + mm + am_pm
        
        if(am_pm == "12 p.m."):
            am_pm = "noon"
        result += am_pm
        result = result.replace("Sep", "Sept")
        return result
    
    @staticmethod
    def _convert_to_email_datetime_fr_FR(date_time):
        key = "MON%.2d" % (date_time.month)
        month_as_text =  ApplicationConst.get_date_time_label(key)
        
        result = re.sub(' +',' ',date_time.strftime('%e {} %Y %H:%M'))
        result = result.format(month_as_text)
        
        return result
    
    @staticmethod
    def _convert_to_email_datetime_ja_JP(date_time):
        _day = str(int(datetime.strftime(date_time, "%d")))
        _month = str(int(datetime.strftime(date_time, "%m")))
        _year = datetime.strftime(date_time, "%Y")
        _hour = str(int(datetime.strftime(date_time, "%H")))
        _minute = datetime.strftime(date_time, "%M")
        
        result = u"{}年{}月{}日{}:{}".format(_year, _month,_day,_hour,_minute)
           
        return result
    
    
    @staticmethod
    def generate_add_to_user_group_email(user_group_name, admin_full_name=None):
        """      
        @summary: Generate an expected "You have been add to the group <User_Group_Name>" email template for asserting with the actual email        
        @param user_group_name: user group name 
        @param admin_full_name: Admin full name
        @return: Message object
        @author: Duy Nguyen
        @created_date: August 10, 2016
        """
#         if admin_full_name is None:
#             admin_full_name = Constant.AdvancedAdminFullName

        msg = Message()
        msg.set_subject(EmailDetailConstants.AddToUserGroupEmailTitle.format(user_group_name))
        
        content = EmailDetailConstants.AddToUserGroupEmailContent.format(user_group_name,
                                                                            admin_full_name,
                                                                            user_group_name,
                                                                            Constant.SuitableTechURL)
        msg.set_text_content(content)
        return msg


    @staticmethod
    def generate_password_changed_email(user_email_address):
        """      
        @summary: Generate an expected "Your password has changed" email template for asserting with the actual email        
        @param user_email_address: Email address of changing password user
        @return: Message object
        @author: Thanh Le
        @created_date: August 15, 2016
        """
        
        msg = Message()
        msg.set_subject(EmailDetailConstants.PasswordChangedEmailTitle)
        content = EmailDetailConstants.PasswordChangedEmailContent.format(user_email_address, Constant.SuitableTechURL)
        msg.set_text_content(content)
        return msg


    @staticmethod
    def generate_password_reset_email(user_email_address):
        """      
        @summary: Generate an expected "Beam Password Reset" email template for asserting with the actual email        
        @param user_email_address: Email address of setting password user
        @return: Message object
        @note: Because this email contains a random activation code so we cannot assert the trimmed_text_content directly. Instead, we should use re.match to check the content
                Please see the below example:            
                result = re.match(tmp_mail.trimmed_text_content, actual_msg[0].trimmed_text_content, re.I|re.M)
                self.assertTrue(result, "Assertion Error: Email content does not display as expected")
        @author: Thanh Le
        @created_date: August 15, 2016
        """
        
        msg = Message()
        msg.set_subject(EmailDetailConstants.PasswordResetEmailTitle)
        content = EmailDetailConstants.PasswordResetEmailContent.format(Constant.SuitableTechURL,
                                                                        user_email_address.replace("+", "(.{1})"))
        msg.set_text_content(content)
        return msg

    
    @staticmethod
    def generate_can_now_accept_sessions_email(user, admin_full_name=None):
        """      
        @summary: Generate an expected "[Beam] You can now accept sessions for <Device Group>" email template for asserting with the actual email        
        @param user: User object 
        @param admin_full_name: Admin full name
        @return: Message object
        @author: Thanh Le
        @created_date: August 16, 2016
        """
#         if admin_full_name is None:
#             admin_full_name = Constant.AdvancedAdminFullName

        msg = Message()
        msg.set_subject(EmailDetailConstants.CanNowAcceptSessionsEmailTitle.format(user.device_group))
        content = EmailDetailConstants.CanNowAcceptSessionsEmailContent.format(user.device_group,
                                                                                admin_full_name,
                                                                                user.device_group,
                                                                                Constant.SuitableTechURL)
        msg.set_text_content(content)
        return msg
    
    
    @staticmethod
    def generate_can_no_longer_accept_sessions_email(user, admin_full_name=None):
        """      
        @summary: Generate an expected "[Beam] You can no longer accept sessions for <Device_Group_Name>" email template for asserting with the actual email        
        @param user: User object 
        @param admin_full_name: Admin full name
        @return: Message object
        @author: Thanh Le
        @created_date: August 16, 2016
        """
#         if admin_full_name is None:
#             admin_full_name = Constant.AdvancedAdminFullName

        msg = Message()
        msg.set_subject(EmailDetailConstants.CanNoLongerAcceptSessionsEmailTitle.format(user.device_group))
        content = EmailDetailConstants.CanNoLongerAcceptSessionsEmailContent.format(user.device_group,
                                                                                    admin_full_name,
                                                                                    user.device_group,
                                                                                    Constant.SuitableTechURL)
        msg.set_text_content(content)
        return msg
    
    
    @staticmethod
    def generate_delete_device_group_email(user,organization_name):
        """      
        @summary: Generate an expected " [Beam] A device group was removed from <Organization Name>" email template for asserting with the actual email        
        @param user: User object 
        @param organization_name: Organization name
        @param admin_full_name: Admin full name
        @return: Message object
        @author: Duy Nguyen
        @created_date: Sep 15, 2016
        """    
        msg = Message()
        msg.set_subject(EmailDetailConstants.DeleteDeviceGroupEmailTitle.format(organization_name))
        content = EmailDetailConstants.DeleteDeviceGroupEmailContent.format(organization_name, organization_name, user.device_group, Constant.SuitableTechURL)
        msg.set_text_content(content)
        return msg
    
            