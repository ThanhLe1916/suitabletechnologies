# -*- coding: utf-8 -*-

class ApplicationConst(object):
    
#     LBL_SIGN_IN = u"" #"Login"
#     LBL_SIGN_OUT = u"" #"Sign Out"
    LBL_MANAGE_YOUR_BEAMS = u"" #"Manage Your Beams"
#     LBL_ACCOUNT_SETTINGS = u"" #"Account Settings"
#     LBL_YOUR_ACCOUNT = u"" #"Your Account"
#     LBL_DOWNLOAD_BEAM_SOFTWARE = u"" #"Download the Beam Software"
    LBL_DOWNLOAD_INSTALLER = u"" #"Download Installer"
#     LBL_SETUP_NEW_BEAM = u""  #"Set Up a New Beam"
#     LBL_VIEW_DOCUMENT = u"" #"View Documentation"
    LBL_DOCUMENTATION = u"" #"Documentation"
#     LBL_GET_HELP = u"" #"Get Help"
    LBL_BEAM_HELP = u"" #"Beam Help Center"
    
    #Application strings on Beams Settings page
    CHK_NOTIFY_REQUEST_ACCESS = u"" #"Notify administrators when someone requests access to this group"
    CHK_NOTIFY_ADMIN_IF_BEAM_LEFT_OFF_CHARGER = u"" #"Notify administrators if a Beam is left off the charger"
    CHK_NOTIFY_PILOT_IF_BEAM_LEFT_OFF_CHARGER = u"" #"Notify the last pilot if a Beam is left off the charger"
    
    #Application string on Beams Access Times page
    LBL_ALL_MEMBERS = u"" #"All Members"

    #Application strings on Your Account -> Notifications page
    CHK_NOTIFY_BECOME_AN_ADMIN = u"" #"I become an administrator"

    CHK_NOTIFY_ADDED_OR_REMOVED_FROM_DEVICE_GROUP = u"" #"I am added to or removed from a device group"
    CHK_NOTIFY_DEVICE_MEMBERS_GROUP_ARE_ADDED_REMOVED = u"" # "Device group members are added or removed"
    CHK_NOTIFY_DEVICE_GROUPS_ARE_ADDED_REMOVED = u""
    CHK_NOTIFY_DEVICE_GROUPS_SETTINGS_ARE_CHANGED = u"" #Device group settings are changed
    CHK_NOTIFY_I_CAN_CHANGE_DEVICE_SETTINGS_OR_ANSWER_CALLS =u"" #I can change device settings or answer calls
    
    #Application string on User Group Detail page
    LBL_DEVICE_GROUPS_PROPERTY = u"" #"Device Groups"
    LBL_LOCATION_NAME = u"" #"Name"
    LBL_LOCATION_PROPERTY = u"" #"Location"
    LBL_LABEL_PROPERTY = u"" #"Labels"
    LBL_GROUP_PROPERTY = u"" #"Group"
    LBL_TIMEZONE_PROPERTY = u"" #"Time Zone"
    
    #Page Header labels
#     LBL_LOGIN_PAGE_HEADER = u"" #"Welcome!"
#     LBL_ADMIN_HOME_PAGE_HEADER = u"" #"Your Account"
#     LBL_SIGNOUT_PAGE_HEADER = u"" #"Signout Complete"
#     LBL_ADMIN_DASHBOARD_PAGE_HEADER = u"" #"Dashboard"
#     LBL_ADMIN_USERS_PAGE_HEADER = u"" #"Users"
    LBL_ADMIN_USERS_SECTION_USERS = u"" #"Users"
#     LBL_ADMIN_USERS_SECTION_USER_GROUPS = u"" #"User Groups"
#     LBL_ADMIN_ACTIVITY_PAGE_HEADER = u"" #"Activity"
    LBL_ACCOUNT_SETTINGS_PAGE_HEADER = u"" #"Account Settings"
    LBL_ACCOUNT_NOTIFICATIONS_PAGE_HEADER = u"" #"Notifications"
    LBL_ORG_SETTINGS_PAGE_HEADER = u"" #"Organization Settings"
    LBL_APPROVED_REQUEST_ACCESS_BEAM_HEADER = u"" #Approved
    LBL_REJECTED_REQUEST_ACCESS_BEAM_HEADER = u"" #Rejected
    LBL_ERROR_REQUEST_ACCESS_BEAM_HEADER = u""
#     LBL_BEAM_SUPPORT_PAGE_HEADER = u"" #"Beam Support"
#     LBL_BEAMS_ALL_PAGE_HEADER = u"" #"All Devices"
#     LBL_BEAMS_ACCESS_TIMES_PAGE_HEADER = u"" #"Access Times for"
    LBL_BEAMS_ACCESS_TIMES_TEMPORARY_USERS = u"" #"Temporary Users"
    LBL_BEAMS_DEVICES_PAGE_HEADER = u"" #"Devices in"
#     LBL_BEAMS_MEMBERS_PAGE_HEADER = u"" #"Members in"
    LBL_BEAMS_MEMBERS_USERS = u"" #"Users"
    LBL_BEAMS_MEMBERS_USER_GROUPS = u"" #"User Groups"
    LBL_BEAMS_MEMBERS_REMOVE_USER = u"" #"Remove"
#     LBL_BEAMS_RESERVATIONS_PAGE_HEADER = u"" #"Reservations in"
#     LBL_BEAMS_SETTINGS_PAGE_HEADER = u"" #"Settings for"
    
#     LBL_BEAMS_DETAILS_ADD_A_LABEL = u"" #"Add a label"
#     LBL_BEAMS_DETAILS_MANAGE = u"" #"Manage"
    
#     LBL_PASSWORD_SETUP_PAGE_HEADER = u"" #"Password Setup"
#     LBL_PASSWORD_RESET_PAGE_HEADER = u"" #"Password Reset"
#     LBL_PASSWORD_RESET_MESSAGE_SENT_PAGE_HEADER = u"" #"Password Reset Message Sent"
#     LBL_REQUEST_BEAM_ACCESS_PAGE_HEADER = u"" #"Request Beam Access"
#     LBL_WELCOME_TO_BEAM_PAGE_HEADER = u"" #"Welcome to Beam!"
#     LBL_YOUR_BEAM_ACCOUNT_PAGE_HEADER = u"" #"Your Beam Account"
#     LBL_PASSWORD_CHANGE_PAGE_HEADER = u"" #"Password Change"
#     LBL_PASSWORD_CHANGE_COMPLETE_PAGE_HEADER = u"" #"Password Change Complete"    
    
#     LBL_SIMPLIFIED_ADMIN_DASHBOARD_HEADER = u"" #"Manage Your Beams"
    
    # MISC LABELS
#     LBL_CHANGE_PASSWORD = u"" #"Change password"
#     LBL_RESET_MY_PASSWORD = u"" #"Reset my password"
#     LBL_SET_PASSWORD = u"" #"Set password"
    LBL_PLAY_VIDEO_CONTINUE = u"" #"Continue"
    LBL_CONFIRM_NEW_AUTH_METHOD = u"" #"Confirm New Authentication Method"
    LBL_DISCONNECT_SUCCESSFUL = u"" #"Disconnect Successful"
    LBL_YES = u"" #"Yes"
    LBL_NO = u"" #"No"
    LBL_DELETE = u"" #"Delete"
    
    #Application strings on dialogs
    WARN_MSG_CHANGE_DEVICE_GROUP_NAME = u"" #"Warning! If you move this device to a different group, members of this group will no longer be able to access it."
    WARN_MSG_REMOVE_USER_GROUP = u"" # "Are you sure you want to remove this user group from the '{}' device group?"
    
    #Application strings on warning message
    INFO_MSG_CREATE_DEVICE_GROUP_SUCCESSFUL = u"" #"The device group was successfully created."
    INFO_MSG_SAVE_DEVICE_GROUP_SETTING_SUCCESSFUL = u"" #"Device group settings were saved successfully."
    INFO_MSG_SET_DEVICE_GROUP_SUCCESSFUL = u"" #"The device was saved successfully."
    INFO_MSG_DELETE_USER_GROUP_SUCCESSFUL = u"" #"The group was successfully deleted."
    INFO_MSG_INVITE_USER_TO_SIMPL_BEAM_SUCCESSFUL = u"" #"{} was added to {}."
    INFO_INVITATION_MAIL_SENT_SUCCESSFUL = "" #"The invitation to {} was successfully sent."
    INFO_MSG_NEW_API_NOTICE = u"" # Your new API Key is:
    INFO_MSG_NEW_API_NOTICE_2 = u""
    INFO_MSG_EDIT_USER_SUCCESS = u"" #The user was successfully saved.
    
    #Device group
    DELETE_USER_FROM_DEVICE_GROUP = u"" #"Are you sure you want to remove this member from the '{}' device group?"
    LBL_DEVICE_GROUPS = u"" #"Device Groups"
    LBL_USER_GROUPS = u"" #"User Groups"
    
    #User Detail Page
    LBL_ADMINISTERS_GROUPS = u"" #"Administers Groups"
    LBL_ADMINISTRATOR = u"" #"Administrator"
    LBL_MENU_ADMINISTRATOR_ONLY = u"" #"Administrators Only"
    LBL_MENU_USER_GROUPS = u"" #"User Groups"
    LBL_MENU_TEMPORARY_USERS = u"" #"Temporary Users"
    

    TXTF_CONFIRM_ASSOCIATION_WARNING_MESSAGE = u"" #"WARNING: Only one authentication method is allowed per account. Your account is currently authenticated with a Suitable Technologies username and password. Continuing will cause your account to be authenticated via Google.\nOnce you change your authentication method to Google, you will no longer be able to login to the website or Beam client using your Suitable Technologies username and password. You will need to click \"Sign in with Google\" from now on. You must also have an up-to-date version of the client.\nDo you really want to change your authentication method to Google?\nYes No"

    #Google Page
#     LBL_DELETE_GOOGLE_ACCOUNT_AND_DATA = u"" #"Delete Google Account and data"
#     BTN_DELETE_ACCOUNT = u"" #"Delete Account"
    LBL_ERROR_MESSAGE = u"" #"Sorry, Google doesn\'t recognize that email"
    
    #Others
    LBL_IMAGE_CROP_TRACKER = u"" #"Select the area you would like to use"
    
    LBL_ACCESS_REQUEST_RECORD_MESSAGE = u"" #requests to be added to group
    LBL_APPROVED_NOTIFICATION_MESSAGE = u"The access request has been approved."
    LBL_REJECTED_NOTIFICATION_MESSAGE = u"The access request has been rejected."
    
    CHK_ALL_AUTHENTICATION_METHODS = u"" # All authentication methods
    CHK_ST_AUTHENTICATION_METHODS = "Suitable Technologies"
    CHK_GOOGLE_AUTHENTICATION_METHODS = "Google"
    
    LBL_WELCOME_TO_BEAM_EMAIL_TITLE = u""
    LBL_BEAM_ALL_AUTHENTICATION_METHODS= u""
    LBL_WELCOME_TEMPORARY_USER_EMAIL_TITLE = u""
    
    #Date-time
    LBL_DATETIME_MERIDIAN_AM = u"" #"AM"
    LBL_DATETIME_MERIDIAN_PM = u"" #"PM"
    
    # Device Advanced Settings dialog
    LBL_DEV_ATTR_SETTINGS = u"" #"Settings"
    LBL_DEV_ATTR_INFORMATION = u"" #"Information"
    
    LBL_DEV_ATTR_SECTION_SYSTEM = u"" #"System"
    LBL_DEV_ATTR_SYSTEM_SERIAL_NUMBER = u"" #"Serial Number" 
    LBL_DEV_ATTR_SYSTEM_UID = u"" #"UID"
    LBL_DEV_ATTR_SYSTEM_SOFTWARE_VERSION = u"" #"Software Version"
    
    LBL_DEV_ATTR_SECTION_NETWORK = u"" #"Network"
    LBL_DEV_ATTR_NETWORK_CURRENT_WIFI_NETWORK = u"" #"Current WiFi Network"
    LBL_DEV_ATTR_NETWORK_SSID = u"" #"SSID"
    LBL_DEV_ATTR_NETWORK_FREQUENCY = u"" #"Frequency"
    LBL_DEV_ATTR_NETWORK_INTERFACE_TYPE = u"" #"Type"
    LBL_DEV_ATTR_NETWORK_MAC = u"" #"MAC"
    LBL_DEV_ATTR_NETWORK_IP_ADDR = u"" #"IP Address"
    LBL_DEV_ATTR_NETWORK_RELAY_SERVER = u"" #"Relay Server"
    
    _date_time_localized_values = {}
    
    @staticmethod
    def initialize():
        ApplicationConst.LBL_SIGN_IN = _("LBL_SIGN_IN")
        ApplicationConst.LBL_SIGN_OUT = _("LBL_SIGN_OUT")
        ApplicationConst.LBL_MANAGE_YOUR_BEAMS = _("LBL_MANAGE_YOUR_BEAMS")
        ApplicationConst.LBL_ACCOUNT_SETTINGS = _("LBL_ACCOUNT_SETTINGS")
        ApplicationConst.LBL_YOUR_ACCOUNT = _("LBL_YOUR_ACCOUNT")
        ApplicationConst.LBL_DOWNLOAD_BEAM_SOFTWARE = _("LBL_DOWNLOAD_BEAM_SOFTWARE")
        ApplicationConst.LBL_DOWNLOAD_INSTALLER = _("LBL_DOWNLOAD_INSTALLER")
        ApplicationConst.LBL_SETUP_NEW_BEAM = _("LBL_SETUP_NEW_BEAM")
        ApplicationConst.LBL_VIEW_DOCUMENT = _("LBL_VIEW_DOCUMENT")
        ApplicationConst.LBL_DOCUMENTATION = _("LBL_DOCUMENTATION")
        ApplicationConst.LBL_GET_HELP = _("LBL_GET_HELP") 
        ApplicationConst.LBL_BEAM_HELP = _("LBL_BEAM_HELP")
        ApplicationConst.CHK_NOTIFY_REQUEST_ACCESS = _("CHK_NOTIFY_REQUEST_ACCESS")
        ApplicationConst.CHK_NOTIFY_ADMIN_IF_BEAM_LEFT_OFF_CHARGER = _("CHK_NOTIFY_ADMIN_IF_BEAM_LEFT_OFF_CHARGER")
        ApplicationConst.CHK_NOTIFY_PILOT_IF_BEAM_LEFT_OFF_CHARGER = _("CHK_NOTIFY_PILOT_IF_BEAM_LEFT_OFF_CHARGER")
        ApplicationConst.LBL_ALL_MEMBERS = _("LBL_ALL_MEMBERS")
        ApplicationConst.CHK_NOTIFY_BECOME_AN_ADMIN = _("CHK_NOTIFY_BECOME_AN_ADMIN")
        ApplicationConst.CHK_NOTIFY_ADDED_OR_REMOVED_FROM_DEVICE_GROUP = _("CHK_NOTIFY_ADDED_OR_REMOVED_FROM_DEVICE_GROUP")
        ApplicationConst.CHK_NOTIFY_DEVICE_MEMBERS_GROUP_ARE_ADDED_REMOVED = _("CHK_NOTIFY_DEVICE_MEMBERS_GROUP_ARE_ADDED_REMOVED")
        ApplicationConst.CHK_NOTIFY_DEVICE_GROUPS_ARE_ADDED_REMOVED = _("CHK_NOTIFY_DEVICE_GROUPS_ARE_ADDED_REMOVED")
        ApplicationConst.CHK_NOTIFY_DEVICE_GROUPS_SETTINGS_ARE_CHANGED = _("CHK_NOTIFY_DEVICE_GROUPS_SETTINGS_ARE_CHANGED")
        ApplicationConst.CHK_NOTIFY_I_CAN_CHANGE_DEVICE_SETTINGS_OR_ANSWER_CALLS = _("CHK_NOTIFY_I_CAN_CHANGE_DEVICE_SETTINGS_OR_ANSWER_CALLS")
        
        ApplicationConst.LBL_DEVICE_GROUPS_PROPERTY = _("LBL_DEVICE_GROUPS_PROPERTY")
        ApplicationConst.LBL_LOCATION_NAME = _("LBL_LOCATION_NAME")
        ApplicationConst.LBL_LOCATION_PROPERTY = _("LBL_LOCATION_PROPERTY")
        ApplicationConst.LBL_LABEL_PROPERTY = _("LBL_LABEL_PROPERTY")
        ApplicationConst.LBL_GROUP_PROPERTY = _("LBL_GROUP_PROPERTY")
        ApplicationConst.LBL_TIMEZONE_PROPERTY = _("LBL_TIMEZONE_PROPERTY")
        ApplicationConst.LBL_LOGIN_PAGE_HEADER = _("LBL_LOGIN_PAGE_HEADER")
        ApplicationConst.LBL_ADMIN_HOME_PAGE_HEADER = _("LBL_ADMIN_HOME_PAGE_HEADER")
        ApplicationConst.LBL_SIGNOUT_PAGE_HEADER = _("LBL_SIGNOUT_PAGE_HEADER")
        ApplicationConst.LBL_ADMIN_DASHBOARD_PAGE_HEADER = _("LBL_ADMIN_DASHBOARD_PAGE_HEADER")
        ApplicationConst.LBL_ADMIN_USERS_PAGE_HEADER = _("LBL_ADMIN_USERS_PAGE_HEADER")
        ApplicationConst.LBL_ADMIN_USERS_SECTION_USERS = _("LBL_ADMIN_USERS_SECTION_USERS")
        ApplicationConst.LBL_ADMIN_USERS_SECTION_USER_GROUPS = _("LBL_ADMIN_USERS_SECTION_USER_GROUPS")
        ApplicationConst.LBL_ADMIN_ACTIVITY_PAGE_HEADER = _("LBL_ADMIN_ACTIVITY_PAGE_HEADER")
        ApplicationConst.LBL_ACCOUNT_SETTINGS_PAGE_HEADER = _("LBL_ACCOUNT_SETTINGS_PAGE_HEADER")
        ApplicationConst.LBL_ACCOUNT_NOTIFICATIONS_PAGE_HEADER = _("LBL_ACCOUNT_NOTIFICATIONS_PAGE_HEADER")
        ApplicationConst.LBL_ORG_SETTINGS_PAGE_HEADER = _("LBL_ORG_SETTINGS_PAGE_HEADER")
        ApplicationConst.LBL_APPROVED_REQUEST_ACCESS_BEAM_HEADER = _("LBL_APPROVED_REQUEST_ACCESS_BEAM_HEADER")
        ApplicationConst.LBL_REJECTED_REQUEST_ACCESS_BEAM_HEADER = _("LBL_REJECTED_REQUEST_ACCESS_BEAM_HEADER")
        ApplicationConst.LBL_ERROR_REQUEST_ACCESS_BEAM_HEADER = _("LBL_ERROR_REQUEST_ACCESS_BEAM_HEADER")
        ApplicationConst.LBL_BEAM_SUPPORT_PAGE_HEADER = _("LBL_BEAM_SUPPORT_PAGE_HEADER")
        ApplicationConst.LBL_BEAMS_ALL_PAGE_HEADER = _("LBL_BEAMS_ALL_PAGE_HEADER")
        ApplicationConst.LBL_BEAMS_ACCESS_TIMES_PAGE_HEADER = _("LBL_BEAMS_ACCESS_TIMES_PAGE_HEADER")
        ApplicationConst.LBL_BEAMS_ACCESS_TIMES_TEMPORARY_USERS = _("LBL_BEAMS_ACCESS_TIMES_TEMPORARY_USERS")
        ApplicationConst.LBL_BEAMS_DEVICES_PAGE_HEADER = _("LBL_BEAMS_DEVICES_PAGE_HEADER")
        ApplicationConst.LBL_BEAMS_MEMBERS_PAGE_HEADER = _("LBL_BEAMS_MEMBERS_PAGE_HEADER")
        ApplicationConst.LBL_BEAMS_MEMBERS_USERS = _("LBL_BEAMS_MEMBERS_USERS")
        ApplicationConst.LBL_BEAMS_MEMBERS_USER_GROUPS = _("LBL_BEAMS_MEMBERS_USER_GROUPS")
        ApplicationConst.LBL_BEAMS_MEMBERS_REMOVE_USER = _("LBL_BEAMS_MEMBERS_REMOVE_USER")
        ApplicationConst.LBL_BEAMS_RESERVATIONS_PAGE_HEADER = _("LBL_BEAMS_RESERVATIONS_PAGE_HEADER")
        ApplicationConst.LBL_BEAMS_SETTINGS_PAGE_HEADER = _("LBL_BEAMS_SETTINGS_PAGE_HEADER")
        
        ApplicationConst.LBL_BEAMS_DETAILS_ADD_A_LABEL = _("LBL_BEAMS_DETAILS_ADD_A_LABEL")
        ApplicationConst.LBL_BEAMS_DETAILS_MANAGE = _("LBL_BEAMS_DETAILS_MANAGE")
        
        ApplicationConst.LBL_PASSWORD_SETUP_PAGE_HEADER = _("LBL_PASSWORD_SETUP_PAGE_HEADER")
        ApplicationConst.LBL_PASSWORD_RESET_PAGE_HEADER = _("LBL_PASSWORD_RESET_PAGE_HEADER")
        ApplicationConst.LBL_PASSWORD_RESET_MESSAGE_SENT_PAGE_HEADER = _("LBL_PASSWORD_RESET_MESSAGE_SENT_PAGE_HEADER")
        ApplicationConst.LBL_REQUEST_BEAM_ACCESS_PAGE_HEADER = _("LBL_REQUEST_BEAM_ACCESS_PAGE_HEADER")
        ApplicationConst.LBL_WELCOME_TO_BEAM_PAGE_HEADER = _("LBL_WELCOME_TO_BEAM_PAGE_HEADER")
        ApplicationConst.LBL_YOUR_BEAM_ACCOUNT_PAGE_HEADER = _("LBL_YOUR_BEAM_ACCOUNT_PAGE_HEADER")
        ApplicationConst.LBL_PASSWORD_CHANGE_PAGE_HEADER = _("LBL_PASSWORD_CHANGE_PAGE_HEADER")
        ApplicationConst.LBL_PASSWORD_CHANGE_COMPLETE_PAGE_HEADER = _("LBL_PASSWORD_CHANGE_COMPLETE_PAGE_HEADER")  
        
        ApplicationConst.LBL_SIMPLIFIED_ADMIN_DASHBOARD_HEADER = _("LBL_SIMPLIFIED_ADMIN_DASHBOARD_HEADER")
        
        # MISC LABELS
        ApplicationConst.LBL_CHANGE_PASSWORD = _("LBL_CHANGE_PASSWORD")
        ApplicationConst.LBL_RESET_MY_PASSWORD = _("LBL_RESET_MY_PASSWORD")
        ApplicationConst.LBL_SET_PASSWORD = _("LBL_SET_PASSWORD")
        ApplicationConst.LBL_PLAY_VIDEO_CONTINUE = _("LBL_PLAY_VIDEO_CONTINUE")
        ApplicationConst.LBL_CONFIRM_NEW_AUTH_METHOD = _("LBL_CONFIRM_NEW_AUTH_METHOD")
        ApplicationConst.LBL_DISCONNECT_SUCCESSFUL = _("LBL_DISCONNECT_SUCCESSFUL")
        ApplicationConst.LBL_YES = _("LBL_YES")
        ApplicationConst.LBL_NO = _("LBL_NO")
        ApplicationConst.LBL_DELETE = _("LBL_DELETE")
        
        #Application strings on dialogs/toast messages
        ApplicationConst.INFO_MSG_CREATE_DEVICE_GROUP_SUCCESSFUL = _("INFO_MSG_CREATE_DEVICE_GROUP_SUCCESSFUL")
        ApplicationConst.INFO_MSG_SAVE_DEVICE_GROUP_SETTING_SUCCESSFUL = _("INFO_MSG_SAVE_DEVICE_GROUP_SETTING_SUCCESSFUL")
        ApplicationConst.INFO_MSG_SET_DEVICE_GROUP_SUCCESSFUL = _("INFO_MSG_SET_DEVICE_GROUP_SUCCESSFUL")
        ApplicationConst.WARN_MSG_CHANGE_DEVICE_GROUP_NAME = _("WARN_MSG_CHANGE_DEVICE_GROUP_NAME")
        ApplicationConst.WARN_MSG_REMOVE_USER_GROUP = _("WARN_MSG_REMOVE_USER_GROUP")
        ApplicationConst.INFO_MSG_DELETE_USER_GROUP_SUCCESSFUL = _("INFO_MSG_DELETE_USER_GROUP_SUCCESSFUL")
        ApplicationConst.INFO_MSG_INVITE_USER_TO_SIMPL_BEAM_SUCCESSFUL = _("INFO_MSG_INVITE_USER_TO_SIMPL_BEAM_SUCCESSFUL")
        ApplicationConst.INFO_INVITATION_MAIL_SENT_SUCCESSFUL = _("INFO_INVITATION_MAIL_SENT_SUCCESSFUL")
        ApplicationConst.INFO_MSG_NEW_API_NOTICE = _("INFO_MSG_NEW_API_NOTICE")
        ApplicationConst.INFO_MSG_NEW_API_NOTICE_2 = _("INFO_MSG_NEW_API_NOTICE_2")
        ApplicationConst.INFO_MSG_EDIT_USER_SUCCESS = _("INFO_MSG_EDIT_USER_SUCCESS")
        
        #Device group
        ApplicationConst.DELETE_USER_FROM_DEVICE_GROUP = _("DELETE_USER_FROM_DEVICE_GROUP")
        ApplicationConst.LBL_DEVICE_GROUPS = _("LBL_DEVICE_GROUPS")
        ApplicationConst.LBL_USER_GROUPS = _("LBL_USER_GROUPS")
        
        #User Detail Page
        ApplicationConst.LBL_ADMINISTERS_GROUPS = _("LBL_ADMINISTERS_GROUPS")
        ApplicationConst.LBL_ADMINISTRATOR = _("LBL_ADMINISTRATOR")
        ApplicationConst.LBL_MENU_ADMINISTRATOR_ONLY = _("LBL_MENU_ADMINISTRATOR_ONLY")
        ApplicationConst.LBL_MENU_USER_GROUPS = _("LBL_MENU_USER_GROUPS")
        ApplicationConst.LBL_MENU_TEMPORARY_USERS = _("LBL_MENU_TEMPORARY_USERS")
        ApplicationConst.TXTF_CONFIRM_ASSOCIATION_WARNING_MESSAGE = _("TXTF_CONFIRM_ASSOCIATION_WARNING_MESSAGE")
    
        #Google Page
        ApplicationConst.LBL_DELETE_GOOGLE_ACCOUNT_AND_DATA = _("LBL_DELETE_GOOGLE_ACCOUNT_AND_DATA")
        ApplicationConst.BTN_DELETE_ACCOUNT = _("BTN_DELETE_ACCOUNT")
        ApplicationConst.LBL_ERROR_MESSAGE = _("LBL_ERROR_MESSAGE")
        
        #Others
        ApplicationConst.LBL_IMAGE_CROP_TRACKER = _("LBL_IMAGE_CROP_TRACKER")
        ApplicationConst.LBL_ACCESS_REQUEST_RECORD_MESSAGE = _("LBL_ACCESS_REQUEST_RECORD_MESSAGE")
        
        ApplicationConst.LBL_APPROVED_NOTIFICATION_MESSAGE = _("LBL_APPROVED_NOTIFICATION_MESSAGE")
        ApplicationConst.LBL_REJECTED_NOTIFICATION_MESSAGE = _("LBL_REJECTED_NOTIFICATION_MESSAGE")
        ApplicationConst.CHK_ALL_AUTHENTICATION_METHODS = _("CHK_ALL_AUTHENTICATION_METHODS")
        ApplicationConst.LBL_WELCOME_TO_BEAM_EMAIL_TITLE = _("LBL_WELCOME_TO_BEAM_EMAIL_TITLE")
        ApplicationConst.LBL_BEAM_ALL_AUTHENTICATION_METHODS = _("LBL_BEAM_ALL_AUTHENTICATION_METHODS")
        ApplicationConst.LBL_WELCOME_TEMPORARY_USER_EMAIL_TITLE = _("LBL_WELCOME_TEMPORARY_USER_EMAIL_TITLE")
        
        #Date-time
        ApplicationConst.LBL_DATETIME_MERIDIAN_AM = _("LBL_DATETIME_MERIDIAN_AM")
        ApplicationConst.LBL_DATETIME_MERIDIAN_PM = _("LBL_DATETIME_MERIDIAN_PM")
        
        # Device Advanced Settings dialog
        ApplicationConst.LBL_DEV_ATTR_SETTINGS = _("LBL_DEV_ATTR_SETTINGS")
        ApplicationConst.LBL_DEV_ATTR_INFORMATION = _("LBL_DEV_ATTR_INFORMATION")
        
        ApplicationConst.LBL_DEV_ATTR_SECTION_SYSTEM = _("LBL_DEV_ATTR_SECTION_SYSTEM")
        ApplicationConst.LBL_DEV_ATTR_SYSTEM_SERIAL_NUMBER = _("LBL_DEV_ATTR_SYSTEM_SERIAL_NUMBER")
        ApplicationConst.LBL_DEV_ATTR_SYSTEM_UID = _("LBL_DEV_ATTR_SYSTEM_UID")
        ApplicationConst.LBL_DEV_ATTR_SYSTEM_SOFTWARE_VERSION = _("LBL_DEV_ATTR_SYSTEM_SOFTWARE_VERSION")
        
        ApplicationConst.LBL_DEV_ATTR_SECTION_NETWORK = _("LBL_DEV_ATTR_SECTION_NETWORK")
        ApplicationConst.LBL_DEV_ATTR_NETWORK_CURRENT_WIFI_NETWORK = _("LBL_DEV_ATTR_NETWORK_CURRENT_WIFI_NETWORK")
        ApplicationConst.LBL_DEV_ATTR_NETWORK_SSID = _("LBL_DEV_ATTR_NETWORK_SSID")
        ApplicationConst.LBL_DEV_ATTR_NETWORK_FREQUENCY = _("LBL_DEV_ATTR_NETWORK_FREQUENCY")
        ApplicationConst.LBL_DEV_ATTR_NETWORK_INTERFACE_TYPE = _("LBL_DEV_ATTR_NETWORK_INTERFACE_TYPE")
        ApplicationConst.LBL_DEV_ATTR_NETWORK_MAC = _("LBL_DEV_ATTR_NETWORK_MAC")
        ApplicationConst.LBL_DEV_ATTR_NETWORK_IP_ADDR = _("LBL_DEV_ATTR_NETWORK_IP_ADDR")
        ApplicationConst.LBL_DEV_ATTR_NETWORK_RELAY_SERVER = _("LBL_DEV_ATTR_NETWORK_RELAY_SERVER")
        
    
    @staticmethod
    def initialize_date_time_localization():
        ApplicationConst._date_time_localized_values["MON01"] = _("MON01")
        ApplicationConst._date_time_localized_values["MON02"] = _("MON02")
        ApplicationConst._date_time_localized_values["MON03"] = _("MON03")
        ApplicationConst._date_time_localized_values["MON04"] = _("MON04")
        ApplicationConst._date_time_localized_values["MON05"] = _("MON05")
        ApplicationConst._date_time_localized_values["MON06"] = _("MON06")
        ApplicationConst._date_time_localized_values["MON07"] = _("MON07")
        ApplicationConst._date_time_localized_values["MON08"] = _("MON08")
        ApplicationConst._date_time_localized_values["MON09"] = _("MON09")
        ApplicationConst._date_time_localized_values["MON10"] = _("MON10")
        ApplicationConst._date_time_localized_values["MON11"] = _("MON11")
        ApplicationConst._date_time_localized_values["MON12"] = _("MON12")
        
        ApplicationConst._date_time_localized_values["Jan"] = _("Jan")
        ApplicationConst._date_time_localized_values["Feb"] = _("Feb")
        ApplicationConst._date_time_localized_values["Mar"] = _("Mar")
        ApplicationConst._date_time_localized_values["Apr"] = _("Apr")
        ApplicationConst._date_time_localized_values["May"] = _("May")
        ApplicationConst._date_time_localized_values["Jun"] = _("Jun")
        ApplicationConst._date_time_localized_values["Jul"] = _("Jul")
        ApplicationConst._date_time_localized_values["Aug"] = _("Aug")
        ApplicationConst._date_time_localized_values["Sep"] = _("Sep")
        ApplicationConst._date_time_localized_values["Sept"] = _("Sept")
        ApplicationConst._date_time_localized_values["Oct"] = _("Oct")
        ApplicationConst._date_time_localized_values["Nov"] = _("Nov")
        ApplicationConst._date_time_localized_values["Dec"] = _("Dec")
        
        ApplicationConst._date_time_localized_values["Mon"] = _("Mon")
        ApplicationConst._date_time_localized_values["Tue"] = _("Tue")
        ApplicationConst._date_time_localized_values["Wed"] = _("Wed")
        ApplicationConst._date_time_localized_values["Thu"] = _("Thu")
        ApplicationConst._date_time_localized_values["Fri"] = _("Fri")
        ApplicationConst._date_time_localized_values["Sat"] = _("Sat")
        ApplicationConst._date_time_localized_values["Sun"] = _("Sun")
        ApplicationConst._date_time_localized_values["AM"] = _("AM")
        ApplicationConst._date_time_localized_values["PM"] = _("PM")
        ApplicationConst._date_time_localized_values["All day"] = _("All day")
        ApplicationConst._date_time_localized_values["every day"] = _("every day")
    
       
    @staticmethod
    def get_date_time_label(english_text):
        return ApplicationConst._date_time_localized_values[english_text]
    