import os
from data import setting

class Browser(object):
    Firefox = "Firefox"
    IE = "IE"
    Chrome = "Chrome"
    Safari = "Safari"
    Edge = "Edge"


class Platform(object):
    WINDOWS = "WINDOWS"
    MAC = "MAC"
    ANY = "ANY"


class Language(object):
    ENGLISH = "ENGLISH"
    JAPANESE = "JAPANESE"
    FRENCH = "FRENCH"

    
class Constant(object):
    
    RunEnvironmentFile = "data/RunEnv.txt"
    
    SuitableTechURL = "https://stg1.suitabletech.com"
    SuitableTechHomeURL = SuitableTechURL + "/manage/#/home/"
    SuitableTechLoginURL = SuitableTechURL + "/accounts/login/"
    GoogleSignInURL = "https://accounts.google.com/ServiceLogin"
    AppsConnectedURL = "https://security.google.com/settings/u/7/security/permissions?pli=1"
    
    SimplifiedAdminEmail = "logigear1+simpleadmin@suitabletech.com"
    SimplifiedAdminFullName = "Simplified Org Admin"
    
    MixedMultiOrgAdminEmail = "thanh.viet.le@logigear.com"
    MixedMultiOrgAdminFullName = "Mixed Multi Org Admin"
    
    SimplifiedGoogleUserEmail = "lgvnggac@gmail.com"
    SimplifiedGoogleUserFullName = "LGVN GGAC"
    
    NotificationEmail = "notifications@suitabletech.com"
    
    DefaultPassword = "Logigear123"     
    DefaultBaseEmail1 = "logigear1@suitabletech.com"
    DefaultBaseEmail2 = "logigear2@suitabletech.com"   
    DefaultBaseEmail3 = "suitabletech3@gmail.com"
    
    # Not need to create new
    NonGSSOUserEmail = "lgvn.non.gsso@gmail.com"
    UnAllowGSSOUserEmail = "lgvn.gsso@gmail.com"
    
#     GMailNonGSSOAuthenForwarded = "lgvn1usertest@gmail.com" # to be deleted
#     GSSOAuthenEmail = "lgvnsuitabletech@gmail.com" # to be deleted
#     GMailNeverAccessSTPage = "lgvnsuitabletech7@gmail.com" # to be deleted
#     GMailGSSOAuthenForwarded = "lgvnggac@gmail.com"
    
    # Need to create new
    DeletedGoogleEmail = "lgvnsuitabletech11@gmail.com"
    GGAccCreatedBySTEmail = "lgvnsuitabletech12@gmail.com"
    NewGmailNonGSSOAuthenForwarded = "lgvnsuitabletech8@gmail.com"
    NewGmailNonGSSOAuthenForwarded_01 = "lgvnsuitabletech9@gmail.com"
    
    # Organization Names
    AdvancedOrgName = "LogiGear Test"  # includes AdvancedAdmin Admin
    SimplifiedOrgName = "LogiGear Test 2"  # includes SimpleAdmin Admin
    AdvancedOrgName_2 = "LogiGear Test 3"  # includes AdvancedAdmin and AdvancedMultiOrg Admins
    
    # Devices/Users Group
    DeviceGroup = "Default LGVN Devices Group"
    UserGroup = "Default LGVN Users Group"
    
    # Beam Names
    BeamPlusName = u"QA BeamPlus Visitor1"
    BeamProNameUTF8 = u"QA BeamPro \u00a9 Visitor1"  # QA BeamPro © Visitor1
    BeamPlusMock1Name = u"LogiGear Mock Beam+ 1"
    BeamPlusMock2Name = u"LogiGear Mock Beam+ 2"
    BeamPlusMock3Name = u"LogiGear Mock Beam+ 3"
    BeamPlusMock4Name = u"LogiGear Mock Beam+ 4"
    
    # API Section
    APIBaseURL = "https://stg1.suitabletech.com/admin-api/1/"
    APIKeys = {'LogiGear Test':'APIKEY:8Qfu4d1B5q/N:bwCoaKdMTDkXn+oILk8uKU68XieX6rYrDWqXDGUkkAHC', 'LogiGear Test 3':'APIKEY:BwTeIJ4EUG05:ULJGvbqjbjQF6Pl1vqJreOpP3zLsQFDQ6K8Gl0K0rMBG'}
    DeviceIDs = {'LogiGear Mock Beam+ 1':'logigear-mock-beamplus-1', 'LogiGear Mock Beam+ 4':'logigear-mock-beamplus-4', 'QA BeamPlus Visitor1':'beam135909030', 'QA BeamPro © Visitor1':'beam100108146', 'LogiGear Mock Beam+ 2':'860', 'LogiGear Mock Beam+ 3':'861'}
    SimplifiedOrgIDs = {SimplifiedOrgName:"130"}
    
    # Beam client
    WindowsBeamClientName = "beam-stable-2.15.10-win32.exe"
    WindowsBeamClientSize = "38.5"  # in MB
    
    MacBeamClientName = "beam-stable-2.15.10-Darwin.dmg"
    MacBeamClientSize = "53.7"  # in MB
    
    
    #Note: use get_dict_value method in Helper to get value
    BaseEmails = {1: "logigear1@suitabletech.com",
                  2: "logigear2@suitabletech.com",
                  3: "suitabletech3@gmail.com",
                  4: "suitabletech4@gmail.com",
                  5: "suitabletech5@gmail.com"}
    
    AdvancedOragnizations = [{"name": "LogiGear Test", "type": "Advanced", "id": 129, "api_key": "APIKEY:8Qfu4d1B5q/N:bwCoaKdMTDkXn+oILk8uKU68XieX6rYrDWqXDGUkkAHC"},
                             {"name": "LogiGear Test 3", "type": "Advanced", "id": 131, "api_key": "APIKEY:BwTeIJ4EUG05:ULJGvbqjbjQF6Pl1vqJreOpP3zLsQFDQ6K8Gl0K0rMBG"}]
    
    SimplifiedOragnizations = [{"name": "LogiGear Test 2", "type": "Simplified", "id": 130, "api_key": ""}]
    
    AdvancedBeams = [{"name": u"QA BeamPlus Visitor1", "id": "beam135909030", "organization": "LogiGear Test"},
                     {"name": u"QA BeamPro \u00a9 Visitor1", "id": "beam100108146", "organization": "LogiGear Test"},
                     {"name": u"LogiGear Mock Beam+ 1", "id": "logigear-mock-beamplus-1", "organization": "LogiGear Test"},
                     {"name": u"LogiGear Mock Beam+ 4", "id": "logigear-mock-beamplus-4", "organization": "LogiGear Test"}]
    
    SimplifiedBeams = [{"name": u"LogiGear Mock Beam+ 2", "id": "860", "organization": "LogiGear Test 2"},
                       {"name": u"LogiGear Mock Beam+ 3", "id": "861", "organization": "LogiGear Test 2"}]
    

    @staticmethod 
    def initialize(language): 
        try:
            from configparser import ConfigParser
        except ImportError:
            from ConfigParser import ConfigParser  # ver. < 3.0
        account_ini_file = os.path.join(os.path.dirname(setting.__file__), "accounts.ini")
        
        config = ConfigParser()
        config.read(account_ini_file)
        
        lang_section = "default"
        if config.has_section(language):
            lang_section = language
        
        Constant.GMailNonGSSOAuthenForwarded = config.get(lang_section, "GMailNonGSSOAuthenForwarded")  # "lgvn1usertest@gmail.com"
        Constant.GSSOAuthenEmail = config.get(lang_section, "GSSOAuthenEmail")  # "lgvnsuitabletech@gmail.com"
        Constant.GMailGSSOAuthenForwarded = config.get(lang_section, "GMailGSSOAuthenForwarded")  # "lgvnggac@gmail.com"
    
