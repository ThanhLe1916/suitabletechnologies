import os
import pytest
from data import setting

def pytest_addoption(parser):
    parser.addoption("--runenv", action = "store", default = 'abc', help = "Specify running environment")

@pytest.fixture(autouse = True)
def runenv(request):
    
    argument = request.config.getoption('--runenv')
    file_path = os.path.dirname(setting.__file__)
    f_stream = open(file_path + os.path.sep + "ExecutionSetting.txt", "w")
    f_stream.write(argument)
    f_stream.close()
    
    return argument
