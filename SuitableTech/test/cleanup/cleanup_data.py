from core.utilities.test_condition import TestCondition
from common.constant import Constant
from core.utilities.gmail_utility import GmailUtility
from test.testbase import TestBase


class CleanUp_Test(TestBase):
    
    def test_clean_up_test_data(self):
        TestCondition.remove_all_device_groups(organization=Constant.AdvancedOrgName)
        TestCondition.remove_all_device_groups(organization=Constant.AdvancedOrgName_2)
        TestCondition.remove_all_user_groups(organization=Constant.AdvancedOrgName)
        TestCondition.remove_all_user_groups(organization=Constant.AdvancedOrgName_2)
        TestCondition.remove_test_advanced_users(keyword_array=["logigear1+user", "logigear2+user", "suitabletech3", "suitabletech4", "suitabletech5"], organization=Constant.AdvancedOrgName)
        TestCondition.remove_test_advanced_users(keyword_array=["logigear1+user", "logigear2+user", "suitabletech3", "suitabletech4", "suitabletech5"], organization=Constant.AdvancedOrgName_2)
        TestCondition.reject_all_access_requests(self._driver)
        TestCondition.remove_test_simplified_users(driver=self._driver, organization=Constant.SimplifiedOrgName, device_name=Constant.BeamPlusMock2Name, keyword_array=["logigear1+user", "logigear2+user", "suitabletech3", "suitabletech4", "suitabletech5"])
        TestCondition.remove_test_simplified_users(driver=self._driver, organization=Constant.SimplifiedOrgName, device_name=Constant.BeamPlusMock3Name, keyword_array=["logigear1+user", "logigear2+user", "suitabletech3", "suitabletech4", "suitabletech5"])
        
        for i in range(5):
            GmailUtility.delete_all_emails(Constant.BaseEmails[i+1])
        