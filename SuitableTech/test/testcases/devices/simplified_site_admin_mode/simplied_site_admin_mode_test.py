from pages.suitable_tech.user.home_page import HomePage
from common.helper import Helper
from common.constant import Constant
from core.utilities.test_condition import TestCondition
from data.dataobjects.user import User
from test.testbase import TestBase

class SimpliedSiteAdminModeTest(TestBase):
    
    def test_c10927_change_device_location_1_x(self):
        """
        @author: Duy.Nguyen
        @date: 8/9/2016
        @summary: Change device location [1.X]
        @precondition: 
            Devices-Mod-Location
            Use the following Admin Test Organization - Accounts used for Suitable Tech Manual/Automation Testing:
            http://wiki.suitabletech.com/display/SUIT/QA+Test+Accounts#QATestAccounts-TestOrgs
            There is a device (Beam1) added.
        @steps:        
            1) Login to Suitabletech.com as simplified org admin and select "Manage Your Beams" from the user dropdown menu
            2) On "Manage Your Beams" page, select a device (Beam1) by clicking on the device icon
            3) Select "Edit Details" button to the right of the Beam Image
            4) Modify the "Location" information then select "Save Changes" button on "Edit Device" pop-up
            5) Back to "Manage Your Beams" page and select a device (Beam1) by clicking "Manage" button
            6) Select "Edit Details" button to the right of the Beam Image
            7) Delete the "Location" information then select "Save Changes" button

        @expected:
            (6) (8)
            _Verify that the message "The device was saved successfully." displays.
            _Verify that all changes are saved.     
        """
        
        try:
            #pre-condition:
            location = Helper.generate_random_string()
            
            test_device_name = Constant.BeamPlusMock3Name
            test_organization=Constant.SimplifiedOrgName
            
            simplified_dev_admin = User()
            simplified_dev_admin.generate_simplified_device_admin_data()
            simplified_dev_admin.device_group = test_device_name
            simplified_dev_admin.organization = test_organization
            
            TestCondition.create_simplified_device_admin(
                                driver=self._driver, 
                                user_array=[simplified_dev_admin], 
                                device_name=simplified_dev_admin.device_group,
                                organization=simplified_dev_admin.organization)
               
            #step:
            simplied_detail_beam_page = HomePage(self._driver).open()\
                .goto_login_page()\
                    .login_as_unwatched_video_user(simplified_dev_admin.email_address, simplified_dev_admin.password)\
                .goto_simplified_dashboard_page_by_menu_item()\
                .goto_manage_beam_page(test_device_name)  
                          
            old_location = simplied_detail_beam_page.get_beam_location()            
            simplied_detail_beam_page.set_beam_location(location)
            
            #verify point:
            self.assertEqual(simplied_detail_beam_page.get_beam_location(), location, "Assertion Error: The Location is not changed")            
            simplied_detail_beam_page.clear_beam_location()
            
            #verify point:
            self.assertEqual(simplied_detail_beam_page.get_beam_location(), "None", "Assertion Error: The Location is not cleared")
            
        finally:
            # post-condition:
            TestCondition.restore_simplified_beam_location(
                    driver=self._driver, 
                    beam_name=test_device_name, 
                    location=old_location, 
                    organization=test_organization)
            
            TestCondition.delete_simplified_users(self._driver, [simplified_dev_admin], test_device_name, test_organization)

        
    def test_c10928_change_device_label_1_x(self):
        """
        @author: Duy.Nguyen
        @date: 8/9/2016
        @summary: Change device Label [1.X]
        @precondition: 
            Use the following Admin Test Organization - Accounts used for Suitable Tech Manual/Automation Testing:
            http://wiki.suitabletech.com/display/SUIT/QA+Test+Accounts#QATestAccounts-TestOrgs
            There is a device (Beam1) added.
        @steps:        
            1) Login to Suitabletech.com as simplified org admin and select "Manage Your Beams" from the user dropdown menu
            2) On "Manage Your Beams" page, select a device (Beam1) by clicking on the device icon
            3) Select "Edit Details" button to the right of the Beam Image
            4) Modify the "Labels" information then select "Save Changes" button on "Edit Device" pop-up
            5) Back to "Manage Your Beams" page and select a device (Beam1) by clicking "Manage" button
            6) Select "Edit Details" button to the right of the Beam Image
            7) Delete the "Labels" information then select "Save Changes" button

        @expected:
            (6) (8)
            _Verify that the message "The device was saved successfully." displays.
            _Verify that all changes are saved.     
        """
        try:
            #pre-condition:
            beam_label = Helper.generate_random_label_name()
            beam_label_list = [beam_label]
            
            test_device_name = Constant.BeamPlusMock3Name
            test_organization=Constant.SimplifiedOrgName
            
            simplified_dev_admin = User()
            simplified_dev_admin.generate_simplified_device_admin_data()
            simplified_dev_admin.device_group = test_device_name
            simplified_dev_admin.organization = test_organization
            
            TestCondition.create_simplified_device_admin(
                                driver=self._driver, 
                                user_array=[simplified_dev_admin], 
                                device_name=simplified_dev_admin.device_group,
                                organization=simplified_dev_admin.organization)
               
            #step:
            simplied_admin_dashboard_page = HomePage(self._driver).open()\
                .goto_login_page()\
                    .login_as_unwatched_video_user(simplified_dev_admin.email_address, simplified_dev_admin.password)\
                .goto_simplified_dashboard_page_by_menu_item()
            
            simplied_detail_beam_page = simplied_admin_dashboard_page.goto_manage_beam_page(test_device_name)            
            old_beam_label = simplied_detail_beam_page.get_beam_label_tag_list()           
            simplied_detail_beam_page.set_beam_label_tag_list(beam_label_list)
            actual_beam_label = simplied_detail_beam_page.get_beam_label()
            
            #verify point:
            self.assertEqual(actual_beam_label, beam_label, 
                              "Assertion Error: The Label is not changed")
                        
            simplied_detail_beam_page.clear_all_beam_label()
                        
            #verify point:
            self.assertEqual(simplied_detail_beam_page.get_beam_label(), "None", 
                             "Assertion Error: The Label is not cleared")
            
        finally:
            # post-condition:
            TestCondition.restore_simplified_beam_labels(
                    driver=self._driver, 
                    beam_name=test_device_name, 
                    label_tag_list=old_beam_label, 
                    organization=test_organization)

            TestCondition.delete_simplified_users(self._driver, [simplified_dev_admin], test_device_name, test_organization)
            
    