from common.helper import Helper
from data.dataobjects.user import User
from pages.suitable_tech.user.home_page import HomePage
from common.application_constants import ApplicationConst
from core.utilities.test_condition import TestCondition
from test.testbase import TestBase
from common.constant import Constant


class User_Group_Test(TestBase):
    
    def test_c10918_rename_usergroup_1_x(self):
        """
        @author: khoi.ngo
        @date: 7/26/2016
        @summary: Rename UserGroup [1.X]
        @precondition: Use the Admin Test Organization
        @steps:
            1. Go to the "Users" tab under the "Manage your Beams" dashboard
            2. Click on a user group icon under the "User Groups" section
            3. Click the "edit" box under the user group title
            4. Change the "Name" string to desired name; click save changes

        @expected:
            User group should automatically update.
        """
        try:
            #pre-condition            
            user_group_name = Helper.generate_random_user_group_name()
            new_user_group_name = Helper.generate_random_user_group_name()
            is_user_group_name_changed = False
            
            org_admin = User()
            org_admin.generate_data()
            
            TestCondition.create_advanced_organization_admins(self._driver, [org_admin])
            TestCondition.create_user_group(user_group_name)
             
            #steps
            admin_dashboard_page = HomePage(self._driver).open().goto_login_page()\
                .login_as_unwatched_video_user(org_admin.email_address, org_admin.password).goto_admin_dashboard_page_by_menu_item()    
            user_group_detail = admin_dashboard_page.goto_users_tab().goto_user_group_detail_page(user_group_name) 
             
            is_user_group_name_changed = user_group_detail.change_group_name(new_user_group_name)\
                .is_user_group_name_displayed(new_user_group_name)
              
            self.assertTrue(is_user_group_name_changed, "Assertion Error: User group's name was not changed.")
        
        finally:
            #post-condition
            TestCondition.delete_advanced_users([org_admin])
            TestCondition.delete_user_groups([new_user_group_name])
    
    
    def test_c10917_create_usergroup_1_x(self):
        """
        @author: Quang Tran
        @date: 08/04/2016
        @summary: Create UserGroup [1.X]
        @precondition: 
        @steps:
            Steps to complete create a user group:
            1. Login to Suitabletech.com as an org admin and select Manage Your Beams from the user dropdown menu
            2. Go to "Users" tab and select "Create New User Group" button
            3. Enter name of user group to "Name" field and click "Create User Group" button
            4. Click on "Users" tab again to reload User page
            5. Search for the newly created User Group in step #3 above

        @expected:
            (3). The user group with name entered at step #3 is created for adding user.
            (5). The newly created User Group is presented in user group list.
        """
        try:       
            #pre-condition
            org_admin = User()
            org_admin.generate_data()
            TestCondition.create_advanced_organization_admins(driver=self._driver, user_array=[org_admin])
            
            #steps
            user_group_name = Helper.generate_random_user_group_name()
            user_group_detail_page = HomePage(self._driver).open_and_goto_login_page()\
                .login_as_unwatched_video_user(org_admin.email_address, org_admin.password)\
                .goto_admin_dashboard_page_by_menu_item()\
                .goto_users_tab().create_new_user_group(user_group_name)
            
            #verify point
            is_user_group_name_displayed = user_group_detail_page.is_user_group_name_displayed(user_group_name)
            self.assertTrue(is_user_group_name_displayed, "Assertion Error: The user group {} cannot be created.".format(user_group_name))
            
            if is_user_group_name_displayed:
                admin_users_page = user_group_detail_page.goto_users_tab()
                self.assertTrue(admin_users_page.is_user_group_existed(user_group_name), 
                    "Assertion Error: The user group {} cannot be found in the user group list.".format(user_group_name))
            
        finally:
            #post-condition:
            TestCondition.delete_user_groups([user_group_name])
            TestCondition.delete_advanced_users(user_array=[org_admin])
    
    
    def test_c10919_delete_usergroup_1_x(self):
        """
        @author: khoi.ngo
        @date: 7/26/2016
        @summary: Delete UserGroup [1.X]
        @precondition: Use the Admin Test Organization
        @steps:
            1. Go to the "Users" tab under the "Manage your Beams" dashboard
            2. Click on a user group icon under the "User Groups" section
            3. Click the "Delete this group" red box in the top right corner of the screen
            4. You should see a message box; click okay (see attached image)

        @expected:
            The user group should be deleted right away.
        """        
        try:
            #pre-condition            
            user_group_name = Helper.generate_random_user_group_name()                
            org_admin = User()
            org_admin.generate_data()
            
            TestCondition.create_advanced_organization_admins(self._driver, [org_admin])
            TestCondition.create_user_group(user_group_name)
                
            #steps
            user_tab = HomePage(self._driver).open().goto_login_page()\
                .login_as_unwatched_video_user(org_admin.email_address, org_admin.password)\
                .goto_admin_dashboard_page_by_menu_item()\
                .goto_users_tab()
                
            admin_user_page = user_tab.goto_user_group_detail_page(user_group_name)\
                .delete_user_group(wait_for_completed=False)
            
            #verify point
            self.assertEqual(admin_user_page.get_msg_success(), ApplicationConst.INFO_MSG_DELETE_USER_GROUP_SUCCESSFUL, 
                             "Assertion Error: Expected message is not displayed")
            self.assertTrue(admin_user_page.is_user_group_not_existed(user_group_name), 
                            "Assertion Error: User group is still displayed")
            
        finally:
            TestCondition.delete_advanced_users([org_admin])
    
    
    def test_c10920_add_user_to_usergroup_1_x(self):
        """
        @author: Quang Tran
        @date: 08/04/2016
        @summary: Create UserGroup [1.X]
        @precondition: 
            Create a User Group (UserGroupA) following these steps:
            
                Login to Suitabletech.com as an org admin and select Manage Your Beams from the user dropdown menu
                Go to "Users" tab and select "Create New User Group" button
                Enter name of user group to "Name" field and click "Create User Group" button
            
            Invite a new user UserA
        @steps:
            Steps To Complete Task: Add user to user group
            
            1. Login to Suitabletech.com as an org admin and select Manage Your Beams from the user dropdown menu
            2. Go to "Users" tab and search for a user group (UserGroupA) in pre-condition
            3. Select this user group and click on "Add Users" button
            4. Search for UserA and select "Add Selected Users" button

        @expected:
            (3) Verify that user is added under the "Users in this group" list.
            (4) Verify that the user group name is added to "User Groups" of UserA's detail page.
        """
        try:
            #pre-condition
            device_group = Helper.generate_random_device_group_name()
            organization = Constant.AdvancedOrgName
            TestCondition.create_device_group(device_group_name=device_group, device_array=[], organization_name=organization)
            new_user_group_name = Helper.generate_random_user_group_name()
            TestCondition.create_user_group(new_user_group_name)
            
            new_user = User()
            new_user.generate_data()
            new_user.device_group = device_group
            new_user.user_group = None     
            new_user.organization = organization       
            TestCondition.create_advanced_normal_users(self._driver, [new_user])
            
            org_admin = User()
            org_admin.generate_data()
            TestCondition.create_advanced_organization_admins(driver=self._driver, user_array=[org_admin])
            
            #steps
            user_group_detail_page = HomePage(self._driver).open_and_goto_login_page()\
                .login_as_unwatched_video_user(org_admin.email_address, org_admin.password)\
                .goto_admin_dashboard_page_by_menu_item()\
                .goto_users_tab()\
                .goto_user_group_detail_page(new_user_group_name).add_user_to_group(new_user.email_address)
            
            #verify point
            is_user_added = user_group_detail_page.is_user_existed(new_user.get_displayed_name())
            self.assertTrue(is_user_added, "Assertion Error: The new user {} cannot be added to user group {}.".format(new_user.email_address, new_user_group_name))
            
            admin_user_detail_page = user_group_detail_page.goto_users_tab().goto_user_detail_page(new_user)
            self.assertEqual(new_user_group_name, admin_user_detail_page.get_user_info(ApplicationConst.LBL_USER_GROUPS), 
                             "Assertion Error: The user group name {} is not added to \"User Groups\" of user {}.".format(new_user_group_name, new_user.email_address ))
            
        finally:
            #post-condition:
            TestCondition.delete_user_groups([new_user_group_name])
            TestCondition.delete_advanced_users([new_user, org_admin])
            TestCondition.delete_device_groups([device_group], organization)


    def test_c10921_remove_user_from_usergroup_1_x(self):
        """
        @author: khoi.ngo
        @date: 08/05/2016
        @summary: Remove user from UserGroup [1.X]
        @precondition: 
            Create a User Group (UserGroupA) following these steps:
            1. Login to Suitabletech.com as an org admin and select Manage Your Beams from the user dropdown menu
            2. Go to "Users" tab and select "Create New User Group" button
            3. Enter name of user group to "Name" field and click "Create User Group" button
            
            Invite a new user UserA and add this user to above user group (UserGroupA)
        @steps:
        
            1. Login to Suitabletech.com as an org admin and select Manage Your Beams from the user dropdown menu
            2. Go to "Users" tab and search for exist user group in pre-condition
            3. Click on this user group button
            4. Select "Removed" red button on the user icon
            5. Click on "Ok" button on warning message
        @expected:
            _Verify that user is removed from "Users in this group" list.
            _Verify that the user group is not in "User Groups" of UserA's detail page.
        """
        try:
            # precondition
            user_group_name = Helper.generate_random_user_group_name()
            new_user = User()
            new_user.generate_data()
            new_user.user_group = user_group_name
            
            device_group_name = Helper.generate_random_device_group_name()
            new_user.device_group = device_group_name
            TestCondition.create_device_group(device_group_name)
            
            TestCondition.create_user_group(user_group_name)
            TestCondition.create_advanced_normal_users(self._driver, [new_user])
            
            org_admin = User()
            org_admin.generate_data()
            org_admin.device_group = device_group_name
            TestCondition.create_advanced_organization_admins(driver=self._driver, user_array=[org_admin])
            
            # steps
            user_group_detail = HomePage(self._driver).open_and_goto_login_page()\
                .login_as_unwatched_video_user(org_admin.email_address, org_admin.password)\
                .goto_admin_dashboard_page_by_menu_item()\
                .goto_users_tab()\
                .goto_user_group_detail_page(user_group_name).remove_user(new_user.get_displayed_name())\
            
            is_user_not_existed = user_group_detail.is_user_not_existed(new_user.get_displayed_name())
            
            #verify point
            self.assertTrue(is_user_not_existed, "Assertion Error: User still exists in this group")
            
            user_detail_page = user_group_detail.goto_users_tab().goto_user_detail_page(new_user)
            actual_user_groups = user_detail_page.get_user_groups()
            self.assertEqual([], actual_user_groups, "Assertion Error: User is still in User group " + user_group_name)
        
        finally:
            # post-condition
            TestCondition.delete_advanced_users([new_user, org_admin])
            TestCondition.delete_user_groups([user_group_name])
            TestCondition.delete_device_groups([device_group_name])
   
                
    def test_c10992_view_usergroup_in_list_and_icon_viewing_mode(self):
        """
        @author: Thanh Le
        @date: 08/05/2016
        @summary: Create UserGroup [1.X]
        @precondition: 
            Have a user group (UserGroupA) with a user (UserA) added
        @steps:
            1) Login to Suitabletech.com as an org admin and select Manage Your Beams from the user dropdown menu
            2) Go to "Users" tab
            3) Select icon view button on the top right to view icon mode
            4) Select list view button on the top right to view in list mode

        @expected:
            Verify Users-Groups/Users are all visible in a usable manner:
            1. Size of Icons are correct
            2. Text sizes
        """
        try:
            #pre-condition
            user_group_name = Helper.generate_random_user_group_name()
            new_user = User()
            new_user.generate_data()            
            org_admin = User()
            org_admin.generate_data()
            # create device group
            device_group_name = Helper.generate_random_device_group_name()
            new_user.device_group = device_group_name
            org_admin.device_group = device_group_name
            TestCondition.create_device_group(device_group_name)
            
            TestCondition.create_advanced_organization_admins(self._driver, [org_admin])
            TestCondition.create_advanced_normal_users(self._driver, [new_user])
            TestCondition.create_user_group(user_group_name, [new_user])
                   
            #steps            
            user_page = HomePage(self._driver).open_and_goto_login_page()\
                .login_as_unwatched_video_user(org_admin.email_address, org_admin.password)\
                .goto_admin_dashboard_page_by_menu_item()\
                .goto_users_tab().search_for_user(new_user.email_address)
            
            user_list_view_size = user_page.switch_to_list_view()\
                .get_item_size_in_list_view(new_user.get_displayed_name())
            user_group_list_view_size = user_page.get_item_size_in_list_view(user_group_name)
            
            user_icon_view_size = user_page.switch_to_icon_view()\
                .get_item_size_in_icon_view(new_user.get_displayed_name())
            user_group_icon_view_size = user_page.get_item_size_in_icon_view(user_group_name)
            
            # verify points
            self.assertTrue(user_icon_view_size > user_list_view_size,
                            "Assertion Error: Unable to switch from icon view to list view.")
            self.assertTrue(user_group_icon_view_size > user_group_list_view_size,
                            "Assertion Error: Unable to switch from icon view to list view.")
        finally:
            #post-condition:
            TestCondition.delete_advanced_users([org_admin,new_user])            
            TestCondition.delete_user_groups([user_group_name])
            TestCondition.delete_device_groups([device_group_name])
            
    def test_c11097_move_an_existing_user_from_usergroup_a_to_another_usergroup_b_2_x(self):
        """
        @author: Thanh Le
        @date: 08/05/2016
        @summary: Move an existing User from UserGroup A to another UserGroup B [2.X]
        @precondition: 
            Use the following Admin Test Organization - Accounts used for Suitable Tech Manual/Automation Testing:
            http://wiki.suitabletech.com/display/SUIT/QA+Test+Accounts#QATestAccounts-TestOrgs
            
            Add UserGroupA
            Add UserGroupB
            Create UserA
        @steps:
            1) Login to Suitabletech.com as an org admin and select Manage Your Beams from the user dropdown menu
            2) Go to "Users" tab and select the existing user group (UserGroupA)
            3) Add existing user (UserA) to this user group (UserGroupA)
            4) Go to "User " tab and select another existing user group (UserGroupB) and also add UserA to this group
            5) Go to the initial user group (UserGroupA) and remove the user (A)
            6) Go to UserGroup B and search for the just removed UserA from initial user group

        @expected:
            (6 The removed user from initial user group (UserGroupA) still exist in the other user group (UserGroupB).

            Note:
            General Business logic vitrification for DeviceGroup(s)
            - DeviceGroup(s) are containers for a single or multiple Device(s)
            - Single User(s) can be members of a DeviceGroup outside of a UserGroup via a Default or Custom Access Time Schedule(s)/Template(s), allowing User(s) access to associated Device(s)
            - Single or multiple UserGroup(s) can be members of a DeviceGroup via a Default or Custom Access Time Schedule(s)/Template(s), allowing User(s) access to associated Device(s)
            - Single User can be a members of DeviceGroup(s) via a Default or Custom Access Time Schedule(s)/Template(s), allowing User(s) access to associated Device(s)
            - Single Default or multiple custom Access Time Schedule(s)/Template(s) can be members of a DeviceGroup
            - Single or multiple Temporary Access Time Schedule(s) can be attached to a DeviceGroup
            - Device(s) status Notifications to User(s) can be associated with a DeviceGroup
            - Session Answer Notifications to User(s) to accept call answer requests can be associated with a DeviceGroup
        """
                
        try:
            #pre-condition
            admin_user = User()                                            
            admin_user.generate_data()
            new_user = User()
            new_user.generate_data()            
            user_group_a = Helper.generate_random_user_group_name()
            user_group_b = Helper.generate_random_user_group_name()
            # create device group
            device_group_name = Helper.generate_random_device_group_name()
            new_user.device_group = device_group_name
            admin_user.device_group = device_group_name
            TestCondition.create_device_group(device_group_name)
            
            TestCondition.create_advanced_organization_admins(self._driver, [admin_user])
            TestCondition.create_advanced_normal_users(self._driver, [new_user], False)
            TestCondition.create_user_group(user_group_a, [new_user])
            TestCondition.create_user_group(user_group_b, [new_user])
                               
            #steps
            user_page = HomePage(self._driver).open_and_goto_login_page()\
                .login_as_unwatched_video_user(admin_user.email_address, admin_user.password)\
                .goto_admin_dashboard_page_by_menu_item()\
                .goto_users_tab()\
                .goto_user_group_detail_page(user_group_a).remove_user_from_this_group(new_user)\
                .goto_users_tab().goto_user_group_detail_page(user_group_b)
            
            #verify point
            self.assertTrue(user_page.is_user_existed(new_user.get_displayed_name()),
                            "Assertion Error: User is NOT existed!" )
        finally:
            #post-condition:
            TestCondition.delete_advanced_users([admin_user, new_user])
            TestCondition.delete_user_groups([user_group_a, user_group_b])
            TestCondition.delete_device_groups([device_group_name])
            