from common.constant import Constant
from common.helper import Helper
from data.dataobjects.user import User
from pages.suitable_tech.user.home_page import HomePage
from core.utilities.test_condition import TestCondition
from test.testbase import TestBase


class DeviceGroupAccess_Test(TestBase):
        
    def test_c11079_setup_device_group_users_authentication_by(self):
        """
        @author: Thanh Le
        @date: 08/27/2016
        @summary: Setup Device Group Users Authentication by
        @precondition:           
            Create 2 groups have differences Auth method:
                "LGVN Device Group - Auth ST" (enable "Suitable Tech Auth" only and added a BeamA) 
                "LGVN Device Group - Auth Google" (enable "Google Auth" only and added a BeamB)
            Create 2 users has differences Auth logging 
                UserA = "lgvnsuitabletech1@gmail.com" (Only log by Suitable Tech method) 
                UserB = "lgvnsuitabletech@gmail.com" (Only log by GSSO method)
        @steps:
            1) Log in to Suitabletech with an org admin
            2) Add 2 users in pre-condition to both Device Groups
            3) Log in with each user and check the device they can see
        
        @expected:          
            3). UserA only sees the BeamA and does not see the BeamB.
            3). UserB only sees the BeamB and does not see the BeamA.
        """
        try:
            # precondition:
            untest_device_group = Helper.generate_random_device_group_name()
            organization = Constant.AdvancedOrgName
            TestCondition.create_device_group(device_group_name=untest_device_group, device_array=[], organization_name=organization)
            
            device_group_auth_st = Helper.generate_random_device_group_name()
            device_group_auth_google = Helper.generate_random_device_group_name()
            gsso_user = User()  # Do NOT delete this GSSO user at clean up
            gsso_user.generate_non_gsso_user_data()
            TestCondition.create_advanced_gsso_user(self._driver, gsso_user)
            
            non_gsso_user = User()
            non_gsso_user.generate_data()
            non_gsso_user.device_group = untest_device_group
            non_gsso_user.organization = organization
            
            admin_user = User()                                            
            admin_user.generate_data()
            admin_user.device_group = untest_device_group
            admin_user.organization = organization
            
            TestCondition.create_device_group(device_group_auth_st, [Constant.BeamPlusMock1Name])
            TestCondition.create_device_group(device_group_auth_google, [Constant.BeamPlusMock4Name])
            TestCondition.create_advanced_organization_admins(self._driver, [admin_user])
            TestCondition.create_advanced_normal_users(self._driver, [non_gsso_user])
            
            # steps:
            your_beam_account_page = HomePage(self._driver).open_and_goto_login_page()\
                .login_as_unwatched_video_user(admin_user.email_address, admin_user.password)\
                .goto_admin_dashboard_page_by_menu_item()\
                .goto_settings_tab_of_a_device_group(device_group_auth_st)\
                    .toggle_left_off_all_authentication_methods(False)\
                    .toggle_left_off_google_methods(False)\
                    .save_changes()\
                .goto_members_tab()\
                    .add_user_to_device_group(gsso_user)\
                    .add_user_to_device_group(non_gsso_user)\
                .goto_settings_tab_of_a_device_group(device_group_auth_google)\
                    .toggle_left_off_all_authentication_methods(False)\
                    .toggle_left_off_suitable_technologies_methods(False).save_changes()\
                .goto_members_tab()\
                    .add_user_to_device_group(gsso_user)\
                    .add_user_to_device_group(non_gsso_user)\
                    .logout()\
                .goto_login_page()\
                    .login_as_unwatched_video_user(non_gsso_user.email_address, non_gsso_user.password)\
                .goto_account_settings_page_by_menu_item()\
                .goto_normal_user_home()
            
            # verify point:
            self.assertTrue(your_beam_account_page.is_device_title_displayed(Constant.BeamPlusMock1Name),
                            "Assertion Error: {} is NOT displayed!".format(Constant.BeamPlusMock1Name))
            self.assertFalse(your_beam_account_page.is_device_title_displayed(Constant.BeamPlusMock4Name),
                            "Assertion Error: {} is NOT displayed!".format(Constant.BeamPlusMock4Name))
            
            your_beam_account_page = your_beam_account_page.logout()\
                .goto_login_page()\
                    .login_with_google(gsso_user.email_address)\
                .goto_account_settings_page_by_menu_item()\
                .goto_normal_user_home()
            
            # verify point:
            self.assertTrue(your_beam_account_page.is_device_title_displayed(Constant.BeamPlusMock4Name),
                            "Assertion Error: {} is NOT displayed!".format(Constant.BeamPlusMock4Name))
            self.assertFalse(your_beam_account_page.is_device_title_displayed(Constant.BeamPlusMock1Name),
                            "Assertion Error: {} is NOT displayed!".format(Constant.BeamPlusMock1Name))
                               
        finally:
            TestCondition.delete_advanced_users([non_gsso_user, admin_user])
            TestCondition.delete_device_groups([device_group_auth_st, device_group_auth_google, untest_device_group], organization)
