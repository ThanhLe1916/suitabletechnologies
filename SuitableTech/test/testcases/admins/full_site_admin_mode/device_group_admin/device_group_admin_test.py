from common.constant import Constant
from common.helper import Helper
from data.dataobjects.user import User
from pages.suitable_tech.user.home_page import HomePage
from common.application_constants import ApplicationConst
from core.utilities.test_condition import TestCondition
from test.testbase import TestBase
        
class DeviceGroupAdmin_Test(TestBase):
    
    def test_c11227_grant_administrators_for_a_specific_device_group_2_X(self):
        """
        @author: Duy.Nguyen
        @date: 7/27/2016
        @summary: Grant Administrators for a specific device group [2.X]
        @precondition: 
        Use a Device Group admin account (cannot be an org admin account). Device group admins can be assigned by going to "manage your beams", beams, settings, administrators. 
        @steps:        
            1) Go to the "Manage your Beams" dashboard and click on the "Beams" tab
            2) select a device group
            3) Go to the "Settings" Tab
            4) Click the "add administrators" button to add users as administrators of the device group
            5) Click the save changes button at the top
            
        @expected:
            Login with the device group admin account and verify that the user account that was promoted is now able to adminster the new device group by checking the "adminstrates group" string in their user profile.         
            Verify that the newly added device group admin can invite other users to the device group they have been assigned to manage.
        """
        try:
            # precondition:
            device_group_name = Helper.generate_random_device_group_name()
            org_admin = User()
            org_admin.generate_data()
            
            device_group_admin = User()
            device_group_admin.generate_data()
            device_group_admin.device_group = device_group_name
            
            normal_user = User()
            normal_user.generate_data()
            normal_user.device_group = device_group_name
            
            sub_user = User()
            sub_user.generate_data()
            sub_user.device_group = device_group_name
            
            TestCondition.create_advanced_organization_admins(self._driver, [org_admin])
            TestCondition.create_device_group(device_group_admin.device_group)
            TestCondition.create_advanced_device_group_admins(self._driver, [device_group_admin], org_admin.email_address, org_admin.password, True)
            TestCondition.create_advanced_normal_users(self._driver, [normal_user])
             
            # steps:
            user_detail_page = HomePage(self._driver).open()\
                .goto_login_page()\
                    .login_as_unwatched_video_user(device_group_admin.email_address, device_group_admin.password)\
                .goto_admin_dashboard_page_by_menu_item()\
                .goto_settings_tab_of_a_device_group(device_group_name)\
                    .add_administrator(normal_user)\
                .goto_users_tab().goto_user_detail_page(normal_user)
            
            # verify point:
            self.assertEqual(user_detail_page.get_user_administers_groups(), [device_group_name], "Assertion Error: The Administers Group information is not correct")
            
            user_detail_page = user_detail_page.logout_and_login_again_as_unwatched_video_user(normal_user.email_address, normal_user.password)\
                .goto_admin_dashboard_page_by_menu_item()\
                    .invite_new_user(sub_user)\
                .goto_users_tab().goto_user_detail_page(sub_user)
            
            # verify point:
            self.assertEqual(user_detail_page.get_device_groups(), [device_group_name], "Assertion Error: The Device Groups information is not correct")
            
        finally:
            TestCondition.delete_advanced_users([org_admin, device_group_admin, normal_user, sub_user])
            TestCondition.delete_device_groups([device_group_name])
        
    
    def test_c11591_device_group_admin_add_an_existing_user_as_another_device_group_admin_2_x(self):
        """
        @author: Duy.Nguyen
        @date: 8/3/2016
        @summary: Device Group Admin add an existing user as another Device Group Admin [2.X]
        @precondition: 
            Add UserA as device group admin of DeviceGroupA
            Add a Suitabletech User (UserB)
        @steps:    
            1) Login to Suitabletech.com as a device group admin (DeviceGroupAdminA) and select "Manage Your Beams" from the user dropdown menu
            2) Click on the "Beams" tab under the "Manage your Beams" dashboard
            3) Select a device group (DeviceGroupA) and select "Settings" tab
            4) Under the "Administrators" title in the "Settings" tab, click on the "Add Administrators" box and select one of the existing users in the organization (UserB) then click "Add Selected Users" button
            5) Click the blue button "Save Changes" towards the top-left side of the browser
            6) Go to the "Users" tab under the "Manage your Beams" dashboard
            7) Click on the desired user that you are checking device admin privileges (UserB)
        @expected:
            (5) Verify that a green popup occurs in the top right saying the "Changes were saved successfully". 
            (7). Verify that the selected usergroup (UserGroupB) is displayed in "Administrates Groups" field of UserB's detail page. 
        """        
        try:
            # pre-condition
            device_group_name = Helper.generate_random_device_group_name()
            admin_user = User()
            admin_user.generate_data()
            admin_user.device_group = device_group_name
            
            normal_user = User()
            normal_user.generate_data()
            normal_user.device_group = device_group_name
            
            TestCondition.create_device_group(device_group_name)
            TestCondition.create_advanced_normal_users(self._driver, [normal_user])
            TestCondition.create_advanced_device_group_admins(self._driver, [admin_user])
            
            # steps:
            beam_setting_page = HomePage(self._driver).open_and_goto_login_page()\
                .login(admin_user.email_address, admin_user.password)\
                .goto_admin_dashboard_page_by_menu_item()\
                .goto_settings_tab_of_a_device_group(device_group_name)\
                .add_administrator(normal_user, False)
            
            # verify point:
            actual_msg_success = beam_setting_page.get_msg_success()
            
            self.assertEqual(ApplicationConst.INFO_MSG_SAVE_DEVICE_GROUP_SETTING_SUCCESSFUL, actual_msg_success,
                             "Assertion Error: There is no message with following content display")
            
            user_detail_page = beam_setting_page.goto_users_tab().goto_user_detail_page(normal_user)
            
            # verify point:
            self.assertEqual(user_detail_page.get_user_administers_groups(), [admin_user.device_group],
                              "Assertion Error: The Administers Group information is not correct")
            
        finally:
            TestCondition.delete_advanced_users([admin_user, normal_user])
            TestCondition.delete_device_groups([device_group_name])
            
    
    def test_c11593_device_group_admin_remove_self_from_admin_2_x(self):
        """
        @author: tham.nguyen
        @date: 7/25/2016
        @summary: Device Group Admin remove self from admin [2.X]
        @precondition: Use the following Admin Test Organization - Accounts used for Suitable Tech Manual/Automation Testing:
            http://wiki.suitabletech.com/display/SUIT/QA+Test+Accounts#QATestAccounts-TestOrgs
            A device group admin account.
        @steps:        
            1) Login to suitabletech.com and navigate to the "Manage your Beams" dashboard and go to the "Beams" Tab
            2) Then click on a "Device Group" and go to the "Settings" Tab
            3) Go to the "Administrators" box and try to remove yourself
             
        @expected:
            1) The "x" button (shown in pic above) is missing for your own user-ID in device group settings.
        """  
        try:
            # pre-conditions:
            organization_name = Constant.AdvancedOrgName
            devices = [Constant.BeamPlusName, Constant.BeamProNameUTF8]
            device_group_name = Helper.generate_random_device_group_name(5)
            TestCondition.create_device_group(device_group_name, devices, organization_name)
            
            device_group_admin = User()
            device_group_admin.generate_advanced_device_group_admin_data()
            device_group_admin.device_group = device_group_name
            
            TestCondition.create_advanced_device_group_admins(driver=self._driver, user_array=[device_group_admin])
            
            # steps
            admin_beams_settings_page = HomePage(self._driver).open_and_goto_login_page()\
                .login_as_unwatched_video_user(device_group_admin.email_address, device_group_admin.password)\
                .goto_admin_dashboard_page_by_menu_item()\
                .goto_settings_tab_of_a_device_group(device_group_name)
            
            # verify points
            self.assertFalse(admin_beams_settings_page.is_admin_removable(device_group_admin.get_displayed_name()), "Assertion Error: The device group admin '{}' can remove himself in admin beams setting page.".format(device_group_admin.get_displayed_name()))
        
        finally:
            # post-condition
            TestCondition.delete_advanced_users(user_array=[device_group_admin])
            TestCondition.delete_device_groups([device_group_name], organization_name)
          
        
    def test_c11590_device_group_admin_for_multiple_orgs_sees_a_drop_down_list_to_select_site_1_x(self):
        """
        @author: Quang Tran
        @date: 7/29/2016
        @summary: Device Group Admin for multiple Orgs sees a drop-down list to select site [1.X]
        @precondition:
            Use the following Admin Test Organization - Accounts used for Suitable Tech Manual/Automation Testing:
            http://wiki.suitabletech.com/display/SUIT/QA+Test+Accounts#QATestAccounts-TestOrgs
            
            1) You will need admin access to two or more device groups in two different orgs

        @steps:        
            1) Use a Device Group admin account (cannot be an org admin account)
            2) Go to the "Manage your Beams" dashboard
            3) In the upper right section of the admin page, you should see the organization name.
            4) Toggle the org name to a different organization 
             
        @expected:
            It turns into a drop down menu that allows you to change orgs;
            The web page should be directed to the new org when a different selection is made.
        """
        try:
            # precondition: create a new Device Group admin account
            device_group_name = Helper.generate_random_device_group_name()                    
            device_group_admin = User()                            
            device_group_admin.generate_data()
            TestCondition.create_advanced_device_group_admin_on_multi_organization(self._driver, device_group_admin,device_group_name)
            
            # steps
            admin_dashboard_page = HomePage(self._driver).open().goto_login_page()\
                .login_as_unwatched_video_user(device_group_admin.email_address, device_group_admin.password)\
                .goto_admin_dashboard_page_by_menu_item()
            
            # verify points
            is_organization_dropdown_displayed = admin_dashboard_page.is_organization_dropdown_displayed()
            self.assertTrue(is_organization_dropdown_displayed,
                            "Assertion Error: Organization dropdown list is not displayed")
            
            page_title = admin_dashboard_page.switch_to_advanced_organization(Constant.AdvancedOrgName)\
                .switch_to_advanced_organization(Constant.AdvancedOrgName_2).get_page_title()
            
            # verify points
            is_page_displayed = Constant.AdvancedOrgName_2 in page_title
            self.assertTrue(is_page_displayed, "Assertion Error: Admin page is not switched to selected organization")
        finally:
            # postcondition
            TestCondition.delete_advanced_users([device_group_admin], Constant.AdvancedOrgName)
            TestCondition.delete_advanced_users([device_group_admin], Constant.AdvancedOrgName_2)
        
        
    def test_c11628_change_usergroup_icon_2_x(self):    
        """
        @author: Quang Tran
        @date: 8/18/2016
        @summary: Change UserGroup Icon [2.X] 
        @precondition:
            Login to Suitabletech.com and navigate to the Manage Your Beams dashboard with a Device Group Admin user account

        @steps:        
            1) Go to the "Users" tab under the "Manage your Beams" dashboard
            2) Select a user group
             
        @expected:
            Verify that the change image button is disabled for not allowing device group admin changing icon image.
        """
        
        try:
            # precondition
            user_group_name = Helper.generate_random_user_group_name()
            device_group_name = Helper.generate_random_device_group_name()                          
            device_group_admin = User()                                            
            device_group_admin.generate_data()                                
            device_group_admin.device_group = device_group_name                        
                                                        
            TestCondition.create_device_group(device_group_name)                    
            TestCondition.create_advanced_device_group_admins(self._driver, [device_group_admin])
            TestCondition.create_user_group(user_group_name)

            welcome_page = HomePage(self._driver).open()\
                .goto_login_page().login_as_unwatched_video_user(device_group_admin.email_address, device_group_admin.password)         
            
            # steps
            user_group_detail_page = welcome_page.goto_admin_dashboard_page_by_menu_item()\
                .goto_users_tab()\
                .goto_user_group_detail_page(user_group_name)
            
            # verify point: compare images by pixels
            is_change_icon_link_visible = user_group_detail_page.is_change_icon_link_displayed()
            self.assertFalse(is_change_icon_link_visible, "Assertion Error: The Device Group Admin can see the 'change icon' link.")
            
        finally:
            # clean up
            TestCondition.delete_advanced_users([device_group_admin])
            TestCondition.delete_device_groups([device_group_name])
            TestCondition.delete_user_groups([user_group_name])
            
