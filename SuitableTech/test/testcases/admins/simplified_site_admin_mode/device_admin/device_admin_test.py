from common.constant import Constant
from data.dataobjects.user import User
from pages.suitable_tech.user.home_page import HomePage
from common.application_constants import ApplicationConst
from core.utilities.test_condition import TestCondition
from test.testbase import TestBase

class DeviceGroupAdminTest(TestBase):
    
    def test_c11644_grant_administrators_for_a_specific_device_group_2_x(self):
        """
        @author: Thanh.Le
        @date: 8/02/2016
        @summary: Grant Administrators for a specific device group [2.X]
        @preconditions:
            Use the following Admin Test Organization - Accounts used for Suitable Tech Manual/Automation Testing:
            http://wiki.suitabletech.com/display/SUIT/QA+Test+Accounts#QATestAccounts-TestOrgs
            Ref: NCA-5808 - Site Admin needs new, intermediate user roles implemented

            To get into Simplified Admin Mode: 
            1) Make sure that your organization only has Beam+'s
            2) Refresh the browser page, you may need to switch orgs and come back in order for the full "Manage your Beams" to fully refresh            
            Make sure to login as a Device Group Admin, not an Org Admin
            
            To make a "Device Admin" ~ "Device Group Admin" in simplified "Manage your Beams"
            1) Log onto the simplified "Manage your Beams" as an org admin            
            2) Click the "Manage" button below each of the Beam+'s you want another user to manage            
            3) If the user is not already invited (i.e. listed at bottom of page), fill out the "Invite a person to use this Beam" field            
            5) Verify that the new user gets added    
            --> inform message + new record        
            --> email confirmation            
            5) Then check the "Can Manage" box
            6) logout the org admin
        @steps
            1) Go to the simplified "Manage your Beams" dashboard and login as the recently added device admin
            2) Click the "Manage" button below each of the Beam+'s you want another user to manage
            3) If the user is not already invited (i.e. listed at bottom of page), fill out the "Invite a person to use this Beam" field
            4) Then check the "Can Manage" box
            
            --> These are the same instructions as above except now you are using a "device" admin. An Org admin can only invite you to "manage" new devices, but once you are managing a device you can invite other users to said device.

        @expected result:
            Precondition: 
            - verify that the device admin can manage all of the beam+'s that the org admin allowed privileges for; 
            - verify that the device admin can only see the beam+'s that the org admin gave priviledges for
            Test case: verify that the device admin can:
            1. invite new users to the beam+ device
            2. can add new device admins (i.e. can check the "Can Manage" box)
        """
        try:
            # preconditions:
            
            test_device_name = Constant.BeamPlusMock2Name
            test_organization=Constant.SimplifiedOrgName
            
            test_device_admin = User()  
            test_device_admin.generate_simplified_device_admin_data()
            
            simplified_dev_admin = User()
            simplified_dev_admin.generate_simplified_device_admin_data()
            simplified_dev_admin.device_group = test_device_name
            simplified_dev_admin.organization = test_organization
            
            TestCondition.create_simplified_device_admin(
                                driver=self._driver, 
                                user_array=[simplified_dev_admin], 
                                device_name=simplified_dev_admin.device_group,
                                organization=simplified_dev_admin.organization)
            
            # steps
            simplified_dashboard_page = HomePage(self._driver).open()\
                .goto_login_page()\
                    .login_as_unwatched_video_user(simplified_dev_admin.email_address, simplified_dev_admin.password)\
                .goto_simplified_dashboard_page_by_menu_item()
            
            self.assertTrue(simplified_dashboard_page.can_manage_device(test_device_name), 
                    "Assertion Error: The current device admin is not allowed to manage the beam '{}'".format(test_device_name))
            
            self.assertEqual(simplified_dashboard_page.get_number_of_devices_displayed(), 1, 
                    "Assertion Error: The current device admin can see the beam+'s that the org admin does not gave priviledges for.")
            
                
            simplified_beam_detail_page = simplified_dashboard_page.goto_manage_beam_page(test_device_name).add_user(test_device_admin)
            
            # verify point
            self.assertTrue(simplified_beam_detail_page.is_user_added(test_device_admin), 
                    "Assertion Error: The current device admin cannot invite new user to the device {}".format(test_device_name))                                
            
            simplified_beam_detail_page.set_user_can_manage(test_device_admin)
                    
            # verify point
            self.assertTrue(simplified_beam_detail_page.is_user_can_manage_checkbox_selected(test_device_admin),
                    "Assertion Error: The current device admin cannot grant administrator for other user.")
            
        finally:        
            # clean-up
            TestCondition.delete_simplified_users(
                    self._driver, 
                    user_array=[test_device_admin, simplified_dev_admin], 
                    device_name=test_device_name)
                  
      
    def test_c11645_device_group_admin_for_multiple_orgs_sees_a_drop_down_list_to_select_site_1_x(self):
        """
        @author: Thanh.Le
        @date: 8/02/2016
        @summary: Device Group Admin for multiple Orgs sees a drop-down list to select site [1.X]
        @preconditions:
            Use the following Admin Test Organization - Accounts used for Suitable Tech Manual/Automation Testing:
            http://wiki.suitabletech.com/display/SUIT/QA+Test+Accounts#QATestAccounts-TestOrgs

            1) Need to have an account that has access to two different orgs
            2) one of the orgs the account needs to be a "device admin" in the simplified "Manage your Beams" dashboard (has a can manage checkbox next to user profile). 
            3) In the other org the account needs to be a "device group admin" in the full "Manage your Beams".
        @steps
            1) Login as a device admin in the simplified "Manage your Beams"
            2) In the upper right hand corner of the screen with the org name, if you have access to more than one org (see preconditions), a drop down menu will appear

        @expected Result
            1) Verify that if a "device admin" in the simplified "Manage your Beams" dashboard has access to more than one org, the dropdown menu appears.
        """
        try:
            # steps
            simplied_dashboard_page = HomePage(self._driver).open().goto_login_page()\
                .login(Constant.MixedMultiOrgAdminEmail, Constant.DefaultPassword)\
                .goto_simplified_dashboard_page()
                    
            # verify point
            self.assertTrue(simplied_dashboard_page.is_organization_dropdown_displayed(),
                            "Assertion Error: Organization dropdown is NOT displayed")
        
        finally:
            pass        
    
      
    def test_c11647_device_group_admin_removes_an_existing_device_group_admin_2_x(self):
        """
        @author: khoi.ngo
        @date: 8/4/2016
        @summary: Device Group Admin Removes an existing Device Group Admin [2.X]
        @precondition:
            Follow the steps from the previous test case to set up 2 device group admins
        @steps:
            1. Login to the simplified "Manage your Beams" as a device admin 
            2. click the "manage" box under the device image icon
            3. UNCHECK the box next to the desired user under the "Can manage" field
            4. click "Remove Person" button.
            5. Verify removal toast
            
        @expected:
            the unchecked user is no longer a device admin (i.e. "can't manage")
        """
        try:
            # precondition
            test_device_name = Constant.BeamPlusMock2Name
            test_organization=Constant.SimplifiedOrgName
            
            removed_device_admin = User()  
            removed_device_admin.generate_simplified_device_admin_data()
            removed_device_admin.device_group = test_device_name
            removed_device_admin.organization = test_organization
            
            simplified_dev_admin = User()
            simplified_dev_admin.generate_simplified_device_admin_data()
            simplified_dev_admin.device_group = test_device_name
            simplified_dev_admin.organization = test_organization
            
            TestCondition.create_simplified_device_admin(
                                driver=self._driver, 
                                user_array=[simplified_dev_admin, removed_device_admin], 
                                device_name=simplified_dev_admin.device_group,
                                organization=simplified_dev_admin.organization)
            
            # steps
            simplied_dashboard_page = HomePage(self._driver).open()\
                .goto_login_page()\
                    .login_as_unwatched_video_user(simplified_dev_admin.email_address, simplified_dev_admin.password)\
                .goto_simplified_dashboard_page_by_menu_item()\
                .goto_manage_beam_page(test_device_name)\
                    .set_user_can_manage(removed_device_admin, False)\
                    .remove_user(removed_device_admin)
                
            # verify point
            self.assertTrue(simplied_dashboard_page.is_user_not_existed(removed_device_admin),
                    "Assertion Error: User still exists in this device.")
            
            home_page = simplied_dashboard_page.logout().goto_login_page()\
                .login_as_unwatched_video_user(removed_device_admin.email_address, removed_device_admin.password)
            
            self.assertTrue(home_page.is_dropdownlist_item_unexisted(ApplicationConst.LBL_MANAGE_YOUR_BEAMS),
                    "Assertion Error: The removed admin user still be able to manage the device.")
            
        finally:
            # post-condition            
            TestCondition.delete_simplified_users(self._driver, [removed_device_admin, simplified_dev_admin], test_device_name)
        
       
    def test_c11648_device_group_admin_cannot_remove_self_from_admin_2_x(self):
        """
        @author: Thanh.Le
        @date: 8/02/2016
        @summary: Device Group Admin cannot remove self from admin [2.X]
        @preconditions:
            Use the following Admin Test Organization - Accounts used for Suitable Tech Manual/Automation Testing:
            http://wiki.suitabletech.com/display/SUIT/QA+Test+Accounts#QATestAccounts-TestOrgs
        @steps
            1) Login as a device admin in simplified "Manage your Beams"
            2) Click the "Manage" button under the device image icon
        @expected result:
            1) Verify that a grey "no-symbol" icon blocks you from the "Can't Manage" box
        """
        try:
            # preconditions
            test_device_name = Constant.BeamPlusMock2Name
            test_organization=Constant.SimplifiedOrgName
            
            simplified_dev_admin = User()
            simplified_dev_admin.generate_simplified_device_admin_data()
            simplified_dev_admin.device_group = test_device_name
            simplified_dev_admin.organization = test_organization
            
            TestCondition.create_simplified_device_admin(
                                driver=self._driver, 
                                user_array=[simplified_dev_admin], 
                                device_name=simplified_dev_admin.device_group,
                                organization=simplified_dev_admin.organization)
            
            # steps
            simplified_beam_detail_page = HomePage(self._driver).open()\
                .goto_login_page()\
                    .login_as_unwatched_video_user(simplified_dev_admin.email_address, simplified_dev_admin.password)\
                .goto_simplified_dashboard_page_by_menu_item()\
                .goto_manage_beam_page(test_device_name)       
            
            # verify point
            self.assertTrue(simplified_beam_detail_page.is_user_can_manage_checkbox_disabled(simplified_dev_admin),
                            "Assertion Error: User checkbox is NOT disabled!")
            
        finally:
            TestCondition.delete_simplified_users(self._driver, [simplified_dev_admin], test_device_name)




