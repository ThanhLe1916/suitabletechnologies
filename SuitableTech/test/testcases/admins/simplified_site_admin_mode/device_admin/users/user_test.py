from datetime import datetime
from common.constant import Constant
from common.helper import EmailDetailHelper, Helper
from core.utilities.gmail_utility import GmailUtility
from data.dataobjects.user import User
from pages.suitable_tech.user.home_page import HomePage
from core.utilities.test_condition import TestCondition
from test.testbase import TestBase


class User_Test(TestBase):
    
    def test_c11649_add_new_user_that_is_not_currently_in_the_org_1_X(self):
        """
        @author: khoi.ngo
        @date: 8/17/2016
        @summary: Add a user that already exists in another organization [1.X]
        @precondition:Use the following Admin Test Organization - Accounts used for Suitable Tech Manual/Automation Testing:
                http://wiki.suitabletech.com/display/SUIT/QA+Test+Accounts#QATestAccounts-TestOrgs
        @steps:
            1. Log onto the simplified "Manage your Beams" as an org admin
            2. Click the "Manage" button below each of the Beam+'s you want another user to manage
            3. If the user is not already invited (i.e. listed at bottom of page),
                fill out the "Invite a person to use this Beam" field
            4. Verify that the new user gets added
            5. email confirmation
        @expected:
            New user receives invitation and has appropriate access to a device group.
        @review:
        """
        
        try:
            # precondition
            user_info = User()
            user_info.generate_simplified_normal_user_data()
            
            test_device_name = Constant.BeamPlusMock2Name
            
            simplified_org_admin = User()
            simplified_org_admin.generate_simplified_org_admin_data()
            TestCondition.create_simplified_organization_admin(driver=self._driver, user=simplified_org_admin)

            # steps
            simplified_beam_detail_page = HomePage(self._driver).open()\
                .goto_login_page()\
                    .login(simplified_org_admin.email_address, simplified_org_admin.password)\
                .goto_simplified_dashboard_page()\
                .goto_manage_beam_page(test_device_name)
            
            if simplified_beam_detail_page.is_user_not_existed(user_info):
                simplified_beam_detail_page.add_user(user_info)
                self.assertTrue(simplified_beam_detail_page.is_user_added(user_info), 
                                "Assertion Error: User does not exist in the device user list.")
                # verify point
                email_subject = EmailDetailHelper.generate_welcome_email(user_info, simplified_org_admin.get_displayed_name()).subject
                actual_email_message = GmailUtility.get_messages(
                                            email_subject, None, 
                                            simplified_org_admin.email_address, user_info.email_address, datetime.now())
            
            self.assertEqual(len(actual_email_message), 1, "Assertion Error: The number of confirmation email is not correct")  
        finally:
            # clean up
            TestCondition.delete_simplified_users(driver=self._driver, user_array=[user_info], device_name=user_info.device_group, organization=user_info.organization)
    
    
    def test_c11651_add_a_user_that_already_exists_in_another_organization_1_X(self):
        """
        @author: khoi.ngo
        @date: 8/17/2016
        @summary: Add a user that already exists in another organization [1.X]
        @precondition: Make sure admin account has Can Manage access to two org's 
        @steps:
            1. Log onto the simplified "Manage your Beams" as an org admin
            2. Click the "Manage" button below each of the Beam+'s you want another user to manage
            3. If the user is not already invited (i.e. listed at bottom of page), fill out the "Invite a person to use this Beam" field
            4. Verify that the new user gets added
            5. email confirmation
        @expected:
            The same user record should be added to the new organization as well. The user will now be in both.
            Verify that the user is now in two orgs.
            1. Login as a device admin in the simplified "Manage your Beams"
            2. In the upper right hand corner of the screen with the org name, 
                if you have access to more than one org (see preconditions), a drop down menu will appear
        @review:
        """
        try:
            # precondition
            device_group = Helper.generate_random_device_group_name()
            
            test_device_name = Constant.BeamPlusMock2Name
            
            adv_organization = Constant.AdvancedOrgName
            smp_organization = Constant.SimplifiedOrgName
            
            new_user = User()
            new_user.generate_advanced_normal_user_data()
            new_user.device_group = device_group
            new_user.organization = adv_organization
            
            mixed_org_admin = User()
            mixed_org_admin.generate_mixed_org_admin_data()
            
            TestCondition.create_device_group(device_group_name=device_group, device_array=[], organization_name=adv_organization)
            TestCondition.create_advanced_normal_users(self._driver, [new_user], True)
            TestCondition.create_mixed_organization_admin(driver=self._driver, user=mixed_org_admin)

            # steps          
            simplified_beam_detail = HomePage(self._driver).open()\
                .goto_login_page()\
                    .login(mixed_org_admin.email_address, mixed_org_admin.password)\
                .goto_simplified_dashboard_page()\
                    .switch_to_simplified_organization(smp_organization)\
                .goto_manage_beam_page(test_device_name)
            
#             if simplified_beam_detail.is_user_not_existed(new_user):
            simplified_beam_detail.add_user(new_user)
            new_user.organization = smp_organization
            
            # verify points
            # check user added in the second org
            self.assertTrue(simplified_beam_detail.is_user_added(new_user), 
                            "Assertion Error: User is not exists in the second organization")
            
            # check email arrive
            email_subject = EmailDetailHelper.generate_welcome_existing_email(new_user, mixed_org_admin.get_displayed_name()).subject            
            actual_email_message = GmailUtility.get_messages(email_subject, None, mixed_org_admin.email_address, new_user.email_address, datetime.now())
            
            self.assertEqual(len(actual_email_message), 1, "Assertion Error: The number of confirmation email is not correct")

            # check user added in the first org
            is_user_exists_in_the_first_org = simplified_beam_detail.switch_to_advanced_organization(adv_organization)\
                                                .goto_users_tab()\
                                                    .is_user_existed(new_user.email_address, new_user.get_displayed_name())
            
            self.assertTrue(is_user_exists_in_the_first_org, 
                            "Assertion Error: User is not exists in the first organization")
            
        finally:
            # post-condition
            TestCondition.delete_advanced_users([new_user])
            TestCondition.delete_simplified_users(driver=self._driver, user_array=[new_user], device_name=new_user.device_group, organization=new_user.organization)
            TestCondition.delete_device_groups([device_group], adv_organization)
            

    def test_c11653_remove_user_from_device_2_x(self):
        """
        @author: khoi.ngo
        @date: 8/8/2016
        @summary: Remove User from Device [2.X]
        @precondition: Use the following Admin Test Organization - Accounts used for Suitable Tech Manual/Automation Testing:
                http://wiki.suitabletech.com/display/SUIT/QA+Test+Accounts#QATestAccounts-TestOrgs
        @steps:
            1. Login as the Device Admin navigate to a Device you are authorized to "Manage"
            2. Select the red "Remove Person" button
            3. Click "Ok" on the toast
        @expected:
            Verify that the removed user no longer has access to that device
            verify that the removed user no longer has access to that device 
                (by login to the removed user and verify that the Beam+ is not displayed)
        """
        try:
            # precondition
            test_device_name = Constant.BeamPlusMock2Name
            test_organization=Constant.SimplifiedOrgName
            
            new_user = User()
            new_user.generate_simplified_normal_user_data()            
            new_user.device_group = test_device_name
            
            simplified_dev_admin = User()
            simplified_dev_admin.generate_simplified_device_admin_data()
            simplified_dev_admin.device_group = test_device_name
            simplified_dev_admin.organization = test_organization
            
            TestCondition.create_simplified_normal_users(
                self._driver, 
                user_array=[new_user], 
                device_name=new_user.device_group,
                organization=new_user.organization)
            
            TestCondition.create_simplified_device_admin(
                                driver=self._driver, 
                                user_array=[simplified_dev_admin], 
                                device_name=simplified_dev_admin.device_group,
                                organization=simplified_dev_admin.organization)
            
            # steps
            
            beam_detail_page = HomePage(self._driver).open()\
                .goto_login_page()\
                    .login_as_unwatched_video_user(simplified_dev_admin.email_address, simplified_dev_admin.password)\
                .goto_simplified_dashboard_page_by_menu_item()\
                .goto_manage_beam_page(test_device_name)\
                    .remove_user(new_user)
            
            # verify points       
            self.assertTrue(beam_detail_page.is_user_not_existed(new_user), "Assertion Error: User has access to this device")
            
            # verify points
            user_page = beam_detail_page.logout()\
                .goto_login_page()\
                    .login_as_unwatched_video_user(new_user.email_address, new_user.password)\
                .goto_account_settings_page_by_menu_item()\
                .goto_normal_user_home()
            
            self.assertTrue(user_page.is_device_title_disappeared(test_device_name), 
                            "Assertion Error: The beam " + test_device_name + "is still displayed")
        finally:
            TestCondition.delete_simplified_users(
                self._driver, 
                user_array=[simplified_dev_admin,new_user], 
                device_name=test_device_name)
            
    