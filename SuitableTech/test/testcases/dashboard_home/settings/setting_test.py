import re
from common.helper import Helper
from pages.suitable_tech.user.home_page import HomePage
from data.dataobjects.user import User
from core.utilities.gmail_utility import GmailUtility
from common.helper import EmailDetailHelper
from common.constant import Constant
from common.application_constants import ApplicationConst
from core.suitabletechapis.device_group_api import DeviceGroupAPI
from core.utilities.test_condition import TestCondition
from time import sleep
from test.testbase import TestBase


class SettingTest(TestBase):
    
    def test_c11080_create_api_keys_and_set_email_notifications_contacts(self):
        """
        @author: Khoi Ngo
        @date: 8/9/2016
        @summary: Create API Keys and Set Email Notifications Contacts
        @Precondtions: There is an org admin.
        @steps:
            1. Login to Suitabletech.com as an org admin and select Manage Your Beams from the user dropdown menu
            2. Select the "Settings" icon on far left of screen (looks like a cog)
            3. Enter a name for the new Key in the field provided
            4. Click on "Create New API key" button
        @expected:
            1. Your new API Key entry will be created with your Key name entered.
            2. The message "Your new API key is: '<API key>'. Please copy this key (without surrounding quotes)
                 and store it somewhere safe. Once you have this page, you will no longer have access to the secret portion of this key.\
                 For more information on how to use API keys, please check out the Web API Documentation Page" appears.
        """
        try:
            # preconditions
            org_admin = User()                                            
            org_admin.generate_data()            
            
            TestCondition.create_advanced_organization_admins(self._driver, [org_admin])
            
            # steps
            key_name = Helper.generate_random_string()
            
            setting_page = HomePage(self._driver).open_and_goto_login_page()\
                .login_as_unwatched_video_user(org_admin.email_address, org_admin.password)\
                .goto_admin_dashboard_page_by_menu_item()\
                .goto_org_setting_page()\
                .create_api_key(key_name)
                
            text_msg_alert = setting_page.get_text_msg()
            
            # verify points
            self.assertTrue(setting_page.is_api_key_existed(key_name), "Assertion Error: API is not created.")
            
            # check text exists
            expected_msg_pattern = [ApplicationConst.INFO_MSG_NEW_API_NOTICE, ApplicationConst.INFO_MSG_NEW_API_NOTICE_2]
            
            is_msg_corrected = True
            for pattern in expected_msg_pattern:            
                if not re.search(pattern, text_msg_alert):
                    is_msg_corrected = False
            self.assertTrue(is_msg_corrected, "Assertion Error: Message alert is not correct. Message: " + text_msg_alert)
        finally:
            # post-condition
            TestCondition.delete_api_key(self._driver, key_name)
            TestCondition.delete_advanced_users([org_admin])
            

    def test_c11081_set_email_notifications_sender_information(self):
        """
        @author: Duy Nguyen
        @date: 8/29/2016
        @summary: Set Email Notifications Sender Information
        @Precondtions: 
            Create a Device group and add a Device Group Admin
        @steps:
            1) Login to Suitabletech.com and select "Manage Your Beams" from the user dropdown menu.
            2) In the upper right section of the admin page, you should see the Organization Settings (Gear icon). Click on the Gear icon
            3) Enter a "From" name for Email Notifications
            4) Using API to delete the Device Group created in Precondition
        @expected:
            Verify that Email notifications have the "From" field set to the Email address entered in the 
            "settings->Email Notifications Sender Information"
        """
        try:
            # precondition
            admin_user = User()
            admin_user.generate_data()
            device_group_name = Helper.generate_random_device_group_name()
            admin_user.device_group = device_group_name
            
            TestCondition.create_device_group(device_group_name)
            TestCondition.create_advanced_device_group_admins(self._driver, [admin_user])
            
            #create org admin
            org_admin= User()                                            
            org_admin.generate_data()
            TestCondition.create_advanced_organization_admins(self._driver, [org_admin])    
            
            HomePage(self._driver).open()\
                .goto_login_page()\
                    .login_as_unwatched_video_user(admin_user.email_address, admin_user.password)\
                .goto_account_settings_page_by_menu_item()\
                .goto_notifications_tab()\
                    .toggle_device_groups_are_added_or_removed()\
                    .logout()
                
            # steps:
            org_setting_page = HomePage(self._driver).open_and_goto_login_page()\
                .login_as_unwatched_video_user(org_admin.email_address, org_admin.password)\
                .goto_admin_dashboard_page_by_menu_item()\
                .goto_org_setting_page()
                
            origin_notification_sender = org_setting_page.get_email_notifications_name()
            
            test_notification_sender = Helper.generate_random_string()
            org_setting_page.submit_email_notifications_name(test_notification_sender).logout()
            #This is to make sure the script is not too fast between changing setting and deleting device group
            sleep(10)
            DeviceGroupAPI.delete_device_group(device_group_name, Constant.AdvancedOrgName)

            # verify point:
            email_expected = EmailDetailHelper.generate_delete_device_group_email(admin_user, Constant.AdvancedOrgName)
            actual_email = GmailUtility.get_messages(mail_subject=email_expected.subject, receiver=admin_user.email_address)
            self.assertEqual(len(actual_email), 1, "Assertion Error: The number of return email is incorrect")
            self.assertIn(test_notification_sender, actual_email[0].sender, "Assertion Error: Email Notification has incorrect From string ")
        
        finally:
            # post-condition
            TestCondition.delete_advanced_users([admin_user, org_admin])
            TestCondition.set_organization_setting("email notifications", origin_notification_sender)
