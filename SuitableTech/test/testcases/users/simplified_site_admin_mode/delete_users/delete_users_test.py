from common.constant import Constant
from common.helper import Helper
from core.utilities.test_condition import TestCondition
from data.dataobjects.user import User
from pages.suitable_tech.user.home_page import HomePage
from test.testbase import TestBase


class DeleteUser_Test(TestBase):
    
    def test_c11236_can_not_delete_admin_1_x(self):
        """
        @author: khoi.ngo
        @date: 7/28/2016
        @summary: Can not delete admin [1.X] 
        @precondition: copy the url of a known organizational admin
        @steps:
            1.  log on to suitabletech.com with a standard user account
            2. paste the url of a known admin account
            3. make sure that the link doesn't connect when logged on with a standard user
        @expected:
            standard user cannot connect to admin user's URL
        """

        try:
            # pre-condition
            admin_user = User()
            admin_user.generate_data()
            
            normal_user = User()
            normal_user.generate_data()
            # create device group
            temp_device_group_name = Helper.generate_random_device_group_name()
            admin_user.device_group = temp_device_group_name
            normal_user.device_group = temp_device_group_name
            TestCondition.create_device_group(temp_device_group_name)
            
            #create org admin
            TestCondition.create_advanced_organization_admins(self._driver, [admin_user])
            #create normal admin
            TestCondition.create_advanced_normal_users(self._driver, [normal_user])
                        
            # steps
            user_detail_page = HomePage(self._driver).open_and_goto_login_page()\
                .login_as_unwatched_video_user(admin_user.email_address, admin_user.password)\
                .goto_admin_dashboard_page_by_menu_item()\
                .goto_users_tab().goto_user_detail_page(admin_user)
            url_admin = self._driver.current_url
            
            user_detail_page.logout_and_login_again_as_unwatched_video_user(normal_user.email_address, normal_user.password)
            self._driver.get(url_admin)
            
            # verify point
            self.assertTrue(user_detail_page.is_user_page_disappeared(admin_user.get_displayed_name()), "Assertion Error: Normal user  user can connect to admin user's URL")
        finally:
            TestCondition.delete_advanced_users([admin_user, normal_user])
            TestCondition.delete_device_groups([temp_device_group_name])
        

    def test_c11242_grant_temporary_access(self):
        """
        @author: Thanh Le
        @date: 08/18/2016
        @summary: Grant Temporary Access (Dashboard) [2.X]  
        @precondition: There are a Simplified Org Admin account and an activated UserA
        @steps:
            Steps To Complete Task: Grant Temporary Access for a Simplified user
            1) Login to Suitabletech site with the Simplified Org Admin in pre-condition
            2) Go to "Manage Your Beams" page
            3) Enter email address to invite a person to use this Beam (UserA@suitabletech.com) and click "Add User" button
            4) Log out admin and log in with UserA then go to "Your Account" page
            5) Logout this User
            6) Login with Simplified Org Admin again
            7) Remove above invited user (UserA) from "People with access to this Beam" list then logout Simplified Admin
            8) Login with UserA again       
        @expected:
            4) Verify that invited user can see the added Beam (BeamA).
            8) Verify that user cannot see the Beam any more.    
        """
        try:
            # precondition:
            test_device_name = Constant.BeamPlusMock2Name
            
            new_user = User()
            new_user.generate_simplified_normal_user_data()
            new_user.device_group = test_device_name
            
            simplified_org_admin = User()
            simplified_org_admin.generate_simplified_org_admin_data()
            
            TestCondition.create_simplified_organization_admin(driver=self._driver, user=simplified_org_admin)
            
            # steps
            your_beam_account_page = HomePage(self._driver).open()\
                .goto_login_page()\
                    .login(simplified_org_admin.email_address, simplified_org_admin.password)\
                .goto_simplified_dashboard_page()\
                .goto_manage_beam_page(test_device_name)\
                    .create_completed_simplified_normal_user(new_user)\
                .goto_login_page()\
                    .login_as_unwatched_video_user(new_user.email_address, new_user.password)\
                .goto_account_settings_page_by_menu_item()\
                .goto_normal_user_home()
            
            # verify points
            self.assertTrue(your_beam_account_page.is_device_title_displayed(test_device_name),
                            "Assert Error: {} can NOT see the Beam {}".format(new_user.get_displayed_name(), test_device_name))
            
            your_beam_account_page = your_beam_account_page.logout()\
                .goto_login_page()\
                    .login(simplified_org_admin.email_address, simplified_org_admin.password)\
                .goto_simplified_dashboard_page()\
                .goto_manage_beam_page(test_device_name)\
                    .remove_user(new_user)\
                    .logout()\
                .goto_login_page()\
                    .login_as_unwatched_video_user(new_user.email_address, new_user.password)\
                .goto_account_settings_page_by_menu_item()\
                .goto_normal_user_home()
            
            # verify points
            self.assertFalse(your_beam_account_page.is_device_title_displayed(test_device_name),
                            "Assert Error: {} can see the Beam {}".format(new_user.get_displayed_name(), test_device_name))
            
        finally:
            # post-condition            
            pass
        
