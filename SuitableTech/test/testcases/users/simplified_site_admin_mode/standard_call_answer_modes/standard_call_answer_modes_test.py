from common.helper import Helper
from core.utilities.test_condition import TestCondition
from data.dataobjects.user import User
from pages.suitable_tech.user.home_page import HomePage
from test.testbase import TestBase


class StandardCallAnswerModes(TestBase):
    
    def test_c11273_login_as_guest_user_verify_menu_options(self):
        """
        @author: khoi.ngo
        @date: 7/28/2016
        @summary: Login as guest user verify menu options
        @steps:
            1. log on as a user that is not an admin in any org to suitabletech.com
        @expected:
            All non admins should only see the following button options:
            - Download
            - Link a Beam
            - View Documentation
            - Get help
        """
        try:
            #post-condition:
            user = User()
            user.generate_data()
            # create device group
            temp_device_group_name = Helper.generate_random_device_group_name()
            user.device_group = temp_device_group_name
            TestCondition.create_device_group(temp_device_group_name)
            
            TestCondition.create_advanced_normal_users(self._driver, [user])
            
            #steps:
            home_page = HomePage(self._driver).open_and_goto_login_page()\
                .login_as_unwatched_video_user(user.email_address, user.password)\
                .watch_video()
                        
            self.assertTrue(home_page.is_download_beam_software_displayed(), "Assertion Error: Download beam software is not displayed")
            self.assertTrue(home_page.is_set_up_new_beam_displayed(), "Assertion Error: Set up new beam is not displayed")
            self.assertTrue(home_page.is_view_document_displayed(), "Assertion Error: View documentation is not displayed")
            self.assertTrue(home_page.is_get_help_displayed(), "Assertion Error: Get help is not displayed")
        finally:
            #post-condition:
            TestCondition.delete_advanced_users([user])
            TestCondition.delete_device_groups([temp_device_group_name])
    
    
    def test_c11274_non_logged_in_user_verify_menu_options(self):
        """
        @author: khoi.ngo
        @date: 7/29/2016
        @summary: Non-Logged In User verify menu options
        @steps:
            1. do not log onto suitabletech.com
        @expected:
            Non-logged in users should not get to user account Hub page ("Manage your beams") dashboard
        @review: 
        """
        try:
            welcome_page = HomePage(self._driver).open()
    #       check login display
            is_login_button_displayed = welcome_page.is_login_button_displayed()
            self.assertTrue(is_login_button_displayed, "Assertion Error: Login button is not displayed")
        finally:
            pass    

















