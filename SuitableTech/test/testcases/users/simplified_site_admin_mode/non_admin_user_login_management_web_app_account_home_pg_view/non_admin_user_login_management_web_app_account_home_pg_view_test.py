from common.constant import Constant
from data.dataobjects.user import User
from pages.suitable_tech.user.home_page import HomePage
from core.utilities.test_condition import TestCondition
from test.testbase import TestBase
        
        
class NonAdminUserLoginManagementWebAppAccountHomePgView_Test(TestBase):
        
    def test_c11348_non_admin_user_beam_account_home_get_help(self):
        """
        @author: Thanh Le
        @date: 08/15/2016
        @summary: Non-Admin User Beam Account Home - Get Help
        @precondition:           
            1) Login to Suitabletech with the Simplified Org Admin account
            2) On Manage Your Beams page, enter user's email address which would like to invite and select "Add User" button
            3) Go to mailbox of this user and active it and login to Suitabletech site
            4) Complete see video.            
        @steps:
            1) Select on drop-down menu "Account Settings > Home"
            2) Click the "Get Help" button        
        @expected:          
            2) Verify that you get a new tab created with "Beam Help Center" page
        """
        try:
            # pre-condition
            test_device_name = Constant.BeamPlusMock3Name
            
            normal_user = User()
            normal_user.generate_simplified_normal_user_data()
            normal_user.device_group = test_device_name
            
            simplified_org_admin = User()
            simplified_org_admin.generate_simplified_org_admin_data()
            TestCondition.create_simplified_organization_admin(driver=self._driver, user=simplified_org_admin)

            # steps
            user_home = HomePage(self._driver).open()\
                .goto_login_page()\
                    .login(simplified_org_admin.email_address, simplified_org_admin.password)\
                .goto_simplified_dashboard_page()\
                .goto_manage_beam_page(test_device_name)\
                    .create_completed_simplified_normal_user(normal_user)\
                .goto_login_page()\
                    .login_as_unwatched_video_user(normal_user.email_address, normal_user.password)\
                .goto_account_settings_page_by_menu_item()\
                .goto_normal_user_home()
                
            # steps
            is_page_displayed = user_home.open_beam_help_center_page().is_page_displayed()
        
            # verify points
            self.assertTrue(is_page_displayed, "Assert Error: Beam Help Center page is not displayed!")
            
        finally:
            # post-condition            
            TestCondition.delete_simplified_users(self._driver, [normal_user],test_device_name, normal_user.organization)


    def test_c11349_non_admin_user_beam_account_home_add_a_beam(self):
        """
        @author: Thanh Le
        @date: 08/15/2016
        @summary: Non-Admin User Beam Account Home - Add a Beam
        @precondition:           
            1) Login to Suitabletech with the Simplified Org Admin account
            2) On Manage Your Beams page, enter user's email address which would like to invite and select "Add User" button
            3) Go to mailbox of this user and active it and login to Suitabletech site
            4) Complete see video.            
        @steps:
            1) Select on drop-down menu "Account Settings > Home"
            2) Click the "Add a Beam" button        
        @expected:
            2) Verify that you get a new "Link a Beam with your Account" window asking you to "Enter the code on your Beam's screen:"            
        """
        try:
            # pre-condition
            test_device_name = Constant.BeamPlusMock3Name
            
            normal_user = User()
            normal_user.generate_simplified_normal_user_data()
            normal_user.device_group = test_device_name
            
            simplified_org_admin = User()
            simplified_org_admin.generate_simplified_org_admin_data()
            TestCondition.create_simplified_organization_admin(driver=self._driver, user=simplified_org_admin)

            
            user_home = HomePage(self._driver).open()\
                .goto_login_page()\
                    .login(simplified_org_admin.email_address, simplified_org_admin.password)\
                .goto_simplified_dashboard_page()\
                .goto_manage_beam_page(test_device_name)\
                    .create_completed_simplified_normal_user(normal_user)\
                .goto_login_page()\
                    .login_as_unwatched_video_user(normal_user.email_address, normal_user.password)\
                .goto_account_settings_page_by_menu_item()\
                .goto_normal_user_home()
                
            # verify points
            
            is_link_a_beam_with_your_account_dialog_displayed =  user_home.open_add_a_beam_dialog().is_link_a_beam_with_your_account_dialog_displayed()
            
            self.assertTrue(is_link_a_beam_with_your_account_dialog_displayed, "Assert Error: Link a Beam with your Account dialog is not displayed!")
            
        finally:
            # post-condition            
            TestCondition.delete_simplified_users(self._driver, [normal_user], test_device_name)


    def test_c11350_non_admin_user_beam_account_home_view_documentation(self):
        """
        @author: Thanh Le
        @date: 08/15/2016
        @summary: Non-Admin User Beam Account Home - View Documentation
        @precondition:           
            1) Login to Suitabletech with the Simplified Org Admin account
            2) On Manage Your Beams page, enter user's email address which would like to invite and select "Add User" button
            3) Go to mailbox of this user and active it and login to Suitabletech site
            4) Complete see video.            
        @steps:
            1) Select on drop-down menu "Account Settings > Home"
            2) Click the "View Documentation" button        
        @expected:
            2) Verify that you get a new browser tab created with "Suitable Technology Documentation" page"            
        """
        try:
            # pre-condition
            test_device_name = Constant.BeamPlusMock3Name
            
            normal_user = User()
            normal_user.generate_simplified_normal_user_data()
            normal_user.device_group = test_device_name
            
            simplified_org_admin = User()
            simplified_org_admin.generate_simplified_org_admin_data()
            TestCondition.create_simplified_organization_admin(driver=self._driver, user=simplified_org_admin)

            user_home = HomePage(self._driver).open()\
                .goto_login_page()\
                    .login(simplified_org_admin.email_address, simplified_org_admin.password)\
                .goto_simplified_dashboard_page()\
                .goto_manage_beam_page(test_device_name)\
                    .create_completed_simplified_normal_user(normal_user)\
                .goto_login_page()\
                    .login_as_unwatched_video_user(normal_user.email_address, normal_user.password)\
                .goto_account_settings_page_by_menu_item()\
                .goto_normal_user_home()
                
            # steps
            is_suitable_technology_documentation_page =  user_home.open_suitable_technology_documentation_page().is_page_displayed()
                    
            # verify points
            self.assertTrue(is_suitable_technology_documentation_page, "Assert Error: Suitable Technology Documentation page is not displayed!")
            
        finally:
            # post-condition            
            TestCondition.delete_simplified_users(self._driver, [normal_user], test_device_name)
        
        
    def test_c11387_non_admin_user_beam_account_home_available_beams_view(self):
        """
        @author: Duy Nguyen
        @date: 08/18/2016
        @summary: Non-Admin User Beam Account Home - Available Beams View
        @precondition:           
            Use the following Test users and Organizations - Accounts used for Suitable Tech Manual/Automation Testing:
            http://wiki.suitabletech.com/display/SUIT/QA+Test+Accounts#QATestAccounts-TestUserAccounts
            
            FYI: Non Admin login account Home page will be moved to management App Page as follows:
            New redirected URL: https://staging.suitabletech.com/manage/#/home/
            Old redirected URL: https://staging.suitabletech.com/accounts/home/#
            
            Create a device group (DeviceGroupA) and add a device to this group (Beam1)
            Create a normal user (UserA)         
        @steps:
            1) Login to Suitable tech site with normal user credentials in pre-condition (UserA)
            2) Select "Account Settings > Home" from drop-down menu  
        @expected:
            (2) 
            _Verify the url is redirected to "https://staging.suitabletech.com/manage/#/home/"
            _Verify that all Beam(s) accessible to that non-Admin User within the Device Group they were added as a member are listed with the following properties:
            Name : <Beam Name>
            Location : <text string>
            Labels : <text string>
            Time Zone : <Country>/<Region>
            Connected Status: <Available/Configuring/Off-line/etc..>
            Battery Status : <xx%> <Charging>           
        """
        try:
            test_device_name = Constant.BeamPlusMock3Name
            
            normal_user = User()
            normal_user.generate_simplified_normal_user_data()
            normal_user.device_group = test_device_name
            
            TestCondition.create_simplified_normal_users(
                self._driver, 
                user_array=[normal_user], 
                device_name=normal_user.device_group,
                organization=normal_user.organization)
            
            # steps:
            user_home = HomePage(self._driver).open()\
                .goto_login_page()\
                    .login_as_unwatched_video_user(normal_user.email_address, normal_user.password)\
                .goto_account_settings_page_by_menu_item()\
                .goto_normal_user_home()

            # verify point
            self.assertTrue(user_home.is_device_title_displayed(test_device_name), "Assertion Error: Beam {} is not displayed".format(test_device_name))
            self.assertEqual(self._driver.current_url, Constant.SuitableTechHomeURL, "Assertion Error: The navigate url is not correct")
               
        finally:
            TestCondition.delete_simplified_users(self._driver, [normal_user], test_device_name)
        
        
    def test_c11388_non_admin_user_beam_account_home_add_new_beams_to_available_beams_view(self):
        """
        @author: Duy Nguyen
        @date: 08/18/2016
        @summary: Non-Admin User Beam Account Home - Add New Beams to Available Beams View
        @precondition:           
            Use the following Test users and Organizations - Accounts used for Suitable Tech Manual/Automation Testing:
            http://wiki.suitabletech.com/display/SUIT/QA+Test+Accounts#QATestAccounts-TestUserAccounts
            
            FYI: Non Admin login account Home page will be moved to management App Page as follows:
            New redirected URL: https://staging.suitabletech.com/manage/#/home/
            Old redirected URL: https://staging.suitabletech.com/accounts/home/#
            
            As the Organization Admin add a non-Admin user as part of an Organization with valid devices linked within a Device Group (see Precondition test user account info above, i.e suitabletester2@gmail.com)
            1. Navigate to a Device Group with Active Beam devices i.e "dashboard->Beams->Device Group(i.e DeviceGroupTest-A)->Members"
            2. Select "Add Users" button then either "choose from Available Non-Users" or "Invite a new Non-Admin user" (i.e. suitabletester2@gmail.com)
        @steps:
            1) Using another Browser login with the non-Admin user (i.e. suitabletester2@gmail.com) credentials to their Beam Web Account
            2) Verify that the account web address is as follows: https://staging.suitabletech.com/manage/#/home/
            3) Verify that all Beam(s) accessible to that non-Admin User within the Device Group they were added as a member are listed with the following properties:
                Name : <Beam Name>
                Location : <text string>
                Labels : <text string>
                Time Zone : <Contry>/<Region>
                Connected Status: <Available/Configuring/Off-line/etc..>
                Battery Status : <xx%> <Charging>
            4) As the Organization Admin add the same non-Admin user (i.e. suitabletester2@gmail.com) as a member of another Device Group with additional Beam Devices
        @expected:
            Verify that all Beam(s) accessible to that non-Admin User within the Device Groups(i.e DeviceGroupTest-A & DeviceGroupTest-B) they have been added as a member are listed with the following properties: 
            Name : <Beam Name> Location : <text string> 
            Labels : <text string> 
            Time Zone : <Country>/<Region> 
            Connected Status: <Available/Configuring/Off-line/etc..> 
            Battery Status : <xx%> <Charging>              
        """
        try:
            normal_user = User()
            normal_user.generate_simplified_normal_user_data()
            
            simplified_org_admin = User()
            simplified_org_admin.generate_simplified_org_admin_data()
            TestCondition.create_simplified_organization_admin(driver=self._driver, user=simplified_org_admin)
            
            # steps:
            user_home = HomePage(self._driver).open()\
                .goto_login_page()\
                    .login(simplified_org_admin.email_address, simplified_org_admin.password)\
                .goto_simplified_dashboard_page()\
                .goto_manage_beam_page(Constant.BeamPlusMock2Name)\
                    .create_completed_simplified_normal_user(normal_user)\
                .goto_login_page()\
                    .login_as_unwatched_video_user(normal_user.email_address, normal_user.password)\
                .goto_account_settings_page_by_menu_item()\
                .goto_normal_user_home()

            # verify point
            self.assertTrue(user_home.is_device_title_displayed(Constant.BeamPlusMock2Name), "Assertion Error: Beam {} is not displayed".format(Constant.BeamPlusMock2Name))
            self.assertEqual(self._driver.current_url, Constant.SuitableTechHomeURL, "Assertion Error: The navigate url is not correct")
            
            user_home.logout_and_login_again(simplified_org_admin.email_address, simplified_org_admin.password)\
                .goto_simplified_dashboard_page()\
                .goto_manage_beam_page(Constant.BeamPlusMock3Name)\
                    .add_user(normal_user)\
                    .logout()\
                .goto_login_page()\
                    .login_as_unwatched_video_user(normal_user.email_address, normal_user.password)\
                .goto_account_settings_page_by_menu_item()\
                .goto_normal_user_home()

            # verify point
            self.assertTrue(user_home.is_device_title_displayed(Constant.BeamPlusMock2Name), "Assertion Error: Beam {} is not displayed".format(Constant.BeamPlusMock2Name))
            self.assertTrue(user_home.is_device_title_displayed(Constant.BeamPlusMock3Name), "Assertion Error: Beam {} is not displayed".format(Constant.BeamPlusMock3Name))
            
        finally:
            TestCondition.delete_simplified_users(self._driver, [normal_user], Constant.BeamPlusMock3Name)
            TestCondition.delete_simplified_users(self._driver, [normal_user], Constant.BeamPlusMock2Name)


    def test_c11389_non_admin_user_beam_account_home_remove_beams_from_available_beams_view(self):
        """
        @author: Duy Nguyen
        @date: 08/18/2016
        @summary: Non-Admin User Beam Account Home - Remove Beam from Available Beams View
        @precondition:           
            Use the following Test users and Organizations - Accounts used for Suitable Tech Manual/Automation Testing:
            http://wiki.suitabletech.com/display/SUIT/QA+Test+Accounts#QATestAccounts-TestUserAccounts
            
            FYI: Non Admin login account Home page will be moved to management App Page as follows:
            New redirected URL: https://staging.suitabletech.com/manage/#/home/
            Old redirected URL: https://staging.suitabletech.com/accounts/home/#
            
            As the Beam Organization Admin add a non-Admin user as part of an Organization with valid devices linked within a Device Group (see Precondition test user account info above, i.e suitabletester2@gmail.com)
            1. Navigate to a Device Group with Active Beam devices i.e "dashboard->Beams->Device Group(i.e DeviceGroupTest-A)->Members"
            2. Select "Add Users" button then either "choose from Available Non-Users" or "Invite a new Non-Admin user" (i.e. suitabletester2@gmail.com)
        @steps:
            1) Using another Browser login with the non-Admin user (i.e. suitabletester2@gmail.com) credentials to their Beam Web Account
            2) Verify that the account web address is as follows: https://staging.suitabletech.com/manage/#/home/
            3) Verify that all Beam(s) accessible to that non-Admin User within the Device Group they were added as a member are listed with the following properties:
                    Name : <Beam Name>
                    Location : <text string>
                    Labels : <text string>
                    Time Zone : <Contry>/<Region>
                    Connected Status: <Available/Configuring/Off-line/etc..>
                    Battery Status : <xx%> <Charging>
            4) As the Organization Admin add the same non-Admin user (i.e. suitabletester2@gmail.com) as a member of another Device Group with additional Beam Devices
            5) Verify that all Beam(s) accessible to that non-Admin User within the Device Groups(i.e DeviceGroupTest-A & DeviceGroupTest-B) they have been added as a member are listed with the following properties: 
                    Name : <Beam Name> Location : <text string> 
                    Labels : <text string> 
                    Time Zone : <Country>/<Region> 
                    Connected Status: <Available/Configuring/Off-line/etc..> 
                    Battery Status : <xx%> <Charging>       
            6) As the Organization Admin remove the same non-Admin user (i.e. suitabletester2@gmail.com) as a member of one of their Device Groups
        @expected:
            Verify that only the Beam(s) accessible to that non-Admin User within the Device Groups(i.e DeviceGroupTest-A) they have membership to are listed with the following properties:
             Name : <Beam Name> 
             Location : <text string> 
             Labels : <text string> 
             Time Zone : <Country>/<Region> 
             Connected Status: <Available/Configuring/Off-line/etc..> 
             Battery Status : <xx%> <Charging>            
        """
        try:
            normal_user = User()
            normal_user.generate_simplified_normal_user_data()
            
            simplified_org_admin = User()
            simplified_org_admin.generate_simplified_org_admin_data()
            TestCondition.create_simplified_organization_admin(driver=self._driver, user=simplified_org_admin)
            
            # steps:
            user_home = HomePage(self._driver).open()\
                .goto_login_page()\
                    .login(simplified_org_admin.email_address, simplified_org_admin.password)\
                .goto_simplified_dashboard_page()\
                .goto_manage_beam_page(Constant.BeamPlusMock2Name)\
                    .create_completed_simplified_normal_user(normal_user)\
                .goto_login_page()\
                    .login_as_unwatched_video_user(normal_user.email_address, normal_user.password)\
                .goto_account_settings_page_by_menu_item()\
                .goto_normal_user_home()
                
            # verify point
            self.assertTrue(user_home.is_device_title_displayed(Constant.BeamPlusMock2Name), "Assertion Error: Beam {} is not displayed".format(Constant.BeamPlusMock2Name))
            self.assertEqual(self._driver.current_url, Constant.SuitableTechHomeURL, "Assertion Error: The navigate url is not correct")
            
            user_home.logout_and_login_again(simplified_org_admin.email_address, simplified_org_admin.password)\
                .goto_simplified_dashboard_page()\
                .goto_manage_beam_page(Constant.BeamPlusMock3Name)\
                    .add_user(normal_user)\
                    .logout()\
                .goto_login_page()\
                    .login_as_unwatched_video_user(normal_user.email_address, normal_user.password)\
                .goto_account_settings_page_by_menu_item()\
                .goto_normal_user_home()

            # verify point
            self.assertTrue(user_home.is_device_title_displayed(Constant.BeamPlusMock2Name), "Assertion Error: Beam {} is not displayed".format(Constant.BeamPlusMock2Name))
            self.assertTrue(user_home.is_device_title_displayed(Constant.BeamPlusMock3Name), "Assertion Error: Beam {} is not displayed".format(Constant.BeamPlusMock3Name))
            
            user_home.logout_and_login_again(simplified_org_admin.email_address, simplified_org_admin.password)\
                .goto_simplified_dashboard_page()\
                .goto_manage_beam_page(Constant.BeamPlusMock2Name)\
                    .remove_user(normal_user)\
                    .logout()\
                .goto_login_page()\
                    .login_as_unwatched_video_user(normal_user.email_address, normal_user.password)\
                .goto_account_settings_page_by_menu_item()\
                .goto_normal_user_home()

            # verify point
            self.assertTrue(user_home.is_device_title_displayed(Constant.BeamPlusMock3Name), "Assertion Error: Beam {} is not displayed".format(Constant.BeamPlusMock3Name))
            self.assertFalse(user_home.is_device_title_displayed(Constant.BeamPlusMock2Name), "Assertion Error: Beam {} is not displayed".format(Constant.BeamPlusMock2Name))
        finally:
            TestCondition.delete_simplified_users(self._driver,[normal_user], Constant.BeamPlusMock3Name)

