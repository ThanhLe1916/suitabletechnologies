from common.constant import Constant
from common.helper import EmailDetailHelper, Helper
from core.utilities.gmail_utility import GmailUtility
from data.dataobjects.user import User
from pages.suitable_tech.user.home_page import HomePage
import re
from core.utilities.test_condition import TestCondition
from test.testbase import TestBase


class SuitableUser_Test(TestBase):

    def test_c10952_grant_temporary_access_dashboard_2_x(self):
        """
        @author: Thanh.Le
        @date: 8/08/2016
        @summary: Grant Temporary Access (Dashboard) [2.X]
        @precondition: 
            Login as org admin
        @steps:
            1) Navigate to full "Manage your Beams" dashboard
            2) Click on the blue "Invite a Temporary User" box
            3) Fill out the corresponding toast            
        @expected:
            1) Target Users will be notified via email of
            a. Who Invited them (Organization)
            b. What device group(s) they have been added to
            c. Their Access Times to above device groups
            d. UserName
            e. Link to create a password / get client
            2) Once The Invitation has been created, the system will add the user(s) to your organization, and record of the access times to the target device groups affected
        """
        try:
            # pre-condition:
            organization_name = Constant.AdvancedOrgName
            devices = [Constant.BeamPlusName, Constant.BeamProNameUTF8]
            device_group_name = Helper.generate_random_device_group_name(5)
            TestCondition.create_device_group(device_group_name, devices, organization_name)
            
            org_admin = User()
            org_admin.generate_advanced_org_admin_data()
            org_admin.device_group = device_group_name
            TestCondition.create_advanced_organization_admins(self._driver, user_array=[org_admin])
            
            new_temp_user = User()
            new_temp_user.generate_data()
            new_temp_user.device_group = device_group_name
            
            starting_datetime = Helper.generate_date_time(hour_delta=9)
            ending_datetime = Helper.generate_date_time(hour_delta=15, minute_delta=30)
                                    
            # steps
            HomePage(self._driver).open_and_goto_login_page()\
                .login_as_unwatched_video_user(org_admin.email_address, org_admin.password)\
                .goto_admin_dashboard_page_by_menu_item()\
                .invite_temporary_user(new_temp_user, start_date=starting_datetime, end_date=ending_datetime,
                                   link_to_beam_sofware=True, default_invitation=True, device_group=device_group_name)
            
            email_template = EmailDetailHelper.generate_welcome_temporary_user_email(new_temp_user, starting_datetime, ending_datetime, devices, org_admin.get_displayed_name())
            actual_msgs = GmailUtility.get_messages(email_template.subject, receiver=new_temp_user.email_address)        
            
            self.assertEquals(len(actual_msgs), 1, "Assertion Error: The number of received emails is not correct")
            # TODO: Failed by BUG_34
            match_result = re.match(email_template.trimmed_text_content, actual_msgs[0].trimmed_text_content, re.I | re.M)
            self.assertTrue(match_result, "Assertion Error: Email content is not correct. Expected:\n'{}' but found:\n'{}'".format(email_template.trimmed_text_content, actual_msgs[0].trimmed_text_content))
                    
        finally:
            # post-condition:
            TestCondition.delete_advanced_users([new_temp_user, org_admin])
            TestCondition.delete_device_groups([device_group_name], organization_name)
