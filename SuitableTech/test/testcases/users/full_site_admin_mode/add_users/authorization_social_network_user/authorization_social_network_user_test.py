from data.dataobjects.user import User
from common.constant import Constant
from pages.suitable_tech.user.home_page import HomePage
from core.utilities.gmail_utility import GmailUtility
from common.helper import EmailDetailHelper
from common.helper import Helper
import re
from datetime import datetime
from core.utilities.test_condition import TestCondition
from pages.suitable_tech.user.welcome_to_beam_page import WelcomeToBeamPage
from test.testbase import TestBase
from core.webdriver.driver import Driver
from common.application_constants import ApplicationConst


class AuthorizationSocialNetworkUser_Test(TestBase):

    def test_c11664_suitabletech_account_existed_previously_non_first_time_user_authorized(self):
        """
        @author: tham.nguyen
        @date: 08/17/2016
        @summary: SuitableTech Account existed previously Non-First time user authorized
        @precondition:
            To add new user:
            1. Login to Suitabletech.com as an org admin and then select "Manage Your Beams"
            2. On dashboard page,select "Invite a New User" button
            3. Enter email address , firstname, lastname, select device group, and usergroup
            4. Click Invite button
            5. Log out of Suitabletech
        @steps:
            1) Go to new invited user in pre-condition mailbox
            2) Click "Activate account" button on the "Welcome to Beam at <org name>" email
            3) Enter "New password" and "Confirm password" then select "Set password" button
            4) Logout this user
            5) Go to Suitabletech and login again
            
        @expected:
            (5) . Verify that the "Welcome to Beam!" page displays that means user logins successfully.
        """
        try:
            # pre-condition
            user_group_name = Helper.generate_random_user_group_name()
            device_group = Helper.generate_random_device_group_name()
            organization = Constant.AdvancedOrgName
            
            normal_user = User()
            normal_user.generate_data()
            normal_user.user_group = user_group_name
            normal_user.device_group = device_group
            normal_user.organization = organization
            
            org_admin = User()                                            
            org_admin.generate_data()
            org_admin.device_group = device_group
            org_admin.organization = organization
            
            TestCondition.create_device_group(device_group, [], organization)
            TestCondition.create_user_group(user_group_name)
            TestCondition.create_advanced_organization_admins(self._driver, [org_admin])

            # steps
            welcome_to_beam_page = HomePage(self._driver).open_and_goto_login_page()\
                .login_as_unwatched_video_user(org_admin.email_address, org_admin.password)\
                .goto_admin_dashboard_page_by_menu_item()\
                .create_complete_normal_user(normal_user)\
                .logout_and_login_again_as_new_user(normal_user.email_address, normal_user.password)
            
            # verify points
            self.assertTrue(welcome_to_beam_page.is_welcome_user_page_displayed(),
                    "Assertion Failed: New user '{}' couldn't log in successfully after resetting his password".format(normal_user.email_address))
            
        finally:
            # post-condition
            TestCondition.delete_advanced_users([org_admin, normal_user])
            TestCondition.delete_user_groups([user_group_name])
            TestCondition.delete_device_groups([device_group], organization)
            
        
    def test_c11665_suitabletech_account_not_existed_previously_first_time_user_authorized(self):
        """
        @author: tham.nguyen
        @date: 08/17/2016
        @summary: SuitableTech Account not existed previously First time user authorized
        @precondition:
            To add new user:
            1. Login to Suitabletech.com as an org admin and then select "Manage Your Beams"
            2. On dashboard page,select "Invite a New User" button
            3. Enter email address , firstname, lastname, select device group, and usergroup
            4. Click Invite button
            5. Log out of Suitabletech
        @steps:
            1. Go to new invited user in pre-condition mailbox
            2. Click "Activate account" button on the "Welcome to Beam at <org name>" email
            3. Enter "New password" and "Confirm password" then select "Set password" button
            
        @expected:
            (3). Verify that the "Welcome to Beam!" page displays that means user logins successfully.
        """
        try:
            # pre-condition
            user_group_name = Helper.generate_random_user_group_name()
            device_group = Helper.generate_random_device_group_name()
            organization = Constant.AdvancedOrgName
            
            normal_user = User()
            normal_user.generate_data()
            normal_user.user_group = user_group_name
            normal_user.device_group = device_group
            normal_user.organization = organization
            
            org_admin = User()                                            
            org_admin.generate_data()
            org_admin.device_group = device_group
            org_admin.organization = organization
            
            TestCondition.create_device_group(device_group, [], organization)
            TestCondition.create_user_group(user_group_name)
            TestCondition.create_advanced_organization_admins(self._driver, [org_admin])
            
            # steps
            welcome_to_beam_page = HomePage(self._driver).open_and_goto_login_page()\
                .login_as_unwatched_video_user(org_admin.email_address, org_admin.password)\
                .goto_admin_dashboard_page_by_menu_item()\
                .create_complete_normal_user(normal_user)
            
            # verify points                       
            self.assertTrue(welcome_to_beam_page.is_welcome_user_page_displayed(),
                    "Assertion Failed: New user '{}' couldn't log in successfully after resetting his password".format(normal_user.email_address))
            
        finally:
            # post-condition
            TestCondition.delete_advanced_users([org_admin, normal_user])
            TestCondition.delete_user_groups([user_group_name])
            TestCondition.delete_device_groups([device_group], organization)
            
            
    def test_c11667_single_sign_on_account_suitabletech_only_and_account_state_already_logged_into_suitabletech_at_time_of_sign_on(self):
        """
        @author: khoi.ngo
        @date: 8/15/2016
        @summary: Single Sign-On Account (SuitableTech only) and account state (already logged into SuitableTech at time of sign-on)
        @precondition: 
            To add new user (Suitabletech account):
                1. Login to Suitabletech.com as an org admin and then select "Manage Your Beams"
                2. On dashboard page,select "Invite a New User" button
                3. Enter email address (UserA), firstname, lastname, select device group, and usergroup
                4. Click Invite button
                5. Go to this invited user's mailbox
                6. Click "Activate account" button on the "Welcome to Beam at <org name>" email
                7. Enter "New password" and "Confirm password"
                8. User log in to Suitabletech site and do not logout
        @steps:
            1. On the same browser which the account in pre-condition are logging in, open a new tab
            2. Navigate to https://staging.suitabletech.com/accounts/login/
        @expected:
            The Welcome page of current logged user (UserA) displays.
        """
        try:
            # precondition
            normal_user = User()
            normal_user.generate_data()
            # create device group
            temp_device_group_name = Helper.generate_random_device_group_name()
            normal_user.device_group = temp_device_group_name
            TestCondition.create_device_group(temp_device_group_name)
            
            TestCondition.create_advanced_normal_users(self._driver, [normal_user])
            
            # steps
            HomePage(self._driver).open_and_goto_login_page().login_as_unwatched_video_user(normal_user.email_address, normal_user.password)
            
            self._driver.open_new_tab()
            self._driver.get(Constant.SuitableTechLoginURL)
            
            welcome_to_beam_page = WelcomeToBeamPage(self._driver)
            current_user = welcome_to_beam_page.get_current_user_displayed_name()
            
            # verify point
            self.assertTrue(welcome_to_beam_page.is_logged_in(), "Assertion Error: User cannot login.")
            self.assertEqual(current_user, normal_user.get_displayed_name(), "Assertion Error: User cannot login.")
            
        finally:
            TestCondition.delete_advanced_users([normal_user])
            TestCondition.delete_device_groups([temp_device_group_name])
            
            
    def test_c11663_a_suitable_user_who_does_not_want_to_migrate_to_a_gsso_user(self):
        """
        @author: khoi.ngo
        @date: 8/16/2016
        @summary: A suitable user who does not want to migrate to a GSSO user
        @precondition: 
            Create a Google account (UserA@gmail.com)
            Create a Suitabletech user which has the same email with the above Google account as below steps:
    
            Login to Suitabletech.com as an org admin and then select "Manage Your Beams"
            On dashboard page,select "Invite a New User" button
            Enter email address which is the same Google account above (UserA@gmail.com), firstname, lastname, select device group, and usergroup
            Click Invite button
            Go to this invited user's mailbox
            Click "Activate account" button on the "Welcome to Beam at <org name>" email
            Enter "New password" and "Confirm password"
            Log out this user
        @steps:
            1. Navigate to https://staging.suitabletech.com/accounts/login/
            2. Select "Sign in with Google" red button and sign in with the Google account in pre-condition (UserA@gmail.com)
            3. Proceed to login to Suitabletech site
            4. Select "Account Settings > Settings"
            5. Click on "Disconnect from Google" button
            6. Click "reset your password" link
            7. Enter email address and click "Reset my password" button
            8. Open mailbox of this user and set new password
            9. Log out this user
            10. Clear Browser history
            11. Navigate to https://staging.suitabletech.com/accounts/login/ again
            12. Select "Sign in with Google" red button
            13. Sign in with the same Google account above and proceed
            14. Click "No" button to decline
        @expected:
            (6). The "Disconnect Successful" page displays.
            (8). User logs in to Suitabletech site successfully.
            (13). The "Confirm New Authentication Method" page displays.
            (14) Login page displays.
        """
        try:
            # precondition
            non_gsso_user = User()
            non_gsso_user.generate_non_gsso_user_data()
            TestCondition.create_advanced_non_gsso_user(driver=self._driver, user=non_gsso_user)
            
            # steps
            welcome_page = disconect_page = HomePage(self._driver).open()\
                .goto_login_page().login_with_google_as_new_auth(non_gsso_user.email_address, non_gsso_user.password, logged_in_before=True)\
                    .change_auth(True)
            
            # step
            disconect_page = welcome_page.goto_account_settings_page_by_menu_item().disconect_from_google()
            
            # verify point
            self.assertTrue(disconect_page.is_page_displayed(), "Assertion Error: Disconnect Successful page is not display")

            # steps
            welcome_page = disconect_page.reset_password(non_gsso_user)
            
            # verify point
            self.assertTrue(welcome_page.is_logged_in(), "Assertion Error: User log in to Suitabletech site failed.")
            
            self._driver.delete_all_cookies()
            # steps
            login_page = welcome_page.logout().goto_login_page()
            
            # verify point
            confirm_association_page = login_page.login_with_google_as_new_auth(non_gsso_user.email_address, non_gsso_user.password, logged_in_before=True)
            confirm_msg_displayed = confirm_association_page.is_confirm_new_auth_message_displayed()
            self.assertTrue(confirm_msg_displayed, "Assertion Error: The Confirm New Authentication page is not displays.")
            login_page = confirm_association_page.change_auth(False)
            
            self.assertTrue(login_page.is_page_displayed(), "Assertion Error: Login pages is not display.")
            
        finally:
            # post-condition:
            try:
                GmailUtility.delete_all_emails(non_gsso_user.email_address)
            except:
                pass
        
        
    def test_c11666_single_sign_on_account_google_only_and_account_state_already_logged_into_google_at_time_of_sign_on(self):
        """
        @author: khoi.ngo
        @date: 8/15/2016
        @summary: Single Sign-On Account (Google only) and account state (already logged into Google at time of sign-on)
        @precondition: Create an GSSO account (UserA@gmail.com)
        @steps:
            1. Navigate to the Suitable Tech Web login page URL:https://suitabletech.com/accounts/login/
            2. Select the "Sign in with Google" red button
            3. Sign in with the account in pre-condition
            4. Open a new browser tab and navigate to https://staging.suitabletech.com/accounts/login/
            5. Select the Sign in with Google" red button
            6. Proceed to login
        @expected:
            Verify that the account is successfully logged into the Suitabletech. 
        """
        try:
            # precondition
            gsso_user = User()
            gsso_user.generate_non_gsso_user_data()
            TestCondition.create_advanced_gsso_user(self._driver, gsso_user)
        
            # steps
            HomePage(self._driver).open().goto_login_page()\
                .login_with_google_as_new_auth(gsso_user.email_address, gsso_user.password, logged_in_before=True)
                
            
            self._driver.open_new_tab()
            is_user_logged_in = HomePage(self._driver).open().is_logged_in()

            # verify point
            self.assertTrue(is_user_logged_in, "Assertion Error: The account login failed.")
            
        finally:
            pass
        
        
    def test_c11668_single_sign_on_account_suitabletech_accept_conditions_for_moving_to_GSSO(self):
        """
        @author: duy.nguyen
        @date: 8/4/2016
        @summary: Single Sign-On Account (SuitableTech) Accept Conditions for moving to GSSO (yes)
        @precondition: Pre-existing suitable account that is authenticated with their suitable tech user/pass (i.e. not set up for GSSO)
        @note: This test case need a Google Account which have ST Authentication. So we can automate this testcase without create anything.
        @steps:
            1. Navigate to the Suitable Tech Web login page URL:https://suitabletech.com/accounts/login/
            2. Select the Google(or Social Network) Single Sign On button
            3. Sign on by using the red "sign on with Google button" that is the same google account that you created in the Preconditions steps:(i.e. Add new Google Contact Account, suitabletester2@gmail.com)
            4. Verify that you are presented with the following warning message before you can proceed:
                • Confirm New Authentication Method
                • WARNING: Only one authentication method is allowed per account
                • Do you really want to change your authentication method to Google? [Yes/No]
            5. Select "Yes" to conform you want to change your SuitableTech Account authentication method to Google
            6. Verify that we are successfully logged into the Suitable Tech Web Account 
        @expected:
            New user account is able to successfully sign on with their Google Account. 
        """  
        try:
            # precondition
            non_gsso_user = User()
            non_gsso_user.generate_non_gsso_user_data()
            TestCondition.create_advanced_non_gsso_user(driver=self._driver, user=non_gsso_user)
            
            # steps
            confirm_page = HomePage(self._driver).open().goto_login_page()\
                .login_with_google_as_new_auth(non_gsso_user.email_address, non_gsso_user.password, False, True)   
            # verify point
            self.assertEqual(confirm_page.get_warning_message(), ApplicationConst.TXTF_CONFIRM_ASSOCIATION_WARNING_MESSAGE, "Assertion Error: Warning message's content is not correct")
            
            welcome_page = confirm_page.change_auth(True)
            # verify point
            self.assertTrue(welcome_page.is_logged_in(), "Assertion Error: The account login failed.")
            
        finally:
            # post-condition
            try:
                GmailUtility.delete_all_emails(non_gsso_user.email_address)
            except:
                pass

            
    def test_c11669_google_single_sign_on_account_accept_conditions_for_moving_to_suitabletech_account(self):
        """
        @author: duy.nguyen
        @date: 8/4/2016
        @summary: Single Sign-On Account (Google) Accept Conditions for moving to SuitableTech Account (yes)
        @precondition: Login using an existing suitable account that has GSSO authentication
        @note: This test case need a Google Account which have GSSO Authentication. So we can automate it without create anything.
        @steps:
            1. Login to the account: https://suitabletech.com/accounts/login/ with the user in pre-condition
            2. Select "Account Settings" in drop-down menu under username on top-right page
            3. Select the "Settings" tab on the left-hand side
            4. Under the "Security" field, select "Disconnect from Google" button
        @expected:
            Verify that the "Disconnect Successful" page displays.
            Verify that the your account is unable to login using your the Suitabletech.com credentials.
        """  
        try:
            # pre-condition:
            gsso_user = User()
            gsso_user.generate_non_gsso_user_data()
            TestCondition.create_advanced_gsso_user(self._driver, gsso_user)
            
            # step:

            disassociate_page = HomePage(self._driver).open_and_goto_login_page()\
                    .login_as_unwatched_google_user(gsso_user.email_address, gsso_user.password)\
                    .goto_account_settings_page_by_menu_item()\
                    .disconect_from_google()
            
            # verify point
            self.assertTrue(disassociate_page.is_page_displayed(), "Assertion Error: Disassociate page is not displayed")
            self.assertFalse(disassociate_page.is_logged_in(), "Assertion Error: Use is still logged in")
            
            login_page = disassociate_page.goto_login_page()\
                .login_expecting_error(gsso_user.email_address, gsso_user.password)

            self.assertTrue(login_page.is_page_displayed(), "Assertion Error: User still can access to Suitable Page")
        finally:
            pass


    def test_c11670_suitableTech_account_decline_conditions_for_moving_to_GSSO(self):
        """
        @author: duy.nguyen
        @date: 8/5/2016
        @summary: Single Sign-On: (SuitableTech Account) Decline Conditions for moving to GSSO (No)
        @precondition:  Clear browser cookie making sure there is no Google account signed in beforehand
                        Create a Google account (UserA@gmail.com)
        @note: This test case require a Google Account which never access to ST page. So we can automate without create anything.
        @steps:
            1. Login to the account: https://suitabletech.com/accounts/login/
            2. Select "Sign in with Google" red button
            3. Enter email/ password and do proceed
            4. Select "Deny" button to decline
        @expected:
           (3) The "Beam world like to: View your email address/ View your basic info" page displays.
            (4) Login page displays for re-login.
        """  
        try:
            # pre-condition:
            non_gsso_user = User() # DO NOT DELETE this user
            non_gsso_user.generate_un_allowed_gsso_user_data()
            TestCondition.create_advanced_unallowed_gsso_user(self._driver, non_gsso_user)
            self._driver.quit()
            self._driver = Driver(Helper.load_execution_setting())
            self._driver.maximize_window()
            
            # steps:
            allow_associate_page = HomePage(self._driver).open().goto_login_page()\
                .goto_google_allow_access_account_information_page(non_gsso_user.email_address, non_gsso_user.password)
            
            # verify point:
            self.assertTrue(allow_associate_page.is_policy_message_displayed(), "Assertion Error: There is no allow access account information associate page display")
            
            login_page = allow_associate_page.deny_access_account_info()
            
            # verify point
            self.assertTrue(login_page.is_page_displayed(), "Assertion Error: Login page is not display")
                        
        finally:
            pass
        
        
    def test_c11673_sign_on_account_google_only_and_de_authorization_user_by_removing_web_beam_cookies(self):
        """
        @author: khoi.ngo
        @date: 8/17/2016
        @summary: Sign-On Account (Google only) and DE AUTHORIZATION User by Removing web (Beam cookies)
        @precondition:
            Add new Google Contact Account
                1. Create a google account as follows: https://accounts.google.com/SignUpWithoutGmail?hl=en (i.e. suitabletester2@gmail.com )
            
            Add and login to a new Suitable Tech Web Contact Account
                1. On Site Admin Dashboard, select the “Invite a New Contact” button
                2. Complete the invite Contact form:
                    • Using the same Google contact email address (i.e. suitabletester1@gmail.com, *required field)
                    • select any target contact group(s) (optional) (populated by any detected contact groups) (multiple selections allowed)
                    • select any target device group(s) (optional) (populated by any detected device groups)(multiple selections allowed)
                    • Review and personalize Invite email
                3. Click “send invite” button. (result notification will appear confirming action)
                4. Target contact will be notified via email of
                    • Who Invited them (Organization)
                    • What device group(s) they have been added to (derived from direct association and group association)
                    • Login ID [email address]
                    • Link to create a password / get client
                5. Once an invite has been sent, the system will create a new profile for each new contact invited (regardless if they have activated their account)
                6. Login as the new test user, into your mail service and retrieve your account welcome email for your newly created SuitableTech Account
                7. Follow the instructions within the email (i.e. To get started, click the account initialization link)
                8. Logout out of the contact user account
        @steps:
            1. Within your Browser go to settings and clear browser cookies
            2. Google Chrome Browser: • Clear Content settings->Cookies->"All Cookies and Site data" for all time
            3. Navigate to the Suitable Tech Web login page URL:https://staging.suitabletech.com/accounts/login/
            4. Select the Google(or Social Network) Single Sign On button
            5. Select the same google account that you created in the Preconditions steps:(i.e. Add new Google Contact Account, suitabletester2@gmail.com)
        @expected:
            Verify that you will have re-enter your Google(or Social Network) password before you are forwarded to the test account Suitable Tech Web home page
        
        @note: We using the account lgvn1usertest@gmail.com which activated to skip pre-conditon.
        """
        try:
            # pre-condition:
            non_gsso_user = User()
            non_gsso_user.generate_non_gsso_user_data()
            TestCondition.create_advanced_non_gsso_user(driver=self._driver, user=non_gsso_user)            
            # steps:            
            self._driver.quit()
            self._driver = Driver(Helper.load_execution_setting())
            self._driver.maximize_window()  
            

            is_re_enter_password = HomePage(self._driver).open_and_goto_login_page().goto_google_signin_page().is_reenter_password(non_gsso_user.email_address)
            self.assertTrue(is_re_enter_password, "Assertion Error: User doesn't need re-enter password")
        finally:
            try:
                GmailUtility.delete_all_emails(non_gsso_user.email_address)
            except:
                pass
            
            
    def test_c11294_email_notification_authentication_method_change_gsso_to_st_user(self):
        """
        @author: Thanh Le
        @date: 8/16/2016
        @summary: Email Notification: Authentication method change - GSSO to ST User
        @precondition:  There is a Google account
        @steps:
            1) Navigate to the Suitable Tech Web login page URL:https://suitabletech.com/accounts/login/
            2) Sign in with the Google account in pre-condition (UserA@gmail.com)
            3) Proceed to login to Suitabletech
            4) On "Welcome to Beam!" page, select "Account Setting > Settings > Security"
            5) Click "Disconnect from Google" button
        @expected:
            5)
            _Verify that the "Disconnect Successful" page displays.
            _Verify there is a notification email sent to user as
            from: Suitable Technologies Support <support@suitabletech.com>
            to: "<user name>" <user'email>
            subject: Your authentication method has been changed
            mailed-by: <mailer.suitabletech.com>
            signed-by: <mailer.suitabletech.com>
            
            User account: <user's email>
            
            This email acknowledges that you have disconnected your Beam account from Google.
            
            If you wish to sign in with a Suitable Technologies username and password, you must first reset your password: https://staging.suitabletech.com/accounts/password_reset/.
            
            If you believe this is in error, please contact <support@suitabletech.com>.
            @note:
            Simplified Google User Email (lgvnggac@gmail.com) has set auto fw email to logigear1@suitabletech.com for checking email content
        """
        try:
            # pre-condition:
            gsso_user = User()
            gsso_user.generate_non_gsso_user_data()
            GmailUtility.delete_all_emails(receiver=gsso_user.email_address)
            TestCondition.create_advanced_gsso_user(self._driver, gsso_user)
            expected_gmail_message = EmailDetailHelper.generate_google_change_auth_email(gsso_user.email_address)
            
            # steps:
            account_setting_page = HomePage(self._driver).open().goto_login_page()\
                .login_with_google(gsso_user.email_address).goto_account_settings_page_by_menu_item()\
            
            disassociation_completed_page = account_setting_page.disconect_from_google()
            
            # verify point
            self.assertTrue(disassociation_completed_page.is_disconnect_successful_message_displayed(),
                            "Assert Error: Disconnect Successful page is NOT displayed!")
            
            actual_gmail_message = GmailUtility.get_messages(mail_subject=expected_gmail_message.subject, receiver=gsso_user.email_address, sent_day=datetime.now())
            
            self.assertEqual(len(actual_gmail_message), 1, "Assertion Error: The number of notice email is not correct")
            
            result = re.match(expected_gmail_message.trimmed_text_content, actual_gmail_message[0].trimmed_text_content, re.I | re.M)
            self.assertTrue(result, "Assertion Error: Email content does not display as expected. Expected email content is:\n'{}' but found:\n'{}'".format(expected_gmail_message.trimmed_text_content, actual_gmail_message[0].trimmed_text_content))

        finally:
            pass
        
    
