import unittest
from core.webdriver.driver import Driver
from common.helper import Helper
from core.utilities.test_condition import TestCondition
 
class TestBase(unittest.TestCase):
    
    def setUp(self):
        self._driver = Driver(Helper.load_execution_setting())
        self._driver.maximize_window()
        self._driver.tc_name = self._testMethodName
    
    
    def tearDown(self):
        TestCondition.force_log_out(self._driver)
        self._driver.quit()
    
            
if __name__ == "__main__":
    unittest.main()
