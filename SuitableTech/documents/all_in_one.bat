pip install -U selenium
pip install -U pytest
pip install -U pytest-xdist
pip install -U pytest-html
pip install -U google-api-python-client
pip install -U pillow
pip install -U numpy
pip install -U requests
pip install -U pyautogui

pip show selenium
pip show pytest
pip show pytest-xdist
pip show pytest-html
pip show google-api-python-client
pip show pillow
pip show numpy
pip show requests
pip show pyautogui
pause